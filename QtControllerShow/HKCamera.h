#ifndef _CAMERA_H
#define _CAMERA_H

#include <string>
#include <iomanip>
#include <mutex>
#include <map>
#include <opencv2\opencv.hpp>
#include <QLabel>
#include "HCNetSDK.h"
#include "PlayM4.h"


#define USE_BGR

#define LOG(X)   std::cout << #X  << " " << __FILE__ << ":" << std::setw(4) << __LINE__ << "] "

using std::string;

/**
* @berif Camera ʵ����
*/
class HKCamera : public QObject{
	Q_OBJECT
public:
	HKCamera(string ip, string username, string pwd, WORD port = 8000)
		: device_ip_(ip), username_(username), password_(pwd), device_port_(port)
	{
		id_ = counter_++;
	}

	HKCamera(const HKCamera&) = delete;
	HKCamera& operator=(const HKCamera&) = delete;

	bool open();
	void startRealPlay();
	void stopRealPlay();
	void close();
	void get(cv::Mat& frame);

	long id() const { return id_; }

	HWND hwnd_ = nullptr;
	int LowFloor;
	int HighFloor;

signals:
	void update(HKCamera * camera);

private:
	static void CALLBACK DecodeCallback(long nPort, char * pBuf, long nSize, FRAME_INFO * pFrameInfo, void* nReserved1, void* nReserved2);
	static void CALLBACK RealDataCallBack(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser);

	string device_ip_;
	string username_;
	string password_;
	WORD device_port_ = 0;

	long realplay_handle_ = -1;

	long user_id_ = -1;

	long id_ = -1;

	// (handle, port)
	static std::map<long, long> realplay_ports_;

	// (port, handle)
	static std::map<long, long> realplay_handles_;

	// (port, frame)
	static std::map<long, cv::Mat> frames_;

	// (port, mutex)
	static std::map<long, std::mutex> mtxs_;

	// (port, camera)
	static std::map<long, HKCamera *> cameras_;

	static long counter_;
};

#endif // _CAMERA_H