#pragma once

#include <opencv2\opencv.hpp>
extern "C" {
	//#include "libavutil/avutil.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
	//新版本里的图像转换结构需要引入的头文件
#include "libswscale/swscale.h"
#include "libavutil/avutil.h"
#include "libpostproc/postprocess.h" 
};

class H264_Decoder
{
public:
	H264_Decoder();
	~H264_Decoder();
	void init(); //初始化解码器以及数据
	void decode(unsigned char *inputbuf, size_t size); //解码，数据输入和大小
	void play();  //显示解码帧，测试用
	void close(); //
	cv::Mat getMat(); //获取解码后的一帧

private:

	const AVCodec *codec; //存储编解码器信息结构体
	AVCodecContext *c = nullptr;
	int frame_count;
	AVFrame *frame;
	AVPacket avpkt; //存储压缩编码数据相关信息的结构体
	AVFrame *pFrameBGR;

	int BGRsize;
	uint8_t *out_buffer = nullptr;

	struct SwsContext *img_convert_ctx;
	cv::Mat pCvMat;
	bool matReady; //是否有解码好帧
};

