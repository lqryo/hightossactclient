#pragma once

#include <QWidget>
#include "ui_SetParam.h"

#include "CDocument.h"
#include "titleinter.h"
class SetParam : public QWidget
{
	Q_OBJECT

public:
	CDocument *pDoc;
	SetParam(CDocument * pDoc);
	~SetParam();

	public slots:
	void setParamFun();
	void onStitchStateChanged(int);
	void CancelFun();
signals:
	void InitWidget_Sig();

protected:
	TITLE_EVENT

protected slots :
	TITLE_MINMAXCLOSE

private:
	void initStitchState();
	Ui::SetParam ui;
	TITLE_MEMBER
};
