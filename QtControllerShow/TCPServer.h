#ifndef TCPSERVER_H_
#define TCPSERVER_H_

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMap>

#include<string>
#include"CNetTransmission.h"
#include "CDetectionNet.h"
#include "ControlMsg.h"
#include "ControlResp.h"
#include "CDocument.h"
#include "json/json.h"
#include "H264_Decoder.h"

const unsigned short cTCPDefaultPort = 3721;
//const unsigned int cTCPSocketBufferSize = 1024 * 1024 * 1;		// 1M
const unsigned int cTCPSocketBufferSize = 1024 * 8 * 1;		// 8K

/*
CameraNodeStatus(CNS) - Camera service node status
*/
enum CameraNodeStatus {
	CNSAvailable = 1,
	CNSConnected = 1 << 1
};

/*
struct cameranode - Each camera node struct represent a camera node instance

@status		The camera node status(CNS)
@id			The camera node id within it's own host
@host		The connection ip address
@port		The connection port
@tcpSocket	Qt tcp socket between this server and node 
*/
typedef struct cameranode {
	quint8			status;
	quint16			id;
	bool			isMaster;
	QString			host;
	quint16			port;
	QTcpSocket		*tcpSocket;

	QString cameraIP;
	QString cameraUsername;
	QString cameraPass;
	int cameraID;//摄像机编号

	struct cameranode() {
		status = 0;
		id = 0;
		isMaster = false;
		host = "";
		port = 0;
		tcpSocket = nullptr;
		init2();
	}

	struct cameranode(QTcpSocket *socket, quint16 id = 0, quint8 status = 0, bool isMaster = false) {
		this->tcpSocket = socket;
		this->id = id;
		this->status = status;
		this->isMaster = isMaster;

		if (socket != nullptr) {
			host = socket->peerAddress().toString();
			port = socket->peerPort();
		}
		init2();
	}

	void init2()
	{
		cameraID = 0;
		cameraIP = cameraUsername = cameraPass = "";
	}

	bool isEmpty() {
		return host.isEmpty() && port == 0 && id == 0;
	}

	/*
		key is "host.port"
	*/
	QString key() const {
		return host + "." + QString::number(port);
	}

	/*
		key2 is "host.processID"
	*/
	QString key2() const {
		return host + "." + QString::number(id);
	}

	QString toString() const {
		return QString("CameraNode[host=%1, port=%2, id=%3, status=%4, isMaster=%5]")
			.arg(host).arg(port).arg(id).arg(status).arg(isMaster);
	}

} CameraNode ;

/*
TCP Server - in Singleton mode
*/
class CTCPServer : public QObject
{
	Q_OBJECT

public:

	static int connect_num;


	//H264_Decoder decoder;
	CDocument *cdoc;
	static CTCPServer * instance(unsigned short prt = cTCPDefaultPort)
	{
		//		static CTCPServer inst(prt);
		static CTCPServer inst;
		return &inst;
	}

	~CTCPServer();

	void run(CDocument *doc);
	/*
		Stop this server
	*/
	void stop();

	/*
		Send message to client.
	*/
	void sendMsg(QTcpSocket *client, ControlMsg *msg);//这里的参数应该是控制消息，包含消息类型id和具体消息内容

	/*
		Send message to all avaliable client
	*/
	void sendMsg(ControlMsg *msg);

	/*
		Called when received client's message
	*/
	void handleRespDetection(QTcpSocket *client, CNetTransmission *msg);
	void handleRespJson(QTcpSocket *tcpClient, const JsonResp *json);
	void handleRespCMD(QTcpSocket *tcpClient, quint8 cmd, bool success);
	void handleRespCMD(QTcpSocket *tcpClient, quint8 cmd, bool success, Json::Value root);
	void handelRespMat(QTcpSocket *client, CImageFromNet *mat);
	void handleRespDetection(QTcpSocket *client, CDetectionNet *msg);

	void addPendingNew(CameraNode cameraNode);

	QString keyOf(QTcpSocket *client);

	/*
		@return All available clients 
	*/
	QList<CameraNode> getAllClients() const {
		return clientMap.values();
	}

	/*
		@return All available master clients, same as available hosts
	*/
	QList<CameraNode> getAllHosts() const;

	/*
		@return A host contains minimum or equal client number compare to other hosts.
			If there is no host at all, an empty CameraNode is returned, see CameraNode::isEmpty().
	*/
	CameraNode getMinClientCountHost() const;


	/*
	@return A host map from clientMap.  key: host, value: client number
	*/
	QMap<QString, quint8> getHostMap() const;

	unsigned short getPort() {
		return port;
	}

	bool isRunning() {
		return tcpServer != nullptr && isInited;
	}

signals:
	void update_Sig();//直接在界面中显示，不进行拼接信号
	void start_Sig();
	void AnalyStart_Sig();//对传输过来的图片进行解析信号，放到不同的图片队列中
	void ResultShow_Sig();//结果显示信号

	void LogInResult_Sig(quint8, bool);//登录结果显示信号
	void clientShowUpdate_Sig();//登录界面更新显示

private slots:
	/*
		Receive new connections.
	*/
	void handleConnection();
	/*
		Handle given client: parse input message
	*/
	void handleClient(QTcpSocket *);
	/*
		Remove client.
	*/
	void removeClient(QTcpSocket *);

private:
	/*
	ClientBuffer - temporary save client's data
	*/
	struct CClientBuffer
	{
		uint msgLen;			// complete data length(include `msgType` field)
		uint curLen;			// currently received data length
		quint8 msgType;			// message type
		QByteArray data;		// the actuall data 

		CClientBuffer() : msgLen(0), curLen(0), msgType(0) {
		}

		bool isCompleted() {
			return msgLen != 0 
				&& msgLen <= curLen + sizeof(msgType)
				&& msgLen <= data.size() + sizeof(msgType);// double check ensure complete 
		}

		QByteArray nextMsg() {
			return data.left(msgLen - sizeof(msgType));
		}

		void reset() {
			data.clear();
			msgLen = curLen = msgType = 0;
		}

		void cutdown() {
			uint newLen = curLen - msgLen + sizeof(msgType);
			if (newLen == 0) {
				reset();
			}
			else {
				data = data.right(newLen);

				QDataStream in(data);
				in >> msgLen;
				in >> msgType;
				curLen = newLen - sizeof(uint) - sizeof(msgType);
				data = data.right(curLen);
			}
		}
	};

	bool isInited;
//	unsigned short port;						// server port
	unsigned short port;
	//unsigned short port1;
	//unsigned short port2;
	//unsigned short port3;
	//unsigned short port4;
	//unsigned short port5;
	//unsigned short port6;



	QTcpServer *tcpServer;						// owned server instance
	//QTcpServer *tcpServer1;
	//QTcpServer *tcpServer2;
	//QTcpServer *tcpServer3;
	//QTcpServer *tcpServer4;
	//QTcpServer *tcpServer5;
	//QTcpServer *tcpServer6;






	//QMap<QString, QTcpSocket *> clientMap;		// client map, key is an IP:port string from QTcpSocket, value is avaliable client pointer
	QMap<QString, CameraNode> clientMap;		
	QMap<QString, CClientBuffer> clientBufMap;	// client data map
	QMap<QString, quint8> pendingNewMap;		// pending new command map

	void init();

	//CTCPServer(unsigned short prt) : port(prt), tcpServer(nullptr), isInited(false)
	//{
	//	init();
	//}

	CTCPServer() : tcpServer(nullptr), isInited(false)
	{

		port = cTCPDefaultPort;
		//port1 = cTCPDefaultPort + 1;
		//port2 = cTCPDefaultPort + 2;
		//port3 = cTCPDefaultPort + 3;
		//port4 = cTCPDefaultPort + 4;
		//port5 = cTCPDefaultPort + 5;
		//port6 = cTCPDefaultPort + 6;
		init();


	}


};

#endif	// TCPSERVER_H_