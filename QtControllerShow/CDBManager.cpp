#include "CDBManager.h"

CDBManager::CDBManager()
{
}

CDBManager::~CDBManager()
{
}

bool CDBManager::createDBConnection()
{
	qDebug() << "Available drivers:";
	QStringList drivers = QSqlDatabase::drivers();
	foreach(QString dvr, drivers)
	{
		qDebug() << dvr;
	}


	//创建数据库
	QSqlDatabase db = QSqlDatabase::addDatabase(databaseType,"sqlite1");
	db.setHostName(hostName);
	db.setDatabaseName(DatabaseName);
	db.setUserName(userName);
	db.setPassword(password);
	//判断是否创建成功
	if (!db.open()){
		qDebug() << "无法建立数据库连接";
		return false;
	}
	//创建抛物信息表
	return true;


}

/*bool CDBManager::createDBTables()
{
	bool success;
	QSqlQuery query;
	//创建账号管理表 - 待

	//创建相机信息表 - 待

	//创建抛物信息表
	success = query.exec("create table parabolicInfo(timeParabolic integer, cameraID int, apartmentFloor int, apartmentUnit int, apartmentID int, videoPath varchar, primary key(timeParabolic, cameraID))");
	if (!success)
	{
		qDebug() << "create parabolicInfo table failed.";
		qDebug() << query.lastError();
		return false;
	}
	
	return true;
}*/

bool CDBManager::createDBTables()  //创建数据库表
{
	QSqlDatabase db = QSqlDatabase::database("sqlite1"); //建立数据库连接
	QSqlQuery query(db);
	bool success = query.exec("create table parabolicInfo(timeParabolic integer, cameraID int,"
		"apartmentFloor int, apartmentUnit int, apartmentID int, videoPath varchar, primary key('timeParabolic','cameraID'))");

	if (!success){
		qDebug() << "create table failed!";
		qDebug() << query.lastError().text();
		return true;
	}
	
	return true;
}







//插入一条新抛物数据纪录
bool CDBManager::insertParabolicInfo(long _time, int _cameraID, int _apartmentFloor, int _apartmentUnit, int _apartmentID, QString _videoPath)
{
	QSqlQuery query;
	query.prepare("INSERT INTO parabolicInfo (timeParabolic, cameraID, apartmentFloor, apartmentUnit, apartmentID, videoPath) "
		"VALUES (:timeParabolic, :cameraID, :apartmentFloor, :apartmentUnit, :apartmentID, :videoPath)");
	query.bindValue(":timeParabolic", _time);
	query.bindValue(":cameraID", _cameraID);
	query.bindValue(":apartmentFloor", _apartmentFloor);
	query.bindValue(":apartmentUnit", _apartmentUnit);
	query.bindValue(":apartmentID", _apartmentID);
	query.bindValue(":videoPath", _videoPath);
	if (!query.exec())
	{
		QSqlError lastError = query.lastError();
		qDebug() << lastError.driverText() << QString(QObject::tr("INSERT failed."));
		return false;
	}
	return true;
}

//查询选择一条抛物数据记录
QSqlRecord CDBManager::selectParabolicInfo(long _time, int _cameraID)
{
	QSqlQuery query;
	//查询记录
	query.prepare("select * from parabolicInfo where timeParabolic = :timeParabolic and cameraID = :cameraID");
	query.bindValue(":timeParabolic", _time);
	query.bindValue(":cameraID", _cameraID);
	query.exec();
	QSqlRecord rec = query.record();

	qDebug() << QObject::tr("parabolicInfo table count:") << rec.count();
	while (query.next())
	{
		for (int i = 0; i < rec.count(); i++)
			qDebug() << query.value(i);
	}
	return rec;
}

//修改更新已有的抛物信息记录
bool CDBManager::updateParabolicInfo(long _time, int _cameraID, int _apartmentFloor, int _apartmentUnit, int _apartmentID, QString _videoPath)
{
	QSqlQuery query;
	//更新记录
	query.prepare(QString("update parabolicInfo set "
		"apartmentFloor = :apartmentFloor, "
		"apartmentUnit = :apartmentUnit, "
		"apartmentID = :apartmentID, "
		"videoPath = :videoPath "
		"where timeParabolic = :timeParabolic and cameraID = :cameraID"));
	query.bindValue(":timeParabolic", _time);
	query.bindValue(":cameraID", _cameraID);
	query.bindValue(":apartmentFloor", _apartmentFloor);
	query.bindValue(":apartmentUnit", _apartmentUnit);
	query.bindValue(":apartmentID", _apartmentID);
	query.bindValue(":videoPath", _videoPath);
	if (!query.exec())
	{
		QSqlError lastError = query.lastError();
		qDebug() << lastError.driverText() << QString(QObject::tr("update failed."));
	}
	return true;
}

//删除抛物数据表中一条记录
bool CDBManager::deleteParabolicInfo(long _time, int _cameraID)
{
	QSqlQuery query;
	query.prepare(QString("delete from parabolicInfo where timeParabolic = :timeParabolic and cameraID = :cameraID"));
	query.bindValue(":timeParabolic", _time);
	query.bindValue(":cameraID", _cameraID);
	if (!query.exec())
	{
		qDebug() << "delete failed.";
		qDebug() << query.lastError();
		return false;
	}
	qDebug() << "deleted success!";
	return true;
}