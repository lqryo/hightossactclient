#include<string>
#include <QObject>
#include <QUdpSocket>

const unsigned short cDefaultUDPClientPort = 41314;

class CUDPClient : public QObject
{
	Q_OBJECT			// for slots correctly connect

public:
	static CUDPClient * instance()
	{
		static CUDPClient udpClient;
		return &udpClient;
	}
	~CUDPClient();

	void run();

	//void send_message(std::string s);//这里的参数应该包含客户端自身IP，用于回复服务器广播信号
	//std::string recieve_message();//这个函数主要是接收服务器的广播信号（含服务器IP）
	//根据得到的服务器IP和客户端IP，建立TCP连接，传输数据

private slots:
	void readPendingDatagrams();

private:
	unsigned short port;

	QUdpSocket * udpSocket;

	void init();
	void handleDatagram(QByteArray *, QHostAddress *, quint16);

	CUDPClient(QObject *parent = nullptr, unsigned short prt = cDefaultUDPClientPort) : QObject(parent), 
		port(prt), 
		udpSocket(nullptr)
	{
		init();
	}

	CUDPClient(const CUDPClient & other) {}
	CUDPClient & operator= (const CUDPClient & other) {}

};
