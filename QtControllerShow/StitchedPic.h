#pragma once
#include <opencv2\opencv.hpp>
#include <iostream>
#include <string>
#include "opencv2/opencv_modules.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/stitching/detail/autocalib.hpp"
#include "opencv2/stitching/detail/blenders.hpp"
#include "opencv2/stitching/detail/camera.hpp"
#include "opencv2/stitching/detail/exposure_compensate.hpp"
#include "opencv2/stitching/detail/matchers.hpp"
#include "opencv2/stitching/detail/motion_estimators.hpp"
#include "opencv2/stitching/detail/seam_finders.hpp"
#include "opencv2/stitching/detail/util.hpp"
#include "opencv2/stitching/detail/warpers.hpp"
#include "opencv2/stitching/warpers.hpp"

#include "StitchHeader.h"
#include "MaskConstValue.h"

using namespace cv;
using namespace cv::detail;
using namespace std;

class StitchedPic
{
public:
	StitchedPic();
	~StitchedPic();
	int num;

	Mat stitch(vector<Mat> img_mats, const vector<Mat> img_masks, const float resizeScale);
	void recordeStitchData(const vector<Point>& corners_Recorde, Ptr<RotationWarper> &warper_Recorde,
		const vector<Mat> &K_Recorde, const vector<Mat> &R_Recorde);
	Point cornersMin();
	void warpPoint2XY(const int i, const Point p, Point &pXY);
	void warpPointBack(const int i, const Point p, Point &pXY);

	void pointXY2_T(const Point p, Point &p_T);
	void pointXY2_Inver(const Point p, Point &p_T);
	void countOverlappings();
	void countAlphas();
	Mat countAlphasMask();
	Mat warpPic(int i, const Mat &src, Mat &mask_warped);

	void countAlpha(const Mat &mask, const vector<int> value, Mat &alphaMat);

	Mat mapMask(int value);

	//拼接相关参数
private:
	bool preview = false;//预览
	bool try_gpu = false;//使用gpu
	double work_megapix = 0.6;
	double seam_megapix = 0.1;
	double compose_megapix = -1;
	float conf_thresh = 1.0f;
	string features_type = "surf";//特征类型
	string ba_cost_func = "ray";
	string ba_refine_mask = "xxxxx";
	bool do_wave_correct = true;
	WaveCorrectKind wave_correct = detail::WAVE_CORRECT_HORIZ;
	bool save_graph = false;//中间存图片
	std::string save_graph_to;
	string warp_type = "plane";//"cylindrical";//
	int expos_comp_type = ExposureCompensator::GAIN_BLOCKS;
	float match_conf = 0.65f;
	string seam_find_type = "no"; //gc_color可行，但gpu版本不可用//"dp_color";出现bug//"voronoi";对墙不可行
	int blend_type = Blender::MULTI_BAND;//Blender::NO;// multi版本融合效果比较好,耗时慢//NO;//直接覆盖图，效果差，耗时快
	float blend_strength = 1;
	string result_name = "result.jpg";

	vector<Mat> _img_result_masks;
public:
	Mat _stitchedPic;
	vector<Size> _sizes_Recorde;
	vector<Point> _corners_Recorde;
	Ptr<RotationWarper> _warper_Recorde;
	vector<Mat> _K_Recorde;
	vector<Mat> _R_Recorde;

	//相对拼接图左上角为0，0  内部计算
	vector<Point> pointXY;
	//重叠区域宽度
	vector<int> overlappings_;
	vector<float> alphas_;
	Mat alphasMask_;

	Mat _dst;
	Mat _dst_mask;

	Rect _rect;

};

