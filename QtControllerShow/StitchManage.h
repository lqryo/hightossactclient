#pragma once
#include "acc.h"
#include "StitchedPic.h"
#include "RecordMap.h"
#include "MaskConstValue.h"

//extern int FLAG ;
class StitchManage
{
public:
	StitchManage();
	~StitchManage();
	acc init(cv::Mat gray_01, cv::Mat gray_02, int Rotate_flag);
	Mat excFast(cv::Mat gray_01, cv::Mat gray_02);
	Mat stitch(cv::Mat gray_01, cv::Mat gray_02, int Rotate_flag);

	Mat initMN(const vector<cv::Mat> imgs, int m, int n, vector<Point3i>& excPara, vector<int>& reverseFlag, const float resizeScale);
	Mat excMN(const vector<cv::Mat> imgs, int m, int n);
	Mat stitchMN(vector<vector<cv::Mat> > in_imgs, const bool REVERSE = true, const float resizeScale = 1);//(const vector<cv::Mat> imgs, const int m, const int n, , const float resizeScale = 1);

private:
	//对加速acc中旋转部分数据初始化
	void initReverseData(vector<int> reverseFlag, const vector<Size> flagSize);

	Mat paramStitched(const vector<cv::Mat> pics
		, cv::Mat& variantMask, cv::Mat& alpha, cv::Mat& mapRecord, cv::Mat& out_image, vector<Size>& ij
		, vector<cv::Mat> masks, Mat &stitched_mask, const float resizeScale);
	Mat initColsStitch(vector<Mat> pics, vector<Mat> masks, Mat &stitched_mask, const float resizeScale);
private:
	acc acc_;
	int FLAG;
	struct StitchedFlag
	{
		cv::Mat stitched;//拼接好的图
		cv::Mat stitched_mask;//结果图边角区域没有值
		int flag;//该图对应的flag, 为负数时，表示的是源图对应的在imgs中的序列号
	};

	vector<Mat> _imgs_result_mask;
};

