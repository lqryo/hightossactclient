#pragma once
#include <memory>
#include <iostream>

#include "CDocument.h"

class CStitchThread;
class AnalyThread;

class CTCPServer;
class SetParam;
class PlaybackWidget;
//==============================================================================
//	@name		:	SysController
//	@biref		:	系统控制类，管理模块与线程的调度
//	@author		:	Macy
//===============================================================================
class SysController : public QObject
{
	Q_OBJECT

public:
	SysController();
	static SysController& getInstance();
	~SysController();

	//网络传输模块
	//CRawUDPServer * udpServer;
	//CUDPServer * udpServer;
	CTCPServer * tcpServer;
	QThread udpServerThread;
	QThread tcpServerThread;
	CDocument * pDoc;
	SetParam * pSetParam;
	//PlaybackWidget * pPlaybackWidget; 
	//
	CStitchThread * pCStitchThread;
	AnalyThread * pAnalyThread;
	QThread StitchSubThread;
	QThread AnalySubThread;

	
	public slots:

private:
	void Init();//初始化函数
	void connections();//链接函数
	

};
