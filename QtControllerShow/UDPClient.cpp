#include "UDPClient.h"
#include "TCPClient.h"

#include "Utils.h"

void CUDPClient::run()
{
	readPendingDatagrams();
}

void CUDPClient::readPendingDatagrams()
{

	while (udpSocket->hasPendingDatagrams()) {
		QByteArray datagram;
		datagram.resize(udpSocket->pendingDatagramSize());
		QHostAddress senderAddr;
		quint16 senderPort;

		udpSocket->readDatagram(datagram.data(), datagram.size(), &senderAddr, &senderPort);
		handleDatagram(&datagram, &senderAddr, senderPort);
	}

}

void CUDPClient::handleDatagram(QByteArray *datagram, QHostAddress *addr, quint16 port)
{
	std::string svrIP			= addr->toString().toStdString();
	unsigned short svrPort		= datagram->toUShort();
	debug( "<UDPClient> Message from '" + svrIP + ":" + std::to_string(port) + "' : "
		+ std::to_string(svrPort));
	// TODO: should update clients

	CTCPClient * tcpClient = CTCPClient::instance();
	if ( !tcpClient->isConnected() ) {
		tcpClient->connectServer(svrIP, svrPort);
	}
	else {
		debug("Connected.  Omit broadcast message.");
	}

}

CUDPClient::~CUDPClient()
{

	if (udpSocket != nullptr) {
		delete udpSocket;
		udpSocket = nullptr;
	}

}

void CUDPClient::init()
{
	udpSocket = new QUdpSocket();
	udpSocket->bind(QHostAddress::LocalHost, port);

	connect(udpSocket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
}
