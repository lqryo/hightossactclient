# include<CDocument.h>

CDocument::CDocument()
{

	/*cv::Mat testmat = cv::imread("E:\\HighTossAct\\HighTossActClient\\QtControllerShow\\test.jpg");
	cv::imshow("src",testmat);*/
	pMatQImage = new MatQImage();
	/*capture11.open("../testvedio/test11.mp4");
	capture21.open("../testvedio/test21.mp4");*/
	/*capture11.open("..\\..\\HighTossActTestVedio\\20171212\\D20F19T1059.mp4");
	capture21.open("..\\..\\HighTossActTestVedio\\20171212\\D20F15T1105.mp4");*/
	capture11.open("..\\..\\HighTossActTestVedio\\20180322\\65_1030.mp4");
	capture21.open("..\\..\\HighTossActTestVedio\\20180322\\66_1030.mp4"); 
	if (!capture11.isOpened())
	{
		std::cout << "No camera or video input!\n" << std::endl;
		return;
	}
	long frameToStart = 25;
	std::cout << "capture11 total frames: " << capture11.get(CV_CAP_PROP_FRAME_COUNT) << std::endl;
	std::cout << "capture11 start frames:" << frameToStart << std::endl;
	std::cout << "capture11 rate:" << capture11.get(CV_CAP_PROP_FPS) << std::endl;
	capture11.set(CV_CAP_PROP_POS_FRAMES, frameToStart);
	
	
	if (!capture21.isOpened())
	{
		std::cout << "No camera or video input!\n" << std::endl;
		return;
	}
	std::cout << "capture21 total frames: " << capture21.get(CV_CAP_PROP_FRAME_COUNT) << std::endl;
	std::cout << "capture21 start frames:" << frameToStart << std::endl;
	std::cout << "capture21 rate:" << capture21.get(CV_CAP_PROP_FPS) << std::endl;
	capture21.set(CV_CAP_PROP_POS_FRAMES, frameToStart);

	//摄像头帧率设置
	//视频帧率  17对应25  10-10 11-12  12-16  13-20  14-15  15-18 
	this->par_all.camera_info.dwVideoFrameRate = 14;
	this->par_all.camera_info.dwVideoBitrate = 19;



} 

int CDocument::CameraNum = 0;//初始化已登录相机的个数

CDocument::~CDocument()
{

}

/*************************************************
date：          // 2018-03-26
Author：        // Macy
Function:       // initCameraModule
Description:    // 初始化相机对象队列（vector<vector<> >）系统设置成功后调用，系统运行会有一个默认的初始值
Input:          // null
Return:         // void
Others:         // null
*************************************************/
void CDocument::initCameraModule(){
	CameraModuleMN.resize(par_all.CameraR);//初始化行大小

	for (int i = 0; i < par_all.CameraR; ++i){
		CameraModuleMN[i].resize(par_all.CameraC);//初始化列大小
	}
	//初始化相机对象
	for (int i = 0; i < par_all.CameraR; ++i){
		for (int j = 0; j < par_all.CameraC; ++j){
			CameraModuleMN[i][j]=NULL;
		}
	}

}

/*************************************************
date：          // 2018-03-20
Author：        // Macy
Function:       // initCameraQuene
Description:    // 初始化解码对象队列（vector<vector<> >）系统设置成功后调用，系统运行会有一个默认的初始值
Input:          // null
Return:         // void
Others:         // null
*************************************************/
void CDocument::initDecodeSize(){
	pH264_DecoderMN.resize(par_all.CameraR);

	for (int i = 0; i < par_all.CameraR; ++i){
		pH264_DecoderMN[i].resize(par_all.CameraC);
	}
}

/*************************************************
date：          // 2018-01-12
Author：        // lee
Function:       // initCameraQuene
Description:    // 初始化存储网络传输数据的相机队列和用于图像拼接的队列（vector<vector<> >）
                   Attention！！！:除了在CDocument 构造函数中或者程序初始化的时候调用之外，永远不要主动调用该函数
				   请调用resetCameraQueneParams 函数。（因为仅仅调用该函数不能保证CameraQueneMN和CameraImageMN的线程安全）
Input:          // null
Return:         // void
Others:         // null
*************************************************/
void CDocument::initCameraQuene(){
	CameraQueneMN.resize(par_all.CameraR);
	CameraImageMN.resize(par_all.CameraR);
	//std::vector < CBlockingQueue<CNetTransmission>* > tmp;
	//CBlockingQueue<CNetTransmission>  tnetQuene;
	//tmp.push_back(&tnetQuene);
	

	for (int i = 0; i < par_all.CameraR; ++i){
		CameraQueneMN[i].resize(par_all.CameraC);	
		CameraImageMN[i].resize(par_all.CameraC);
	}

	for (int i = 0; i < par_all.CameraR; ++i){
		for (int j = 0; j < par_all.CameraC; ++j){
			CBlockingQueue<cv::Mat> * netQueue = new CBlockingQueue<cv::Mat>;
			cv::Mat imgMat;
			CameraQueneMN[i].push_back(netQueue);
			CameraImageMN[i][j] = imgMat;
		}
	}

}


/*************************************************
date：          // 2018-01-12
Author：        // lee
Function:       // resetCameraQueneParams
Description:    // 重新设置相机队列参数 m、n，并重新初始化网络存储队列和拼接队列
Input:          // int m --- 相机m行, int n ---相机 n列 详见 @ CDocument par_all.CameraR，par_all.CameraC
Return:         // void
Others:         // null
*************************************************/
void CDocument::resetCameraQueneParams(int m, int n){
	QMutexLocker locker(&vec_mutex_bqueue);
	QMutexLocker locker_mat(&vec_mutex_mat);
	par_all.CameraR = m;
	par_all.CameraC = n;

	auto it = CameraQueneMN.begin();
	while (it != CameraQueneMN.end()){
		auto iter = it->begin();
		while (iter != it->end()){
			//删除vector中的指针。Note：vector中存放指针对象需要手动释放。仅仅释放vector内存并不能删除指针
			delete *iter;
			*iter = nullptr;
			++iter;
		}

		//vector<T>().swap(vec) 强制清空vector的内存，使其size和capacity都为0
		std::vector<CBlockingQueue<cv::Mat>* >().swap( *it );
		++it;
	}
	CameraBlockQueue().swap(CameraQueneMN);

	initCameraQuene();

}

/*************************************************
date：          // 2018-01-12
Author：        // lee
Function:       // isNetQueneNotEmpty
Description:    // 判断所有缓存相机队列是否都不为空
Input:          //
Return:         // bool
Others:         // null
*************************************************/
bool CDocument::isNetQueneNotEmpty(){
	bool notEmpty = true;
	for (int i = 0; i < par_all.CameraR; ++i){
		for (int j = 0; j < par_all.CameraC; ++j){
			if (this->getBlockQueueVector()[i][j]->m_pCount->getValue() > 0){
				continue;
			}
			else{
				notEmpty = false;
				return notEmpty;
			}
		}
	}
	return notEmpty;
}

/*************************************************
date：          // 2018-01-12
Author：        // lee
Function:       // isAllStitchNotEmpty
Description:    // 判断所有等待拼接的队列是否都不为空
Input:          //
Return:         // bool
Others:         // null
*************************************************/
bool CDocument::isAllStitchNotEmpty(){
	bool notEmpty = true;
	for (int i = 0; i < par_all.CameraR; ++i){
		for (int j = 0; j < par_all.CameraC; ++j){
			if (!this->getCameraImage()[i][j].empty()){
				continue;
			}
			else{
				notEmpty = false;
				return notEmpty;
			}
		}
	}
	return notEmpty;
}

/*************************************************
date：          // 2018-01-18
Author：        // lee
Function:       // getBlockQueueVector
Description:    // 获取相机队列二维vector，用加锁的方式获取
Input:          //
Return:         // 相机队列
Others:         // null
*************************************************/
CameraBlockQueue CDocument::getBlockQueueVector(){
	QMutexLocker locker(&vec_mutex_bqueue);
	return this->CameraQueneMN;
}

std::vector< std::vector<cv::Mat> > CDocument::getCameraImage(){
	QMutexLocker locker(&vec_mutex_mat);
	return this->CameraImageMN;
}

void CDocument::setBlockQueueVector(int i, int j, cv::Mat net){
	QMutexLocker locker(&vec_mutex_bqueue);
	this->CameraQueneMN[i][j]->Put(net);
}


void CDocument::setCameraImage(int i, int j, cv::Mat mat){
	QMutexLocker locker(&vec_mutex_mat);
	this->CameraImageMN[i][j] = mat;
}