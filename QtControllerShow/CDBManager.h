#pragma once

#include <QThread>
#include <QtSql>
#include <QtSql/QSqlDatabase>
#include <QSqlDriver>
#include <qstring.h>

const QString databaseType = "QSQLITE";
const QString DatabaseName = "HighTossActDB.db";
const QString hostName = "HighTossAct";
const QString userName = "admin";
const QString password = "admin";

class CDBManager : public QThread
{
	Q_OBJECT

public:
	//实现单例模式
	static CDBManager * instance()
	{
		static CDBManager inst;
		return &inst;
	}
	/*
		create database
	*/
	bool createDBConnection();
	/*
		creat database tables
	*/
	bool createDBTables();

	/*
		插入一条新抛物数据纪录
	*/
	bool insertParabolicInfo(long _time, int _cameraID, int _apartmentFloor, int _apartmentUnit, int _apartmentID, QString _videoPath);
	/*
		查询选择一条抛物数据记录
	*/
	QSqlRecord selectParabolicInfo(long _time, int _cameraID);
	/*
		修改更新已有的抛物信息记录
	*/
	bool updateParabolicInfo(long _time, int _cameraID, int _apartmentFloor, int _apartmentUnit, int _apartmentID, QString _videoPath);
	/*
		删除抛物数据表中一条记录
	*/
	bool deleteParabolicInfo(long _time, int _cameraID);

	

private:
	
public:
	CDBManager();
	~CDBManager();
};
