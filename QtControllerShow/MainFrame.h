#ifndef QTCONTROLLERSHOW_H
#define QTCONTROLLERSHOW_H

#include <QtWidgets/QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QDebug>
#include <QSound>
#include <QMessageBox>
#include <QComboBox>
#include <QScrollarea>//滚动
#include <QHBoxLayout>//水平布局头文件
#include <QVBoxLayout>//垂直布局头文件
#include <QGridLayout>//表格布局头文件
#include <QDesktopWidget>
#include <QTextTable>

#include "ButtonWidget.h"
#include "ui_qtcontrollershow.h"

#include "CDocument.h"
//#include "TCPServer.h"
//#include "UDPServer.h"

class CCamera;
class CHKCamera;
class CStitchThread;
class AnalyThread;
//class CRawUDPServer;
class CTCPServer;
class SetParam;
class PlaybackWidget;

class MainFrame : public QMainWindow
{
	Q_OBJECT

public:
	MainFrame(QWidget *parent = 0);
	~MainFrame();

public:
	QTimer* theTimer;
	double t1, t2;
	//操作Widget中的插件定义
	QPushButton* StartBtn;
	QPushButton* StopBtn;
	QPushButton* ExitBtn;
	QPushButton* RestartBtn;
	QPushButton* OpenCameraBtn;
	QPushButton* StartAcBtn;
	QLabel *showlabel;
	QTextEdit *table;
	QSound *sound;

	QGridLayout *pQGridLayout;
	QVector<QVector<QLabel*>> pShow;
	
	
	//网络传输模块
	//CRawUDPServer * udpServer;
	//CUDPServer * udpServer;
	CTCPServer * tcpServer;
	QThread udpServerThread;
	QThread tcpServerThread;
	//主机的参数
	int g_nActScreenW;//主机的宽
	int g_nActScreenH;//主机的高
	int g_nScreenCount;//系统的屏幕个事故

	CDocument * pDoc;
	SetParam * pSetParam;
	//PlaybackWidget * pPlaybackWidget; 
	//
	CStitchThread * pCStitchThread;
	AnalyThread * pAnalyThread;
	QThread StitchSubThread;
	QThread AnalySubThread;
	
public slots:
	void SendmsgStart();
	void SendmsgStop();
	void SendmsgExit();
	void SendmsgRestart();

	void ReadFrame();
	void updateFrame();
	void NetupdateFrame();

	//测试拼接显示槽函数
	void testShow();
	void actionShowClientList();

	
signals:
	void updateFrame_Sig();

	void showFrame_Sig();

	//打开相机信号
	void OpenCaemra_Sig();

	//打开采集图像信号
	void StartAcquiring_Sig(CBlockingQueue<cv::Mat> &);

	void StitchedPic_Sig(CDocument*, int); 

	void test_sig();

	public slots:
	void StartFun();
	void PerShowFun();
	void VedioShowFun();
	void StitchShowFun();
	void LogInCameraFun();
	//控制面板二级界面
	void SubWidgetSetP();//设置系统参数二级界面
	void InitShowWidget();//初始化单摄像头显示界面函数

public:
	ButtonWidget * BtnStart;
	ButtonWidget * BtnVedioShow;
	ButtonWidget * BtnVedioStitchShow;
	ButtonWidget * BtnPlayback;
	ButtonWidget * BtnLogIn;

	ButtonWidget * BtnUserManager;
	ButtonWidget * BtnParmSet;
	ButtonWidget * button3;
	ButtonWidget * button4;
	
	/*SetParam * pSetParam;*/
	Ui::QtControllerShowClass ui;

private:
	void getScreenInfo();//获得主机屏幕分辨率
	void connections();//链接函数
	void Init();//初始化函数
	
	void InitOperateWidget();//初始化控制界面函数
	
	
};

#endif // QTCONTROLLERSHOW_H
