//==============================================================================
//	@name		:	Params
//	@biref		:	类的各种参数初始化
//	@author		:	
//===============================================================================
#pragma once
#include <Windows.h>
#include <minwindef.h>
#include <opencv2\opencv.hpp>
#include "qstring.h"
#include "QMessageBox"
#include "QDateTime"

# pragma execution_character_set("utf-8")

//定义摄像机参数
//200W: 分辨率：27（1920 * 1080） 码率：25（4096K）
//100W: 分辨率：19（1280 * 720） 码率：23（2048K）
//帧率：14（15帧）17（25帧）
typedef struct CAMERA_INFO{
	BYTE     byStreamType = 0;  //码流类型
	BYTE     byResolution = 27;  //分辨率19-1280*720; 27-1920*1080; 30-2048*1536; 0xff-Auto(使用当前码流分辨率)
	BYTE     byBitrateType;
	BYTE     byPicQuality;
	DWORD    dwVideoBitrate = 25;  //视频码率
	DWORD    dwVideoFrameRate = 17; //帧率
	WORD     wIntervalFrameI;
	BYTE     byIntervalBPFrame;
	BYTE     byres1;
	BYTE     byVideoEncType;
	BYTE     byAudioEncType;
	BYTE     byVideoEncComplexity;
	BYTE     byEnableSvc;
	BYTE     byFormatType;
	BYTE     byAudioBitRate;
	BYTE     bySteamSmooth;
	BYTE     byAudioSamplingRate;
	BYTE     bySmartCodec;
	BYTE     byres;
	WORD     wAverageVideoBitrate;
	BYTE	 begintime = 6;
	BYTE	 begintimemin = 0;
	BYTE	 begintimesec = 0;
	BYTE	 endtime = 18;
	BYTE	 endtimemin = 0;
	BYTE	 endtimesec = 0;
}CAMERA_INFO, *LPCAMERA_INFO;


//参数定义与配置
class CParams
{
public:
	int camera_ID=11;
	
	double fpsRate;
	CAMERA_INFO camera_info;//相机读取参数

	//相机登录参数
	QString cameraIP;//相机登录IP
	QString cameraUsername;//相机登录账号名
	QString cameraPass;//相机登录密码

	//
	int Rota_Type = 1;//图片是否需要旋转
	bool stitchtest = false;//true：实时拼接，false：测试拼接（本地视频）
	bool stitchMsk = false;//是否进行分析与拼接,默认是不拼接，可在设置中设置

	int frame_width = 1920;//图像宽
	int frame_height = 1080;//图像高

	int CameraRMax = 10;//竖直方向相机最大的个数
	int CameraCMax = 4;//水平方向上相机最大的个数

	int CameraR = 7;//竖直方向相机个数
	int CameraC = 2;//水平方向相机个数
	int FloorNum = 33;//总的楼层数
	int startFloor = 4;//开始监控的楼层
	int pFloors = 6;//每个相机拍摄楼层数
	float PerFloor = 3;//每层楼的高度
	double distance = 15;//相机安装与大楼的距离
	double CaHeight = 3;//相机安装的高度

	bool SetParamMsk = false;//标志是否进行了设置

	int lag_frames = 50;//延迟判断的帧数

	std::string save_path = "../VideoSave/";//视频保存的路径 （调试版）
	//std::string save_path = "../VedioSave/";//视频保存的路径 (发布版)
};