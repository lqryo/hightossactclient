#include "AnalyThread.h"

AnalyThread::AnalyThread(CDocument *pDoc)
{
	this->mDoc = pDoc;
	connect(this, SIGNAL(NextAna_Sig()), this, SLOT(AnalysisFunction()), Qt::QueuedConnection);
}

AnalyThread::~AnalyThread()
{
}

void AnalyThread::AnalysisFunction(){
	qDebug() << "Analysis Start！！！   " << mDoc->CameraQueue.m_pCount->getValue();
	//CNetTransmission mNetT = mDoc->CameraQueue.Take();
	CNetTransmission mNetT = mDoc->VedioQueue.Take();
	//decoder.decode(mNetT.buf, mNetT.sizebuf);
	
	if (!mNetT.frame.empty()){

		int row = mNetT.camera_ID / 10;
		int col = mNetT.camera_ID % 10;

		//mDoc->getBlockQueueVector()[row][col]->Put(mNetT);
		mDoc->setBlockQueueVector(row, col, mNetT.frame);
		qDebug() << "Queue[" << row << "][" << col << "] size = " << mDoc->getBlockQueueVector()[row][col]->m_pCount->getValue();
		if (mDoc->getBlockQueueVector()[row][col]->m_pCount->getValue() > 50)
			mDoc->getBlockQueueVector()[row][col]->Take();

		//保证所有相机队列都不为空

		else if (mDoc->isNetQueneNotEmpty()){
			start_Sig(mDoc, mDoc->par_all.Rota_Type);
		}


		//if (mNetT.camera_ID == 00)
		//{
		//	//cv::imshow("00", mNetT.frame); cvWaitKey(1);
		//	mDoc->CameraQueue11.Put(mNetT);
		//	/*if (mDoc->par_all.mask11 == 0)
		//		mDoc->par_all.mask11 = 1;*/
		//	if (mDoc->CameraQueue11.m_pCount->getValue() > 50)
		//		mDoc->CameraQueue11.Take();
		//}
		//if (mNetT.camera_ID == 01)
		//{
		//	mDoc->CameraQueue12.Put(mNetT);
		//	cv::imshow("01", mDoc->CameraQueue12.Take().frame);//cv::imshow("12", mNetT.frame);
		//	cvWaitKey(1);
		//}
		//if (mNetT.camera_ID == 10)
		//{
		//	//cv::imshow("10", mNetT.frame); cv::waitKey(1);
		//	mDoc->CameraQueue21.Put(mNetT);
		//	/*if (mDoc->par_all.mask21 == 0)
		//		mDoc->par_all.mask21 = 1;*/
		//	if (mDoc->CameraQueue21.m_pCount->getValue() > 50)
		//		mDoc->CameraQueue21.Take();
		//}
		//if (mNetT.camera_ID == 11)
		//{
		//	mDoc->CameraQueue22.Put(mNetT);
		//	cv::imshow("11", mDoc->CameraQueue22.Take().frame);//cv::imshow("22", mNetT.frame);
		//	cvWaitKey(1);
		//}
		//qDebug() << "Queue11 size = " << mDoc->CameraQueue11.m_pCount->getValue()
		//	<< ",  Queue21 size = " << mDoc->CameraQueue21.m_pCount->getValue();
		//if (mDoc->CameraQueue11.m_pCount->getValue() > 0 && mDoc->CameraQueue21.m_pCount->getValue() > 0 /*&& mDoc->par_all.sendsig == 1*/)
		//{
		//	start_Sig(mDoc, mDoc->par_all.Rota_Type);
		//	//mDoc->par_all.sendsig = 0;
		//	//mDoc->par_all.initstitch = 0;
		//	//start_Sig();
		//}
		//qDebug() << "Analysis End！！！" << endl;
	}
	else{
		qDebug() << "empty" << endl;
	}
}

/*************************************************
date：          // 2018-01-12
Author：        // lee
Function:       // enqueneByCameraID
Description:    // 将网络传输的数据按camera id分配到相对应的队列中 发出拼接信号
Calls:          // 被本函数调用的函数清单
Input:          // CNetTransmission net 网络传输的数据
Return:         // void
Others:         // null
*************************************************/
void AnalyThread::enqueneByCameraID(CNetTransmission net){
	/*这里按照 camera 排列不超过m × 10计算，即相机的排列 竖列不会超过10列 */
	int row = net.camera_ID / 10;
	int col = net.camera_ID % 10;
	//decoder.decode(net.buf, net.sizebuf);
	mDoc->getBlockQueueVector()[row][col]->Put(net.frame);
	//mDoc->setBlockQueueVector(row, col, decoder.getMat());
	qDebug() << "Queue[" << row << "][" << col << "] size = " << mDoc->getBlockQueueVector()[row][col]->m_pCount->getValue();
	if (mDoc->getBlockQueueVector()[row][col]->m_pCount->getValue() > 50)
		mDoc->getBlockQueueVector()[row][col]->Take();

	//保证所有相机队列都不为空

	if (mDoc->isNetQueneNotEmpty()){
		start_Sig(mDoc, mDoc->par_all.Rota_Type);
	}
}