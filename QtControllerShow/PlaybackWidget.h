#pragma once

#include <QWidget>
#include "ui_PlaybackWidget.h"
#include "CDocument.h"

class PlaybackWidget : public QWidget
{
	Q_OBJECT

public:
	CDocument * pDoc;
	PlaybackWidget(CDocument * doc);
	~PlaybackWidget();

	QLabel *showLabel;
	QListWidget * ListWidgetBtn;
	QTimer * theTimer;
	QTimer * TimerShow;

	public slots:
	void ShowVedio();//播放视频
	void AddItem(QString filename);//实时的刷新，增加点击按钮
	void updateImage();
	void updateListItem();

private:
	cv::VideoCapture capture;
	cv::Mat frame;
	QString strFilePath;
	//QStringList m_Filelist;//找到的文件存入此队列
	QMap<QString, QString> FileMap;
	
	QMap<QString, QString> FileMaplast;
	/*
	函数功能：通过Qt实现在一个目录下查找指定后缀的所有文件
	*/
	void FindFileFromPath();
	QString FindLocalFileFromPath(QString& filename);
	void connections();//链接函数
	void Init();//初始化函数
	QMap<QString, QString> merge_maps(QMap<QString, QString> & FileMap, QMap<QString, QString> & FileMaplast);
	Ui::PlaybackWidget ui;
};
