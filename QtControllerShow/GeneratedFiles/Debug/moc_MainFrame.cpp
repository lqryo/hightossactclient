/****************************************************************************
** Meta object code from reading C++ file 'MainFrame.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../MainFrame.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainFrame.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainFrame_t {
    QByteArrayData data[26];
    char stringdata0[346];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainFrame_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainFrame_t qt_meta_stringdata_MainFrame = {
    {
QT_MOC_LITERAL(0, 0, 9), // "MainFrame"
QT_MOC_LITERAL(1, 10, 15), // "updateFrame_Sig"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 13), // "showFrame_Sig"
QT_MOC_LITERAL(4, 41, 14), // "OpenCaemra_Sig"
QT_MOC_LITERAL(5, 56, 18), // "StartAcquiring_Sig"
QT_MOC_LITERAL(6, 75, 24), // "CBlockingQueue<cv::Mat>&"
QT_MOC_LITERAL(7, 100, 15), // "StitchedPic_Sig"
QT_MOC_LITERAL(8, 116, 10), // "CDocument*"
QT_MOC_LITERAL(9, 127, 8), // "test_sig"
QT_MOC_LITERAL(10, 136, 12), // "SendmsgStart"
QT_MOC_LITERAL(11, 149, 11), // "SendmsgStop"
QT_MOC_LITERAL(12, 161, 11), // "SendmsgExit"
QT_MOC_LITERAL(13, 173, 14), // "SendmsgRestart"
QT_MOC_LITERAL(14, 188, 9), // "ReadFrame"
QT_MOC_LITERAL(15, 198, 11), // "updateFrame"
QT_MOC_LITERAL(16, 210, 14), // "NetupdateFrame"
QT_MOC_LITERAL(17, 225, 8), // "testShow"
QT_MOC_LITERAL(18, 234, 20), // "actionShowClientList"
QT_MOC_LITERAL(19, 255, 8), // "StartFun"
QT_MOC_LITERAL(20, 264, 10), // "PerShowFun"
QT_MOC_LITERAL(21, 275, 12), // "VedioShowFun"
QT_MOC_LITERAL(22, 288, 13), // "StitchShowFun"
QT_MOC_LITERAL(23, 302, 14), // "LogInCameraFun"
QT_MOC_LITERAL(24, 317, 13), // "SubWidgetSetP"
QT_MOC_LITERAL(25, 331, 14) // "InitShowWidget"

    },
    "MainFrame\0updateFrame_Sig\0\0showFrame_Sig\0"
    "OpenCaemra_Sig\0StartAcquiring_Sig\0"
    "CBlockingQueue<cv::Mat>&\0StitchedPic_Sig\0"
    "CDocument*\0test_sig\0SendmsgStart\0"
    "SendmsgStop\0SendmsgExit\0SendmsgRestart\0"
    "ReadFrame\0updateFrame\0NetupdateFrame\0"
    "testShow\0actionShowClientList\0StartFun\0"
    "PerShowFun\0VedioShowFun\0StitchShowFun\0"
    "LogInCameraFun\0SubWidgetSetP\0"
    "InitShowWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainFrame[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x06 /* Public */,
       3,    0,  125,    2, 0x06 /* Public */,
       4,    0,  126,    2, 0x06 /* Public */,
       5,    1,  127,    2, 0x06 /* Public */,
       7,    2,  130,    2, 0x06 /* Public */,
       9,    0,  135,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,  136,    2, 0x0a /* Public */,
      11,    0,  137,    2, 0x0a /* Public */,
      12,    0,  138,    2, 0x0a /* Public */,
      13,    0,  139,    2, 0x0a /* Public */,
      14,    0,  140,    2, 0x0a /* Public */,
      15,    0,  141,    2, 0x0a /* Public */,
      16,    0,  142,    2, 0x0a /* Public */,
      17,    0,  143,    2, 0x0a /* Public */,
      18,    0,  144,    2, 0x0a /* Public */,
      19,    0,  145,    2, 0x0a /* Public */,
      20,    0,  146,    2, 0x0a /* Public */,
      21,    0,  147,    2, 0x0a /* Public */,
      22,    0,  148,    2, 0x0a /* Public */,
      23,    0,  149,    2, 0x0a /* Public */,
      24,    0,  150,    2, 0x0a /* Public */,
      25,    0,  151,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void, 0x80000000 | 8, QMetaType::Int,    2,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainFrame::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainFrame *_t = static_cast<MainFrame *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updateFrame_Sig(); break;
        case 1: _t->showFrame_Sig(); break;
        case 2: _t->OpenCaemra_Sig(); break;
        case 3: _t->StartAcquiring_Sig((*reinterpret_cast< CBlockingQueue<cv::Mat>(*)>(_a[1]))); break;
        case 4: _t->StitchedPic_Sig((*reinterpret_cast< CDocument*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->test_sig(); break;
        case 6: _t->SendmsgStart(); break;
        case 7: _t->SendmsgStop(); break;
        case 8: _t->SendmsgExit(); break;
        case 9: _t->SendmsgRestart(); break;
        case 10: _t->ReadFrame(); break;
        case 11: _t->updateFrame(); break;
        case 12: _t->NetupdateFrame(); break;
        case 13: _t->testShow(); break;
        case 14: _t->actionShowClientList(); break;
        case 15: _t->StartFun(); break;
        case 16: _t->PerShowFun(); break;
        case 17: _t->VedioShowFun(); break;
        case 18: _t->StitchShowFun(); break;
        case 19: _t->LogInCameraFun(); break;
        case 20: _t->SubWidgetSetP(); break;
        case 21: _t->InitShowWidget(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainFrame::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainFrame::updateFrame_Sig)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainFrame::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainFrame::showFrame_Sig)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainFrame::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainFrame::OpenCaemra_Sig)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainFrame::*_t)(CBlockingQueue<cv::Mat> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainFrame::StartAcquiring_Sig)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainFrame::*_t)(CDocument * , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainFrame::StitchedPic_Sig)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainFrame::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainFrame::test_sig)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject MainFrame::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainFrame.data,
      qt_meta_data_MainFrame,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainFrame::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainFrame::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainFrame.stringdata0))
        return static_cast<void*>(const_cast< MainFrame*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainFrame::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void MainFrame::updateFrame_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void MainFrame::showFrame_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void MainFrame::OpenCaemra_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void MainFrame::StartAcquiring_Sig(CBlockingQueue<cv::Mat> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainFrame::StitchedPic_Sig(CDocument * _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainFrame::test_sig()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
