/****************************************************************************
** Meta object code from reading C++ file 'TCPServer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../TCPServer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TCPServer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CTCPServer_t {
    QByteArrayData data[12];
    char stringdata0[155];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CTCPServer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CTCPServer_t qt_meta_stringdata_CTCPServer = {
    {
QT_MOC_LITERAL(0, 0, 10), // "CTCPServer"
QT_MOC_LITERAL(1, 11, 10), // "update_Sig"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 9), // "start_Sig"
QT_MOC_LITERAL(4, 33, 14), // "AnalyStart_Sig"
QT_MOC_LITERAL(5, 48, 14), // "ResultShow_Sig"
QT_MOC_LITERAL(6, 63, 15), // "LogInResult_Sig"
QT_MOC_LITERAL(7, 79, 20), // "clientShowUpdate_Sig"
QT_MOC_LITERAL(8, 100, 16), // "handleConnection"
QT_MOC_LITERAL(9, 117, 12), // "handleClient"
QT_MOC_LITERAL(10, 130, 11), // "QTcpSocket*"
QT_MOC_LITERAL(11, 142, 12) // "removeClient"

    },
    "CTCPServer\0update_Sig\0\0start_Sig\0"
    "AnalyStart_Sig\0ResultShow_Sig\0"
    "LogInResult_Sig\0clientShowUpdate_Sig\0"
    "handleConnection\0handleClient\0QTcpSocket*\0"
    "removeClient"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CTCPServer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,
       3,    0,   60,    2, 0x06 /* Public */,
       4,    0,   61,    2, 0x06 /* Public */,
       5,    0,   62,    2, 0x06 /* Public */,
       6,    2,   63,    2, 0x06 /* Public */,
       7,    0,   68,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   69,    2, 0x08 /* Private */,
       9,    1,   70,    2, 0x08 /* Private */,
      11,    1,   73,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::UChar, QMetaType::Bool,    2,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void, 0x80000000 | 10,    2,

       0        // eod
};

void CTCPServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CTCPServer *_t = static_cast<CTCPServer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_Sig(); break;
        case 1: _t->start_Sig(); break;
        case 2: _t->AnalyStart_Sig(); break;
        case 3: _t->ResultShow_Sig(); break;
        case 4: _t->LogInResult_Sig((*reinterpret_cast< quint8(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 5: _t->clientShowUpdate_Sig(); break;
        case 6: _t->handleConnection(); break;
        case 7: _t->handleClient((*reinterpret_cast< QTcpSocket*(*)>(_a[1]))); break;
        case 8: _t->removeClient((*reinterpret_cast< QTcpSocket*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QTcpSocket* >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QTcpSocket* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CTCPServer::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CTCPServer::update_Sig)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CTCPServer::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CTCPServer::start_Sig)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (CTCPServer::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CTCPServer::AnalyStart_Sig)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (CTCPServer::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CTCPServer::ResultShow_Sig)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (CTCPServer::*_t)(quint8 , bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CTCPServer::LogInResult_Sig)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (CTCPServer::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CTCPServer::clientShowUpdate_Sig)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject CTCPServer::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CTCPServer.data,
      qt_meta_data_CTCPServer,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CTCPServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CTCPServer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CTCPServer.stringdata0))
        return static_cast<void*>(const_cast< CTCPServer*>(this));
    return QObject::qt_metacast(_clname);
}

int CTCPServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void CTCPServer::update_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void CTCPServer::start_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void CTCPServer::AnalyStart_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void CTCPServer::ResultShow_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void CTCPServer::LogInResult_Sig(quint8 _t1, bool _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void CTCPServer::clientShowUpdate_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
