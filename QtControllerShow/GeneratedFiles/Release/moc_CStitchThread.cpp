/****************************************************************************
** Meta object code from reading C++ file 'CStitchThread.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../CStitchThread.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CStitchThread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CStitchThread_t {
    QByteArrayData data[9];
    char stringdata0[93];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CStitchThread_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CStitchThread_t qt_meta_stringdata_CStitchThread = {
    {
QT_MOC_LITERAL(0, 0, 13), // "CStitchThread"
QT_MOC_LITERAL(1, 14, 12), // "testShow_Sig"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 14), // "NextStitch_Sig"
QT_MOC_LITERAL(4, 43, 10), // "CDocument*"
QT_MOC_LITERAL(5, 54, 4), // "pDoc"
QT_MOC_LITERAL(6, 59, 13), // "ShowFrame_Sig"
QT_MOC_LITERAL(7, 73, 14), // "StitchFunction"
QT_MOC_LITERAL(8, 88, 4) // "flag"

    },
    "CStitchThread\0testShow_Sig\0\0NextStitch_Sig\0"
    "CDocument*\0pDoc\0ShowFrame_Sig\0"
    "StitchFunction\0flag"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CStitchThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    2,   35,    2, 0x06 /* Public */,
       6,    0,   40,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    2,   41,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, QMetaType::Int,    5,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 4, QMetaType::Int,    5,    8,

       0        // eod
};

void CStitchThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CStitchThread *_t = static_cast<CStitchThread *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->testShow_Sig(); break;
        case 1: _t->NextStitch_Sig((*reinterpret_cast< CDocument*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->ShowFrame_Sig(); break;
        case 3: _t->StitchFunction((*reinterpret_cast< CDocument*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CStitchThread::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CStitchThread::testShow_Sig)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CStitchThread::*_t)(CDocument * , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CStitchThread::NextStitch_Sig)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (CStitchThread::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CStitchThread::ShowFrame_Sig)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject CStitchThread::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CStitchThread.data,
      qt_meta_data_CStitchThread,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CStitchThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CStitchThread::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CStitchThread.stringdata0))
        return static_cast<void*>(const_cast< CStitchThread*>(this));
    return QObject::qt_metacast(_clname);
}

int CStitchThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void CStitchThread::testShow_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void CStitchThread::NextStitch_Sig(CDocument * _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CStitchThread::ShowFrame_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
