/****************************************************************************
** Meta object code from reading C++ file 'MainWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../MainWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWidget_t {
    QByteArrayData data[35];
    char stringdata0[436];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWidget_t qt_meta_stringdata_MainWidget = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWidget"
QT_MOC_LITERAL(1, 11, 15), // "updateFrame_Sig"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 13), // "ReadFrame_Sig"
QT_MOC_LITERAL(4, 42, 14), // "OpenCaemra_Sig"
QT_MOC_LITERAL(5, 57, 18), // "StartAcquiring_Sig"
QT_MOC_LITERAL(6, 76, 24), // "CBlockingQueue<cv::Mat>&"
QT_MOC_LITERAL(7, 101, 15), // "StitchedPic_Sig"
QT_MOC_LITERAL(8, 117, 10), // "CDocument*"
QT_MOC_LITERAL(9, 128, 8), // "test_sig"
QT_MOC_LITERAL(10, 137, 8), // "StartFun"
QT_MOC_LITERAL(11, 146, 16), // "perCameraShowFun"
QT_MOC_LITERAL(12, 163, 10), // "PerShowFun"
QT_MOC_LITERAL(13, 174, 13), // "StitchShowFun"
QT_MOC_LITERAL(14, 188, 11), // "PlaybackFun"
QT_MOC_LITERAL(15, 200, 14), // "LogInCameraFun"
QT_MOC_LITERAL(16, 215, 13), // "SubWidgetSetP"
QT_MOC_LITERAL(17, 229, 14), // "InitShowWidget"
QT_MOC_LITERAL(18, 244, 17), // "CameraFloorUpdate"
QT_MOC_LITERAL(19, 262, 3), // "row"
QT_MOC_LITERAL(20, 266, 3), // "col"
QT_MOC_LITERAL(21, 270, 7), // "ExitFun"
QT_MOC_LITERAL(22, 278, 13), // "StitchtestFun"
QT_MOC_LITERAL(23, 292, 9), // "ReadFrame"
QT_MOC_LITERAL(24, 302, 8), // "testShow"
QT_MOC_LITERAL(25, 311, 11), // "updateFrame"
QT_MOC_LITERAL(26, 323, 9), // "perCaShow"
QT_MOC_LITERAL(27, 333, 16), // "ResultShowUpdate"
QT_MOC_LITERAL(28, 350, 20), // "actionShowClientList"
QT_MOC_LITERAL(29, 371, 12), // "update_frame"
QT_MOC_LITERAL(30, 384, 9), // "HKCamera*"
QT_MOC_LITERAL(31, 394, 6), // "camera"
QT_MOC_LITERAL(32, 401, 10), // "MaxClicked"
QT_MOC_LITERAL(33, 412, 10), // "MinClicked"
QT_MOC_LITERAL(34, 423, 12) // "CloseClicked"

    },
    "MainWidget\0updateFrame_Sig\0\0ReadFrame_Sig\0"
    "OpenCaemra_Sig\0StartAcquiring_Sig\0"
    "CBlockingQueue<cv::Mat>&\0StitchedPic_Sig\0"
    "CDocument*\0test_sig\0StartFun\0"
    "perCameraShowFun\0PerShowFun\0StitchShowFun\0"
    "PlaybackFun\0LogInCameraFun\0SubWidgetSetP\0"
    "InitShowWidget\0CameraFloorUpdate\0row\0"
    "col\0ExitFun\0StitchtestFun\0ReadFrame\0"
    "testShow\0updateFrame\0perCaShow\0"
    "ResultShowUpdate\0actionShowClientList\0"
    "update_frame\0HKCamera*\0camera\0MaxClicked\0"
    "MinClicked\0CloseClicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      27,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  149,    2, 0x06 /* Public */,
       3,    0,  150,    2, 0x06 /* Public */,
       4,    0,  151,    2, 0x06 /* Public */,
       5,    1,  152,    2, 0x06 /* Public */,
       7,    2,  155,    2, 0x06 /* Public */,
       9,    0,  160,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,  161,    2, 0x0a /* Public */,
      11,    0,  162,    2, 0x0a /* Public */,
      12,    0,  163,    2, 0x0a /* Public */,
      13,    0,  164,    2, 0x0a /* Public */,
      14,    0,  165,    2, 0x0a /* Public */,
      15,    0,  166,    2, 0x0a /* Public */,
      16,    0,  167,    2, 0x0a /* Public */,
      17,    0,  168,    2, 0x0a /* Public */,
      18,    2,  169,    2, 0x0a /* Public */,
      21,    0,  174,    2, 0x0a /* Public */,
      22,    0,  175,    2, 0x0a /* Public */,
      23,    0,  176,    2, 0x0a /* Public */,
      24,    0,  177,    2, 0x0a /* Public */,
      25,    0,  178,    2, 0x0a /* Public */,
      26,    0,  179,    2, 0x0a /* Public */,
      27,    0,  180,    2, 0x0a /* Public */,
      28,    0,  181,    2, 0x0a /* Public */,
      29,    1,  182,    2, 0x0a /* Public */,
      32,    0,  185,    2, 0x09 /* Protected */,
      33,    0,  186,    2, 0x09 /* Protected */,
      34,    0,  187,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void, 0x80000000 | 8, QMetaType::Int,    2,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   19,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 30,   31,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWidget *_t = static_cast<MainWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updateFrame_Sig(); break;
        case 1: _t->ReadFrame_Sig(); break;
        case 2: _t->OpenCaemra_Sig(); break;
        case 3: _t->StartAcquiring_Sig((*reinterpret_cast< CBlockingQueue<cv::Mat>(*)>(_a[1]))); break;
        case 4: _t->StitchedPic_Sig((*reinterpret_cast< CDocument*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->test_sig(); break;
        case 6: _t->StartFun(); break;
        case 7: _t->perCameraShowFun(); break;
        case 8: _t->PerShowFun(); break;
        case 9: _t->StitchShowFun(); break;
        case 10: _t->PlaybackFun(); break;
        case 11: _t->LogInCameraFun(); break;
        case 12: _t->SubWidgetSetP(); break;
        case 13: _t->InitShowWidget(); break;
        case 14: _t->CameraFloorUpdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 15: _t->ExitFun(); break;
        case 16: _t->StitchtestFun(); break;
        case 17: _t->ReadFrame(); break;
        case 18: _t->testShow(); break;
        case 19: _t->updateFrame(); break;
        case 20: _t->perCaShow(); break;
        case 21: _t->ResultShowUpdate(); break;
        case 22: _t->actionShowClientList(); break;
        case 23: _t->update_frame((*reinterpret_cast< HKCamera*(*)>(_a[1]))); break;
        case 24: _t->MaxClicked(); break;
        case 25: _t->MinClicked(); break;
        case 26: _t->CloseClicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 23:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< HKCamera* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWidget::updateFrame_Sig)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWidget::ReadFrame_Sig)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWidget::OpenCaemra_Sig)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWidget::*_t)(CBlockingQueue<cv::Mat> & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWidget::StartAcquiring_Sig)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainWidget::*_t)(CDocument * , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWidget::StitchedPic_Sig)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainWidget::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWidget::test_sig)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject MainWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MainWidget.data,
      qt_meta_data_MainWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWidget.stringdata0))
        return static_cast<void*>(const_cast< MainWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int MainWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    }
    return _id;
}

// SIGNAL 0
void MainWidget::updateFrame_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void MainWidget::ReadFrame_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void MainWidget::OpenCaemra_Sig()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void MainWidget::StartAcquiring_Sig(CBlockingQueue<cv::Mat> & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainWidget::StitchedPic_Sig(CDocument * _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainWidget::test_sig()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
