/****************************************************************************
** Meta object code from reading C++ file 'CameraLogIn.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../CameraLogIn.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CameraLogIn.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CameraLogIn_t {
    QByteArrayData data[27];
    char stringdata0[372];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CameraLogIn_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CameraLogIn_t qt_meta_stringdata_CameraLogIn = {
    {
QT_MOC_LITERAL(0, 0, 11), // "CameraLogIn"
QT_MOC_LITERAL(1, 12, 21), // "CameraFloorUpdate_Sig"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 17), // "CameraDisplay_Sig"
QT_MOC_LITERAL(4, 53, 12), // "LogInRresult"
QT_MOC_LITERAL(5, 66, 4), // "type"
QT_MOC_LITERAL(6, 71, 7), // "success"
QT_MOC_LITERAL(7, 79, 14), // "LogInCameraFun"
QT_MOC_LITERAL(8, 94, 3), // "row"
QT_MOC_LITERAL(9, 98, 3), // "col"
QT_MOC_LITERAL(10, 102, 16), // "ClientShowupdate"
QT_MOC_LITERAL(11, 119, 12), // "ClientAddFun"
QT_MOC_LITERAL(12, 132, 15), // "ClientAddupdate"
QT_MOC_LITERAL(13, 148, 15), // "DisplayUpdate00"
QT_MOC_LITERAL(14, 164, 15), // "DisplayUpdate01"
QT_MOC_LITERAL(15, 180, 15), // "DisplayUpdate10"
QT_MOC_LITERAL(16, 196, 15), // "DisplayUpdate11"
QT_MOC_LITERAL(17, 212, 15), // "DisplayUpdate20"
QT_MOC_LITERAL(18, 228, 15), // "DisplayUpdate21"
QT_MOC_LITERAL(19, 244, 15), // "DisplayUpdate30"
QT_MOC_LITERAL(20, 260, 15), // "DisplayUpdate31"
QT_MOC_LITERAL(21, 276, 15), // "DisplayUpdate40"
QT_MOC_LITERAL(22, 292, 15), // "DisplayUpdate41"
QT_MOC_LITERAL(23, 308, 15), // "DisplayUpdate50"
QT_MOC_LITERAL(24, 324, 15), // "DisplayUpdate51"
QT_MOC_LITERAL(25, 340, 15), // "DisplayUpdate60"
QT_MOC_LITERAL(26, 356, 15) // "DisplayUpdate61"

    },
    "CameraLogIn\0CameraFloorUpdate_Sig\0\0"
    "CameraDisplay_Sig\0LogInRresult\0type\0"
    "success\0LogInCameraFun\0row\0col\0"
    "ClientShowupdate\0ClientAddFun\0"
    "ClientAddupdate\0DisplayUpdate00\0"
    "DisplayUpdate01\0DisplayUpdate10\0"
    "DisplayUpdate11\0DisplayUpdate20\0"
    "DisplayUpdate21\0DisplayUpdate30\0"
    "DisplayUpdate31\0DisplayUpdate40\0"
    "DisplayUpdate41\0DisplayUpdate50\0"
    "DisplayUpdate51\0DisplayUpdate60\0"
    "DisplayUpdate61"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CameraLogIn[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  119,    2, 0x06 /* Public */,
       3,    2,  124,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    2,  129,    2, 0x0a /* Public */,
       7,    2,  134,    2, 0x0a /* Public */,
      10,    0,  139,    2, 0x0a /* Public */,
      11,    0,  140,    2, 0x0a /* Public */,
      12,    0,  141,    2, 0x0a /* Public */,
      13,    0,  142,    2, 0x0a /* Public */,
      14,    0,  143,    2, 0x0a /* Public */,
      15,    0,  144,    2, 0x0a /* Public */,
      16,    0,  145,    2, 0x0a /* Public */,
      17,    0,  146,    2, 0x0a /* Public */,
      18,    0,  147,    2, 0x0a /* Public */,
      19,    0,  148,    2, 0x0a /* Public */,
      20,    0,  149,    2, 0x0a /* Public */,
      21,    0,  150,    2, 0x0a /* Public */,
      22,    0,  151,    2, 0x0a /* Public */,
      23,    0,  152,    2, 0x0a /* Public */,
      24,    0,  153,    2, 0x0a /* Public */,
      25,    0,  154,    2, 0x0a /* Public */,
      26,    0,  155,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::UChar, QMetaType::Bool,    5,    6,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    8,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CameraLogIn::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CameraLogIn *_t = static_cast<CameraLogIn *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->CameraFloorUpdate_Sig((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->CameraDisplay_Sig((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->LogInRresult((*reinterpret_cast< quint8(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 3: _t->LogInCameraFun((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->ClientShowupdate(); break;
        case 5: _t->ClientAddFun(); break;
        case 6: _t->ClientAddupdate(); break;
        case 7: _t->DisplayUpdate00(); break;
        case 8: _t->DisplayUpdate01(); break;
        case 9: _t->DisplayUpdate10(); break;
        case 10: _t->DisplayUpdate11(); break;
        case 11: _t->DisplayUpdate20(); break;
        case 12: _t->DisplayUpdate21(); break;
        case 13: _t->DisplayUpdate30(); break;
        case 14: _t->DisplayUpdate31(); break;
        case 15: _t->DisplayUpdate40(); break;
        case 16: _t->DisplayUpdate41(); break;
        case 17: _t->DisplayUpdate50(); break;
        case 18: _t->DisplayUpdate51(); break;
        case 19: _t->DisplayUpdate60(); break;
        case 20: _t->DisplayUpdate61(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CameraLogIn::*_t)(int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CameraLogIn::CameraFloorUpdate_Sig)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CameraLogIn::*_t)(int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CameraLogIn::CameraDisplay_Sig)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject CameraLogIn::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CameraLogIn.data,
      qt_meta_data_CameraLogIn,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CameraLogIn::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CameraLogIn::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CameraLogIn.stringdata0))
        return static_cast<void*>(const_cast< CameraLogIn*>(this));
    return QWidget::qt_metacast(_clname);
}

int CameraLogIn::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void CameraLogIn::CameraFloorUpdate_Sig(int _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CameraLogIn::CameraDisplay_Sig(int _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
