/********************************************************************************
** Form generated from reading UI file 'CameraLogIn.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERALOGIN_H
#define UI_CAMERALOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CameraLogIn
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QWidget *camera_manage_3;
    QHBoxLayout *horizontalLayout_9;
    QLabel *camera_manage;
    QPushButton *refresh_button;
    QWidget *camera_manage_2;
    QVBoxLayout *verticalLayout_3;
    QTableWidget *camera_table;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_4;
    QWidget *add_manage_3;
    QHBoxLayout *horizontalLayout_10;
    QLabel *add_manage;
    QPushButton *refresh_button_2;
    QWidget *add_manage_2;
    QVBoxLayout *verticalLayout_6;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout;
    QLabel *host;
    QLabel *IPAddress;
    QLabel *Account;
    QLabel *PassWord;
    QLabel *CameraID;
    QLabel *low;
    QLabel *high;
    QLabel *Add;
    QWidget *widget_6;
    QHBoxLayout *horizontalLayout_12;
    QWidget *widget_7;
    QHBoxLayout *horizontalLayout_3;
    QComboBox *hostBox;
    QWidget *widget_9;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *CameraIPEdit;
    QWidget *widget_10;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *AccountEdit;
    QWidget *widget_11;
    QHBoxLayout *horizontalLayout_7;
    QLineEdit *CameraPWEdit;
    QWidget *widget_12;
    QHBoxLayout *horizontalLayout_8;
    QLineEdit *CameraIDEdit;
    QWidget *widget_8;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lowfloor;
    QWidget *widget_14;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *highfloor;
    QWidget *widget_13;
    QHBoxLayout *horizontalLayout_11;
    QPushButton *add_button;
    QWidget *widget_5;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout_5;
    QLabel *information_label;

    void setupUi(QWidget *CameraLogIn)
    {
        if (CameraLogIn->objectName().isEmpty())
            CameraLogIn->setObjectName(QStringLiteral("CameraLogIn"));
        CameraLogIn->resize(1224, 850);
        verticalLayout = new QVBoxLayout(CameraLogIn);
        verticalLayout->setSpacing(30);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(20, 20, 20, 20);
        widget = new QWidget(CameraLogIn);
        widget->setObjectName(QStringLiteral("widget"));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        camera_manage_3 = new QWidget(widget);
        camera_manage_3->setObjectName(QStringLiteral("camera_manage_3"));
        horizontalLayout_9 = new QHBoxLayout(camera_manage_3);
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 5, 20, 5);
        camera_manage = new QLabel(camera_manage_3);
        camera_manage->setObjectName(QStringLiteral("camera_manage"));

        horizontalLayout_9->addWidget(camera_manage);

        refresh_button = new QPushButton(camera_manage_3);
        refresh_button->setObjectName(QStringLiteral("refresh_button"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(refresh_button->sizePolicy().hasHeightForWidth());
        refresh_button->setSizePolicy(sizePolicy);

        horizontalLayout_9->addWidget(refresh_button);

        horizontalLayout_9->setStretch(0, 12);
        horizontalLayout_9->setStretch(1, 1);

        verticalLayout_2->addWidget(camera_manage_3);

        camera_manage_2 = new QWidget(widget);
        camera_manage_2->setObjectName(QStringLiteral("camera_manage_2"));
        verticalLayout_3 = new QVBoxLayout(camera_manage_2);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(3, 0, 3, 3);
        camera_table = new QTableWidget(camera_manage_2);
        if (camera_table->columnCount() < 9)
            camera_table->setColumnCount(9);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        camera_table->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        camera_table->setObjectName(QStringLiteral("camera_table"));
        camera_table->setToolTipDuration(0);
        camera_table->setRowCount(0);
        camera_table->horizontalHeader()->setDefaultSectionSize(140);
        camera_table->horizontalHeader()->setMinimumSectionSize(100);
        camera_table->horizontalHeader()->setStretchLastSection(false);

        verticalLayout_3->addWidget(camera_table);


        verticalLayout_2->addWidget(camera_manage_2);

        verticalLayout_2->setStretch(0, 1);
        verticalLayout_2->setStretch(1, 12);

        verticalLayout->addWidget(widget);

        widget_2 = new QWidget(CameraLogIn);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        verticalLayout_4 = new QVBoxLayout(widget_2);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        add_manage_3 = new QWidget(widget_2);
        add_manage_3->setObjectName(QStringLiteral("add_manage_3"));
        horizontalLayout_10 = new QHBoxLayout(add_manage_3);
        horizontalLayout_10->setSpacing(0);
        horizontalLayout_10->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 5, 20, 5);
        add_manage = new QLabel(add_manage_3);
        add_manage->setObjectName(QStringLiteral("add_manage"));

        horizontalLayout_10->addWidget(add_manage);

        refresh_button_2 = new QPushButton(add_manage_3);
        refresh_button_2->setObjectName(QStringLiteral("refresh_button_2"));
        sizePolicy.setHeightForWidth(refresh_button_2->sizePolicy().hasHeightForWidth());
        refresh_button_2->setSizePolicy(sizePolicy);

        horizontalLayout_10->addWidget(refresh_button_2);

        horizontalLayout_10->setStretch(0, 12);
        horizontalLayout_10->setStretch(1, 1);

        verticalLayout_4->addWidget(add_manage_3);

        add_manage_2 = new QWidget(widget_2);
        add_manage_2->setObjectName(QStringLiteral("add_manage_2"));
        verticalLayout_6 = new QVBoxLayout(add_manage_2);
        verticalLayout_6->setSpacing(0);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        widget_4 = new QWidget(add_manage_2);
        widget_4->setObjectName(QStringLiteral("widget_4"));
        horizontalLayout = new QHBoxLayout(widget_4);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(4, 0, 2, 0);
        host = new QLabel(widget_4);
        host->setObjectName(QStringLiteral("host"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(host->sizePolicy().hasHeightForWidth());
        host->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        host->setFont(font);
        host->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(host);

        IPAddress = new QLabel(widget_4);
        IPAddress->setObjectName(QStringLiteral("IPAddress"));
        IPAddress->setFont(font);
        IPAddress->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(IPAddress);

        Account = new QLabel(widget_4);
        Account->setObjectName(QStringLiteral("Account"));
        Account->setFont(font);
        Account->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(Account);

        PassWord = new QLabel(widget_4);
        PassWord->setObjectName(QStringLiteral("PassWord"));
        PassWord->setFont(font);
        PassWord->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(PassWord);

        CameraID = new QLabel(widget_4);
        CameraID->setObjectName(QStringLiteral("CameraID"));
        CameraID->setFont(font);
        CameraID->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(CameraID);

        low = new QLabel(widget_4);
        low->setObjectName(QStringLiteral("low"));
        QFont font1;
        font1.setFamily(QStringLiteral("SimSun-ExtB"));
        font1.setPointSize(11);
        font1.setBold(true);
        font1.setWeight(75);
        low->setFont(font1);
        low->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(low);

        high = new QLabel(widget_4);
        high->setObjectName(QStringLiteral("high"));
        high->setFont(font1);
        high->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(high);

        Add = new QLabel(widget_4);
        Add->setObjectName(QStringLiteral("Add"));
        Add->setFont(font);
        Add->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(Add);

        horizontalLayout->setStretch(0, 5);
        horizontalLayout->setStretch(1, 4);
        horizontalLayout->setStretch(2, 4);
        horizontalLayout->setStretch(3, 4);
        horizontalLayout->setStretch(4, 3);
        horizontalLayout->setStretch(5, 3);
        horizontalLayout->setStretch(6, 3);
        horizontalLayout->setStretch(7, 3);

        verticalLayout_6->addWidget(widget_4);

        widget_6 = new QWidget(add_manage_2);
        widget_6->setObjectName(QStringLiteral("widget_6"));
        horizontalLayout_12 = new QHBoxLayout(widget_6);
        horizontalLayout_12->setSpacing(0);
        horizontalLayout_12->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(4, 0, 2, 0);
        widget_7 = new QWidget(widget_6);
        widget_7->setObjectName(QStringLiteral("widget_7"));
        horizontalLayout_3 = new QHBoxLayout(widget_7);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        hostBox = new QComboBox(widget_7);
        hostBox->setObjectName(QStringLiteral("hostBox"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(hostBox->sizePolicy().hasHeightForWidth());
        hostBox->setSizePolicy(sizePolicy2);

        horizontalLayout_3->addWidget(hostBox);


        horizontalLayout_12->addWidget(widget_7);

        widget_9 = new QWidget(widget_6);
        widget_9->setObjectName(QStringLiteral("widget_9"));
        horizontalLayout_5 = new QHBoxLayout(widget_9);
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        CameraIPEdit = new QLineEdit(widget_9);
        CameraIPEdit->setObjectName(QStringLiteral("CameraIPEdit"));
        CameraIPEdit->setMinimumSize(QSize(0, 32));
        CameraIPEdit->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(CameraIPEdit);


        horizontalLayout_12->addWidget(widget_9);

        widget_10 = new QWidget(widget_6);
        widget_10->setObjectName(QStringLiteral("widget_10"));
        horizontalLayout_6 = new QHBoxLayout(widget_10);
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        AccountEdit = new QLineEdit(widget_10);
        AccountEdit->setObjectName(QStringLiteral("AccountEdit"));
        AccountEdit->setMinimumSize(QSize(0, 32));
        AccountEdit->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(AccountEdit);


        horizontalLayout_12->addWidget(widget_10);

        widget_11 = new QWidget(widget_6);
        widget_11->setObjectName(QStringLiteral("widget_11"));
        horizontalLayout_7 = new QHBoxLayout(widget_11);
        horizontalLayout_7->setSpacing(0);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        CameraPWEdit = new QLineEdit(widget_11);
        CameraPWEdit->setObjectName(QStringLiteral("CameraPWEdit"));
        CameraPWEdit->setMinimumSize(QSize(0, 32));
        CameraPWEdit->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(CameraPWEdit);


        horizontalLayout_12->addWidget(widget_11);

        widget_12 = new QWidget(widget_6);
        widget_12->setObjectName(QStringLiteral("widget_12"));
        horizontalLayout_8 = new QHBoxLayout(widget_12);
        horizontalLayout_8->setSpacing(0);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        CameraIDEdit = new QLineEdit(widget_12);
        CameraIDEdit->setObjectName(QStringLiteral("CameraIDEdit"));
        CameraIDEdit->setMinimumSize(QSize(0, 32));
        CameraIDEdit->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(CameraIDEdit);


        horizontalLayout_12->addWidget(widget_12);

        widget_8 = new QWidget(widget_6);
        widget_8->setObjectName(QStringLiteral("widget_8"));
        horizontalLayout_2 = new QHBoxLayout(widget_8);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        lowfloor = new QLineEdit(widget_8);
        lowfloor->setObjectName(QStringLiteral("lowfloor"));
        lowfloor->setMinimumSize(QSize(0, 32));
        lowfloor->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lowfloor);


        horizontalLayout_12->addWidget(widget_8);

        widget_14 = new QWidget(widget_6);
        widget_14->setObjectName(QStringLiteral("widget_14"));
        horizontalLayout_4 = new QHBoxLayout(widget_14);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        highfloor = new QLineEdit(widget_14);
        highfloor->setObjectName(QStringLiteral("highfloor"));
        highfloor->setMinimumSize(QSize(0, 32));
        highfloor->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(highfloor);


        horizontalLayout_12->addWidget(widget_14);

        widget_13 = new QWidget(widget_6);
        widget_13->setObjectName(QStringLiteral("widget_13"));
        horizontalLayout_11 = new QHBoxLayout(widget_13);
        horizontalLayout_11->setSpacing(0);
        horizontalLayout_11->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        add_button = new QPushButton(widget_13);
        add_button->setObjectName(QStringLiteral("add_button"));
        add_button->setMinimumSize(QSize(50, 30));
        add_button->setMaximumSize(QSize(50, 30));
        add_button->setLayoutDirection(Qt::LeftToRight);

        horizontalLayout_11->addWidget(add_button);


        horizontalLayout_12->addWidget(widget_13);

        horizontalLayout_12->setStretch(0, 5);
        horizontalLayout_12->setStretch(1, 4);
        horizontalLayout_12->setStretch(2, 4);
        horizontalLayout_12->setStretch(3, 4);
        horizontalLayout_12->setStretch(4, 3);
        horizontalLayout_12->setStretch(5, 3);
        horizontalLayout_12->setStretch(6, 3);
        horizontalLayout_12->setStretch(7, 3);

        verticalLayout_6->addWidget(widget_6);

        widget_5 = new QWidget(add_manage_2);
        widget_5->setObjectName(QStringLiteral("widget_5"));

        verticalLayout_6->addWidget(widget_5);

        verticalLayout_6->setStretch(0, 1);
        verticalLayout_6->setStretch(1, 1);
        verticalLayout_6->setStretch(2, 1);

        verticalLayout_4->addWidget(add_manage_2);

        verticalLayout_4->setStretch(0, 1);
        verticalLayout_4->setStretch(1, 3);

        verticalLayout->addWidget(widget_2);

        widget_3 = new QWidget(CameraLogIn);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        verticalLayout_5 = new QVBoxLayout(widget_3);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        information_label = new QLabel(widget_3);
        information_label->setObjectName(QStringLiteral("information_label"));
        QFont font2;
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        information_label->setFont(font2);
        information_label->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(information_label);


        verticalLayout->addWidget(widget_3);

        verticalLayout->setStretch(0, 17);
        verticalLayout->setStretch(1, 5);
        verticalLayout->setStretch(2, 2);

        retranslateUi(CameraLogIn);

        QMetaObject::connectSlotsByName(CameraLogIn);
    } // setupUi

    void retranslateUi(QWidget *CameraLogIn)
    {
        CameraLogIn->setWindowTitle(QApplication::translate("CameraLogIn", "CameraLogIn", 0));
        camera_manage->setText(QApplication::translate("CameraLogIn", " \347\256\241\347\220\206\350\256\276\345\244\207", 0));
        refresh_button->setText(QApplication::translate("CameraLogIn", "  \345\210\267\346\226\260", 0));
        QTableWidgetItem *___qtablewidgetitem = camera_table->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("CameraLogIn", "\344\270\273\346\234\272", 0));
        QTableWidgetItem *___qtablewidgetitem1 = camera_table->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("CameraLogIn", "\345\256\242\346\210\267\347\253\257", 0));
        QTableWidgetItem *___qtablewidgetitem2 = camera_table->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("CameraLogIn", "IP\345\234\260\345\235\200", 0));
        QTableWidgetItem *___qtablewidgetitem3 = camera_table->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("CameraLogIn", "\350\264\246\345\217\267", 0));
        QTableWidgetItem *___qtablewidgetitem4 = camera_table->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("CameraLogIn", "\345\257\206\347\240\201", 0));
        QTableWidgetItem *___qtablewidgetitem5 = camera_table->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("CameraLogIn", "\347\233\270\346\234\272ID", 0));
        QTableWidgetItem *___qtablewidgetitem6 = camera_table->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("CameraLogIn", "\346\234\200\344\275\216\346\245\274\345\261\202", 0));
        QTableWidgetItem *___qtablewidgetitem7 = camera_table->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QApplication::translate("CameraLogIn", "\346\234\200\351\253\230\346\245\274\345\261\202", 0));
        QTableWidgetItem *___qtablewidgetitem8 = camera_table->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QApplication::translate("CameraLogIn", "\347\231\273\345\275\225\347\212\266\346\200\201", 0));
#ifndef QT_NO_WHATSTHIS
        camera_table->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_ACCESSIBILITY
        camera_table->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
        add_manage->setText(QApplication::translate("CameraLogIn", " \346\267\273\345\212\240\350\256\276\345\244\207", 0));
        refresh_button_2->setText(QApplication::translate("CameraLogIn", "  \345\210\267\346\226\260", 0));
        host->setText(QApplication::translate("CameraLogIn", "\344\270\273\346\234\272", 0));
        IPAddress->setText(QApplication::translate("CameraLogIn", "\347\233\270\346\234\272IP\345\234\260\345\235\200", 0));
        Account->setText(QApplication::translate("CameraLogIn", "\347\233\270\346\234\272\350\264\246\345\217\267", 0));
        PassWord->setText(QApplication::translate("CameraLogIn", "\347\233\270\346\234\272\345\257\206\347\240\201", 0));
        CameraID->setText(QApplication::translate("CameraLogIn", "\347\233\270\346\234\272ID", 0));
        low->setText(QApplication::translate("CameraLogIn", "\346\234\200\344\275\216\346\245\274\345\261\202", 0));
        high->setText(QApplication::translate("CameraLogIn", "\346\234\200\351\253\230\346\245\274\345\261\202", 0));
        Add->setText(QApplication::translate("CameraLogIn", "\346\267\273\345\212\240", 0));
        CameraIPEdit->setText(QApplication::translate("CameraLogIn", "192.168.1.66", 0));
        AccountEdit->setText(QApplication::translate("CameraLogIn", "admin", 0));
        CameraPWEdit->setText(QApplication::translate("CameraLogIn", "IDM8779235102", 0));
        CameraIDEdit->setText(QApplication::translate("CameraLogIn", "10", 0));
        lowfloor->setText(QApplication::translate("CameraLogIn", "0", 0));
        highfloor->setText(QApplication::translate("CameraLogIn", "0", 0));
        add_button->setText(QApplication::translate("CameraLogIn", "\346\267\273\345\212\240", 0));
        information_label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class CameraLogIn: public Ui_CameraLogIn {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERALOGIN_H
