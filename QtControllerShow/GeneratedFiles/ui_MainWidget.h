/********************************************************************************
** Form generated from reading UI file 'MainWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWIDGET_H
#define UI_MAINWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWidget
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QWidget *titleWdg;
    QHBoxLayout *horizontalLayout_16;
    QWidget *widget_3N;
    QHBoxLayout *horizontalLayout_17;
    QLabel *title_N;
    QWidget *widget_2N;
    QHBoxLayout *horizontalLayout_18;
    QPushButton *minbtn_N;
    QPushButton *maxbtn_N;
    QPushButton *closebtn_N;
    QWidget *MainCentralWidget;
    QVBoxLayout *verticalLayout_14;
    QTabWidget *MainTabWidget;
    QWidget *MainOperateTab;
    QVBoxLayout *verticalLayout_15;
    QWidget *widget_N;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_N;
    QWidget *widget_5N;
    QGroupBox *Operate_N;
    QVBoxLayout *verticalLayout_16;
    QGridLayout *gridLayout_Operate;
    QWidget *widget_4N;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_2N;
    QWidget *widget_6N;
    QGroupBox *Manager_N;
    QVBoxLayout *verticalLayout_17;
    QGridLayout *gridLayout_Manager;
    QWidget *MainPerShowTab;
    QHBoxLayout *horizontalLayout_21;
    QWidget *gridWidget;
    QGridLayout *gridLayout_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidget;
    QGridLayout *gridLayout;
    QGroupBox *resultshowBox_1;
    QVBoxLayout *verticalLayout_18;
    QTextEdit *ResultText;
    QWidget *MainStitchShowTab;
    QHBoxLayout *horizontalLayout_22;
    QWidget *StitchShow;
    QVBoxLayout *verticalLayout_20;
    QLabel *StitchShowLabel;
    QGroupBox *resultshowBox_2;
    QVBoxLayout *verticalLayout_19;
    QTextEdit *resultshow;
    QPushButton *stitchBtn;
    QPushButton *exitBtn;
    QWidget *bottom;
    QHBoxLayout *horizontalLayout;

    void setupUi(QWidget *MainWidget)
    {
        if (MainWidget->objectName().isEmpty())
            MainWidget->setObjectName(QStringLiteral("MainWidget"));
        MainWidget->resize(1024, 766);
        verticalLayout = new QVBoxLayout(MainWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        widget = new QWidget(MainWidget);
        widget->setObjectName(QStringLiteral("widget"));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        titleWdg = new QWidget(widget);
        titleWdg->setObjectName(QStringLiteral("titleWdg"));
        titleWdg->setStyleSheet(QStringLiteral("background: rgb(35, 35, 35);"));
        horizontalLayout_16 = new QHBoxLayout(titleWdg);
        horizontalLayout_16->setSpacing(0);
        horizontalLayout_16->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        horizontalLayout_16->setContentsMargins(0, 0, 0, 0);
        widget_3N = new QWidget(titleWdg);
        widget_3N->setObjectName(QStringLiteral("widget_3N"));
        widget_3N->setStyleSheet(QStringLiteral(""));
        horizontalLayout_17 = new QHBoxLayout(widget_3N);
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        title_N = new QLabel(widget_3N);
        title_N->setObjectName(QStringLiteral("title_N"));

        horizontalLayout_17->addWidget(title_N, 0, Qt::AlignHCenter);


        horizontalLayout_16->addWidget(widget_3N);

        widget_2N = new QWidget(titleWdg);
        widget_2N->setObjectName(QStringLiteral("widget_2N"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget_2N->sizePolicy().hasHeightForWidth());
        widget_2N->setSizePolicy(sizePolicy);
        horizontalLayout_18 = new QHBoxLayout(widget_2N);
        horizontalLayout_18->setSpacing(0);
        horizontalLayout_18->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        horizontalLayout_18->setContentsMargins(0, 8, 0, 8);
        minbtn_N = new QPushButton(widget_2N);
        minbtn_N->setObjectName(QStringLiteral("minbtn_N"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(minbtn_N->sizePolicy().hasHeightForWidth());
        minbtn_N->setSizePolicy(sizePolicy1);
        minbtn_N->setMinimumSize(QSize(35, 35));
        minbtn_N->setMaximumSize(QSize(35, 35));

        horizontalLayout_18->addWidget(minbtn_N);

        maxbtn_N = new QPushButton(widget_2N);
        maxbtn_N->setObjectName(QStringLiteral("maxbtn_N"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(maxbtn_N->sizePolicy().hasHeightForWidth());
        maxbtn_N->setSizePolicy(sizePolicy2);
        maxbtn_N->setMinimumSize(QSize(35, 35));
        maxbtn_N->setMaximumSize(QSize(35, 35));

        horizontalLayout_18->addWidget(maxbtn_N);

        closebtn_N = new QPushButton(widget_2N);
        closebtn_N->setObjectName(QStringLiteral("closebtn_N"));
        sizePolicy1.setHeightForWidth(closebtn_N->sizePolicy().hasHeightForWidth());
        closebtn_N->setSizePolicy(sizePolicy1);
        closebtn_N->setMinimumSize(QSize(35, 35));
        closebtn_N->setMaximumSize(QSize(35, 35));

        horizontalLayout_18->addWidget(closebtn_N);


        horizontalLayout_16->addWidget(widget_2N);

        horizontalLayout_16->setStretch(0, 30);

        verticalLayout_2->addWidget(titleWdg);

        MainCentralWidget = new QWidget(widget);
        MainCentralWidget->setObjectName(QStringLiteral("MainCentralWidget"));
        MainCentralWidget->setAutoFillBackground(false);
        MainCentralWidget->setStyleSheet(QStringLiteral(""));
        verticalLayout_14 = new QVBoxLayout(MainCentralWidget);
        verticalLayout_14->setSpacing(0);
        verticalLayout_14->setContentsMargins(11, 11, 11, 11);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        verticalLayout_14->setContentsMargins(0, 0, 0, 0);
        MainTabWidget = new QTabWidget(MainCentralWidget);
        MainTabWidget->setObjectName(QStringLiteral("MainTabWidget"));
        QFont font;
        font.setPointSize(9);
        MainTabWidget->setFont(font);
        MainTabWidget->setAutoFillBackground(false);
        MainOperateTab = new QWidget();
        MainOperateTab->setObjectName(QStringLiteral("MainOperateTab"));
        MainOperateTab->setAutoFillBackground(false);
        verticalLayout_15 = new QVBoxLayout(MainOperateTab);
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setContentsMargins(11, 11, 11, 11);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        verticalLayout_15->setContentsMargins(40, 20, 40, 20);
        widget_N = new QWidget(MainOperateTab);
        widget_N->setObjectName(QStringLiteral("widget_N"));
        horizontalLayout_19 = new QHBoxLayout(widget_N);
        horizontalLayout_19->setSpacing(0);
        horizontalLayout_19->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        horizontalLayout_19->setContentsMargins(0, 0, 0, 0);
        label_N = new QLabel(widget_N);
        label_N->setObjectName(QStringLiteral("label_N"));

        horizontalLayout_19->addWidget(label_N);

        widget_5N = new QWidget(widget_N);
        widget_5N->setObjectName(QStringLiteral("widget_5N"));

        horizontalLayout_19->addWidget(widget_5N);

        horizontalLayout_19->setStretch(0, 3);
        horizontalLayout_19->setStretch(1, 4);

        verticalLayout_15->addWidget(widget_N);

        Operate_N = new QGroupBox(MainOperateTab);
        Operate_N->setObjectName(QStringLiteral("Operate_N"));
        verticalLayout_16 = new QVBoxLayout(Operate_N);
        verticalLayout_16->setSpacing(0);
        verticalLayout_16->setContentsMargins(11, 11, 11, 11);
        verticalLayout_16->setObjectName(QStringLiteral("verticalLayout_16"));
        verticalLayout_16->setContentsMargins(0, 0, 0, 0);
        gridLayout_Operate = new QGridLayout();
        gridLayout_Operate->setSpacing(6);
        gridLayout_Operate->setObjectName(QStringLiteral("gridLayout_Operate"));
        gridLayout_Operate->setHorizontalSpacing(60);
        gridLayout_Operate->setVerticalSpacing(30);
        gridLayout_Operate->setContentsMargins(20, 20, 20, 20);

        verticalLayout_16->addLayout(gridLayout_Operate);


        verticalLayout_15->addWidget(Operate_N);

        widget_4N = new QWidget(MainOperateTab);
        widget_4N->setObjectName(QStringLiteral("widget_4N"));
        horizontalLayout_20 = new QHBoxLayout(widget_4N);
        horizontalLayout_20->setSpacing(0);
        horizontalLayout_20->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        horizontalLayout_20->setContentsMargins(0, 0, 0, 0);
        label_2N = new QLabel(widget_4N);
        label_2N->setObjectName(QStringLiteral("label_2N"));

        horizontalLayout_20->addWidget(label_2N);

        widget_6N = new QWidget(widget_4N);
        widget_6N->setObjectName(QStringLiteral("widget_6N"));

        horizontalLayout_20->addWidget(widget_6N);

        horizontalLayout_20->setStretch(0, 3);
        horizontalLayout_20->setStretch(1, 4);

        verticalLayout_15->addWidget(widget_4N);

        Manager_N = new QGroupBox(MainOperateTab);
        Manager_N->setObjectName(QStringLiteral("Manager_N"));
        verticalLayout_17 = new QVBoxLayout(Manager_N);
        verticalLayout_17->setSpacing(6);
        verticalLayout_17->setContentsMargins(11, 11, 11, 11);
        verticalLayout_17->setObjectName(QStringLiteral("verticalLayout_17"));
        verticalLayout_17->setContentsMargins(0, 0, 0, 0);
        gridLayout_Manager = new QGridLayout();
        gridLayout_Manager->setSpacing(6);
        gridLayout_Manager->setObjectName(QStringLiteral("gridLayout_Manager"));
        gridLayout_Manager->setHorizontalSpacing(60);
        gridLayout_Manager->setVerticalSpacing(30);
        gridLayout_Manager->setContentsMargins(20, 20, 20, 20);

        verticalLayout_17->addLayout(gridLayout_Manager);


        verticalLayout_15->addWidget(Manager_N);

        verticalLayout_15->setStretch(0, 1);
        verticalLayout_15->setStretch(1, 7);
        verticalLayout_15->setStretch(2, 1);
        verticalLayout_15->setStretch(3, 7);
        MainTabWidget->addTab(MainOperateTab, QString());
        MainPerShowTab = new QWidget();
        MainPerShowTab->setObjectName(QStringLiteral("MainPerShowTab"));
        horizontalLayout_21 = new QHBoxLayout(MainPerShowTab);
        horizontalLayout_21->setSpacing(10);
        horizontalLayout_21->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        horizontalLayout_21->setContentsMargins(10, 10, 10, 10);
        gridWidget = new QWidget(MainPerShowTab);
        gridWidget->setObjectName(QStringLiteral("gridWidget"));
        gridWidget->setAutoFillBackground(true);
        gridLayout_2 = new QGridLayout(gridWidget);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        scrollArea = new QScrollArea(gridWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setAutoFillBackground(true);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidget = new QWidget();
        scrollAreaWidget->setObjectName(QStringLiteral("scrollAreaWidget"));
        scrollAreaWidget->setGeometry(QRect(0, 0, 816, 634));
        gridLayout = new QGridLayout(scrollAreaWidget);
        gridLayout->setSpacing(5);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        scrollArea->setWidget(scrollAreaWidget);

        gridLayout_2->addWidget(scrollArea, 0, 0, 1, 1);


        horizontalLayout_21->addWidget(gridWidget);

        resultshowBox_1 = new QGroupBox(MainPerShowTab);
        resultshowBox_1->setObjectName(QStringLiteral("resultshowBox_1"));
        resultshowBox_1->setMinimumSize(QSize(170, 0));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        resultshowBox_1->setFont(font1);
        resultshowBox_1->setLayoutDirection(Qt::LeftToRight);
        resultshowBox_1->setAlignment(Qt::AlignCenter);
        resultshowBox_1->setFlat(true);
        verticalLayout_18 = new QVBoxLayout(resultshowBox_1);
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setContentsMargins(11, 11, 11, 11);
        verticalLayout_18->setObjectName(QStringLiteral("verticalLayout_18"));
        verticalLayout_18->setContentsMargins(0, 5, 0, 0);
        ResultText = new QTextEdit(resultshowBox_1);
        ResultText->setObjectName(QStringLiteral("ResultText"));
        ResultText->setAutoFillBackground(true);
        ResultText->setStyleSheet(QStringLiteral(""));
        ResultText->setReadOnly(true);

        verticalLayout_18->addWidget(ResultText);


        horizontalLayout_21->addWidget(resultshowBox_1);

        horizontalLayout_21->setStretch(0, 8);
        horizontalLayout_21->setStretch(1, 1);
        MainTabWidget->addTab(MainPerShowTab, QString());
        MainStitchShowTab = new QWidget();
        MainStitchShowTab->setObjectName(QStringLiteral("MainStitchShowTab"));
        MainStitchShowTab->setAutoFillBackground(true);
        horizontalLayout_22 = new QHBoxLayout(MainStitchShowTab);
        horizontalLayout_22->setSpacing(10);
        horizontalLayout_22->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        horizontalLayout_22->setContentsMargins(10, 10, 10, 10);
        StitchShow = new QWidget(MainStitchShowTab);
        StitchShow->setObjectName(QStringLiteral("StitchShow"));
        StitchShow->setAutoFillBackground(true);
        StitchShow->setStyleSheet(QStringLiteral(""));
        verticalLayout_20 = new QVBoxLayout(StitchShow);
        verticalLayout_20->setSpacing(0);
        verticalLayout_20->setContentsMargins(11, 11, 11, 11);
        verticalLayout_20->setObjectName(QStringLiteral("verticalLayout_20"));
        verticalLayout_20->setContentsMargins(0, 0, 0, 0);
        StitchShowLabel = new QLabel(StitchShow);
        StitchShowLabel->setObjectName(QStringLiteral("StitchShowLabel"));
        StitchShowLabel->setAutoFillBackground(false);
        StitchShowLabel->setStyleSheet(QStringLiteral(""));

        verticalLayout_20->addWidget(StitchShowLabel);


        horizontalLayout_22->addWidget(StitchShow);

        resultshowBox_2 = new QGroupBox(MainStitchShowTab);
        resultshowBox_2->setObjectName(QStringLiteral("resultshowBox_2"));
        resultshowBox_2->setMinimumSize(QSize(170, 0));
        resultshowBox_2->setFont(font1);
        resultshowBox_2->setAlignment(Qt::AlignCenter);
        resultshowBox_2->setFlat(true);
        resultshowBox_2->setCheckable(false);
        verticalLayout_19 = new QVBoxLayout(resultshowBox_2);
        verticalLayout_19->setSpacing(0);
        verticalLayout_19->setContentsMargins(11, 11, 11, 11);
        verticalLayout_19->setObjectName(QStringLiteral("verticalLayout_19"));
        verticalLayout_19->setContentsMargins(0, 5, 0, 0);
        resultshow = new QTextEdit(resultshowBox_2);
        resultshow->setObjectName(QStringLiteral("resultshow"));
        resultshow->setAutoFillBackground(true);
        resultshow->setStyleSheet(QStringLiteral(""));
        resultshow->setReadOnly(true);

        verticalLayout_19->addWidget(resultshow);

        stitchBtn = new QPushButton(resultshowBox_2);
        stitchBtn->setObjectName(QStringLiteral("stitchBtn"));

        verticalLayout_19->addWidget(stitchBtn);

        exitBtn = new QPushButton(resultshowBox_2);
        exitBtn->setObjectName(QStringLiteral("exitBtn"));

        verticalLayout_19->addWidget(exitBtn);


        horizontalLayout_22->addWidget(resultshowBox_2);

        horizontalLayout_22->setStretch(0, 8);
        horizontalLayout_22->setStretch(1, 1);
        MainTabWidget->addTab(MainStitchShowTab, QString());

        verticalLayout_14->addWidget(MainTabWidget);

        bottom = new QWidget(MainCentralWidget);
        bottom->setObjectName(QStringLiteral("bottom"));
        bottom->setAutoFillBackground(true);
        horizontalLayout = new QHBoxLayout(bottom);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));

        verticalLayout_14->addWidget(bottom);

        verticalLayout_14->setStretch(0, 22);
        verticalLayout_14->setStretch(1, 1);

        verticalLayout_2->addWidget(MainCentralWidget);

        verticalLayout_2->setStretch(0, 4);
        verticalLayout_2->setStretch(1, 100);

        verticalLayout->addWidget(widget);


        retranslateUi(MainWidget);

        MainTabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWidget);
    } // setupUi

    void retranslateUi(QWidget *MainWidget)
    {
        MainWidget->setWindowTitle(QApplication::translate("MainWidget", "MainWidget", 0));
        title_N->setText(QApplication::translate("MainWidget", "         \344\270\255\345\273\272\344\270\211\345\261\200\346\231\272\350\203\275\351\253\230\347\251\272\346\212\233\347\211\251\347\233\221\346\216\247\347\263\273\347\273\237", 0));
        minbtn_N->setText(QString());
        maxbtn_N->setText(QString());
        closebtn_N->setText(QString());
        label_N->setText(QApplication::translate("MainWidget", "  \346\223\215\344\275\234\344\270\216\346\216\247\345\210\266", 0));
        Operate_N->setTitle(QString());
        label_2N->setText(QApplication::translate("MainWidget", "  \347\273\264\346\212\244\344\270\216\347\256\241\347\220\206", 0));
        Manager_N->setTitle(QString());
        MainTabWidget->setTabText(MainTabWidget->indexOf(MainOperateTab), QApplication::translate("MainWidget", "\346\216\247\345\210\266\351\235\242\346\235\277", 0));
        resultshowBox_1->setTitle(QApplication::translate("MainWidget", "\346\243\200\346\265\213\347\273\223\346\236\234", 0));
        MainTabWidget->setTabText(MainTabWidget->indexOf(MainPerShowTab), QApplication::translate("MainWidget", "\345\215\225\344\270\252\346\230\276\347\244\272", 0));
        StitchShowLabel->setText(QString());
        resultshowBox_2->setTitle(QApplication::translate("MainWidget", "\346\243\200\346\265\213\347\273\223\346\236\234", 0));
        stitchBtn->setText(QApplication::translate("MainWidget", "\346\213\274\346\216\245\346\265\213\350\257\225", 0));
        exitBtn->setText(QApplication::translate("MainWidget", "\351\200\200\345\207\272", 0));
        MainTabWidget->setTabText(MainTabWidget->indexOf(MainStitchShowTab), QApplication::translate("MainWidget", "\346\213\274\346\216\245\346\230\276\347\244\272", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWidget: public Ui_MainWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWIDGET_H
