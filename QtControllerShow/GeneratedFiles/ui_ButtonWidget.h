/********************************************************************************
** Form generated from reading UI file 'ButtonWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BUTTONWIDGET_H
#define UI_BUTTONWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>
#include <qclickedlabel.h>

QT_BEGIN_NAMESPACE

class Ui_ButtonWidget
{
public:
    QGridLayout *gridLayout;
    QClickedLabel *icon;
    QClickedLabel *TitleLabel;
    QClickedLabel *TextLabel;

    void setupUi(QWidget *ButtonWidget)
    {
        if (ButtonWidget->objectName().isEmpty())
            ButtonWidget->setObjectName(QStringLiteral("ButtonWidget"));
        ButtonWidget->resize(280, 120);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ButtonWidget->sizePolicy().hasHeightForWidth());
        ButtonWidget->setSizePolicy(sizePolicy);
        ButtonWidget->setMinimumSize(QSize(0, 0));
        ButtonWidget->setMaximumSize(QSize(16777215, 160));
        ButtonWidget->setAutoFillBackground(false);
        ButtonWidget->setStyleSheet(QStringLiteral("background: rgba(35, 35, 35, 0);"));
        gridLayout = new QGridLayout(ButtonWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setHorizontalSpacing(15);
        gridLayout->setVerticalSpacing(10);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        icon = new QClickedLabel(ButtonWidget);
        icon->setObjectName(QStringLiteral("icon"));
        icon->setMinimumSize(QSize(80, 96));
        icon->setMaximumSize(QSize(80, 96));
        icon->setLayoutDirection(Qt::LeftToRight);
        icon->setAutoFillBackground(false);
        icon->setStyleSheet(QStringLiteral(""));
        icon->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(icon, 0, 0, 2, 1);

        TitleLabel = new QClickedLabel(ButtonWidget);
        TitleLabel->setObjectName(QStringLiteral("TitleLabel"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        TitleLabel->setFont(font);
        TitleLabel->setAutoFillBackground(false);
        TitleLabel->setStyleSheet(QStringLiteral(""));
        TitleLabel->setLineWidth(0);
        TitleLabel->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        gridLayout->addWidget(TitleLabel, 0, 1, 1, 1);

        TextLabel = new QClickedLabel(ButtonWidget);
        TextLabel->setObjectName(QStringLiteral("TextLabel"));
        QFont font1;
        font1.setPointSize(10);
        TextLabel->setFont(font1);
        TextLabel->setAutoFillBackground(false);
        TextLabel->setStyleSheet(QStringLiteral(""));
        TextLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(TextLabel, 1, 1, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setRowStretch(1, 2);
        gridLayout->setColumnStretch(0, 1);
        gridLayout->setColumnStretch(1, 2);

        retranslateUi(ButtonWidget);

        QMetaObject::connectSlotsByName(ButtonWidget);
    } // setupUi

    void retranslateUi(QWidget *ButtonWidget)
    {
        ButtonWidget->setWindowTitle(QApplication::translate("ButtonWidget", "ButtonWidget", 0));
        icon->setText(QApplication::translate("ButtonWidget", "icon", 0));
        TitleLabel->setText(QApplication::translate("ButtonWidget", "TextLabel", 0));
        TextLabel->setText(QApplication::translate("ButtonWidget", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class ButtonWidget: public Ui_ButtonWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BUTTONWIDGET_H
