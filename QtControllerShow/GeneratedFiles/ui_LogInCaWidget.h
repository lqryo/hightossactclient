/********************************************************************************
** Form generated from reading UI file 'LogInCaWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINCAWIDGET_H
#define UI_LOGINCAWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LogInCaWidget
{
public:
    QGridLayout *gridLayout;
    QLabel *label_3;
    QPushButton *ConfirmBtn;
    QLineEdit *PWText;
    QPushButton *CancelBtn;
    QLineEdit *AccountText;
    QLineEdit *IPText;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_4;
    QLineEdit *CamIDText;

    void setupUi(QWidget *LogInCaWidget)
    {
        if (LogInCaWidget->objectName().isEmpty())
            LogInCaWidget->setObjectName(QStringLiteral("LogInCaWidget"));
        LogInCaWidget->resize(418, 372);
        gridLayout = new QGridLayout(LogInCaWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setHorizontalSpacing(5);
        gridLayout->setVerticalSpacing(30);
        gridLayout->setContentsMargins(30, 30, 50, 20);
        label_3 = new QLabel(LogInCaWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        ConfirmBtn = new QPushButton(LogInCaWidget);
        ConfirmBtn->setObjectName(QStringLiteral("ConfirmBtn"));

        gridLayout->addWidget(ConfirmBtn, 4, 1, 1, 1);

        PWText = new QLineEdit(LogInCaWidget);
        PWText->setObjectName(QStringLiteral("PWText"));
        PWText->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(PWText, 2, 1, 1, 2);

        CancelBtn = new QPushButton(LogInCaWidget);
        CancelBtn->setObjectName(QStringLiteral("CancelBtn"));

        gridLayout->addWidget(CancelBtn, 4, 2, 1, 1);

        AccountText = new QLineEdit(LogInCaWidget);
        AccountText->setObjectName(QStringLiteral("AccountText"));

        gridLayout->addWidget(AccountText, 1, 1, 1, 2);

        IPText = new QLineEdit(LogInCaWidget);
        IPText->setObjectName(QStringLiteral("IPText"));

        gridLayout->addWidget(IPText, 0, 1, 1, 2);

        label_2 = new QLabel(LogInCaWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label = new QLabel(LogInCaWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font);
        label->setLayoutDirection(Qt::LeftToRight);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_4 = new QLabel(LogInCaWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font);
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        CamIDText = new QLineEdit(LogInCaWidget);
        CamIDText->setObjectName(QStringLiteral("CamIDText"));

        gridLayout->addWidget(CamIDText, 3, 1, 1, 2);


        retranslateUi(LogInCaWidget);

        QMetaObject::connectSlotsByName(LogInCaWidget);
    } // setupUi

    void retranslateUi(QWidget *LogInCaWidget)
    {
        LogInCaWidget->setWindowTitle(QApplication::translate("LogInCaWidget", "\347\233\270\346\234\272\347\231\273\345\275\225", 0));
        label_3->setText(QApplication::translate("LogInCaWidget", "\345\257\206\347\240\201\357\274\232", 0));
        ConfirmBtn->setText(QApplication::translate("LogInCaWidget", "\347\241\256\345\256\232", 0));
        PWText->setText(QApplication::translate("LogInCaWidget", "IDM8779235101", 0));
        CancelBtn->setText(QApplication::translate("LogInCaWidget", "\345\217\226\346\266\210", 0));
        AccountText->setText(QApplication::translate("LogInCaWidget", "admin", 0));
        IPText->setText(QApplication::translate("LogInCaWidget", "192.168.1.65", 0));
        label_2->setText(QApplication::translate("LogInCaWidget", "\350\264\246\345\217\267\357\274\232", 0));
        label->setText(QApplication::translate("LogInCaWidget", "IP\345\234\260\345\235\200\357\274\232", 0));
        label_4->setText(QApplication::translate("LogInCaWidget", "\347\233\270\346\234\272ID\357\274\232", 0));
        CamIDText->setText(QApplication::translate("LogInCaWidget", "00", 0));
    } // retranslateUi

};

namespace Ui {
    class LogInCaWidget: public Ui_LogInCaWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINCAWIDGET_H
