/********************************************************************************
** Form generated from reading UI file 'SetParam.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETPARAM_H
#define UI_SETPARAM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SetParam
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *system_title_widget;
    QHBoxLayout *horizontalLayout;
    QWidget *system_title_widget_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *SetParamTitle;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *system_close_button;
    QWidget *system_widget_2;
    QVBoxLayout *verticalLayout_2;
    QWidget *system_widget_3;
    QVBoxLayout *verticalLayout_4;
    QTabWidget *system_tabwidget;
    QWidget *system_tab;
    QPushButton *system_save;
    QWidget *widget;
    QFormLayout *formLayout;
    QLabel *system_label;
    QLineEdit *FloorNum;
    QLabel *label;
    QLineEdit *PerFloor;
    QLabel *label_2;
    QLineEdit *Distance;
    QLabel *label_3;
    QLineEdit *CaHeight;
    QLabel *label_7;
    QLineEdit *startFloor;
    QLabel *label_6;
    QLineEdit *pFloors;
    QLineEdit *CameraR;
    QLabel *label_4;
    QLabel *label_5;
    QLineEdit *CameraC;
    QCheckBox *stitchCheck;
    QWidget *system_tab_2;

    void setupUi(QWidget *SetParam)
    {
        if (SetParam->objectName().isEmpty())
            SetParam->setObjectName(QStringLiteral("SetParam"));
        SetParam->resize(1024, 768);
        QFont font;
        font.setPointSize(8);
        SetParam->setFont(font);
        verticalLayout = new QVBoxLayout(SetParam);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        system_title_widget = new QWidget(SetParam);
        system_title_widget->setObjectName(QStringLiteral("system_title_widget"));
        horizontalLayout = new QHBoxLayout(system_title_widget);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        system_title_widget_2 = new QWidget(system_title_widget);
        system_title_widget_2->setObjectName(QStringLiteral("system_title_widget_2"));
        verticalLayout_3 = new QVBoxLayout(system_title_widget_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        SetParamTitle = new QLabel(system_title_widget_2);
        SetParamTitle->setObjectName(QStringLiteral("SetParamTitle"));

        verticalLayout_3->addWidget(SetParamTitle, 0, Qt::AlignHCenter);


        horizontalLayout->addWidget(system_title_widget_2);

        widget_3 = new QWidget(system_title_widget);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        horizontalLayout_2 = new QHBoxLayout(widget_3);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(3, 3, 3, 3);
        system_close_button = new QPushButton(widget_3);
        system_close_button->setObjectName(QStringLiteral("system_close_button"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(system_close_button->sizePolicy().hasHeightForWidth());
        system_close_button->setSizePolicy(sizePolicy);
        system_close_button->setMinimumSize(QSize(35, 35));
        system_close_button->setMaximumSize(QSize(35, 35));

        horizontalLayout_2->addWidget(system_close_button);


        horizontalLayout->addWidget(widget_3);

        horizontalLayout->setStretch(0, 19);
        horizontalLayout->setStretch(1, 1);

        verticalLayout->addWidget(system_title_widget);

        system_widget_2 = new QWidget(SetParam);
        system_widget_2->setObjectName(QStringLiteral("system_widget_2"));
        verticalLayout_2 = new QVBoxLayout(system_widget_2);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(55, 55, 55, 55);
        system_widget_3 = new QWidget(system_widget_2);
        system_widget_3->setObjectName(QStringLiteral("system_widget_3"));
        verticalLayout_4 = new QVBoxLayout(system_widget_3);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        system_tabwidget = new QTabWidget(system_widget_3);
        system_tabwidget->setObjectName(QStringLiteral("system_tabwidget"));
        system_tab = new QWidget();
        system_tab->setObjectName(QStringLiteral("system_tab"));
        system_save = new QPushButton(system_tab);
        system_save->setObjectName(QStringLiteral("system_save"));
        system_save->setGeometry(QRect(570, 450, 93, 28));
        widget = new QWidget(system_tab);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(230, 20, 331, 411));
        formLayout = new QFormLayout(widget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setHorizontalSpacing(10);
        formLayout->setVerticalSpacing(30);
        formLayout->setContentsMargins(0, 0, 0, 0);
        system_label = new QLabel(widget);
        system_label->setObjectName(QStringLiteral("system_label"));
        system_label->setMinimumSize(QSize(150, 32));
        system_label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(0, QFormLayout::LabelRole, system_label);

        FloorNum = new QLineEdit(widget);
        FloorNum->setObjectName(QStringLiteral("FloorNum"));
        FloorNum->setMinimumSize(QSize(0, 32));
        FloorNum->setMaximumSize(QSize(16777215, 32));
        FloorNum->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(0, QFormLayout::FieldRole, FloorNum);

        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(150, 32));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        PerFloor = new QLineEdit(widget);
        PerFloor->setObjectName(QStringLiteral("PerFloor"));
        PerFloor->setMinimumSize(QSize(0, 32));
        PerFloor->setMaximumSize(QSize(16777215, 32));
        PerFloor->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(1, QFormLayout::FieldRole, PerFloor);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(150, 32));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        Distance = new QLineEdit(widget);
        Distance->setObjectName(QStringLiteral("Distance"));
        Distance->setMinimumSize(QSize(0, 32));
        Distance->setMaximumSize(QSize(16777215, 32));
        Distance->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(2, QFormLayout::FieldRole, Distance);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(150, 32));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        CaHeight = new QLineEdit(widget);
        CaHeight->setObjectName(QStringLiteral("CaHeight"));
        CaHeight->setMinimumSize(QSize(0, 32));
        CaHeight->setMaximumSize(QSize(16777215, 32));
        CaHeight->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(3, QFormLayout::FieldRole, CaHeight);

        label_7 = new QLabel(widget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMinimumSize(QSize(150, 32));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(4, QFormLayout::LabelRole, label_7);

        startFloor = new QLineEdit(widget);
        startFloor->setObjectName(QStringLiteral("startFloor"));
        startFloor->setMinimumSize(QSize(0, 32));
        startFloor->setMaximumSize(QSize(16777215, 33));
        startFloor->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(4, QFormLayout::FieldRole, startFloor);

        label_6 = new QLabel(widget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMinimumSize(QSize(150, 32));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        pFloors = new QLineEdit(widget);
        pFloors->setObjectName(QStringLiteral("pFloors"));
        pFloors->setMinimumSize(QSize(0, 32));
        pFloors->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(5, QFormLayout::FieldRole, pFloors);

        CameraR = new QLineEdit(widget);
        CameraR->setObjectName(QStringLiteral("CameraR"));
        CameraR->setMinimumSize(QSize(0, 32));
        CameraR->setMaximumSize(QSize(16777215, 32));
        CameraR->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(6, QFormLayout::FieldRole, CameraR);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(150, 32));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(6, QFormLayout::LabelRole, label_4);

        label_5 = new QLabel(widget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMinimumSize(QSize(150, 32));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(7, QFormLayout::LabelRole, label_5);

        CameraC = new QLineEdit(widget);
        CameraC->setObjectName(QStringLiteral("CameraC"));
        CameraC->setMinimumSize(QSize(0, 32));
        CameraC->setMaximumSize(QSize(16777215, 32));
        CameraC->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(7, QFormLayout::FieldRole, CameraC);

        stitchCheck = new QCheckBox(widget);
        stitchCheck->setObjectName(QStringLiteral("stitchCheck"));
        stitchCheck->setLayoutDirection(Qt::LeftToRight);
        stitchCheck->setIconSize(QSize(30, 30));

        formLayout->setWidget(8, QFormLayout::FieldRole, stitchCheck);

        system_tabwidget->addTab(system_tab, QString());
        system_tab_2 = new QWidget();
        system_tab_2->setObjectName(QStringLiteral("system_tab_2"));
        system_tabwidget->addTab(system_tab_2, QString());

        verticalLayout_4->addWidget(system_tabwidget);


        verticalLayout_2->addWidget(system_widget_3);


        verticalLayout->addWidget(system_widget_2);

        verticalLayout->setStretch(0, 1);
        verticalLayout->setStretch(1, 16);

        retranslateUi(SetParam);

        system_tabwidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SetParam);
    } // setupUi

    void retranslateUi(QWidget *SetParam)
    {
        SetParam->setWindowTitle(QApplication::translate("SetParam", "\345\217\202\346\225\260\350\256\276\347\275\256", 0));
        SetParamTitle->setText(QApplication::translate("SetParam", "\347\263\273\347\273\237\350\256\276\347\275\256", 0));
        system_close_button->setText(QString());
        system_save->setText(QApplication::translate("SetParam", "\344\277\235\345\255\230", 0));
        system_label->setText(QApplication::translate("SetParam", "\346\245\274\345\261\202\346\225\260\357\274\232", 0));
        label->setText(QApplication::translate("SetParam", "\346\245\274\351\253\230/\345\261\202\357\274\232", 0));
        label_2->setText(QApplication::translate("SetParam", "\345\256\211\350\243\205\350\267\235\347\246\273\357\274\232", 0));
        label_3->setText(QApplication::translate("SetParam", "\345\256\211\350\243\205\351\253\230\345\272\246\357\274\232", 0));
        label_7->setText(QApplication::translate("SetParam", "\345\274\200\345\247\213\347\233\221\346\216\247\346\245\274\345\261\202\357\274\232", 0));
        label_6->setText(QApplication::translate("SetParam", "\344\270\200\344\270\252\347\233\270\346\234\272\347\233\221\346\216\247\346\245\274\345\261\202\346\225\260\357\274\232", 0));
        label_4->setText(QApplication::translate("SetParam", "\347\233\270\346\234\272\344\270\252\346\225\260(\345\236\202\347\233\264)\357\274\232", 0));
        label_5->setText(QApplication::translate("SetParam", "\347\233\270\346\234\272\344\270\252\346\225\260(\346\260\264\345\271\263)\357\274\232", 0));
        stitchCheck->setText(QApplication::translate("SetParam", "\346\213\274\346\216\245", 0));
        system_tabwidget->setTabText(system_tabwidget->indexOf(system_tab), QString());
        system_tabwidget->setTabText(system_tabwidget->indexOf(system_tab_2), QString());
    } // retranslateUi

};

namespace Ui {
    class SetParam: public Ui_SetParam {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETPARAM_H
