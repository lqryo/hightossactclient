/********************************************************************************
** Form generated from reading UI file 'qtcontrollershow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTCONTROLLERSHOW_H
#define UI_QTCONTROLLERSHOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QtControllerShowClass
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_2;
    QTabWidget *tabWidget;
    QWidget *OperateTab;
    QVBoxLayout *verticalLayout;
    QGroupBox *Operate;
    QGridLayout *gridLayout_Operate;
    QGroupBox *Manager;
    QGridLayout *gridLayout_Manager;
    QWidget *ShowFrame;
    QHBoxLayout *horizontalLayout_5;
    QWidget *gridWidget;
    QGridLayout *gridLayout_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidget;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_4;
    QPushButton *testshowbtn;
    QTextEdit *ResultText;
    QWidget *showTab;
    QHBoxLayout *horizontalLayout;
    QWidget *OpWidget;
    QVBoxLayout *verticalLayout_3;
    QPushButton *startBtn;
    QPushButton *stopBtn;
    QPushButton *restartBtn;
    QPushButton *exitBtn;
    QLabel *label_2;
    QLabel *showWidget;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton;
    QTextEdit *ResultText_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QtControllerShowClass)
    {
        if (QtControllerShowClass->objectName().isEmpty())
            QtControllerShowClass->setObjectName(QStringLiteral("QtControllerShowClass"));
        QtControllerShowClass->resize(991, 672);
        centralWidget = new QWidget(QtControllerShowClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_2 = new QHBoxLayout(centralWidget);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QFont font;
        font.setPointSize(9);
        font.setBold(false);
        font.setWeight(50);
        tabWidget->setFont(font);
        tabWidget->setStyleSheet(QStringLiteral("QTabBar::tab{height:35}"));
        tabWidget->setIconSize(QSize(20, 20));
        OperateTab = new QWidget();
        OperateTab->setObjectName(QStringLiteral("OperateTab"));
        verticalLayout = new QVBoxLayout(OperateTab);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        Operate = new QGroupBox(OperateTab);
        Operate->setObjectName(QStringLiteral("Operate"));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        Operate->setFont(font1);
        Operate->setTitle(QString::fromUtf8("\346\223\215\344\275\234\344\270\216\346\216\247\345\210\266"));
        gridLayout_Operate = new QGridLayout(Operate);
        gridLayout_Operate->setSpacing(6);
        gridLayout_Operate->setContentsMargins(11, 11, 11, 11);
        gridLayout_Operate->setObjectName(QStringLiteral("gridLayout_Operate"));
        gridLayout_Operate->setHorizontalSpacing(50);
        gridLayout_Operate->setVerticalSpacing(30);
        gridLayout_Operate->setContentsMargins(25, 15, 25, 15);

        verticalLayout->addWidget(Operate);

        Manager = new QGroupBox(OperateTab);
        Manager->setObjectName(QStringLiteral("Manager"));
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Manager->sizePolicy().hasHeightForWidth());
        Manager->setSizePolicy(sizePolicy);
        Manager->setFont(font1);
        Manager->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        gridLayout_Manager = new QGridLayout(Manager);
        gridLayout_Manager->setSpacing(6);
        gridLayout_Manager->setContentsMargins(11, 11, 11, 11);
        gridLayout_Manager->setObjectName(QStringLiteral("gridLayout_Manager"));
        gridLayout_Manager->setHorizontalSpacing(50);
        gridLayout_Manager->setVerticalSpacing(30);
        gridLayout_Manager->setContentsMargins(25, 15, 25, 15);

        verticalLayout->addWidget(Manager);

        verticalLayout->setStretch(0, 1);
        verticalLayout->setStretch(1, 1);
        tabWidget->addTab(OperateTab, QString());
        ShowFrame = new QWidget();
        ShowFrame->setObjectName(QStringLiteral("ShowFrame"));
        horizontalLayout_5 = new QHBoxLayout(ShowFrame);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        gridWidget = new QWidget(ShowFrame);
        gridWidget->setObjectName(QStringLiteral("gridWidget"));
        gridLayout_2 = new QGridLayout(gridWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(2, 0, 0, 0);
        scrollArea = new QScrollArea(gridWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidget = new QWidget();
        scrollAreaWidget->setObjectName(QStringLiteral("scrollAreaWidget"));
        scrollAreaWidget->setGeometry(QRect(0, 0, 846, 543));
        gridLayout = new QGridLayout(scrollAreaWidget);
        gridLayout->setSpacing(5);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(5, 5, 5, 5);
        scrollArea->setWidget(scrollAreaWidget);

        gridLayout_2->addWidget(scrollArea, 0, 0, 1, 1);


        horizontalLayout_5->addWidget(gridWidget);

        groupBox_2 = new QGroupBox(ShowFrame);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_4 = new QVBoxLayout(groupBox_2);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(2, 2, 2, -1);
        testshowbtn = new QPushButton(groupBox_2);
        testshowbtn->setObjectName(QStringLiteral("testshowbtn"));

        verticalLayout_4->addWidget(testshowbtn);

        ResultText = new QTextEdit(groupBox_2);
        ResultText->setObjectName(QStringLiteral("ResultText"));

        verticalLayout_4->addWidget(ResultText);


        horizontalLayout_5->addWidget(groupBox_2);

        horizontalLayout_5->setStretch(0, 8);
        horizontalLayout_5->setStretch(1, 1);
        tabWidget->addTab(ShowFrame, QString());
        showTab = new QWidget();
        showTab->setObjectName(QStringLiteral("showTab"));
        horizontalLayout = new QHBoxLayout(showTab);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        OpWidget = new QWidget(showTab);
        OpWidget->setObjectName(QStringLiteral("OpWidget"));
        QFont font2;
        font2.setBold(false);
        font2.setWeight(50);
        OpWidget->setFont(font2);
        verticalLayout_3 = new QVBoxLayout(OpWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        startBtn = new QPushButton(OpWidget);
        startBtn->setObjectName(QStringLiteral("startBtn"));

        verticalLayout_3->addWidget(startBtn);

        stopBtn = new QPushButton(OpWidget);
        stopBtn->setObjectName(QStringLiteral("stopBtn"));

        verticalLayout_3->addWidget(stopBtn);

        restartBtn = new QPushButton(OpWidget);
        restartBtn->setObjectName(QStringLiteral("restartBtn"));

        verticalLayout_3->addWidget(restartBtn);

        exitBtn = new QPushButton(OpWidget);
        exitBtn->setObjectName(QStringLiteral("exitBtn"));

        verticalLayout_3->addWidget(exitBtn);

        label_2 = new QLabel(OpWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_3->addWidget(label_2);


        horizontalLayout->addWidget(OpWidget);

        showWidget = new QLabel(showTab);
        showWidget->setObjectName(QStringLiteral("showWidget"));

        horizontalLayout->addWidget(showWidget);

        widget = new QWidget(showTab);
        widget->setObjectName(QStringLiteral("widget"));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout_2->addWidget(pushButton);

        ResultText_2 = new QTextEdit(widget);
        ResultText_2->setObjectName(QStringLiteral("ResultText_2"));
        ResultText_2->setCursorWidth(1);

        verticalLayout_2->addWidget(ResultText_2);


        horizontalLayout->addWidget(widget);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 10);
        horizontalLayout->setStretch(2, 1);
        tabWidget->addTab(showTab, QString());

        horizontalLayout_2->addWidget(tabWidget);

        QtControllerShowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QtControllerShowClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 991, 26));
        QtControllerShowClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(QtControllerShowClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        QtControllerShowClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(QtControllerShowClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        QtControllerShowClass->setStatusBar(statusBar);

        retranslateUi(QtControllerShowClass);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(QtControllerShowClass);
    } // setupUi

    void retranslateUi(QMainWindow *QtControllerShowClass)
    {
        QtControllerShowClass->setWindowTitle(QApplication::translate("QtControllerShowClass", "\344\270\255\345\273\272\344\270\211\345\261\200\346\231\272\350\203\275\351\253\230\347\251\272\346\212\233\347\211\251\347\233\221\346\216\247\347\263\273\347\273\237", 0));
        Manager->setTitle(QApplication::translate("QtControllerShowClass", "\347\273\264\346\212\244\344\270\216\347\256\241\347\220\206", 0));
        tabWidget->setTabText(tabWidget->indexOf(OperateTab), QApplication::translate("QtControllerShowClass", "\346\216\247\345\210\266\351\235\242\346\235\277", 0));
        groupBox_2->setTitle(QApplication::translate("QtControllerShowClass", "GroupBox", 0));
        testshowbtn->setText(QApplication::translate("QtControllerShowClass", "PushButton", 0));
        tabWidget->setTabText(tabWidget->indexOf(ShowFrame), QApplication::translate("QtControllerShowClass", "\346\230\276\347\244\272(\347\275\221\346\240\274\345\270\203\345\261\200)", 0));
        startBtn->setText(QApplication::translate("QtControllerShowClass", "\345\274\200\345\247\213", 0));
        stopBtn->setText(QApplication::translate("QtControllerShowClass", "\346\230\276\347\244\272\346\265\213\350\257\225", 0));
        restartBtn->setText(QApplication::translate("QtControllerShowClass", "\346\213\274\346\216\245\346\265\213\350\257\225", 0));
        exitBtn->setText(QApplication::translate("QtControllerShowClass", "\351\200\200\345\207\272", 0));
        label_2->setText(QApplication::translate("QtControllerShowClass", "TextLabel", 0));
        showWidget->setText(QString());
        pushButton->setText(QApplication::translate("QtControllerShowClass", "\347\273\223\346\236\234\346\230\276\347\244\272", 0));
        tabWidget->setTabText(tabWidget->indexOf(showTab), QApplication::translate("QtControllerShowClass", "\346\265\217\350\247\210\346\230\276\347\244\272", 0));
    } // retranslateUi

};

namespace Ui {
    class QtControllerShowClass: public Ui_QtControllerShowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTCONTROLLERSHOW_H
