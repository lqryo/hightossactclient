/********************************************************************************
** Form generated from reading UI file 'PlaybackWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAYBACKWIDGET_H
#define UI_PLAYBACKWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PlaybackWidget
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *Playbackwidget;
    QHBoxLayout *horizontalLayout;
    QWidget *video_widget;
    QVBoxLayout *verticalLayout_2;
    QWidget *video_widget_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *PlaybackVideo_label;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_4;
    QListWidget *PlaybacklistWidget;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout_5;
    QLabel *PlaybackTitle;
    QLabel *PlaybackLabel;

    void setupUi(QWidget *PlaybackWidget)
    {
        if (PlaybackWidget->objectName().isEmpty())
            PlaybackWidget->setObjectName(QStringLiteral("PlaybackWidget"));
        PlaybackWidget->resize(1024, 768);
        PlaybackWidget->setStyleSheet(QStringLiteral(""));
        verticalLayout = new QVBoxLayout(PlaybackWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        Playbackwidget = new QWidget(PlaybackWidget);
        Playbackwidget->setObjectName(QStringLiteral("Playbackwidget"));
        Playbackwidget->setMinimumSize(QSize(0, 0));
        Playbackwidget->setMaximumSize(QSize(16777215, 16777215));
        horizontalLayout = new QHBoxLayout(Playbackwidget);
        horizontalLayout->setSpacing(20);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(20, 20, 20, 20);
        video_widget = new QWidget(Playbackwidget);
        video_widget->setObjectName(QStringLiteral("video_widget"));
        video_widget->setMinimumSize(QSize(200, 0));
        video_widget->setMaximumSize(QSize(220, 16777215));
        verticalLayout_2 = new QVBoxLayout(video_widget);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        video_widget_2 = new QWidget(video_widget);
        video_widget_2->setObjectName(QStringLiteral("video_widget_2"));
        verticalLayout_3 = new QVBoxLayout(video_widget_2);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        PlaybackVideo_label = new QLabel(video_widget_2);
        PlaybackVideo_label->setObjectName(QStringLiteral("PlaybackVideo_label"));

        verticalLayout_3->addWidget(PlaybackVideo_label, 0, Qt::AlignHCenter);


        verticalLayout_2->addWidget(video_widget_2);

        widget_2 = new QWidget(video_widget);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        verticalLayout_4 = new QVBoxLayout(widget_2);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(2, 0, 2, 2);
        PlaybacklistWidget = new QListWidget(widget_2);
        PlaybacklistWidget->setObjectName(QStringLiteral("PlaybacklistWidget"));

        verticalLayout_4->addWidget(PlaybacklistWidget);


        verticalLayout_2->addWidget(widget_2);

        verticalLayout_2->setStretch(0, 1);
        verticalLayout_2->setStretch(1, 21);

        horizontalLayout->addWidget(video_widget);

        widget_3 = new QWidget(Playbackwidget);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        verticalLayout_5 = new QVBoxLayout(widget_3);
        verticalLayout_5->setSpacing(20);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        PlaybackTitle = new QLabel(widget_3);
        PlaybackTitle->setObjectName(QStringLiteral("PlaybackTitle"));
        PlaybackTitle->setMinimumSize(QSize(0, 40));
        PlaybackTitle->setMaximumSize(QSize(16777215, 40));
        PlaybackTitle->setStyleSheet(QStringLiteral(""));

        verticalLayout_5->addWidget(PlaybackTitle);

        PlaybackLabel = new QLabel(widget_3);
        PlaybackLabel->setObjectName(QStringLiteral("PlaybackLabel"));

        verticalLayout_5->addWidget(PlaybackLabel);

        verticalLayout_5->setStretch(0, 1);
        verticalLayout_5->setStretch(1, 15);

        horizontalLayout->addWidget(widget_3);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 5);

        verticalLayout->addWidget(Playbackwidget);


        retranslateUi(PlaybackWidget);

        QMetaObject::connectSlotsByName(PlaybackWidget);
    } // setupUi

    void retranslateUi(QWidget *PlaybackWidget)
    {
        PlaybackWidget->setWindowTitle(QApplication::translate("PlaybackWidget", "\351\253\230\347\251\272\346\212\233\347\211\251\350\247\206\351\242\221\345\233\236\346\224\276", 0));
        PlaybackVideo_label->setText(QApplication::translate("PlaybackWidget", "\350\247\206\351\242\221\351\200\211\346\213\251", 0));
        PlaybackTitle->setText(QString());
        PlaybackLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class PlaybackWidget: public Ui_PlaybackWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLAYBACKWIDGET_H
