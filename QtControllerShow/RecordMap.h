#pragma once
#include <opencv2\opencv.hpp>
#include "StitchedPic.h"
#include "MaskConstValue.h"

using namespace cv;

class RecordMap
{
public:
	RecordMap();
	~RecordMap();
	//为record函数设置参数
	
	void setValue(const Mat variantMask_, const StitchedPic stitch1_, const vector<Size> imgSizes, const Size resultSize_, 
		vector<int> singleValues, vector<int> overlapValues);
	//设置读取的各个小图
	void setImgMat(const vector<Mat> imgs);
	
	//记录映射图
	Mat Record();
	//进行映射
	Mat mapping(const Mat &alphaMat, const Mat &dst_mask, const Mat &m_mapRecord, const vector<Mat> pics);

	//传入需要使用到的数据
	StitchedPic _stitch1;
	Mat _variantMask;
	Size _resultSize;
	vector<Size> _imgSizes;
	vector<int> _singleValues;
	vector<int> _overlapValues;

	vector<Mat> _imgs;

private://内部计算的得到的数据
	//本类函数计算出的映射图
	Mat _mapRecord;
	Mat _mapRecord_assit;
	//最终的映射结果图
	Mat _mapMat; int _rows; int _cols;
private:
	void internalRecordImg(const Point& p, Vec4s &dstValue, const int tmp_i);
	void internalRecord2imgs(const Point& p, Vec4s &dstValue, const int tmp_i);
	void internalRecord2imgsL_R(const Point& p, Vec4s &dstValue, const int tmpLR_i);
};

