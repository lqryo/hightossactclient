#ifndef SETTINGS_H_
#define SETTINGS_H_

/*
	Comment blow line to cancel debug informations
*/
#define DEBUG

//#ifdef CONFIG_TCP_SK

/*
	Adjust tcp socket buffer size 
*/
#define TCP_SK_BUFFER_SIZE (1024 * 1024 * 1)	// 1M
//#define TCP_SK_BUFFER_SIZE (1024 * 8 * 1)	// 8K (qt default)

//#endif

#endif	// SETTINGS_H_