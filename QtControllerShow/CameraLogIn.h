#pragma once

#include <QWidget>
#include <QButtonGroup>
#include "TCPServer.h"
#include "ui_CameraLogIn.h"
const int CAMERAID = 1;


class CDocument;

typedef struct ClientMsgkey
{
	QString			host;
	quint16			id;
	struct ClientMsgkey()
	{
		this->host = "";
		this->id = 0;

	}
	struct ClientMsgkey(CameraNode cameraNode)
	{
		this->host = cameraNode.host;
		this->id = cameraNode.id;
	}
	bool operator <(const struct ClientMsgkey& other) const
	{
		if (host < other.host)
		{
			return true;
		}
		else if (host == other.host)
		{
			return id < other.id;
		}
		return false;
	}	
}ClientMsgKey;
typedef struct ClientMsgvalue
{
	quint8			status;
	bool			isMaster;
	QTcpSocket		*tcpSocket;
	quint16			port;

	QString			LogIP;
	QString			LogName;
	QString			LogPass;
	int				CameraID;

	struct ClientMsgvalue()
	{
		this->isMaster = false;
		this->port = 0;
		this->tcpSocket = nullptr;
		this->status = 0;
		this->LogIP = "";
		this->LogName = "";
		this->LogPass = "";
		this->CameraID = 0;
	}

	struct ClientMsgvalue(CameraNode cameraNode)
	{
		this->isMaster = cameraNode.isMaster;
		this->port = cameraNode.port;
		this->tcpSocket = cameraNode.tcpSocket;
		this->status = cameraNode.status;
		this->LogIP = "";
		this->LogName = "";
		this->LogPass = "";
		this->CameraID = 0;
	}
	
} ClientMsgValue;

class CameraLogIn : public QWidget
{
	Q_OBJECT

public:
	CDocument * pDoc;
	CameraLogIn(CDocument * pDoc);
	~CameraLogIn();

	public slots:
	/*
		相机登录结果显示
		type:结果的类型
		success:是否成功
	*/
	void LogInRresult(quint8 type, bool success);
	/*
		登录对应主机以及客户端的相机
		row:点击的table的行数
		col: 点击的table的列数
	*/
	void LogInCameraFun(int row, int col);
	/*
		主机客户端显示刷新
	*/
	void ClientShowupdate();
	/*
		向主机中增加客户端
	*/
	void ClientAddFun();
	/*
		客户端增加主机选择的刷新
	*/
	void ClientAddupdate();

	/*
		客户端显示画面刷新
	*/
	void DisplayUpdate00();
	void DisplayUpdate01();
	void DisplayUpdate10();
	void DisplayUpdate11();
	void DisplayUpdate20();
	void DisplayUpdate21();
	void DisplayUpdate30();
	void DisplayUpdate31();
	void DisplayUpdate40();
	void DisplayUpdate41();
	void DisplayUpdate50();
	void DisplayUpdate51();
	void DisplayUpdate60();
	void DisplayUpdate61();

signals:
	void CameraFloorUpdate_Sig(int, int);
	void CameraDisplay_Sig(int, int);

private:
	int LowFloor;
	int HighFloor;
	//相机登录参数
	QString qstr;
	bool cameraRight = true;//标志相机登录信息是否合法
	//QString cameraIP;//相机登录IP
	//QString cameraUsername;//相机登录账号名
	//QString cameraPass;//相机登录密码
	//int cameraID;//相机登录ID
	bool MSK = false;//标志是否进行登录结果的提示
	void connections();
	void ClientShowInit();//主机客户端显示初始化
	void ClientAddInit();//主机增加界面初始化
	QString getCameraIP(bool table, int row=-1);//获得相机登录IP
	QString getCameraUser(bool table, int row=-1);//获得相机登录用户名
	QString getCameraPSW(bool table, int row=-1);//获得相机登录密码
	int getCameraID(bool table, int row=-1);//获得登录相机的ID
	int getCameraLowFloor(bool table, int row = -1);//获取相机拍摄的低楼层
	int getCameraHighFloor(bool table, int row = -1);//获取相机拍着的高楼层
	bool AssertCameraID(QString cameraIDStr);//判断相机ID设置是否合法
	void InsertRow(int row, CameraNode client);//向表格中插入一行
	CameraNode SelectCameraNode(QString host, quint16 id);//查找主机号与客户端id相等的实例
	CameraNode SelectCameraNode(QString host);//查找主机号相等的实例
	Ui::CameraLogIn ui;


	
	void GetIpString(string strIpAdd, int iIp[]);  //将ip地址拆分为数组

};
