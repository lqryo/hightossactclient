#include "TCPClient.h"
#include <QDataStream>
#include "Utils.h"
#include "Settings.h"

bool CTCPClient::connectServer(std::string ip, unsigned short port)
{

	if (ip != "" && port != 0) {
		svrIP = ip;
		svrPort = port;

		debug("<TCPClient> try connect to : " + svrIP + ":" + std::to_string(svrPort));
		tcpClient->connectToHost(svrIP.c_str(), svrPort);
		return true;
	}

	return false;
}

void CTCPClient::handleConnected()
{
	_isConnected = true;

	// adjust socket buffer size to send an image completely( < bufferSize)
#ifdef TCP_SK_BUFFER_SIZE
	int bufferSize = TCP_SK_BUFFER_SIZE;
	tcpClient->setSocketOption(QAbstractSocket::SendBufferSizeSocketOption, bufferSize);
	tcpClient->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, bufferSize);
#endif 
	debug("<TCPClient> Connected to : " + svrIP + ":" + std::to_string(svrPort));
	
}

void CTCPClient::sendMsg(CNetTransmission * msg)
{
	if (!_isConnected) {
		error("Connection is not established yet.");
		return;
	}

	debug("<TCPClient> send out message: ", UTIL_PREFIX);

	uint msgLen = 0;
	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_4_0);

	out << msgLen;							// reserve the len position
	out << *msg;								// write msg
	out.device()->seek(0);						// point to start position
	msgLen = block.size() - sizeof(uint);		// calculate msg length
	out << msgLen;							// write length

	tcpClient->write(block);

	debug("totalLen = " + std::to_string(block.size()) + ", msgLen = " + std::to_string(msgLen), UTIL_NEWLINE);

}

void CTCPClient::readPendingStreams()
{

	debug("<TCPClient> get server message: ", UTIL_PREFIX);

	QDataStream in(tcpClient);
	in.setVersion(QDataStream::Qt_4_0);

	ControlMsg msg;
	in >> msg;
	handleMsg(&msg);

}

void CTCPClient::handleMsg(ControlMsg *msg)
{
	
	debug("type=" + std::to_string(msg->type) + ",text=" + msg->text.toStdString(), UTIL_NEWLINE);
	// next work ...

	// ====== Test only ======
	CNetTransmission faked;
	faked.camera_ID = 12306;
	faked._time = 1234567890;
	faked.frame = cv::imread("E:/Pictures/100.jpg");

	//cv::imwrite("100_copy.jpg", faked.frame);
	//cv::Mat rewrite = cv::Mat(faked.frame.rows, faked.frame.cols, faked.frame.type(), faked.frame.data);
	//cv::imwrite("100_rewrite.jpg", rewrite);

	sendMsg(&faked);
	// ====== /Test only ======

}

CTCPClient::~CTCPClient()
{

	if (tcpClient != nullptr) {
		delete tcpClient;
		tcpClient = nullptr;
	}

	if (tcpServer != nullptr) {
		delete tcpServer;
		tcpServer = nullptr;
	}

}

void CTCPClient::init()
{
	debug("<TCPClient> init.");

	tcpClient = new QTcpSocket();

	connect(tcpClient, SIGNAL(connected()), this, SLOT(handleConnected()));
	connect(tcpClient, SIGNAL(readyRead()), this, SLOT(readPendingStreams()));
	connect(tcpClient, &QTcpSocket::disconnected, [=](){
		debug(tcpClient->localAddress().toString().toStdString() + "Disconnect from server");
		_isConnected = false;
	});

}
