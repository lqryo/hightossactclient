#pragma once
#include "CCamera.h"
#include <qdebug.h>
#include <memory>
#include "HCNetSDK.h"
#include "time.h"
#include "PlayM4.h"
#include <opencv2\opencv.hpp>
#include "qstring.h"
#include "qlabel.h"

//==============================================================================
//	@name		:	CHKCamera
//	@biref		:	海康威视相机接口 继承CCamera类
//	@author		:	
//===============================================================================

class QWidget;
class CDocument;
class CHKCamera : public CCamera{

public:
	//相机监控楼层
	int LowFloor;
	int HighFloor;
	
	//登录接口返回
	LONG IUserID;
	//设备登录返回信息
	NET_DVR_DEVICEINFO_V30 struDeviceInfo;
	//预览接口返回
	LONG IRealPlayHandle;
	HWND hWnd;
	CAMERA_INFO camera_info;
	CHKCamera(CAMERA_INFO pDoc);
	CHKCamera();
	~CHKCamera();

	// 设置相机参数
	bool SetCameraParams();

	// 保存录像
	bool SavaVideo();

	// 停止保存
	bool StopSava();

	// 打开相机进行初始化
	bool OpenCamera(QString cameraIP, QString cameraUsername, QString cameraPass,int cameraLowFloor, int cameraHighFloor);
	
	// 关闭相机
	bool CloseCamera();

	// 开始采集图像 并把采集到的原始图像存入imgDoc的缓冲区队列中
	void StartAcquiring(QLabel *parent);

	// 停止采集图像
	void StopAcquiring();
	void slot(){
		//qDebug() << "camera from thread slot:" << QThread::currentThreadId();
	}
signals:
	void DetectTrack_Sig();//检测与追踪信号

};

