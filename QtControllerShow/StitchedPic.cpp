#include "StitchedPic.h"
#include "Math.h"
using namespace cv;
//#define STITCH_TIME
StitchedPic::StitchedPic()
{
	preview = false;//预览
	try_gpu = false;//使用gpu
	work_megapix = 0.6;
	seam_megapix = 0.1;
	compose_megapix = -1;
	conf_thresh = 1.0f;
	features_type = "surf";//"surf";//特征类型
	ba_cost_func = "ray";
	ba_refine_mask = "xxxxx";
	do_wave_correct = true;
	wave_correct = detail::WAVE_CORRECT_HORIZ;
	save_graph = false;//中间存图片

	warp_type = "plane";//"cylindrical";//
	expos_comp_type = ExposureCompensator::GAIN_BLOCKS;
	match_conf = 0.5f;// 0.65f;//------------------------------------------------------------是否需要设置匹配阈值
	seam_find_type = "no"; //可行，但gpu版本不可用//"dp_color";出现bug//"voronoi";对墙不可行
	blend_type = Blender::MULTI_BAND;//Blender::NO;//multi版本融合效果比较好,耗时慢//NO;//直接覆盖图，效果差，耗时快
	blend_strength = 1;
	result_name = "result.jpg";
}


StitchedPic::~StitchedPic()
{
}

//根据corners得到左顶点最小值，且设置PointXY小图左顶点以大图左上顶点为原点的坐标
Point StitchedPic::cornersMin()
{
	Point _min;
	_min = _corners_Recorde[0];
	for (int i = 1; i < _corners_Recorde.size(); i++)
	{
		if (_corners_Recorde[i].x < _min.x)
			_min.x = _corners_Recorde[i].x;
		if (_corners_Recorde[i].y < _min.y)
			_min.y = _corners_Recorde[i].y;
	}
	for (int i = 0; i < num; i++)
	{
		pointXY.push_back(Point(_corners_Recorde[i] - _min));
	}
	return _min;
}

void StitchedPic::warpPointBack(const int i, const Point p, Point &pXY)
{
	Point2f uv;
	//将坐标转换为corners下的坐标
	uv = static_cast<Point2f>(p + _corners_Recorde[i] - pointXY[i]);
	pXY = static_cast<Point>(_warper_Recorde->warpPointBack(uv, _K_Recorde[i], _R_Recorde[i]));
}

//对点变换，并利用pointXY对应到大图上坐标点
void StitchedPic::warpPoint2XY(const int i, const Point p, Point &pXY)
{
	Point2f pWarped;
	//对点进行变换
	pWarped = static_cast<Point>(_warper_Recorde->warpPoint(static_cast<Point2f>(p), _K_Recorde[i], _R_Recorde[i]));
	//四舍五入
	int x = floorf(pWarped.x);
	int y = floorf(pWarped.y);
	if (pWarped.x - x > 0.5)
		x += 1;
	if (pWarped.y - y > 0.5)
		y += 1;

	pXY = Point(x, y) + pointXY[i] - _corners_Recorde[i];
}

Mat StitchedPic::warpPic(int i, const Mat &src, Mat &mask_warped)
{
	Mat dst;
	_warper_Recorde->warp(src, _K_Recorde[i], _R_Recorde[i], INTER_LINEAR, BORDER_CONSTANT, dst);//BORDER_TRANSPARENT//BORDER_REFLECT
	Mat mask;
	mask.create(src.size(), CV_8U);
	mask.setTo(Scalar::all(255));
	_warper_Recorde->warp(mask, _K_Recorde[i], _R_Recorde[i], INTER_NEAREST, BORDER_CONSTANT, mask_warped);
	return dst;
}

//记录每张小图的所处位置,默认两张小图，改为通用的长度太麻烦！--------------------------
Mat StitchedPic::mapMask(const int valueStart)
{
//	int increase = 10;//每张图间隔增加数值，第1，2，3，4.。。分别为10， 20， 30.。。。110， 120， 130.。。
	Mat mask;
	_dst_mask.copyTo(mask);
	//cv::imwrite("_dst_mask.jpg", _dst_mask);
	//记录重叠区域
	//顺时针旋转为竖着
	Mat mask_wise = stitch::transAndFlipClockwise(mask);
	vector<Mat> _img_result_masks_wise;
	for (int i = 0; i < _img_result_masks.size(); i++)
	{
		_img_result_masks_wise.push_back(stitch::transAndFlipClockwise(_img_result_masks[i]));
		//imwrite("imgs_mask" + to_string(i) + ".jpg", _img_result_masks[i]);
	}
	vector<Point> pointXY_wise;
	//顺时针旋转，对应点坐标变化-------其对应在大图上位置，height应为原图的
	for (int i = 0; i < pointXY.size(); i++)
	{
		//对应点旋转后
		int x = _dst_mask.size().height - pointXY[i].y;
		int y = pointXY[i].x;
		//减去小图width，就是左上角点
		x = x - _sizes_Recorde[i].height;
		pointXY_wise.push_back(Point(x, y));
	}
	vector<Size> _sizes_Recorde_wise;
	for (int i = 0; i < _sizes_Recorde.size(); i++)
	{
		Size s; 
		s.width = _sizes_Recorde[i].height;
		s.height = _sizes_Recorde[i].width;
		_sizes_Recorde_wise.push_back(s);
	}
	
	for (int y = 0; y < mask_wise.size().height; ++y)
	{
		uchar *src = mask_wise.ptr<uchar>(y);
		uchar *img_mask0 = NULL, *img_mask1 = NULL;
		bool img0_yb = false, img1_yb = false;
		//这里_size_Recorde和img_result_masks对应的大小应该一样
		//此时y行处于那副图或那两幅
		int imgIdx;
		for (int idx = 0; idx < pointXY_wise.size(); idx++)
		{
			if ((y - pointXY_wise[idx].y) >= 0 && y < (_sizes_Recorde_wise[idx].height + pointXY_wise[idx].y))
			{
				img_mask0 = _img_result_masks_wise[idx].ptr<uchar>(y - pointXY_wise[idx].y);
				img0_yb = true;
				if ((idx+1) < pointXY_wise.size() && (y - pointXY_wise[idx+1].y) >= 0 && y<(_sizes_Recorde_wise[idx+1].height + pointXY_wise[idx+1].y))
				{
					img_mask1 = _img_result_masks_wise[idx + 1].ptr<uchar>(y - pointXY_wise[idx + 1].y);
					img1_yb = true;
				}
				imgIdx = idx;
				break;
			}
		}
		for (int x = 0; x < mask_wise.size().width; ++x)
		{
			if (src[x] != 0)//此处滤掉大图的四角边缘
			{
				src[x] = 0;

				if (img0_yb && (x - pointXY_wise[imgIdx].x) >= 0 && x<(_sizes_Recorde_wise[imgIdx].width + pointXY_wise[imgIdx].x))
				{
					if (img_mask0[x - pointXY_wise[imgIdx].x] != 0)
					{
						src[x] = (imgIdx+1) * _INCREASEVALUE;//处于第一幅图,第一张图不能为0，故加1, 20  40
					}
					else{
						//src[x] = 100;
					}
				}
				if (img1_yb && (x - pointXY_wise[imgIdx + 1].x) >= 0 && x<(_sizes_Recorde_wise[imgIdx + 1].width + pointXY_wise[imgIdx+1].x))
				{
					if (img_mask1[x - pointXY_wise[imgIdx+1].x] != 0)
					{
						if (src[x] == 0)//表明重叠区域，前一个图不在范围内，判别mask == 0，没有赋值
						{
							src[x] = (imgIdx + 2) * _INCREASEVALUE;
						}
						else{
							src[x] += _OVERLAPVALUE;//处于重叠区域//这些数值与头文件maskconstvalue.h对应  25

						}
					}
					else{
					//	src[x] = 200;
					}
				}
			}
		}
	}

	//旋转回去，逆时针
	mask = stitch::transAndFlip(mask_wise);
	return mask;
}

/*
const Mat &mask:传入的有不同灰度值的mask，不同灰度值代表重叠区或者单个图映射部分
const vector<int> value：需要处理的重叠区域对应的灰度值大小
vector<Mat> &overlapMask： 得到每个重叠区域的mask
vector<Mat> &alphaMasks： 每个重叠区域对应的mask中相应的alpha值
*/
void StitchedPic::countAlpha(const Mat &mask, const vector<int> values, Mat &alphaMat)
{
	int num = values.size();
	alphaMat.create(mask.size(), CV_32FC1);//记录的每个重叠区域对应alpha
	//alphaMat.setTo(Scalar::all(255));

	for (int y = 0; y < mask.size().height; ++y)
	{
		const uchar *srcY = mask.ptr<uchar>(y);
		float *alphaDst = alphaMat.ptr<float>(y);
		
		
		int lastend;////////////************//////************////*****************//////////******//////////////////////
		//计算每个value值，上一次的结束来开始，记录这次结束值
		for (int i = 0; i < num; i++)
		{
			int start=0;
			int end=0;
			float total;
			bool firstXSet = false;
			int value = values[i];
			int xTraver;
			if (i == 0)
			{
				xTraver = 0;
			}else
			{
				xTraver = lastend;
			}
			//第一次遍历，得到开始结束值
			for (int x = xTraver; x < mask.size().width; ++x)
			{
				if (srcY[x] == value)
				{
					if (!firstXSet)//若第一个没有被记录
					{
						start = x;
						firstXSet = true;
					}
				}
				else
				{
					if (firstXSet)//当已经到value值时，这一次不在，则为结束点
					{
						end = x;
						total = end - start;
						break;//是否在这里就跳出x循环了
					}
				}
			}
			//再次遍历，计算alpha值
			for (int x = start; x < end; ++x)
			{
				alphaDst[x] = (x - start) / total;
			}
			//将此次的记录赋予last，便于下次记录
			lastend = end;
		}
	}

}

//顺时针旋转90图片，对应点坐标变化
void StitchedPic::pointXY2_T(const Point p, Point &p_T)
{
	int maxWidth_T = _dst.size().height;
	p_T = Point(maxWidth_T - p.y, p.x);
}
//将图片逆时针旋转90，对应点坐标变化
void StitchedPic::pointXY2_Inver(const Point p, Point &p_T)
{
	int width = _dst.size().height;
	p_T = Point(p.y, width - p.x);
}

//记录拼接过程中数据，在stitch中调用
void StitchedPic::recordeStitchData(const vector<Point>& corners_Recorde, Ptr<RotationWarper> &warper_Recorde,
	const vector<Mat> &K_Recorde, const vector<Mat> &R_Recorde)
{
	_corners_Recorde = corners_Recorde;
	_warper_Recorde = warper_Recorde;
	_K_Recorde = K_Recorde;
	_R_Recorde = R_Recorde;
}

void error(String info)
{
	std::cout << "M_ERROR:" << info << endl;
}


Mat StitchedPic::stitch(vector<Mat> img_mats, const vector<Mat> img_masks, const float resizeScale)
{
	//想尝试cut掉边缘地区，是否能增加匹配成功几几率，也能减少识别特征数量和匹配消耗时间
	//for (int i = 0; i < img_mats.size();i++)//3)
	//{
	//	Rect rect;
	//	if (i == 0)
	//	{
	//		rect = Rect(0, 0, img_mats[i].size().width / 3 * 2, img_mats[i].size().height);
	//		img_mats[i](rect).setTo(Scalar::all(255));
	//	}
	//	else if (i == (img_mats.size() - 1))
	//	{
	//		rect = Rect(img_mats[i].size().width / 3, 0, img_mats[i].size().width / 3 * 2, img_mats[i].size().height);
	//		img_mats[i](rect).setTo(Scalar::all(255));
	//	}
	//	else
	//	{
	//		rect = Rect(img_mats[i].size().width / 3, 0, img_mats[i].size().width / 3, img_mats[i].size().height);
	//		img_mats[i](rect).setTo(Scalar::all(255));
	//	}
	//}

	int resizeScale_R = 1;
	vector<Mat> K_Recorde;
	vector<Mat> R_Recorde;
	vector<Point> corners_Recorde;

#if ENABLE_LOG
	int64 app_start_time = getTickCount();
#endif
	//double t0 = (double)getTickCount();

	cv::setBreakOnError(true);

	// Check if have enough images 读取的图片数量
	int num_images = static_cast<int>(img_mats.size());
	if (num_images < 2)
	{
		LOGLN("Need more images");
	}

	//开始时定义的scale都为1
	double work_scale = 1, seam_scale = 1, compose_scale = 1;
	//开始时定义的相关bool变量，都为false
	/*bool is_work_scale_set = false,*/
	bool is_seam_scale_set = false, is_compose_scale_set = false;

#if ENABLE_LOG
	int64 t = getTickCount();
#endif
# if defined STITCH_TIME
	double t2 = (double)getTickCount();
#endif
	//特征寻找类
	Ptr<FeaturesFinder> finder;
	//确定feature类型：surf 、orb，surf有gpu版本
	if (features_type == "surf")
	{
#if defined(HAVE_OPENCV_NONFREE) && defined(HAVE_OPENCV_GPU)
		if (try_gpu && gpu::getCudaEnabledDeviceCount() > 0)
			finder = new SurfFeaturesFinderGpu();
		else
#endif
			finder = new SurfFeaturesFinder();
	}
	else if (features_type == "orb")
	{
		finder = new OrbFeaturesFinder();
	}
	else
	{
		cout << "Unknown 2D features type: '" << features_type << "'.\n";
	}

	//读取图片到full_img,resize到img
	Mat full_img, img;
	//每张图片特征
	vector<ImageFeatures> features(num_images);
	//最终处理过图片（resize)到images容器
	vector<Mat> images(num_images);
	//存储每张图片大小容器
	vector<Size> full_img_sizes(num_images);
	//处理图片时定义为1，后来赋值seam / work
	double seam_work_aspect = 1;

	for (int i = 0; i < num_images; ++i)
	{
		full_img = img_mats[i];
		full_img_sizes[i] = full_img.size();
		//压缩图片、拼接缝比例值确定
		if (full_img.empty())
		{
			LOGLN("Can't open image " << img_names[i]);
		}
		//舍弃work_scale,确定寻找特征点数量的值
		//if (work_megapix < 0)
		//{
		//	img = full_img;
		//	work_scale = 1;
		//	is_work_scale_set = true;
		//}
		//else
		//{
		//	if (!is_work_scale_set)
		//	{
		//		work_scale = min(1.0, sqrt(work_megapix * 1e6 / full_img.size().area()));
		//		is_work_scale_set = true;
		//	}
		//work_scale = 0.5;
		//	resize(full_img, img, Size(), work_scale, work_scale);//确定resize图片的大小
		//}
		if (!is_seam_scale_set)
		{
			seam_scale = min(1.0, sqrt(seam_megapix * 1e6 / full_img.size().area()));
			seam_work_aspect = seam_scale / work_scale;
			is_seam_scale_set = true;
		}

		(*finder)(full_img, features[i]);
		features[i].img_idx = i;
		LOGLN("Features in image #" << i + 1 << ": " << features[i].keypoints.size());
		cout << "        Features in image #" << i + 1 << ": " << features[i].keypoints.size() << endl;

		resize(full_img, img, Size(), seam_scale, seam_scale);
		images[i] = img.clone();
		//resize是将图片变小
		//imshow("resize", images[i]);
	}
#if defined STITCH_TIME
	t2 = ((double)getTickCount() - t2) / getTickFrequency();
	cout << "new特征因子，每张图片读取resize去特征:" << t2 << endl;
	double t3 = (double)getTickCount();
#endif 
	//相关内存处理？
	finder->collectGarbage();
	full_img.release();
	img.release();

	LOGLN("Finding features, time: " << ((getTickCount() - t) / getTickFrequency()) << " sec");

	LOG("Pairwise matching");
	//匹配
#if ENABLE_LOG
	t = getTickCount();
#endif
	//成对匹配的match信息，由features——>得到，通过匹配类
	//匹配的点对的置信度值是public, 可以设置，即最后可以手动设置其置信度，但如何剔除？没有合适的图片测试
	//将特征点匹配阈值设置低，当特征点不够时，匹配成功，但如何解决一列图片的间隔匹配错误？-------------------------------
	vector<MatchesInfo> pairwise_matches;
	BestOf2NearestMatcher matcher(try_gpu, match_conf);
	matcher(features, pairwise_matches);
	matcher.collectGarbage();
	LOGLN("Pairwise matching, time: " << ((getTickCount() - t) / getTickFrequency()) << " sec");
#if defined STITCH_TIME
	t3 = ((double)getTickCount() - t3) / getTickFrequency();
	cout << "成对匹配: " << t3 << endl;
	double t4 = (double)getTickCount();
#endif
#if defined STITCH_TIME
	t4 = ((double)getTickCount() - t4) / getTickFrequency();
	cout << "是否存图，剔除不在一个全景图中的图片：" << t4 << endl;
	double t5 = (double)getTickCount();
#endif
	//初步粗评估
	HomographyBasedEstimator estimator;//继承于estimator，旋转矩阵的估计基类
	vector<CameraParams> cameras;
	estimator(features, pairwise_matches, cameras);
	for (size_t i = 0; i < cameras.size(); ++i)
	{
		Mat R;
		cameras[i].R.convertTo(R, CV_32F);//旋转矩阵变换格式
		cameras[i].R = R;
	}
#if defined STITCH_TIME
	t5 = ((double)getTickCount() - t5) / getTickFrequency();
	cout << "摄像机参数粗评估：" << t5 << endl;
	double t6 = (double)getTickCount();
#endif
	//细化， 解决累积误差
	Ptr<detail::BundleAdjusterBase> adjuster;//继承于estimator，摄像机参数细化方法的基类
	//继承base，相机参数细化算法，投影误差平方和的最小值
	if (ba_cost_func == "reproj") adjuster = new detail::BundleAdjusterReproj();
	//最小化通过相机中心的光线和特征之间的距离和
	else if (ba_cost_func == "ray") adjuster = new detail::BundleAdjusterRay();
	else
	{
		cout << "Unknown bundle adjustment cost function: '" << ba_cost_func << "'.\n";
	}
	//应该在此处设置置信度，剔除pairwise_match,尚未测试！
	adjuster->setConfThresh(conf_thresh);//设置筛选不太匹配的图像的阈值
	Mat_<uchar> refine_mask = Mat::zeros(3, 3, CV_8U);
	if (ba_refine_mask[0] == 'x') refine_mask(0, 0) = 1;
	if (ba_refine_mask[1] == 'x') refine_mask(0, 1) = 1;
	if (ba_refine_mask[2] == 'x') refine_mask(0, 2) = 1;
	if (ba_refine_mask[3] == 'x') refine_mask(1, 1) = 1;
	if (ba_refine_mask[4] == 'x') refine_mask(1, 2) = 1;
	adjuster->setRefinementMask(refine_mask);//设置细化参数?0表示不细化各自的参数，否则细化
	(*adjuster)(features, pairwise_matches, cameras);//对相机参数细化？
#if defined STITCH_TIME
	t6 = ((double)getTickCount() - t6) / getTickFrequency();
	cout << "相机参数细化：" << t6 << endl;
	double t7 = (double)getTickCount();
#endif
	// Find median focal length//找到中间焦距
	//所有camera的focal
	vector<double> focals;
	for (size_t i = 0; i < cameras.size(); ++i)//复制到focal
	{
		//LOGLN("Camera #" << indices[i] + 1 << ":\n" << cameras[i].K());
		//cout << "               Camera #" << indices[i] + 1 << ":\n" << cameras[i].K() << endl;
		focals.push_back(cameras[i].focal);
	}

	sort(focals.begin(), focals.end());
	//由焦距focal得到的打包图片比例
	float warped_image_scale;
	if (focals.size() % 2 == 1)
		warped_image_scale = static_cast<float>(focals[focals.size() / 2]);//取focal【1】
	else//取中间两个值均值
		warped_image_scale = static_cast<float>(focals[focals.size() / 2 - 1] + focals[focals.size() / 2]) * 0.5f;

	if (do_wave_correct)//是否波形矫正，对camera.R进行矫正
	{
		vector<Mat> rmats;
		for (size_t i = 0; i < cameras.size(); ++i)
			rmats.push_back(cameras[i].R.clone());
		waveCorrect(rmats, wave_correct);
		for (size_t i = 0; i < cameras.size(); ++i){
			cameras[i].R = rmats[i];
		}
	}
#if defined STITCH_TIME
	t7 = ((double)getTickCount() - t7) / getTickFrequency();
	cout << "取中间焦距和波形矫正：" << t7 << endl;
	double t8 = (double)getTickCount();
#endif
#if ENABLE_LOG
	t = getTickCount();
#endif

#pragma region[forSeam]
	//图片左上角corner
	vector<Point> corners(num_images);
	//warp时定义的遮罩容器
	vector<Mat> masks_warped(num_images);
	//warp时定义的变换的图片容器
	vector<Mat> images_warped(num_images);
	//warp时定义的图片大小，赋值为warp后图片的大小
	vector<Size> sizes(num_images);
	//warp时定义的遮罩，赋值后都是无符号255
	vector<Mat> masks(num_images);

	// Preapre images masks
	for (int i = 0; i < num_images; ++i)
	{
		masks[i].create(images[i].size(), CV_8U);
		masks[i].setTo(Scalar::all(255));
	}

	// Warp images and their masks
	//warpCreator指针
	Ptr<WarperCreator> warper_creator;
#if defined(HAVE_OPENCV_GPU)
	if (try_gpu && gpu::getCudaEnabledDeviceCount() > 0)
	{
		if (warp_type == "plane") warper_creator = new cv::PlaneWarperGpu();
		else if (warp_type == "cylindrical") warper_creator = new cv::CylindricalWarperGpu();
		else if (warp_type == "spherical") warper_creator = new cv::SphericalWarperGpu();
	}
	else
#endif
	{
		if (warp_type == "plane") warper_creator = new cv::PlaneWarper();
		else if (warp_type == "cylindrical") warper_creator = new cv::CylindricalWarper();
		else if (warp_type == "spherical") warper_creator = new cv::SphericalWarper();
		else if (warp_type == "fisheye") warper_creator = new cv::FisheyeWarper();
		else if (warp_type == "stereographic") warper_creator = new cv::StereographicWarper();
		else if (warp_type == "compressedPlaneA2B1") warper_creator = new cv::CompressedRectilinearWarper(2, 1);
		else if (warp_type == "compressedPlaneA1.5B1") warper_creator = new cv::CompressedRectilinearWarper(1.5, 1);
		else if (warp_type == "compressedPlanePortraitA2B1") warper_creator = new cv::CompressedRectilinearPortraitWarper(2, 1);
		else if (warp_type == "compressedPlanePortraitA1.5B1") warper_creator = new cv::CompressedRectilinearPortraitWarper(1.5, 1);
		else if (warp_type == "paniniA2B1") warper_creator = new cv::PaniniWarper(2, 1);
		else if (warp_type == "paniniA1.5B1") warper_creator = new cv::PaniniWarper(1.5, 1);
		else if (warp_type == "paniniPortraitA2B1") warper_creator = new cv::PaniniPortraitWarper(2, 1);
		else if (warp_type == "paniniPortraitA1.5B1") warper_creator = new cv::PaniniPortraitWarper(1.5, 1);
		else if (warp_type == "mercator") warper_creator = new cv::MercatorWarper();
		else if (warp_type == "transverseMercator") warper_creator = new cv::TransverseMercatorWarper();
	}

	if (warper_creator.empty())
	{
		cout << "Can't create the following warper '" << warp_type << "'\n";
	}
	//由WarperCreator创建的rotationWarper指针
	Ptr<RotationWarper> warper = warper_creator->create(static_cast<float>(warped_image_scale * seam_work_aspect));
	/*
	for (int i = 0; i < num_images; ++i)
	{
	Mat_<float> K;
	cameras[i].K().convertTo(K, CV_32F);//32位浮点数
	float swa = (float)seam_work_aspect;
	K(0, 0) *= swa; K(0, 2) *= swa;
	K(1, 1) *= swa; K(1, 2) *= swa;
	//warp返回的是左上角的图像投影
	corners[i] = warper->warp(images[i], K, cameras[i].R, INTER_LINEAR, BORDER_REFLECT, images_warped[i]);//warp image
	sizes[i] = images_warped[i].size();
	//warp是变形
	//imwrite("warp" + to_string(i) + ".jpg", images_warped[i]);

	warper->warp(masks[i], K, cameras[i].R, INTER_NEAREST, BORDER_CONSTANT, masks_warped[i]);//warp mask
	//imwrite("warp_mask" + to_string(i) + ".jpg", masks_warped[i]);//黑色的遮罩
	}
	//warped后的图片转换为32位浮点数double
	vector<Mat> images_warped_f(num_images);
	for (int i = 0; i < num_images; ++i){
	images_warped[i].convertTo(images_warped_f[i], CV_32F);
	//32位浮点数肉眼发现不了什么变化
	//imwrite("warp_f" + to_string(i) + ".jpg", images_warped_f[i]);
	}

	#if defined STITCH_TIME
	t8 = ((double)getTickCount() - t8) / getTickFrequency();
	cout << "创建warp，warp imgae和mask？：" << t8 << endl;
	double t9 = (double)getTickCount();
	#endif
	//曝光ExposureCompensator指针
	Ptr<ExposureCompensator> compensator_ = ExposureCompensator::createDefault(expos_comp_type);

	compensator_->feed(corners, images_warped, masks_warped);
	Ptr<BlocksGainCompensator> compensator = compensator_;
	//vector<Mat_<float>> gain_maps= compensator->gainMapReturn();
	//曝光作用于遮罩
	for (int i = 0; i < num_images; i++){
	//imwrite("exposure" + to_string(i) + ".jpg", masks_warped[i]);
	}
	//拼接缝SeamFinder指针
	Ptr<SeamFinder> seam_finder;
	if (seam_find_type == "no")
	seam_finder = new detail::NoSeamFinder();
	else if (seam_find_type == "voronoi")
	seam_finder = new detail::VoronoiSeamFinder();
	else if (seam_find_type == "gc_color")
	{
	#if defined(HAVE_OPENCV_GPU)
	if (try_gpu && gpu::getCudaEnabledDeviceCount() > 0)
	seam_finder = new detail::GraphCutSeamFinderGpu(GraphCutSeamFinderBase::COST_COLOR);
	else
	#endif
	seam_finder = new detail::GraphCutSeamFinder(GraphCutSeamFinderBase::COST_COLOR);
	}
	else if (seam_find_type == "gc_colorgrad")
	{
	#if defined(HAVE_OPENCV_GPU)
	if (try_gpu && gpu::getCudaEnabledDeviceCount() > 0)
	seam_finder = new detail::GraphCutSeamFinderGpu(GraphCutSeamFinderBase::COST_COLOR_GRAD);
	else
	#endif
	seam_finder = new detail::GraphCutSeamFinder(GraphCutSeamFinderBase::COST_COLOR_GRAD);
	}
	else if (seam_find_type == "dp_color")
	seam_finder = new detail::DpSeamFinder(DpSeamFinder::COLOR);
	else if (seam_find_type == "dp_colorgrad")
	seam_finder = new detail::DpSeamFinder(DpSeamFinder::COLOR_GRAD);
	if (seam_finder.empty())
	{
	cout << "Can't create the following seam finder '" << seam_find_type << "'\n";
	}
	//
	seam_finder->find(images_warped_f, corners, masks_warped);
	// Release unused memory
	images.clear();
	images_warped.clear();
	images_warped_f.clear();
	masks.clear();
	#if defined STITCH_TIME
	t9 = ((double)getTickCount() - t9) / getTickFrequency();
	cout << "曝光，拼接缝，释放内存：" << t9 << endl;
	double t10 = (double)getTickCount();
	#endif
	*/
	#pragma endregion[forSeam]
	
	LOGLN("Compositing...");//合成
#if ENABLE_LOG
	t = getTickCount();
#endif
	//合成定义warp
	Mat img_warped, img_warped_s;
	//合成定义mask
	Mat dilated_mask, seam_mask, mask, mask_warped;
	//合成定义的Blender指针
	Ptr<Blender> blender;
	//double compose_seam_aspect = 1;
	//合成定义的工作视野
	double compose_work_aspect = 1;
	vector<Mat> masks_warped_m(num_images);

	for (int img_idx = 0; img_idx < num_images; ++img_idx)
	{
		LOGLN("Compositing image #" << indices[img_idx] + 1);

		// Read image and resize it if necessary需要时读取并resize图片
		full_img = img_mats[img_idx];
		//如没有设置合成系数
		if (!is_compose_scale_set)
		{
			//if (compose_megapix > 0)
			//	compose_scale = min(1.0, sqrt(compose_megapix * 1e6 / full_img.size().area()));
			is_compose_scale_set = true;

			//// Compute relative scales计算相关系数
			////compose_seam_aspect = compose_scale / seam_scale;
			compose_scale = resizeScale;
			compose_work_aspect = compose_scale / work_scale;//----------------为1，匹配work_megapix, 拼接compose-megapix都为1
			
			//compose_work_aspect = 1;//原来大小时， 为1 
			//compose_work_aspect = resizeScale;
			// Update warped image scale更新压缩图片比例
			warped_image_scale *= static_cast<float>(compose_work_aspect);
			warper = warper_creator->create(warped_image_scale);

			// Update corners and sizes
			for (int i = 0; i < num_images; ++i)
			{
				// Update intrinsics更新内联函数
				cameras[i].focal *= compose_work_aspect;
				cameras[i].ppx *= compose_work_aspect;
				cameras[i].ppy *= compose_work_aspect;

				// Update corner and size
				Size sz = full_img_sizes[i];
				if (std::abs(compose_scale - 1) > 1e-1)
				{
					sz.width = cvRound(full_img_sizes[i].width * compose_scale);
					sz.height = cvRound(full_img_sizes[i].height * compose_scale);
				}

				Mat K;
				cameras[i].K().convertTo(K, CV_32F);
				Rect roi = warper->warpRoi(sz, K, cameras[i].R);
				corners[i] = roi.tl();//rect的左上顶点的坐标
				sizes[i] = roi.size();
			}
		}
	//	if (abs(compose_scale - 1) > 1e-1)
	//	{
		resize(full_img, img, Size(), resizeScale, resizeScale);// compose_scale, compose_scale);
			//resizeScale_R = compose_scale;
		//}
		//else
		//	img = full_img;//还是原图大小，没有改变--------------------------------------------
		full_img.release();
		//至此，为了resize图片，更新Corner等，从上一步得到corners，cameras
		Size img_size = img.size();
		//cameras.K转换为32位浮点数
		Mat K;
		cameras[img_idx].K().convertTo(K, CV_32F);

		// Warp the current image
		warper->warp(img, K, cameras[img_idx].R, INTER_LINEAR, BORDER_TRANSPARENT, img_warped);
		//记录数据

		K_Recorde.push_back(K);
		R_Recorde.push_back(cameras[img_idx].R);

		// Warp the current image mask
		//mask.create(img_size, CV_8U);
		//mask.setTo(Scalar::all(255));
		warper->warp(img_masks[img_idx], K, cameras[img_idx].R, INTER_NEAREST, BORDER_CONSTANT, mask_warped);
		/////////////////
		Mat m_mask;
		Mat m_mask_warped;
		m_mask = img_masks[img_idx];//------------------------------------------------------
		warper->warp(m_mask, K, cameras[img_idx].R, INTER_NEAREST, BORDER_CONSTANT, m_mask_warped);
		//对于记录参数，只需要被拼接图经过warp的mask，不需要拼接缝的mask
		_img_result_masks.push_back(m_mask_warped);//-----------------------------------------------------------------
		m_mask.release();
		m_mask_warped.release();
		//////////////////////////
		// Compensate exposure
		//	compensator->apply(img_idx, corners[img_idx], img_warped, mask_warped);

		img_warped.convertTo(img_warped_s, CV_16S);//没有改变通道数？
		img_warped.release();
		img.release();
		mask.release();

		//细节 此处用到前面存储的masks_warped 曝光和遮罩--------------------//膨胀第一次warp的masks（还经历过拼接缝寻找），resize到本次融合中的warpmask的大小，得到seam和本次mask做与运算
		//dilate(masks_warped[img_idx], dilated_mask, Mat());//原来存储的mask图像膨胀
		//resize(dilated_mask, seam_mask, mask_warped.size());//大小变换后得到seam_mask(变大)
		//mask_warped = seam_mask & mask_warped;
		//masks_warped_m[img_idx] = mask_warped;//存每张图片的mask——m得到的mask_warped用于blend，先注释
		//最终得到mask_warped,img_warped_s
		//创建blender时会用到corners和sizes,也可以不相关
		if (blender.empty())
		{
			blender = Blender::createDefault(blend_type, try_gpu);
			Size dst_sz = resultRoi(corners, sizes).size();
			float blend_width = sqrt(static_cast<float>(dst_sz.area())) * blend_strength / 100.f;
			if (blend_width < 1.f)
				blender = Blender::createDefault(Blender::NO, try_gpu);
			
			blender->prepare(corners, sizes);//给融合图像给出最后大小
		}
		// Blend the current image
		blender->feed(img_warped_s, mask_warped, corners[img_idx]);
		corners_Recorde.push_back(corners[img_idx]);
	}
	//记录到本类中参数
	recordeStitchData(corners_Recorde, warper, K_Recorde, R_Recorde);
	_sizes_Recorde = sizes;//变形后图大小
	num = num_images;
	//利用拼接中的corners,但不能解决有warp变形了四角问题

	blender->blend(_dst, _dst_mask);
	//_rect = cutResult(_dst);
	/*Mat _dst_r = _dst(_rect);
	Mat _dst_mask_r = _dst_mask(_rect);*/
#if defined STITCH_TIME
	t10 = ((double)getTickCount() - t10) / getTickFrequency();
	cout << "compose合成：" << t10 << endl;

	t0 = ((double)getTickCount() - t0) / getTickFrequency();
	cout << "总共花费时间:" << t0 << endl;
#endif
	return _dst;
	}

