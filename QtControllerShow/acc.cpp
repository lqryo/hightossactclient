#include"acc.h"
#define ACC_TMP_TEST
#define DEBUG_TEST
//需要更改的地方都已注释，未注释的地方无需更改

//设备初始化
void acc::initial_head()
{
	cl_platform_id* platforms = (cl_platform_id*)malloc(numPlatforms* sizeof(cl_platform_id));
	clGetPlatformIDs(numPlatforms, platforms, NULL);
	//如果输出是空，尝试platform = platforms[1]
	platform = platforms[1];
	char name_data[48];
	status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &numDevices);
	devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
	status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numDevices, devices, NULL);
	clGetDeviceInfo(devices[0], CL_DEVICE_NAME, sizeof(name_data), name_data, NULL);
	printf("NAME: %s\n", name_data);
	context = clCreateContext(NULL, 1, devices, NULL, NULL, NULL);
	commandQueue = clCreateCommandQueue(context, devices[0], 0, NULL);
	
	const char *filename = "HelloWorld_Kernel.cl";//"Reverse_kernel.cl";//
	string sourceStr;
	status = convertToString(filename, sourceStr);
	const char *source = sourceStr.c_str();
	size_t sourceSize[] = { strlen(source) };
	program = clCreateProgramWithSource(context, 1, &source, sourceSize, NULL);
	status = clBuildProgram(program, 1, devices, NULL, NULL, NULL);

	kernel = clCreateKernel(program, "helloworld"/*"Reverse"*/, NULL);
}

//开辟内存？
//初始化，INIT_FLAG根据其初始化不同情况的参数
//输入分别是小图的行数，列数，大图的行数，列数，大图同样尺寸大小的图片
void acc::initial(vector<cv::Size> ij, const int m, cv::Mat variantMask, cv::Mat alpha, cv::Mat mapRecord, cv::Mat out_image, int INIT_FLAG)
{
	
		if (ij.size() == 2)
		{
			_si = ij[0].height; _sj = ij[0].width;
			_si2 = ij[0].height; _sj2 = ij[0].width;
			_bi = ij[1].height; _bj = ij[1].width;
		}
		else{
			cout << "error ij lenth not enough!" << endl;
		}
#pragma region [mallocMemorry]
		//创建各个数组，注意这是用一维数组实现二维数组，作为之后缓冲区的初始值
		Ns = _si*_sj;
		Ns2 = _si2 * _sj2;
		Nb = _bi*_bj;
		//第一个输入图像大小_si,_sj
		//uchar** _input1; input1 = _input1;
		input1 = (uchar **)malloc(_si * sizeof(uchar*));
		input1[0] = (uchar *)malloc(Ns * sizeof(uchar));
		for (int i = 1; i < _si; i++)
			input1[i] = input1[i - 1] + _sj;
		memset(input1[0], 0, (Ns)*sizeof(uchar));

		//第二个输入图像大小为_si2,_sj2
		input2 = (uchar **)malloc(_si2 * sizeof(uchar*));
		input2[0] = (uchar *)malloc(Ns2 * sizeof(uchar));
		for (int i = 1; i < _si2; i++)
			input2[i] = input2[i - 1] + _sj2;
		memset(input2[0], 0, (Ns2)*sizeof(uchar));
#pragma region [more input pics]
		//其他输入小图：
		input6 = (uchar **)malloc(_si * sizeof(uchar*));
		input6[0] = (uchar *)malloc(Ns * sizeof(uchar));
		for (int i = 1; i < _si; i++)
			input6[i] = input6[i - 1] + _sj;
		memset(input6[0], 0, (Ns)*sizeof(uchar));

		input7 = (uchar **)malloc(_si * sizeof(uchar*));
		input7[0] = (uchar *)malloc(Ns * sizeof(uchar));
		for (int i = 1; i < _si; i++)
			input7[i] = input7[i - 1] + _sj;
		memset(input7[0], 0, (Ns)*sizeof(uchar));

		input8 = (uchar **)malloc(_si * sizeof(uchar*));
		input8[0] = (uchar *)malloc(Ns * sizeof(uchar));
		for (int i = 1; i < _si; i++)
			input8[i] = input8[i - 1] + _sj;
		memset(input8[0], 0, (Ns)*sizeof(uchar));

		input9 = (uchar **)malloc(_si * sizeof(uchar*));
		input9[0] = (uchar *)malloc(Ns * sizeof(uchar));
		for (int i = 1; i < _si; i++)
			input9[i] = input9[i - 1] + _sj;
		memset(input9[0], 0, (Ns)*sizeof(uchar));

		input10 = (uchar **)malloc(_si * sizeof(uchar*));
		input10[0] = (uchar *)malloc(Ns * sizeof(uchar));
		for (int i = 1; i < _si; i++)
			input10[i] = input10[i - 1] + _sj;
		memset(input10[0], 0, (Ns)*sizeof(uchar));

		input11 = (uchar **)malloc(_si * sizeof(uchar*));
		input11[0] = (uchar *)malloc(Ns * sizeof(uchar));
		for (int i = 1; i < _si; i++)
			input11[i] = input11[i - 1] + _sj;
		memset(input11[0], 0, (Ns)*sizeof(uchar));
#pragma endregion[]

		input3 = (uchar **)malloc(_bi * sizeof(uchar*));
		input3[0] = (uchar *)malloc(Nb * sizeof(uchar));
		for (int i = 1; i < _bi; i++)
			input3[i] = input3[i - 1] + _bj;
		memset(input3[0], 0, (Nb)*sizeof(uchar));

		input4 = (float **)malloc(_bi * sizeof(float*));//float*指向_bj长度数组
		input4[0] = (float *)malloc(Nb * sizeof(float));//首地址为_bi*_bj*type
		for (int i = 1; i < _bi; i++)
			input4[i] = input4[i - 1] + _bj;
		memset(input4[0], 0, (Nb)*sizeof(float));

		input5 = (short ***)malloc(_bi * sizeof(short**));
		short **aa, *a;
		aa = (short**)malloc(sizeof(short*)*_bj * _bi);
		a = (short*)malloc(sizeof(short) * 4 * _bi*_bj);

		for (int i = 0; i < _bj * _bi; i++)
			aa[i] = &a[i*4];
		for (int j = 0; j < _bi; j++)
			input5[j] = &aa[j*_bj];
		memset(input5[0][0], 0, 4 * _bi*_bj*sizeof(short));//原始版本
		//运行出问题，出掉【k】后
		//input5 = (short ***)malloc(_bi * sizeof(short**));
		//short **aa, *a;                                      //*a指向_bj*4长度数组，**a指向4长度数组
		//aa = (short**)malloc(sizeof(short*)*_bj * _bi);       //*a的地址为
		////aa = (short**)malloc(sizeof(short*)*_bi);   
		//a = (short*)malloc(sizeof(short) * _bi * _bj * 4);

		//for (int i = 0; i < _bi * _bj; i++)
		//	aa[i] = &a[i * 4];
		//for (int j = 0; j < _bi; j++)
		//	input5[j] = &aa[j*_bj];
		//memset(input5[0][0], 0, 4 * _bi*_bj*sizeof(short));

		output = (uchar **)malloc(_bi * sizeof(uchar*));
		output[0] = (uchar *)malloc(Nb * sizeof(uchar));
		for (int i = 1; i < _bi; i++)
			output[i] = output[i - 1] + _bj;
		memset(output[0], 0, (Nb)*sizeof(uchar));

		//_bi行数，_bj列数
		for (int i = 0; i < _bi; i++)
		{
			const uchar *src = variantMask.ptr<uchar>(i);
			for (int j = 0; j < _bj; j++)
			{
				input3[i][j] = src[j];
			}
		}

		for (int i = 0; i < _bi; i++)
		{
			const float *src = alpha.ptr<float>(i);
			for (int j = 0; j < _bj; j++)
			{
				input4[i][j] = src[j];
			}
		}


	{
		for (int i = 0; i < _bi; i++)//对应,rows
		{
			const cv::Vec4s *src = mapRecord.ptr<cv::Vec4s>(i);
			for (int j = 0; j < _bj; j++)//cols
			{
				for (int p = 0; p < 4; p++)
				{
					input5[i][j][p] = src[j][p];
				}
			}
		}
	}
#pragma endregion []
	//加载kernel函数
	//kernel = clCreateKernel(program, "helloworld", NULL);

#pragma region [clCreateBuffer]
	//创建用于GPU的缓冲区，前面四个是输入，根据需要更改，(Ns)* sizeof(uchar)是设定缓冲区大小， (void *)input1[0]是设定初始值，其余变量不用改
	input1_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Ns)* sizeof(uchar), (void *)input1[0], nullptr);
	input2_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Ns2)* sizeof(uchar), (void *)input2[0], nullptr);

	input6_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Ns)* sizeof(uchar), (void *)input6[0], nullptr);
	input7_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Ns)* sizeof(uchar), (void *)input7[0], nullptr);
	input8_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Ns)* sizeof(uchar), (void *)input8[0], nullptr);
	input9_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Ns2)* sizeof(uchar), (void *)input9[0], nullptr);
	input10_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Ns2)* sizeof(uchar), (void *)input10[0], nullptr);
	input11_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Ns2)* sizeof(uchar), (void *)input11[0], nullptr);

	input3_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Nb)* sizeof(uchar), (void *)input3[0], nullptr);
	input4_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (Nb)* sizeof(float), (void *)input4[0], nullptr);
	input5_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, 4 * (Nb)* sizeof(short), (void *)input5[0][0], nullptr);
	output_buffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, (Nb)* sizeof(uchar), NULL, NULL);
#pragma endregion []
	
#pragma region [clSetKernelArg]
	status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&input1_buffer);
	if (status != CL_SUCCESS)
	{
		cout << "SetKernelArg---status fail： 0 "  << endl;
	}

	status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&input2_buffer);
	if (status != CL_SUCCESS)
	{
		cout << "SetKernelArg---status fail： 1 "  << endl;
	}

	status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&input6_buffer);
	status = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&input7_buffer);
	status = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&input8_buffer);
	status = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&input9_buffer);
	status = clSetKernelArg(kernel, 6, sizeof(cl_mem), (void *)&input10_buffer);
	status = clSetKernelArg(kernel, 7, sizeof(cl_mem), (void *)&input11_buffer);

	status = clSetKernelArg(kernel, 8, sizeof(cl_mem), (void *)&input3_buffer);
	if (status != CL_SUCCESS)
	{
		cout << "SetKernelArg---status fail：8 "  << endl;
	}
	status = clSetKernelArg(kernel, 9, sizeof(cl_mem), (void *)&input4_buffer);
	if (status != CL_SUCCESS)
	{
		cout << "excCols---status fail： 3 "  << endl;
	}
	status = clSetKernelArg(kernel, 10, sizeof(cl_mem), (void *)&input5_buffer);
	if (status != CL_SUCCESS)
	{
		cout << "excCols---status fail： 3 "  << endl;
	}
	status = clSetKernelArg(kernel, 11, sizeof(cl_mem), (void *)&output_buffer);

	int x1 = _si;
	int x2 = _sj;
	int x3 = _si2;
	int x4 = _sj2;
	int x5 = _bi;
	int x6 = _bj;
	clSetKernelArg(kernel, 12, sizeof(int), &x1);
	clSetKernelArg(kernel, 13, sizeof(int), &x2);
	clSetKernelArg(kernel, 14, sizeof(int), &x3);
	clSetKernelArg(kernel, 15, sizeof(int), &x4);
	clSetKernelArg(kernel, 16, sizeof(int), &x5);
	clSetKernelArg(kernel, 17, sizeof(int), &x6);
	
	int reverse = 0;//目前不需要旋转
	clSetKernelArg(kernel, 18, sizeof(int), &reverse);
	//clSetKernelArg(kernel, 19, sizeof(int), &m);

	global_work_size[0] = Nb;
#pragma endregion []

	image = out_image.clone();
}

//
////执行旋转的kernel函数，输入为需要旋转的图对应flag
//void acc::exc_Reverse(const int flag, const int reverseIndex)
//{//////////////////////////////
//	status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&input1_buffer[1]);
//	status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&input2_buffer[1]);
//	status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&input3_buffer[1]);
//	status = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&input4_buffer[1]);
//	status = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&input5_buffer[1]);
//	//status = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&output_buffer[0]);
//	int x1 = _si[1];
//	int x2 = _sj[1];
//	int x3 = _si2[1];
//	int x4 = _sj2[1];
//	
//	clSetKernelArg(kernel, 6, sizeof(int), &x1);
//	clSetKernelArg(kernel, 7, sizeof(int), &x2);
//	clSetKernelArg(kernel, 8, sizeof(int), &x3);
//	clSetKernelArg(kernel, 9, sizeof(int), &x4);
//////////////////////
//	//设置output需要操作的数据部分，对output缓存数据改变
//	status = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&output_buffer[flag]);
//	if (status != CL_SUCCESS)
//	{
//		cout << "status fail： kernel旋转设置函数：output_buffer[ "<<flag<<"]" << endl;
//	}
//
//	//int x4 = -1;//将第四个int参数设置为flag
//	//clSetKernelArg(kernel, 9, sizeof(int), &x4);
//
//	//操作图、原图的长宽
//	int x5 = _bi[flag];
//	int x6 = _bj[flag];
//	clSetKernelArg(kernel, 10, sizeof(int), &x5);
//	clSetKernelArg(kernel, 11, sizeof(int), &x6);
//	
//	//设置旋转后，数据存放处
//	
//	status = clSetKernelArg(kernel, 12, sizeof(cl_mem), (void *)&reverse_buffer[reverseIndex]);
//	if (status != CL_SUCCESS)
//	{
//		cout << "reverse---status fail：kernel旋转设置函数：reverse_buffer[ " << flag << "]" << endl;
//	}
//	int reverse = -1;
//	clSetKernelArg(kernel, 13, sizeof(int), &reverse);
//	//该reverse和output大小
//	//输出图大小global_work_size
//	global_work_size[0] = Nb[flag];
//	status = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, global_work_size, NULL, 0, NULL, NULL);
//	if (status != CL_SUCCESS)
//	{
//		cout << "reverse---status fail： 执行kernel旋转函数：output_buffer[ " << flag << "]" << endl;
//	}
//	//旋转后存储的图放在reverse_buffer中
//
//#if defined ACC_TMP_TEST
//	cv::Mat imgNull; 
//	imgNull.create(cv::Size(_bi[flag], _bj[flag]), CV_8UC1);
//	cv::Mat img = imgNull.clone();
//	status = clEnqueueReadBuffer(commandQueue, reverse_buffer[reverseIndex], CL_TRUE, 0, Nb[flag] * sizeof(uchar), img.data, 0, NULL, NULL);
//	if (status != CL_SUCCESS)
//	{
//		cout << "reverse---status fail： 取出数据 " << flag << endl;
//	}
//	cv::imshow("reverse", img);
//
//#endif
//
//}
////执行每一列大图，前两个输入：改变了缓存
//cv::Mat acc::excCols(cv::Mat i1, cv::Mat i2, const cv::Point3i p)
//{
//	cv::Mat imgNull;
//	{
//	//	//p.x如何对应到reverse_buffer[]哪个数组---------------------------------
//	//	//对已存储需要旋转图的flag，一一对应
//	//	int reverseIndex0, reverseIndex1, single=0;
//	//	for (int i = 1; i < _reverseFlag.size(); i++)
//	//	{
//	//		//reverseFlag[]中数值唯一存在，不会出现重复，故single++正确
//	//		if (_reverseFlag[i] == p.x)
//	//		{
//	//			reverseIndex0 = i - 1;
//	//			single++;
//	//		}
//	//		else if (_reverseFlag[i] == p.y)
//	//		{
//	//			reverseIndex1 = i - 1;
//	//			single++;
//	//		}
//	//		if (single == 2)
//	//		{
//	//			break;
//	//		}
//	//	}
//	//		//需要对x, y寻找对应的reverse
//	//	status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&reverse_buffer[reverseIndex0]);
//	//	
//	//	status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&reverse_buffer[reverseIndex1]);
//
//		if (p.x > 0)
//		{
//			status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&output_buffer[p.x]);
//		}
//		else//表示小于0时候，原小图,就输入本次执行对应的数
//		{
//			status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&input1_buffer[p.z]);
//		}
//
//		if (p.y > 0)
//		{
//			status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&output_buffer[p.y]);
//		}
//		else//表示小于0时候，原小图,就输入本次执行对应的数
//		{
//			status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&input2_buffer[p.z]);
//		}
//
//		status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&input3_buffer[p.z]);
//		if (status != CL_SUCCESS)
//		{
//			cout << "excCols---status fail： 3 " << p.z << endl;
//		}
//		status = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&input4_buffer[p.z]);
//		if (status != CL_SUCCESS)
//		{
//			cout << "excCols---status fail： 4 " << p.z << endl;
//		}
//		status = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&input5_buffer[p.z]);
//		if (status != CL_SUCCESS)
//		{
//			cout << "excCols---status fail： 5 " << p.z << endl;
//		}
//		status = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&output_buffer[p.z]);
//
//		int x1 = _si[p.z];
//		int x2 = _sj[p.z];
//		int x3 = _si2[p.z];
//		int x4 = _sj2[p.z];
//		int x5 = _bi[p.z];
//		int x6 = _bj[p.z];
//		clSetKernelArg(kernel, 6, sizeof(int), &x1);
//		clSetKernelArg(kernel, 7, sizeof(int), &x2);
//		clSetKernelArg(kernel, 8, sizeof(int), &x3);
//		clSetKernelArg(kernel, 9, sizeof(int), &x4);
//		clSetKernelArg(kernel, 10, sizeof(int), &x5);
//		clSetKernelArg(kernel, 11, sizeof(int), &x6);
//////////////////
//		//status = clSetKernelArg(kernel, 12, sizeof(cl_mem), (void *)&input1_buffer[1]);
//		//if (status != CL_SUCCESS)
//		//{
//		//	cout << "	excCols---status fail：kernel旋转设置函数" << endl;
//		//}
//		int reverse = 1;//////改为cl文件中判断
//		clSetKernelArg(kernel, 12, sizeof(int), &reverse);
////////////////
//		global_work_size[0] = Nb[p.z];//_bi[p.z]*_bj[p.z];
//
//		//算法具体执行，无需更改
//		status = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, global_work_size, NULL, 0, NULL, NULL);
//		if (status != CL_SUCCESS)
//		{
//			cout << "excCols---status fail： 执行kernel函数 " << p.z << endl;
//		}
//		//当前为最后一次执行时，取出处理后的数据——拼接后的图像
//		if (p.z == _FLAG_FINAL)
//		{//status = -5报错，在设备上资源内存定位失败
//			status = clEnqueueReadBuffer(commandQueue, output_buffer[p.z], CL_TRUE, 0, Nb[p.z] * sizeof(uchar), image[p.z].data, 0, NULL, NULL);
//			if (status != CL_SUCCESS)
//			{
//				cout << "excCols---status fail： 取出数据 " << p.z << endl;
//			}
//			return image[p.z];
//		}
//		else
//		{
////#if defined ACC_TMP_TEST
////			status = clEnqueueReadBuffer(commandQueue, output_buffer[p.z], CL_TRUE, 0, Nb[p.z] * sizeof(uchar), image[p.z].data, 0, NULL, NULL);
////			if (status != CL_SUCCESS)
////			{
////				cout << "excCols---status fail： 取出数据 " << p.z << endl;
////			}
////			imgNull = image[p.z];
////#endif
//			return imgNull;
//		}
//	}
//}

//执行两个图：输入原小图和输入存在output中两种情况
cv::Mat acc::excMN(const vector<cv::Mat> imgs, int m, int n)
{
	
		status = clEnqueueWriteBuffer(commandQueue, input1_buffer, CL_TRUE, 0, Ns * sizeof(uchar), imgs[0].data, NULL, NULL, NULL);
		if (status != CL_SUCCESS)
		{
			cout << "exc---status fail：第1个参数buffer"  << endl;
		}
	
		status = clEnqueueWriteBuffer(commandQueue, input2_buffer, CL_TRUE, 0, Ns2 * sizeof(uchar), imgs[1].data, NULL, NULL, NULL);
		if (status != CL_SUCCESS)
		{
			cout << "exc---status fail：第二个参数buffer"  << endl;
		}
		if (imgs.size() >= 3)
		{
			status = clEnqueueWriteBuffer(commandQueue, input6_buffer, CL_TRUE, 0, Ns * sizeof(uchar), imgs[2].data, NULL, NULL, NULL);
			if (imgs.size() >= 4)
			{
				status = clEnqueueWriteBuffer(commandQueue, input7_buffer, CL_TRUE, 0, Ns * sizeof(uchar), imgs[3].data, NULL, NULL, NULL);
				if (imgs.size() >= 5)
				{
					status = clEnqueueWriteBuffer(commandQueue, input8_buffer, CL_TRUE, 0, Ns * sizeof(uchar), imgs[4].data, NULL, NULL, NULL);
					if (imgs.size() >= 6)
					{
						status = clEnqueueWriteBuffer(commandQueue, input9_buffer, CL_TRUE, 0, Ns * sizeof(uchar), imgs[5].data, NULL, NULL, NULL);
						if (imgs.size() >= 7)
						{
							status = clEnqueueWriteBuffer(commandQueue, input10_buffer, CL_TRUE, 0, Ns * sizeof(uchar), imgs[6].data, NULL, NULL, NULL);
							if (imgs.size() >= 8)
							{
								status = clEnqueueWriteBuffer(commandQueue, input11_buffer, CL_TRUE, 0, Ns * sizeof(uchar), imgs[7].data, NULL, NULL, NULL);
							}
						}
					}
				}
			}
		}
		//算法具体执行，无需更改
		status = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, global_work_size, NULL, 0, NULL, NULL);
		if (status != CL_SUCCESS)
		{
			cout << "exc---status fail： 执行kernel函数 " << endl;
		}
		//当前为最后一次执行时，取出处理后的数据——拼接后的图像
		if (_FLAG_FINAL)
		{
			status = clEnqueueReadBuffer(commandQueue, output_buffer, CL_TRUE, 0, Nb * sizeof(uchar), image.data, 0, NULL, NULL);
			if (status != CL_SUCCESS)
			{
				cout << "exc---status fail： 取出数据 "<< endl;
			}
			return image;
		}
		else
		{
#if defined ACC_TMP_TEST
			status = clEnqueueReadBuffer(commandQueue, output_buffer, CL_TRUE, 0, Nb * sizeof(uchar), image.data, 0, NULL, NULL);
		    if (status != CL_SUCCESS)
			{
				cout << "exc---status fail： 取出数据 "  << endl;
			}
		return image;
#endif
		}
}

//最后可以释放一下资源，不过应该用不到，毕竟是实时处理
void acc::release_source(const int k)
{
	status = clReleaseKernel(kernel);//*Release kernel.
	status = clReleaseProgram(program);    //Release the program object.
	status = clReleaseMemObject(input1_buffer);
	status = clReleaseMemObject(input2_buffer);
	status = clReleaseMemObject(input3_buffer);
	status = clReleaseMemObject(input4_buffer);
	status = clReleaseMemObject(output_buffer);
	status = clReleaseCommandQueue(commandQueue);//Release  Command queue.
	status = clReleaseContext(context);//Release context.
	free(devices);
}

