__kernel void helloworld(__global uchar* input1, __global uchar* input2, __global uchar* input3, __global uchar* input4, __global uchar* input5, __global uchar* input6, __global uchar* input7, __global uchar* input8 ,__global uchar* input9, __global float* input10, __global short* input11, __global uchar* output, const int si, const int sj, const int s2i, const int s2j, const int bi, const int bj, const int reverse)
{
	//要使得对任意一列进行加速，kernel函数输入参数是否可以改为动态的输入图片数组，以及开辟内存？
	//取得index（下标），由于代码没有像素间的关系，直接使用一维数组处理，前面传入也是按照一维数组处理   /*__global uchar* output_reverse,*/ 
     int index = get_global_id(0);
	 
		//具体的操作，理论上就是将循环体加入进来，然后将二维坐标更改为一维坐标
		short p0 = input11[index * 4];
		short p1 = input11[index * 4 + 1];
		short p2 = input11[index * 4 + 2];
		short p3 = input11[index * 4 + 3];
		if (input9[index] % 20 == 0)
		{
			int idx = input9[index] / 20;

			if (p1 > 0 && p1 < si && p0>0 && p0 < sj)
			{
				if (idx == 1)
					output[index] = input1[p1*sj + p0];
				else if (idx == 2)
					output[index] = input2[p1*sj + p0];
				else if (idx == 3)
					output[index] = input3[p1*sj + p0];
				else if (idx == 4)
					output[index] = input4[p1*sj + p0];
				else if (idx == 5)
					output[index] = input5[p1*sj + p0];
				else if (idx == 6)
					output[index] = input6[p1*sj + p0];
				else if (idx == 7)
					output[index] = input7[p1*sj + p0];
				else  //(idx == 8)
					output[index] = input8[p1*sj + p0];
			}
		}
		if (input9[index] % 20 == 5)
		{
			uchar value1 = 0, value2 = 0;

			int idx = input9[index] / 20;
			if (idx == 1)
			{
				if (p1 > 0 && p1 < si && p0>0 && p0 < sj)
				{
					value1 = input1[p1*sj + p0];
				}
				if (p3 > 0 && p3 < si && p2>0 && p2 < sj)//暂时都是小图拼接
				{
					value2 = input2[p3*s2j + p2];//第二张输入对应ij
				}
				output[index] = value1 *(1 - input10[index]) + value2 * input10[index];
			}
			else if (idx == 2)
			{
				if (p1 > 0 && p1 < si && p0>0 && p0 < sj)
				{
					value1 = input2[p1*sj + p0];
				}
				if (p3 > 0 && p3 < si && p2>0 && p2 < sj)//暂时都是小图拼接
				{
					value2 = input3[p3*s2j + p2];//第二张输入对应ij
				}
				output[index] = value1 *(1 - input10[index]) + value2 * input10[index];
			}
			else if (idx == 3)
			{
				if (p1 > 0 && p1 < si && p0>0 && p0 < sj)
				{
					value1 = input3[p1*sj + p0];
				}
				if (p3 > 0 && p3 < si && p2>0 && p2 < sj)//暂时都是小图拼接
				{
					value2 = input4[p3*s2j + p2];//第二张输入对应ij
				}
				output[index] = value1 *(1 - input10[index]) + value2 * input10[index];
			}
			else if (idx == 4)
			{
				if (p1 > 0 && p1 < si && p0>0 && p0 < sj)
				{
					value1 = input4[p1*sj + p0];
				}
				if (p3 > 0 && p3 < si && p2>0 && p2 < sj)//暂时都是小图拼接
				{
					value2 = input5[p3*s2j + p2];//第二张输入对应ij
				}
				output[index] = value1 *(1 - input10[index]) + value2 * input10[index];
			}
			else if (idx == 5)
			{
				if (p1 > 0 && p1 < si && p0>0 && p0 < sj)
				{
					value1 = input5[p1*sj + p0];
				}
				if (p3 > 0 && p3 < si && p2>0 && p2 < sj)//暂时都是小图拼接
				{
					value2 = input6[p3*s2j + p2];//第二张输入对应ij
				}
				output[index] = value1 *(1 - input10[index]) + value2 * input10[index];
			}
			else if (idx == 6)
			{
				if (p1 > 0 && p1 < si && p0>0 && p0 < sj)
				{
					value1 = input6[p1*sj + p0];
				}
				if (p3 > 0 && p3 < si && p2>0 && p2 < sj)//暂时都是小图拼接
				{
					value2 = input7[p3*s2j + p2];//第二张输入对应ij
				}
				output[index] = value1 *(1 - input10[index]) + value2 * input10[index];
			}
			else //if (idx == 7)
			{
				if (p1 > 0 && p1 < si && p0>0 && p0 < sj)
				{
					value1 = input7[p1*sj + p0];
				}
				if (p3 > 0 && p3 < si && p2>0 && p2 < sj)//暂时都是小图拼接
				{
					value2 = input8[p3*s2j + p2];//第二张输入对应ij
				}
				output[index] = value1 *(1 - input10[index]) + value2 * input10[index];
			}
		}
}