#include "StitchManage.h"

#define TIME_TEST
//#define DEBUG_TEST 
//#define WRITE_TEST
using namespace cv;
using namespace std;

/*
对参数的输入进行解析，相关设置，将图片名称push到img_name中
*/
//逆时针旋转90度
Mat transAndFlip(Mat img)
{
	Mat img_trans, img_transFlip;
	//逆时针旋转90°
	flip(img, img_trans, 1);
	transpose(img_trans, img_transFlip);

	return img_transFlip;
}
//顺时针旋转90度
Mat transAndFlipClockwise(Mat img)
{
	Mat img_trans, img_transFlip;
	//逆时针旋转90°
	transpose(img, img_trans);
	flip(img_trans, img_transFlip, 1);

	return img_transFlip;
}

/*************************************************
//  Method:    convertTo3Channels
//  Description: 将单通道图像转为三通道图像
//  Returns:   cv::Mat
//  Parameter: binImg 单通道图像对象
*************************************************/
Mat convertTo3Channels(const Mat& binImg)
{
	Mat three_channel = Mat::zeros(binImg.rows, binImg.cols, CV_8UC3);
	vector<Mat> channels;
	for (int i = 0; i<3; i++)
	{
		channels.push_back(binImg);
	}
	merge(channels, three_channel);
	return three_channel;
}

StitchManage::StitchManage()
{
	FLAG = 0;
}

StitchManage::~StitchManage()
{
}
//改为拼接一列的参数记录（拼接两张图的记录参数）
//
Mat StitchManage::paramStitched(const vector<cv::Mat> pics
	, cv::Mat& variantMask, cv::Mat& alpha, cv::Mat& mapRecord, cv::Mat& out_image, vector<Size>& ij
	, vector<cv::Mat> masks, Mat &stitched_mask, const float resizeScale)
{

	///////////////////////////////////////////////////
	//变为三通道图像
	vector<Mat> pics_3C;
	CV_Assert(pics[0].type() != CV_8UC3);
	for (int i = 0; i < pics.size(); i++)
	{
		Mat tmp = convertTo3Channels(pics[i]);
		pics_3C.push_back(tmp);
	}
	
	CV_Assert(pics_3C[0].type() == CV_8UC3);

	//拼接内部函数需要图像类型为：CV_8UC3
	StitchedPic stitchTmp1;
	
#if defined TIME_TEST
	int64 t_stitch = getTickCount();
#endif
	Mat tmp1 = stitchTmp1.stitch(pics_3C, masks, resizeScale);//得到的图为三通道----------------------------------
	stitched_mask = stitchTmp1._dst_mask;///////////////存储本次拼接结果图的mask
#if defined TIME_TEST
	std::cout << "		拼接耗时：" << (getTickCount() - t_stitch) / (getTickFrequency()) << "秒" << std::endl;
#endif

	tmp1.convertTo(tmp1, CV_8U);
#if defiend DEBUG_TEST
	namedWindow("each cols stitch result", WINDOW_NORMAL);
	imshow("each cols stitch result", tmp1);
	waitKey(1);
#endif
#if defined WRITE_TEST
	imwrite("colsstitchresult.jpg", tmp1);
#endif
	///////////////////////////////////////////////////////

	stitchTmp1.cornersMin();
	//映射之前确定重叠区域是否需要映射

	Size resultSize_m = tmp1.size();
	//cout << "resultSize:" << resultSize_m << endl;
#if defined(TIME_TEST)
	int64 t_r = cvGetTickCount();
#endif
	//记录不同区域的不同值mask
	variantMask = stitchTmp1.mapMask(10);
#if defined(WRITE_TEST)
	imwrite("a_mask.jpg", variantMask);
#endif
	//记录重叠区域的alpha权重值
	//计算重叠区域的alpha
	vector<int> values_L;
	for (int i = 1; i < pics.size(); i++)
	{

		values_L.push_back(_INCREASEVALUE*i + _OVERLAPVALUE);
	}
	stitchTmp1.countAlpha(variantMask, values_L, alpha);
#if defined(WRITE_TEST)
	cv::imwrite("alpha.jpg", alpha);
#endif
#if defined(WRITE_TEST)
	Mat alphaMat_ = alpha;
	Mat writeAlpha;
	alphaMat_.convertTo(writeAlpha, CV_8U, 255);

	cv::imwrite("alphaMat_visual.jpg", writeAlpha);//准备可视化alpha这部分数值,此处alpha是单通道
#endif
	//此处开始准备gpu运算部分相关参数,初始化
	RecordMap record;

	vector<int> values_single;
	for (int i = 1; i<= pics.size(); i++)//从1开始
	{
		values_single.push_back(_INCREASEVALUE*i);
	}
	//record类中，重新设置输入图大小
	vector<Mat> pics_resize;
	for (int i = 0; i < pics.size(); i++)
	{
		Mat tmp;
		resize(pics[i], tmp, Size(pics[i].size().width *resizeScale, pics[i].size().height *resizeScale));
		pics_resize.push_back(tmp);
	}
	vector<Size> imgSizes;
	for (int i = 0; i < pics_resize.size(); i++)
	{
		imgSizes.push_back(pics_resize[i].size());
	}
	record.setValue(variantMask, stitchTmp1, imgSizes, resultSize_m, values_single, values_L);
	//record.setImgMat(pics);//记录参数部分，记录的是单通道
	//得到映射实时地输入，记录图mat
	
	mapRecord = record.Record();
#if defined(WRITE_TEST)
	cv::imwrite("mapRecord.jpg", mapRecord);
#endif

	out_image.create(tmp1.size(), CV_8UC1);
	out_image.setTo(Scalar::all(255));

	/*for (int i = 0; i < pics.size(); i++)
	{
		ij.push_back(pics[i].size());
	}*/
	ij.push_back(pics_resize[0].size());//默认原小图都一样大
	ij.push_back(tmp1.size());
	//int iS = pic0.rows;//对应行数
	//int jS = pic0.cols;

	//int iS2 = pic1.rows;
	//int jS2 = pic1.cols;

	//int iB = tmp1.rows;
	//int jB = tmp1.cols;//传入（i, j)表示：行数，列数

	CV_Assert(variantMask.type() == CV_8UC1);
	CV_Assert(alpha.type() == CV_32FC1);
	CV_Assert(mapRecord.type() == CV_16SC4);
	CV_Assert(out_image.type() == CV_8UC1);

#if defined DEBUG_TEST
	Mat mapRecordTest = record.mapping(alpha, variantMask, mapRecord, pics_resize);

	cvNamedWindow("cpuMapTest", WINDOW_NORMAL);
	imshow("cpuMapTest", mapRecordTest);
	waitKey(1);
	imwrite("cpuMapTest.jpg", mapRecordTest);
#endif
	return tmp1;//已转化为8U，拼接完成后还是三通道
}

//输入一列待拼接图片
Mat StitchManage::initColsStitch(vector<Mat> pics, vector<Mat> masks, Mat &stitched_mask, const float resizeScale)
{
	Mat stitched;
	Mat variantMask, alpha, mapRecord, out_image;
	vector<Size> ij;

#if defined TIME_TEST
	int64 t_paramStitch = cvGetTickCount();
#endif
	//对一列图进行拼接，记录参数
	stitched = paramStitched(pics, variantMask, alpha, mapRecord, out_image, ij, masks, stitched_mask, resizeScale);
#if defined TIME_TEST
		std::cout << endl << "	每列拼接-总记录耗时" << " 第 " << FLAG << "轮：" << (cvGetTickCount() - t_paramStitch) / (cvGetTickFrequency() * (1e6)) << "秒" << std::endl;
#endif
		
#if defined TIME_TEST
	int64 t_accInit = cvGetTickCount();
#endif
	acc_.initial(ij, pics.size(), variantMask, alpha, mapRecord, out_image, FLAG);
#if defined TIME_TEST
	std::cout << "		每次acc初始化每对图" << " 第 " << FLAG << "轮：" << (cvGetTickCount() - t_accInit) / (cvGetTickFrequency() * (1e6)) << "秒" << std::endl;
#endif
	return stitched;
}
//m行n列，对每一列先拼接
//vector<cv::Mat> imgs：输入m行n列图片
//CPU拼接并相关记录，输出结果
Mat StitchManage::initMN(const vector<cv::Mat> imgs, int m, int n, vector<Point3i>& excPara, vector<int>& reverseFlag, const float resizeScale)
{
	vector<cv::Mat> colsStitchedImg;

	vector<Mat> masks;

	Mat stitched, stitched_mask_m;
	vector<Mat> finalPics;
	vector<Mat> finalMasks;;

	for (int i = 0; i < n; i++)
	{
		vector<Mat> pic_col;
		for (int j = 0; j < m; j++)
		{
			pic_col.push_back(imgs[j + i*m]);
		}
		for (int i = 0; i < m; i++)
		{
			Mat mask;
			mask.create(Size(pic_col[i].size().width*resizeScale, pic_col[i].size().height*resizeScale), CV_8UC1);
			mask.setTo(Scalar::all(255));//原小图没有遮罩区域
			masks.push_back(mask);
		}
		stitched = initColsStitch(pic_col, masks, stitched_mask_m, resizeScale);
		masks.clear();
		finalPics.push_back(stitched);
		finalMasks.push_back(stitched_mask_m);
	}

	//顺时针旋转后，再对每一列大图进行拼接记录

	//此处初始化，旋转过，对应执行函数也需要旋转，即gpu缓存中output
	//旋转过后的size
	vector<Size> flagSize;
	vector<Mat> reverseImgSize;
	for (int i = 0; i < n; i++)
	{
		Mat tmp = finalPics[i];
		tmp = transAndFlipClockwise(tmp);
		finalPics[i] = tmp;
		Mat tmp_mask = finalMasks[i];
		tmp_mask = transAndFlipClockwise(tmp_mask);
		finalMasks[i] = tmp_mask;

	}
	Mat finalStitched_mask;
	if (n > 1)
	{
		stitched = initColsStitch(finalPics, finalMasks, finalStitched_mask, resizeScale);
	}
	else
	{
		stitched = finalPics[0];
	}

	return stitched;
}

////输入待旋转的每一列图的flag
//void StitchManage::initReverseData(const vector<int> reverseFlag, const vector<Size> flagSize)
//{
//	//存储的旋转flag从1开始，reverseFlag[0]记录此前一共有多少flag对
//	//reverse_buffer[i-1] ， i-1 -------reverseFlag[i] 对应关系：第一列为[0],第二列为[1]........
//	for (int i = 1; i < reverseFlag.size(); i++)
//	{
//		//i表示需要初始化的reverseData处于数组中哪部分，flag对应大图的大小
//		acc_.initial_dataReverseUnit(flagSize[i-1].width,flagSize[i-1].height , i-1);//i=2，超过大小flagSize
//		/////////////////////////////////////////记录参数
//	}
//}

//将图片处理为，每一列放置，每一列旋转好为拼接方向
vector<cv::Mat> REVERSEImgs(vector<cv::Mat>& imgs, const int m, const int n)
{	//预想是：传入图片是按照每一列排列
	vector<cv::Mat> resultImgs;
	for (int nn = 0; nn < n; nn++)
	{	//对于每一列都是逆时针旋转90
		for (int mm = 0; mm < m; mm++)
		{
			Mat tmp = transAndFlip(imgs[mm + m*nn]);
			resultImgs.push_back(tmp);
		}
	}
	return resultImgs;
}
//传入m行n列图片, REVERSR==1表示传入图片需要旋转
Mat StitchManage::stitchMN(vector<vector<cv::Mat> > in_imgs, const bool REVERSE,const float resizeScale)//, int m, int n, , const float resizeScale = 1)
{
	
	vector<cv::Mat> imgs;
	int m, n;
	m = in_imgs.size();
	n = in_imgs[0].size();
	for (int i = 0; i < n; i++){
		for (int j = 0; j < m; j++){
			imgs.push_back(in_imgs[j][i]);
		}
	}
	if (n != 1)
	{
		std::cout << "stitchMN inout para wrong, now n must be 1!one cols!" << endl;
	}
	vector<cv::Mat> m_imgs;
	if (REVERSE == true)
	{
		m_imgs = REVERSEImgs(imgs, m, n);
	}
	else
	{
		m_imgs = imgs;
	}
	vector<cv::Point3i> para;
	vector<int> reversePara;
	//先要初始化，实际拼接记录一系列参数，映射图
	if (FLAG == 0)
	{
#if defined TIME_TEST
		int64 t_initMN = getTickCount();
#endif
		acc_.initial_head();
		Mat stitched;

		stitched = initMN(m_imgs, m, n, para, reversePara, resizeScale);

#if defined TIME_TEST
		std::cout << "总初始化initMN耗时：" << (getTickCount() - t_initMN) / (getTickFrequency()) << "秒" << std::endl;
#endif
		
#if defined DEBUG_TEST
		namedWindow("init cpu stitched final", WINDOW_NORMAL);
		imshow("init stitched final", stitched);
		waitKey(1);
#endif
		
		FLAG = 1;
	}
	else
	{
		FLAG++;
	}
#if defined TIME_TEST
	int64 t_excMN = cvGetTickCount();
#endif
	//缩小后映射
	vector<Mat> m_imgs_resize;
	for (int i = 0; i < m_imgs.size(); i++)
	{
		Mat tmp;
		resize(m_imgs[i], tmp, Size(m_imgs[i].size().width *resizeScale, m_imgs[i].size().height *resizeScale));
		m_imgs_resize.push_back(tmp);
	}

	Mat result = acc_.excMN(m_imgs_resize, m, n);
	//对于一列图，结果需要旋转回来
	if (REVERSE == true)
	{
		result = stitch::transAndFlipClockwise(result);
	}

#if defined TIME_TEST
	std::cout << "TIME:执行MN映射拼接耗时：" << (cvGetTickCount() - t_excMN) / (cvGetTickFrequency() * 1e3) << " 毫 秒" << std::endl;
#endif
	return result;
}