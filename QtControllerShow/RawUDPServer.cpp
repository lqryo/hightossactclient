#include "RawUDPServer.h"

#include <iphlpapi.h>
#include <ws2tcpip.h>
#include <mstcpip.h>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include "TCPServer.h"

#include "Utils.h"

// Link with Iphlpapi.lib
#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(lib,"Ws2_32.lib")

#define WORKING_BUFFER_SIZE 15000
#define MAX_TRIES 3

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

#define SIZE_UDP 0	// 8 actually, here we don't use UDP header

uint16_t cksum(const u_char *const buf, size_t size)
{
	uint32_t sum;
	uint16_t *p = (uint16_t *)buf;

	sum = 0;
	while (size > 1) {
		sum += *p++;
		size -= 2;
	}

	// padding as needed
	if (size == 1) {
		sum += *((u_char *)p);
	}

	while (sum >> 16)
		sum = (sum & 0xFFFF) + (sum >> 16);

	return (uint16_t)((~sum) & 0xFFFF);
}

void CRawUDPServer::run()
{
	while (true) {
		broadcast();

		Sleep(cTimeoutBroadcast);
	}
}

bool CRawUDPServer::broadcast()
{
	ControlMsg msg;
//	QString port = QString::number(3721 + CTCPServer::connect_num, 10);
	QString port = QString::number(3721, 10);
	msg.type = CtrlTypeInit;
//	msg.text = "3721";
	msg.text = port;
	msg.len = msg.text.size();

	return broadcast(msg);

}

bool CRawUDPServer::broadcast(const ControlMsg &controlMsg)
{
	char buf[1024];
	char *pBuf = buf;

	memcpy(pBuf, &controlMsg.type, sizeof(controlMsg.type));
	pBuf += sizeof(controlMsg.type);

	memcpy(pBuf, &controlMsg.len, sizeof(controlMsg.len));
	pBuf += sizeof(controlMsg.len);

	std::string msgStr = controlMsg.text.toLocal8Bit();
	memcpy(pBuf, msgStr.data(), controlMsg.len);

	return broadcast(buf, controlMsg.size());

}

bool CRawUDPServer::broadcast(const char *data, uint16_t len)
{
	char buf[1024];						// maximum buffer size is 1024, so len < 1024 

	static uint32_t k = 1;

	// Set UDP header
	// ...

	// Set payload
	int payload = sizeof(len) + len;
	if (payload >= 1024 - SIZE_IP - SIZE_UDP) {
		error("Payload overflow, payload = " + std::to_string(payload));
		return false;
	}
	char *pData = &buf[SIZE_IP + SIZE_UDP];
	memcpy(pData, &len, sizeof(len));
	memcpy(pData + sizeof(len), data, len);

	int sizeDatagram = SIZE_IP + SIZE_UDP + payload;

	iphdr_t *v4hdr = (iphdr_t *)buf; //lets point to the ip header portion
	v4hdr->ip_vhl = 0x45;
	v4hdr->ip_tos = 0;
	v4hdr->ip_len = htons(sizeDatagram);
	v4hdr->ip_id = htons(0xF96D);
	v4hdr->ip_off = htons(IP_DF);    // don't fragment
	v4hdr->ip_ttl = 255;				 // default TTL
	v4hdr->ip_p = IPPROTO_RAWUDP;
	v4hdr->ip_sum = 0;
	inet_pton(AF_INET, _ip.c_str(), &v4hdr->ip_src);
	v4hdr->ip_dst = dest.sin_addr;

	v4hdr->ip_sum = cksum((const u_char *)v4hdr, sizeDatagram);

	if ((sendto(_sockfd, buf, sizeDatagram, 0,
		(SOCKADDR *)&dest, sizeof(dest))) == SOCKET_ERROR)
	{
		printf("Error sending Packet : %d", WSAGetLastError());
		return false;
	}
	else {
		printf(" %d packets send, payload=%d, len=%d.\r", k++, payload, len);
		return true;
	}

}

CRawUDPServer::~CRawUDPServer()
{

}

/*
Refer to https://msdn.microsoft.com/en-us/library/windows/desktop/aa366058(v=vs.85).aspx
*/
void CRawUDPServer::getLocalIP()
{
	DWORD dwSize = 0;
	DWORD dwRetVal = 0;

	unsigned int i = 0;

	// Set the flags to pass to GetAdaptersAddresses
	ULONG flags = GAA_FLAG_INCLUDE_PREFIX;

	// default to IPv4 address family
	ULONG family = AF_INET;

	LPVOID lpMsgBuf = NULL;

	PIP_ADAPTER_ADDRESSES pAddresses = NULL;
	ULONG outBufLen = 0;
	ULONG Iterations = 0;

	PIP_ADAPTER_ADDRESSES pCurrAddresses = NULL;
	PIP_ADAPTER_UNICAST_ADDRESS pUnicast = NULL;

	// Allocate a 15 KB buffer to start with.
	outBufLen = WORKING_BUFFER_SIZE;

	do {

		pAddresses = (IP_ADAPTER_ADDRESSES *)MALLOC(outBufLen);
		if (pAddresses == NULL) {
			printf
				("Memory allocation failed for IP_ADAPTER_ADDRESSES struct\n");
			exit(1);
		}

		dwRetVal =
			GetAdaptersAddresses(family, flags, NULL, pAddresses, &outBufLen);

		if (dwRetVal == ERROR_BUFFER_OVERFLOW) {
			FREE(pAddresses);
			pAddresses = NULL;
		}
		else {
			break;
		}

		Iterations++;

	} while ((dwRetVal == ERROR_BUFFER_OVERFLOW) && (Iterations < MAX_TRIES));

	if (dwRetVal == NO_ERROR) {
		// If successful, output some information from the data we received
		pCurrAddresses = pAddresses;
		while (pCurrAddresses) {
			if (pCurrAddresses->OperStatus != IfOperStatusUp) {			// Interface should be up state
				pCurrAddresses = pCurrAddresses->Next;
				continue;
			}
			if (!(pCurrAddresses->IfType == IF_TYPE_ETHERNET_CSMACD		// Interface should be Ethernet
				|| pCurrAddresses->IfType == IF_TYPE_IEEE80211)) {		//   or IEEE 802.11 Wireless 
				pCurrAddresses = pCurrAddresses->Next;
				continue;
			}

			printf("Adapter friendly name: %wS\n", pCurrAddresses->FriendlyName);

			pUnicast = pCurrAddresses->FirstUnicastAddress;
			if (pUnicast != NULL) {
				for (i = 0; pUnicast != NULL; i++) {
					struct sockaddr_in *sa = (struct sockaddr_in *)pUnicast->Address.lpSockaddr;

					char ipstr[20];
					inet_ntop(AF_INET, &sa->sin_addr, ipstr, 20);

					if (_ip == "") {
						_ip = std::string(ipstr);
					}
					else {}

					pUnicast = pUnicast->Next;
				}
				printf("Number of Unicast Addresses: %d\n", i);
			}
			else
				printf("No Unicast Addresses\n");

			pCurrAddresses = pCurrAddresses->Next;
		}
	}
	else {
		printf("Call to GetAdaptersAddresses failed with error: %d\n",
			dwRetVal);
		if (dwRetVal == ERROR_NO_DATA)
			printf("\tNo addresses were found for the requested parameters\n");
		else {

			if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL, dwRetVal, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				// Default language
				(LPTSTR)& lpMsgBuf, 0, NULL)) {
				printf("\tError: %s", lpMsgBuf);
				LocalFree(lpMsgBuf);
				if (pAddresses)
					FREE(pAddresses);
				exit(1);
			}
		}
	}

	if (pAddresses) {
		FREE(pAddresses);
	}
}

void CRawUDPServer::init()
{
	getLocalIP();
	printf("Set local ip to %s\n", _ip.c_str());

	int optval;

	//Initialise Winsock
	WSADATA wsock;
	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsock) != 0)
	{
		fprintf(stderr, "WSAStartup() failed\n");
		exit(EXIT_FAILURE);
	}
	printf("Initialised successfully.\n");

	//Create Raw UDP Packet
	printf("\nCreating Raw UDP Socket...");
	if ((_sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_IP)) == SOCKET_ERROR)
	{
		printf("Creation of raw socket failed.\n");
		exit(0);
	}
	printf("Raw UDP Socket Created successfully.\n");

	printf("\nSetting the socket in RAW mode...");
	optval = 1;
	if (setsockopt(_sockfd, IPPROTO_IP, IP_HDRINCL, (char *)&optval, sizeof(optval)) == SOCKET_ERROR)
	{
		printf("failed to set socket in raw mode.\n");
		exit(0);
	}
	printf("Successful.\n");

	printf("\nSetting the socket in broadcast mode...");
	optval = 1;
	if (setsockopt(_sockfd, SOL_SOCKET, SO_BROADCAST, (char *)&optval, sizeof(optval)) == SOCKET_ERROR)
	{
		printf("failed to set socket in broadcast mode.\n");
		exit(0);
	}
	printf("Successful.\n");

	dest.sin_family = AF_INET;
	dest.sin_port = htons(cUDPClientPort); //your destination port
	inet_pton(AF_INET, "255.255.255.255", &dest.sin_addr);
}
