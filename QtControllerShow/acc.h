#include <CL/cl.h>
#include "tool.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <cstdio>   
#include<iostream>
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc.hpp"

using namespace std; 

class acc
{
public:
	void initial_head();

	//初始化opencl环境
	void initial(const vector<cv::Size> ij, const int k, const cv::Mat variantMask, const cv::Mat alpha, const cv::Mat mapRecord, const cv::Mat out_image, const int INIT_FLAG);
	//初始化数据
	
	cv::Mat excMN(vector<cv::Mat> imgs, int m, int n);

	//执行opencl函数
	//执行每一列的拼接，输入需要旋转，换了内存数据
	//cv::Mat excCols(cv::Mat i1, cv::Mat i2, const cv::Point3i p);

	void exc_Reverse(const int i, const int reverseIndex);

	//释放opencl占用的资源
	void release_source(const int k);

	//下面是一系列参数定义，不要更改
	cl_int status;
	cl_platform_id platform;
	cl_uint numPlatforms = 2;
	cl_uint numDevices = 0;
	cl_device_id *devices;
	cl_context context;
	cl_command_queue commandQueue;
	cl_program program;
	cl_kernel kernel;

	uchar** input1;
	uchar** input2;
	//更多的输入图
	uchar** input6;
	uchar** input7; 
	uchar** input8;
	uchar** input9;
	uchar** input10;
	uchar** input11;

	uchar** input3;
	float** input4;
	short*** input5;
	uchar** output;
	
	cl_mem input1_buffer;
	cl_mem input2_buffer;
	//更多的输入图
	cl_mem input6_buffer;
	cl_mem input7_buffer; 
	cl_mem input8_buffer;
	cl_mem input9_buffer;
	cl_mem input10_buffer;
	cl_mem input11_buffer; 

	cl_mem input3_buffer;
	cl_mem input4_buffer;
	cl_mem input5_buffer;
	cl_mem output_buffer;
	//global_size数组，长度表示getindex的个数，值表示每个getindex的数目
	size_t global_work_size[1];
	int Ns, Ns2, Nb;
	int _si, _si2, _sj2, _sj, _bi, _bj;

	cv::Mat image;

	int _FLAG_FINAL;
	vector<cv::Point3i> _para;
	//需要顺时针旋转回来的，拼接结果图
	vector<int> _reverseFlag;

	//转置复制的内存部分,对应共有多少列,暂时和前面多少次两张拼接一样，写死了
	uchar** output_reverse[4];
	cl_mem reverse_buffer[4];//顶多四列相机？
	uchar** output_reverse_R[4];
	cl_mem reverse_buffer_R[4];
	
	cv::Mat _reverseImgSize[4];//和image相似
};