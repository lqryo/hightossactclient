#ifndef PARABOLICINFO_H_
#define PARABOLICINFO_H_
#include <qstring.h>

typedef struct parabolicInfo {
	long _time ;
	int _cameraID ;
	int _apartmentFloor ;
	int _apartmentUnit ;
	int _apartmentID ;
	QString _videoPath ;

	parabolicInfo()
	{
		_time = -1;
		_cameraID = -1;
		_apartmentFloor = -1;
		_apartmentUnit = -1;
		_apartmentID = -1;
		_videoPath = "";
	}

	quint16 size() const
	{
		return sizeof(_time) + sizeof(_cameraID) + sizeof(_apartmentFloor) + sizeof(_apartmentUnit) + sizeof(_apartmentID) + _videoPath.size();
	}
} ParabolicInfo;
#endif	// PARABOLICINFO_H_