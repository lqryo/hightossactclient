#include "PlaybackWidget.h"

PlaybackWidget::PlaybackWidget(CDocument * doc)
{
	ui.setupUi(this);
	ui.Playbackwidget->setMaximumHeight(QApplication::desktop()->height());
	ui.Playbackwidget->setMaximumWidth(QApplication::desktop()->width());
	ui.Playbackwidget->setMinimumHeight(int(QApplication::desktop()->height()*0.6));
	ui.Playbackwidget->setMinimumWidth(int(QApplication::desktop()->width()*0.6));
	this->pDoc = doc;
	Init();
	connections();
	FindFileFromPath();
	QMap<QString, QString>::iterator it;
	for (it = FileMap.begin(); it != FileMap.end(); it++){
		QListWidgetItem *pItem = new QListWidgetItem();
		pItem->setText(it.key());
		ListWidgetBtn->addItem(pItem);
	}
	FileMaplast = FileMap;
	TimerShow->start(500);
}
PlaybackWidget::~PlaybackWidget()
{
	TimerShow->stop();
	theTimer->stop();
	capture.release();
	delete theTimer;
	delete TimerShow;
}
void PlaybackWidget::Init()
{
	showLabel = ui.PlaybackLabel;
	ListWidgetBtn = ui.PlaybacklistWidget;
	theTimer = new QTimer(this);
	TimerShow = new QTimer(this);
	//设置边框和边框大小，颜色，图片的填充方式
	//showLabel->setFrameShape(QFrame::Box);
	//showLabel->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 170, 0);");
	showLabel->setScaledContents(true);

	strFilePath = QString::fromStdString(pDoc->par_all.save_path);

}

void PlaybackWidget::connections()
{
	connect(TimerShow, SIGNAL(timeout()), this, SLOT(updateListItem()));
	connect(theTimer, SIGNAL(timeout()), this, SLOT(updateImage()));
	connect(ListWidgetBtn, SIGNAL(itemSelectionChanged()), this, SLOT(ShowVedio()));
}

void PlaybackWidget::updateListItem()
{
	FindFileFromPath();
	QMap<QString, QString> AddMap = merge_maps(this->FileMap,this->FileMaplast);
	QMap<QString, QString>::iterator it;
	for (it = AddMap.begin(); it != AddMap.end(); it++){
		QListWidgetItem *pItem = new QListWidgetItem();
		pItem->setText(it.key());
		ListWidgetBtn->addItem(pItem);
	}
	ListWidgetBtn->show();
}

void PlaybackWidget::ShowVedio()
{
	qDebug() << ListWidgetBtn->currentItem()->text();
	QString key = ListWidgetBtn->currentItem()->text();
	qDebug() << FileMap[key];
	capture.open(FileMap[key].toStdString());
	if (!capture.isOpened())
	{
		QMessageBox::about(NULL, "提醒", "没有此视频!\n请重新选择");
	}
	else
	{
		frame = cv::Mat::zeros(capture.get(CV_CAP_PROP_FRAME_HEIGHT), capture.get(CV_CAP_PROP_FRAME_WIDTH), CV_8UC3);
		theTimer->start(20);
	}

}

void PlaybackWidget::updateImage()
{
//	double t1 = (double)cv::getTickCount();
	capture >> frame;
	if (!frame.data)
	{
		QMessageBox::about(NULL, "提醒", "播放完毕");
		theTimer->stop();
		return;
	}
	//cv::resize(frame, frame, cv::Size(739, 638), (0, 0), (0, 0));
	//显示视频
	QImage qimg = pDoc->pMatQImage->cvMat2QImage(frame);
	//if (frame.channels() == 3)    // RGB image  
	//{
	//	cvtColor(frame, frame, CV_BGR2RGB);
	//	qimg = QImage((const uchar*)(frame.data),
	//		frame.cols, frame.rows,
	//		QImage::Format_RGB888);
	//}
	//else                     // gray image  
	//{
	//	qimg = QImage((const uchar*)(frame.data),
	//		frame.cols, frame.rows,
	//		QImage::Format_Indexed8);
	//}
	//QImage image = cvMat2QImage(srcImage11);
	showLabel->setPixmap(QPixmap::fromImage(qimg));
	//imageLabel11->setPixmap(QPixmap::fromImage(image));
	//double t2 = (double)cv::getTickCount();
	//qDebug() << "waste time : " << (t2 - t1) / cv::getTickFrequency() << endl;
}


void PlaybackWidget::AddItem(QString filename)
{
	QString filePath = FindLocalFileFromPath(filename);
	if (filePath.isEmpty())
	{
		return;
	}
	FileMap.insert(filename, filePath);
	QListWidgetItem *pItem = new QListWidgetItem();
	pItem->setText(filename);
	ListWidgetBtn->addItem(pItem);
}

void PlaybackWidget::FindFileFromPath()
{
	
/*	if (strFilePath.isEmpty())
	{
		QMessageBox::about(NULL, "警告", "没有此目录");
		return;
	}*/

	QStringList ImgFilter = {"*.avi","*.mp4"};
//	QString strExt = "*.avi"; ImgFilter << strExt;
	
	//定义迭代器并设置过滤器  
	QDirIterator dir_iterator(strFilePath, ImgFilter, QDir::Files | QDir::NoSymLinks, QDirIterator::Subdirectories);
	while (dir_iterator.hasNext())
	{
		dir_iterator.next();
		QFileInfo file_info = dir_iterator.fileInfo();
		if (file_info.isFile())
		{
			QString file_path = file_info.absoluteFilePath();

			file_path = QDir::toNativeSeparators(file_path);
			QString file_name = file_info.fileName();
			file_name.truncate(file_name.lastIndexOf("."));
			FileMap.insert(file_name, file_path);
		}
	}
}

QString PlaybackWidget::FindLocalFileFromPath(QString& filename)
{
	if (strFilePath.isEmpty() || filename.isEmpty())
	{
		return "";
	}
	//QDir dir;
	//QStringList filters;
	//filters << filename;//过滤条件，可以添加多个选项，可以匹配文件后缀等。我这里只是找指定文件
	//dir.setPath(m_filepathdb);
	QStringList ImgFilter;
	ImgFilter << filename;
	//定义迭代器并设置过滤器  
	QDirIterator iter(strFilePath, ImgFilter, QDir::Files | QDir::NoSymLinks, QDirIterator::Subdirectories);
	//dir.setNameFilters(filters);//添加过滤器
	//QDirIterator 此类可以很容易的返回指定目录的所有文件及文件夹，可以再递归遍历，也可以自动查找指定的文件
	//QDirIterator iter(dir, QDirIterator::Subdirectories);
	while (iter.hasNext())
	{
		iter.next();
		QFileInfo info = iter.fileInfo();
		if (info.isFile())
		{
			QString file_path = info.absoluteFilePath();
			file_path = QDir::toNativeSeparators(file_path);
			filename.truncate(filename.lastIndexOf("."));
			return file_path;
		}
	}
	return "";
}

QMap<QString, QString> PlaybackWidget::merge_maps(QMap<QString, QString> & FileMap, QMap<QString, QString> & FileMaplast)
{
	QMap<QString, QString>::iterator it1, it2;
	QMap<QString, QString> result;
	for (it1 = FileMap.begin(), it2 = FileMaplast.begin(); it1 != FileMap.end() && it2 != FileMaplast.end();)
	{
		if (it1.key() == it2.key())
		{
			++it1; ++it2;
		}
		else
		{
			FileMaplast.insert(it1.key(), it1.value());
			result.insert(it1.key(), it1.value());
			if (it1.key() < it2.key())
				++it1;
			else
				++it2;
		}
	}
	while (it1 != FileMap.end())
	{
		FileMaplast.insert(it1.key(), it1.value());
		result.insert(it1.key(), it1.value());
		++it1;
	}
	return result;
}
