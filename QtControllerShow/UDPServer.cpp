
#include "UDPServer.h"
#include "TCPServer.h"

unsigned short CUDPServer::DEFAULT_PORT = cUDPDefaultPort;

void CUDPServer::run() 
{

	broadcast();
	timerIDBroadcast = this->startTimer(cTimeoutBroadcast);

}

void CUDPServer::timerEvent(QTimerEvent *event)
{

	if (event->timerId() == timerIDBroadcast)
		broadcast();

}

bool CUDPServer::broadcast() 
{

	CTCPServer *tcpServer = CTCPServer::instance();
	unsigned short port = tcpServer->getPort();
	return broadcast(std::to_string(port));

}

bool CUDPServer::broadcast(std::string s) 
{

	if (s != "") {

		udpSocket->writeDatagram(s.c_str(), s.size(), QHostAddress::Broadcast, cUDPClientPort);
		debug( "<UDPServer> Broadcast to port " + std::to_string(cUDPClientPort) + " with message: " + s);
		return true;

	}
	return false;

}

void CUDPServer::readPendingDatagrams()
{
	debug( "UDP Server: received datagram." );
	while (udpSocket->hasPendingDatagrams()) {
		QByteArray datagram;
		datagram.resize(udpSocket->pendingDatagramSize());
		QHostAddress senderAddr;
		quint16 senderPort;

		udpSocket->readDatagram(datagram.data(), datagram.size(), &senderAddr, &senderPort);
		handleDatagram(&datagram, &senderAddr, senderPort);
	}
}

void CUDPServer::handleDatagram(QByteArray* datagram, QHostAddress *addr, quint16 port) 
{
	debug( "Message from '" + addr->toString().toStdString() + ":" + std::to_string(port) + "' : "
		+ datagram->data() );
	// TODO: should update clients
}

CUDPServer::~CUDPServer() 
{
	if (udpSocket != nullptr) {
		delete udpSocket;
		udpSocket = nullptr;
	}

}

void CUDPServer::init()
{
	debug( "Do UDP server init stuff..." );

	clients.reserve(8);			// reserve client IP address space

	udpSocket = new QUdpSocket();
	udpSocket->bind(QHostAddress::AnyIPv4, port);

	connect(udpSocket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));

}