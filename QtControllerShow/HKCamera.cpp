#include "HKCamera.h"
#include <iostream>
#include <QDebug>

std::map<long, long> HKCamera::realplay_ports_;
std::map<long, long> HKCamera::realplay_handles_;
std::map<long, cv::Mat> HKCamera::frames_;
std::map<long, std::mutex> HKCamera::mtxs_;
std::map<long, HKCamera *> HKCamera::cameras_;
long HKCamera::counter_ = 0;

//回调函数，将传入的码流调用解码函数解码
void CALLBACK HKCamera::DecodeCallback(long nPort, char * pBuf, long nSize, FRAME_INFO * pFrameInfo, void* nReserved1, void* nReserved2)
{
	if (pFrameInfo->nType == T_YV12)
	{
		mtxs_[nPort].lock();
#ifdef USE_BGR
		cv::Mat image_yv12(pFrameInfo->nHeight + pFrameInfo->nHeight / 2, pFrameInfo->nWidth, CV_8UC1, pBuf);
		if (frames_[nPort].empty())
			frames_[nPort].create(pFrameInfo->nHeight, pFrameInfo->nWidth, CV_8UC3);

		cv::cvtColor(image_yv12, frames_[nPort], CV_YUV2BGR_YV12);

		emit cameras_[realplay_handles_[nPort]]->update(cameras_[realplay_handles_[nPort]]);
#else 
		frame[nPort] = cv::Mat(pFrameInfo->nHeight, pFrameInfo->nWidth, CV_8UC1);
		memcpy(frame[nPort].data, pBuf, pFrameInfo->nHeight * pFrameInfo->nWidth);
#endif // USE_BGR
		mtxs_[nPort].unlock();
	}
}


void CALLBACK HKCamera::RealDataCallBack(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser)
{
	BOOL inData = FALSE;

	switch (dwDataType)
	{
	case NET_DVR_SYSHEAD:
		// (handle, port)
		realplay_ports_[lRealHandle] = 0;

		if (!PlayM4_GetPort(&realplay_ports_[lRealHandle])) {
			LOG(ERROR) << "PlayM4_GetPort:" << PlayM4_GetLastError(realplay_ports_[lRealHandle]) << std::endl;
			break;
		}

		// (port, handle)
		realplay_handles_[realplay_ports_[lRealHandle]] = lRealHandle;

		LOG(INFO) << "RealPlay Handle:" << lRealHandle << " -> Port:" << realplay_ports_[lRealHandle] << std::endl;

		if (!PlayM4_OpenStream(realplay_ports_[lRealHandle], pBuffer, dwBufSize, 1024 * 1024)) {
			LOG(ERROR) << "PlayM4_OpenStream:" << PlayM4_GetLastError(realplay_ports_[lRealHandle]) << std::endl;
			break;
		}

		if (!PlayM4_SetDecCallBack(realplay_ports_[lRealHandle], DecodeCallback)) {
			LOG(ERROR) << "PlayM4_SetDecCallBack:" << PlayM4_GetLastError(realplay_ports_[lRealHandle]) << std::endl;
			break;
		}

		LOG(INFO) << "Start RealPlay:" << lRealHandle << std::endl;

		if (!PlayM4_Play(realplay_ports_[lRealHandle], nullptr)) {
			LOG(ERROR) << "PlayM4_Play:" << PlayM4_GetLastError(realplay_ports_[lRealHandle]) << std::endl;
			break;
		}
		break;

	case NET_DVR_STREAMDATA:
		if (dwBufSize > 0 && realplay_ports_[lRealHandle] != -1) {
			if (!PlayM4_InputData(realplay_ports_[lRealHandle], pBuffer, dwBufSize)) {
				//LOG(ERROR) << "PlayM4_InputData:" << PlayM4_GetLastError(realplay_ports_[lRealHandle]) << std::endl;
				break;
			}
		}
		break;

	default:
		if (dwBufSize > 0 && realplay_ports_[lRealHandle] != -1) {
			if (!PlayM4_InputData(realplay_ports_[lRealHandle], pBuffer, dwBufSize)) {
				//LOG(ERROR) << "PlayM4_InputData:" << PlayM4_GetLastError(realplay_ports_[lRealHandle]) << std::endl;
				break;
			}
		}
		break;
	}
}

bool HKCamera::open()
{
	NET_DVR_Init();

	NET_DVR_USER_LOGIN_INFO login_info = { 0 };
	strncpy(login_info.sDeviceAddress, device_ip_.c_str(), device_ip_.size());
	strncpy(login_info.sUserName, username_.c_str(), username_.size());
	strncpy(login_info.sPassword, password_.c_str(), password_.size());
	login_info.wPort = device_port_;

	NET_DVR_DEVICEINFO_V40 device_info = { 0 };
	user_id_ = NET_DVR_Login_V40(&login_info, &device_info);

	return (user_id_ >= 0);
}

void HKCamera::startRealPlay()
{
	//启动预览
	NET_DVR_PREVIEWINFO struPlayInfo = { 0 };

	struPlayInfo.hPlayWnd = hwnd_;	//需要 SDK 解码时句柄设为有效值，仅取流不解码时可设为空
	struPlayInfo.lChannel = 1;		//预览通道号
	struPlayInfo.dwStreamType = 0;		//0 - 主码流，		1 - 子码流，			2 - 码流 3，		3 - 码流	 4，	以此类推
	struPlayInfo.dwLinkMode = 0;		//0 - TCP 方式，		1 - UDP 方式，		2 - 多播方式，	3 - RTP 方式，	4-RTP/RTSP， 5 - RSTP/HTTP
	struPlayInfo.bBlocked = 1;		//0 - 非阻塞取流，	1 - 阻塞取流
	struPlayInfo.dwDisplayBufNum = 1;

	//回调对获取的视频流进行处理
	realplay_handle_ = NET_DVR_RealPlay_V40(user_id_, &struPlayInfo, nullptr, nullptr);
	LOG(INFO) << "RealPlay Handle:" << realplay_handle_ << std::endl;

	if (realplay_handle_ < 0) {
		LOG(ERROR) << "NET DVR Last Error: " << NET_DVR_GetLastError();
		close();
		return;
	}

	cameras_[realplay_handle_] = this;

	//if (!NET_DVR_SetRealDataCallBack(realplay_handle_, RealDataCallBack, NULL)) {
	//	LOG(ERROR) << std::endl;
	//}
}

void HKCamera::stopRealPlay()
{
	NET_DVR_StopRealPlay(realplay_handle_);
}

void HKCamera::close()
{
	NET_DVR_Logout(user_id_);
	NET_DVR_Cleanup();
}

void HKCamera::get(cv::Mat& _frame)
{
	auto port = realplay_ports_[realplay_handle_];

	if (port != -1) {
		mtxs_[port].lock();
		frames_[port].copyTo(_frame);
		mtxs_[port].unlock();
	}
}