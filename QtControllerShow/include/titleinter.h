#ifndef TITLEINTER_H
#define TITLEINTER_H
#include <QtEvents>
/**
 * 标准的自绘标题栏，必备宏
 */
// 标题栏-成员变量
#define TITLE_MEMBER \
                    QPoint mousePoint;\
                    bool mousePressed;\
                    bool max;\
// 标题栏-事件
#define TITLE_EVENT \
                    bool eventFilter(QObject *obj, QEvent *event){\
                        if (event->type() == QEvent::MouseButtonDblClick) {\
                            this->MaxClicked();\
                            return true;\
                        }\
                        return QObject::eventFilter(obj, event);\
                    };\
                    void mouseMoveEvent(QMouseEvent *e){\
                        if (mousePressed && (e->buttons() && Qt::LeftButton) && !max) {\
                            this->move(e->globalPos() - mousePoint);\
                            e->accept();\
                        }\
                    };\
                    void mousePressEvent(QMouseEvent *e){\
                        if (e->button() == Qt::LeftButton) {\
                            mousePressed = true;\
                            mousePoint = e->globalPos() - this->pos();\
                            e->accept();\
                        }\
                    };\
                    void mouseReleaseEvent(QMouseEvent *){\
                        mousePressed = false;\
                    }
// 标题栏-最大化、最小化、关闭按钮的槽定义
#define TITLE_MINMAXCLOSE \
                    void MaxClicked(){max ? showNormal() : showMaximized();max = !max;}\
                    void MinClicked(){showMinimized();}\
                    void CloseClicked(){close();}
// 标题栏-初始化(参数：标题栏的对象名称)
#define TITLE_INIT(title_obj)  \
                    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);\
                    max = false;\
                    mousePressed = false;\
                    ui.title_obj->installEventFilter(this);\

#endif // TITLEINTER_H
