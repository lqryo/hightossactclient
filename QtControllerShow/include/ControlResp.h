#pragma once

#include <QString>
#include <QDataStream>

/*
Control response type enums(quint8)
*/
enum CtrlRespType {
	RespTypeDetection = 1,
	RespTypeDetectionOnly = 2,
	RespTypeJson = 4,
	RespTypeMat = 6
};

enum JsonRespType {
	JsonRespLogin = 1,
	JsonRespNew = 2,
	JsonRespLogout = 4,
	// more ...
	JsonRespCMD = 128
};

const quint8 cNewPending	= 1;
const quint8 cNewCMDSuccess = (1 << 1);
const quint8 cNewSuccess	= (1 << 2);

/*
struct jsonresp - Json response from client
*/
typedef struct jsonresp {
	quint8 type;
	QString text;

	QString toString() const {
		return QString("JsonResp [type=%1, text=%2]").arg(type).arg(text);
	}
} JsonResp;

inline QDataStream & operator<< (QDataStream & out, const JsonResp & resp)
{
	out << resp.type
		<< resp.text;
	return out;

}

inline QDataStream & operator>> (QDataStream & in, JsonResp & resp)
{
	in >> resp.type
		>> resp.text;
	return in;

}