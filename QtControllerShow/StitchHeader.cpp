#include "StitchHeader.h"

cv::Mat stitch::transAndFlip(cv::Mat img)
{
	cv::Mat img_trans, img_transFlip;
	//��ʱ����ת90��
	flip(img, img_trans, 1);
	transpose(img_trans, img_transFlip);

	return img_transFlip;
}
cv::Mat stitch::transAndFlipClockwise(cv::Mat img)
{
	cv::Mat img_trans, img_transFlip;
	
	transpose(img, img_trans);
	flip(img_trans, img_transFlip, 1);

	return img_transFlip;
}