#include "RecordMap.h"
//#define WRITE_TEST
RecordMap::RecordMap()
{
}

RecordMap::~RecordMap()
{
}

void RecordMap::setValue(const Mat variantMask_, const StitchedPic stitch1_, const vector<Size> imgSizes
	, const Size resultSize_, vector<int> singleValues, vector<int> overlapValues)
{
	_variantMask = variantMask_;
	_imgSizes = imgSizes;
	_stitch1 = stitch1_;
	_resultSize = resultSize_;
	_singleValues = singleValues;
	_overlapValues = overlapValues;
}

void RecordMap::setImgMat(const vector<Mat> imgs)
{
	/*img01 = img0_;
	img02 = img1_;
	img11 = img2_;
	img12 = img3_;*/
}

Mat RecordMap::Record()
{
	int64 t_r = cvGetTickCount();
	
	_mapRecord.create(_resultSize, CV_16SC4);
	//mapRecord_assit.create(resultSize, CV_16SC4);
	for (int y = 0; y < _mapRecord.size().height; ++y)
	{
		Vec4s *map = _mapRecord.ptr<Vec4s>(y);
		//Vec4s *map_assit = mapRecord_assit.ptr<Vec4s>(y);
		const uchar *mask = _variantMask.ptr<uchar>(y);
		for (int x = 0; x < _mapRecord.size().width; ++x)
		{
			//分别在两张图区域确定
			Point mapPos;
			for (int i = 0; i < _imgSizes.size(); i++)
			{
				if (mask[x] == _singleValues[i])
				{
					Vec4s value(0, 0, 0, 0);
					internalRecordImg(Point(x, y), value, i);
					map[x] = value;
				}
			}
			//重叠区域
			for (int i = 0; i < _imgSizes.size()-1; i++)
			{
				if (mask[x] == _overlapValues[i])
				{
					Vec4s value(0, 0, 0, 0);
					internalRecord2imgs(Point(x, y), value, i);
					map[x] = value;
				}
			}
		}
	}

	std::cout << "record记录参数耗时：" << (cvGetTickCount() - t_r) / (cvGetTickFrequency() * 1000)<<"ms";
#if defined(WRITE_TEST)
	cv::imwrite("mapRecord.jpg", mapRecord);
#endif
	//cv::imwrite("mapRecord_assit.jpg", _mapRecord);/////////////////////////////////////////////////////
	/*vector<Mat> channels8;
	split(mapRecord, channels8);
	vector<Mat> channels4 = { channels8[0], channels8[1], channels8[2], channels8[3] };
	Mat visual;
	merge(channels4, visual);
	cv::imwrite("mapRecordVisual.jpg", visual);*/
	return _mapRecord;
}

/*
const Point& p:需要变换的原始点
Vec4s &dstValue：记录的结果值
const int vertical_i：位于竖直方向拼接的序图数，左边0，右边1
const int tmp_i：位于左或者右边拼接图中重叠区域图序列数0，1，2, 3, 4, 5,
0对应01,02  1对应02,03   2对应03,04
*/
void RecordMap::internalRecord2imgs(const Point& p, Vec4s &dstValue, const int tmp_i)
{
	Size size_1, size_2;
	size_1 = _imgSizes[tmp_i];
	size_2 = _imgSizes[tmp_i + 1];

	Point mapPos_1, mapPos_2;
	Point warpTmp, tmp;

	//第一张图，得到的warpTmp是左大图上点，竖直方向的
	//verticalStitch.warpPointBack(vertical_i, p, warpTmp);
	//将warpTmp点转换为tmp，即逆时针旋转90，作为左大图正常方向上的点
	//stitchTmp.pointXY2_Inver(warpTmp, tmp);//旋转

	_stitch1.warpPointBack(tmp_i, p, mapPos_1);
	if (mapPos_1.x > 0 && mapPos_1.y >0 && mapPos_1.x < size_1.width && mapPos_1.y < size_1.height)
	{
		//mapPos可能会小于0或超出原图区域
		dstValue[0] = mapPos_1.x;
		dstValue[1] = mapPos_1.y;
	}

	_stitch1.warpPointBack(tmp_i + 1, p, mapPos_2);
	if (mapPos_2.x > 0 && mapPos_2.y > 0 && mapPos_2.x < size_2.width && mapPos_2.y < size_2.height)
	{
		dstValue[2] = mapPos_2.x;
		dstValue[3] = mapPos_2.y;
	}

}



/*
const Point& p:需要变换的原始点
Vec4s &dstValue：记录的结果值
const int vertical_i：位于竖直方向拼接的序图数，左边0，右边1
const int tmp_i：位于左或者右边拼接图中图序列数0，1，2, 3, 4, 5,
0对应01  1对应02   2对应03
*/

void RecordMap::internalRecordImg(const Point& p, Vec4s &dstValue, const int tmp_i)
{
	
	Size size;
	size = _imgSizes[tmp_i];
	Point warpTmp, tmp, mapPos;

	_stitch1.warpPointBack(tmp_i, p, mapPos);
	if (mapPos.x > 0 && mapPos.y > 0 && mapPos.x < size.width && mapPos.y < size.height)
	{
		dstValue[0] = mapPos.x;
		dstValue[1] = mapPos.y;
		dstValue[2] = tmp_i;
	}
	else
	{
		dstValue = Vec4s(0, 0, 0, 0);
	}
}

/*映射部分：mapRecord 映射图的数据记录
mask 不同区域分情况映射
alpha  重叠区域的融合权重因子  
用于CPU端检验前面记录是否正确*/
Mat RecordMap::mapping(const Mat &alphaMat, const Mat &dst_mask, const Mat &m_mapRecord, const vector<Mat> pics)
{
	
	CV_Assert(pics[0].type() == CV_8UC1);

	int64 t_map = cvGetTickCount();
	//typedef Point_<uchar> dst_channel;
	int channel = 1;
	if (channel == 1)
	{
		typedef Point_<uchar> dst_channel;
		_mapMat.create(Size(_resultSize.width, _resultSize.height), CV_8UC1);
	}
	else if (channel == 3)
	{
		typedef Point3_<uchar> dst_channel;
		_mapMat.create(Size(_resultSize.width, _resultSize.height), CV_8UC3);
	}
	else
	{
		std::cout << "channel value wrong!" << endl;
	}	_mapMat.setTo(Scalar::all(255));
	
	Mat img0, img1;
	//之前放在第一个循环内，即每一行元素相同，但有些还是没有元素值
	//Point_<uchar> value;
	for (int y = 0; y < _resultSize.height - 1; ++y)
	{
		const Vec4s *map = m_mapRecord.ptr<Vec4s>(y);//记录映射数据，对应哪张图片对应原图上点的位置
		const uchar *mask = dst_mask.ptr<uchar>(y);//区分映射不同区域
		const float *alphaPtr = alphaMat.ptr<float>(y);
		uchar *dst = _mapMat.ptr<uchar>(y);
		for (int x = 0; x < _resultSize.width; ++x)
		{
			uchar value =0 ;
			Vec4s p = map[x];//之前提示边缘访问错误

			for (int i = 0; i < pics.size(); i++)
			{
				if (mask[x] == _singleValues[i])
				{
					img0 = pics[i];
					if (p[1] > 0 && p[1] < img0.rows && p[0]>0 && p[0] < img0.cols)
					{
						value = img0.at<uchar>(p[1], p[0]);
					}
				}
			}
			for (int i = 0; i < pics.size()-1; i++)
			{
				if (mask[x] == _overlapValues[i])
				{
					img0 = pics[i];
					img1 = pics[i + 1];
					uchar value1 = 0, value2 = 0;
					if (p[1] > 0 && p[1] < img0.rows && p[0]>0 && p[0] < img0.cols)
					{
						value1 = img0.at<uchar>(p[1], p[0]);
					}
					if (p[3] > 0 && p[3] < img1.rows && p[2]>0 && p[2] < img1.cols)
					{
						value2 = img1.at<uchar>(p[3], p[2]);
					}
					value = value1 * (1 - alphaPtr[x]) + value2 * alphaPtr[x];
				}
			}
			dst[x] = value;

		}
	}
	std::cout << "映射图耗时：" << (cvGetTickCount() - t_map) / (cvGetTickFrequency() * 1000) << endl;
#if defined(WRITE_TEST)
	cv::imwrite("mapMatfun.jpg", mapMat);
#endif
	return _mapMat;
}