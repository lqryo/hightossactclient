#include "CHKCamera.h"
//#include "ui_cmainframe.h"
#include "CDocument.h"

#define USECOLOR 0

CBlockingQueue<cv::Mat> * outputQueue = NULL;

//异常消息回调函数
void CALLBACK g_ExceptionCallBack(DWORD dwType, LONG lUserID, LONG lHandle, void *pUser)
{
	char tempbuf[256] = { 0 };
	switch (dwType)
	{
	case EXCEPTION_RECONNECT: //预览时重连
		qDebug() << "reconnect," << time(NULL);
		break;
	default:
		break;
	}
}

//对接收的视频码流进行解码，获取彩色图像
void yv12toYUV(char *outYuv, char *inYv12, int width, int height, int widthStep)
{
	int col, row;
	unsigned int Y, U, V;
	int tmp;
	int idx;

	//printf("widthStep=%d.\n",widthStep);

	for (row = 0; row<height; row++)
	{
		idx = row * widthStep;
		int rowptr = row*width;

		for (col = 0; col<width; col++)
		{
			//int colhalf=col>>1;
			tmp = (row / 2)*(width / 2) + (col / 2);
			//         if((row==1)&&( col>=1400 &&col<=1600))
			//         { 
			//          printf("col=%d,row=%d,width=%d,tmp=%d.\n",col,row,width,tmp);
			//          printf("row*width+col=%d,width*height+width*height/4+tmp=%d,width*height+tmp=%d.\n",row*width+col,width*height+width*height/4+tmp,width*height+tmp);
			//         } 
			Y = (unsigned int)inYv12[row*width + col];
			U = (unsigned int)inYv12[width*height + width*height / 4 + tmp];
			V = (unsigned int)inYv12[width*height + tmp];
			//         if ((col==200))
			//         { 
			//         printf("col=%d,row=%d,width=%d,tmp=%d.\n",col,row,width,tmp);
			//         printf("width*height+width*height/4+tmp=%d.\n",width*height+width*height/4+tmp);
			//         return ;
			//         }
			if ((idx + col * 3 + 2)> (1200 * widthStep))
			{
				//printf("row * widthStep=%d,idx+col*3+2=%d.\n",1200 * widthStep,idx+col*3+2);
			}
			outYuv[idx + col * 3] = Y;
			outYuv[idx + col * 3 + 1] = U;
			outYuv[idx + col * 3 + 2] = V;
		}
	}
	//printf("col=%d,row=%d.\n",col,row);
}

//获取帧图片中的时间
PLAYM4_SYSTEM_TIME timeNow;
//回调函数，将传入的码流调用解码函数解码
void CALLBACK DecCBFun(long nPort, char * pBuf, long nSize, FRAME_INFO * pFrameInfo, void* nReserved1, void* nReserved2)
{
	long lFrameType = pFrameInfo->nType;
	//qDebug() << "deccb";

	if (lFrameType == T_YV12)
	{
#if USECOLOR
		//int start = clock();
		IplImage* pImgYCrCb = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 3);//得到图像的Y分量  
		yv12toYUV(pImgYCrCb->imageData, pBuf, pFrameInfo->nWidth, pFrameInfo->nHeight, pImgYCrCb->widthStep);
		IplImage* pImg = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 3);

		cvCvtColor(pImgYCrCb, pImg, CV_YCrCb2RGB);//得到全部RGB图像
		cv::Mat matimg;
		matimg = cv::cvarrToMat(pImg);
		PlayM4_GetSystemTime(nPort, &timeNow);
		qDebug() << timeNow.dwHour << ":" << timeNow.dwMin << ":" << timeNow.dwSec << ":" << timeNow.dwMs;
		//cv::imshow("test", matimg);
		//float d_size = 0.5;       //缩放因子
		//cv::resize(matimg, matimg, cv::Size(matimg.cols * d_size, matimg.rows *d_size), (0, 0), (0, 0));
		//存入缓冲区队列
		outputQueue->Put(matimg);
		//qDebug() << "the num of queue:" << cd.cameraImage.m_pCount->getValue();
		//cv::Mat fromTake = outputQueue->Take();
		//cv::imshow("test", fromTake);
		//qDebug() << "the num of queue:" << cd.cameraImage.m_pCount->getValue();
		//cv::imshow("test",matimg);
		//cv::waitKey(1);
		//int end = clock();
#else
		IplImage* pImg = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 1);
		memcpy(pImg->imageData, pBuf, pFrameInfo->nWidth*pFrameInfo->nHeight);
		cv::Mat matimg;
		matimg = cv::cvarrToMat(pImg);
		PlayM4_GetSystemTime(nPort, &timeNow);
		//qDebug() << timeNow.dwHour << ":" << timeNow.dwMin << ":" << timeNow.dwSec << ":" << timeNow.dwMs;
		//outputQueue->Put(matimg);

		//emit DetectTrack_Sig();//发送检测信号
		//qDebug() << "size:" << outputQueue->m_pCount->getValue() << endl;
		//cv::Mat fromTake = outputQueue->Take();
		//cv::imshow("src", fromTake);
		//cv::waitKey(1);
#endif
		//printf("%d\n",end-start);
		//qDebug() << "ipcamera";
		//cvShowImage("IPCamera", pImg);
		//qDebug() << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss:zzz");
		//cvWaitKey(1);
#if USECOLOR
		cvReleaseImage(&pImgYCrCb);
		cvReleaseImage(&pImg);
#else
		cvReleaseImage(&pImg);
#endif
	}
}

LONG nPort = -1;
void CALLBACK g_RealDataCallBack_V30(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, DWORD dwUser)
{
	DWORD dRet = 0;
	BOOL inData = FALSE;

	switch (dwDataType)
	{
	case NET_DVR_SYSHEAD:

		qDebug() << "syshead";
		if (!PlayM4_GetPort(&nPort))
		{
			break;
		}
		if (!PlayM4_OpenStream(nPort, pBuffer, dwBufSize, 1024 * 1024))
		{
			dRet = PlayM4_GetLastError(nPort);
			break;
		}


		//设置解码回调函数 只解码不显示
		if (!PlayM4_SetDecCallBack(nPort, DecCBFun))
		{

			dRet = PlayM4_GetLastError(nPort);
			qDebug() << "dret:" << dRet;
			break;
		}

		////设置解码回调函数 解码且显示
		//if (!PlayM4_SetDecCallBackEx(nPort, DecCBFun, NULL, NULL))
		//{
		//	dRet = PlayM4_GetLastError(nPort);
		//	break;
		//}

		qDebug() << "start play";
		//打开视频解码
		if (!PlayM4_Play(nPort, NULL))
		{
			dRet = PlayM4_GetLastError(nPort);
			break;
		}


		////打开音频解码, 需要码流是复合流
		//if (!PlayM4_PlaySound(nPort))
		//{
		//	dRet = PlayM4_GetLastError(nPort);
		//	break;
		//}
		break;

	case NET_DVR_STREAMDATA:
		//qDebug() << "streamdata";
		inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);

		while (!inData)
		{
			Sleep(10);
			inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
			//qDebug() << "PlayM4_InputData failed";
			//OutputDebugString("PlayM4_InputData failed \n");
		}
		break;
	default:
		//qDebug() << "default";
		inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
		while (!inData)
		{
			Sleep(10);
			inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
			//qDebug() << "PlayM4_InputData failed";
			//OutputDebugString("PlayM4_InputData failed \n");
		}
		break;
	}
}

CHKCamera::CHKCamera(CAMERA_INFO pDoc){
	this->camera_info = pDoc;
	//this->SetParameter(this->pDoc->par_all);
	//this->parameter.LogIP = this->pDoc->par_all.LogIP;
	//this->parameter.LogName = this->pDoc->par_all.LogName;
	//this->parameter.LogPass = this->pDoc->par_all.LogPass;
	//hWnd = (HWND)parent->winId();
	
}

CHKCamera::~CHKCamera(){

}


bool CHKCamera::OpenCamera(QString cameraIP, QString cameraUsername, QString cameraPass,int cameraLowFloor,int cameraHighFloor){

	NET_DVR_Init();

	//注册设备
	//IUserID = NET_DVR_Login_V30("192.168.1.64", 8000, "admin", "IDM87792351", &struDeviceInfo);
	LowFloor = cameraLowFloor;
	HighFloor = cameraHighFloor;

	char* cIP;
	char* cName;
	char* cPass;

	const int lenIP = cameraIP.toStdString().length();
	cIP = new char[lenIP + 1];
	strcpy(cIP, cameraIP.toStdString().c_str());

	const int lenName = cameraUsername.toStdString().length();
	cName = new char[lenName + 1];
	strcpy(cName, cameraUsername.toStdString().c_str());

	const int lenPass = cameraPass.toStdString().length();
	cPass = new char[lenPass + 1];
	strcpy(cPass, cameraPass.toStdString().c_str());

	IUserID = NET_DVR_Login_V30(cIP, 8000, cName, cPass, &struDeviceInfo);
	qDebug() << "iuserid:" << IUserID;
	if (IUserID < 0){
		qDebug() << "login error:" << NET_DVR_GetLastError();
		NET_DVR_Cleanup();
		return false;
	}
	//消息异常回调
	NET_DVR_SetExceptionCallBack_V30(0, NULL, g_ExceptionCallBack, NULL);
	this->SetCameraParams();
	
	return true;
}

bool CHKCamera::CloseCamera(){
	qDebug() << "close camera:" << IUserID;
	//注销用户
	bool camera;
	camera = NET_DVR_Logout(IUserID);
	if (!camera){
		qDebug() << "camera:" << NET_DVR_GetLastError();
		return false;
	}
	NET_DVR_Cleanup();
	return true;
}

//采集视频
void CHKCamera::StartAcquiring(QLabel *parent){

	hWnd = (HWND)parent->winId();
	//启动预览
	NET_DVR_PREVIEWINFO struPlayInfo = { 0 };

	struPlayInfo.hPlayWnd = hWnd;
	struPlayInfo.lChannel = 1;
	struPlayInfo.dwStreamType = 0;
	struPlayInfo.dwLinkMode = 0;
	struPlayInfo.bBlocked = 1;
	struPlayInfo.dwDisplayBufNum = 1;
	qDebug() << "start";

	//设置输出Mat区

//	this->dstQueue = &(this->pDoc->cameraImage);
	//outputQueue = this->dstQueue;

	//cv::namedWindow("test",cv::WINDOW_AUTOSIZE);
	//回调对获取的视频流进行处理
	IRealPlayHandle = NET_DVR_RealPlay_V40(IUserID, &struPlayInfo, NULL, NULL);
	qDebug() << "irealplayhandle:" << IRealPlayHandle;
	
	if (IRealPlayHandle < 0){
		qDebug() << "error";
		NET_DVR_Logout(IUserID);
		return;
	}
	//NET_DVR_SetRealDataCallBack(IRealPlayHandle, g_RealDataCallBack_V30, NULL);
	return;
}

//停止采集，使用StartRealPlay返回的参数
void CHKCamera::StopAcquiring(){
	qDebug() << "stop acquring:" << IRealPlayHandle;
	//关闭预览
	bool play;
	play = NET_DVR_StopRealPlay(IRealPlayHandle);
	if (!play){
		qDebug() << "stopac:" << NET_DVR_GetLastError();
	}
	return;
}


//设置相机的参数：先获取相机的参数，修改目标参数后设置
bool CHKCamera::SetCameraParams(){
	//压缩
	int iRet;
	//获取压缩参数
	DWORD dwReturnLen;
	NET_DVR_COMPRESSIONCFG_V30 struParams = { 0 };
	iRet = NET_DVR_GetDVRConfig(IUserID, NET_DVR_GET_COMPRESSCFG_V30, struDeviceInfo.byStartChan, \
		&struParams, sizeof(NET_DVR_COMPRESSIONCFG_V30), &dwReturnLen);
	if (!iRet)
	{
		qDebug() << "get:" << NET_DVR_GetLastError();
		NET_DVR_Logout(IUserID);
		NET_DVR_Cleanup();
		return false;
	}

	//qDebug() << "fps:" << struParams.struNormHighRecordPara.dwVideoFrameRate;
	NET_DVR_CAMERAPARAMCFG param = { 0 };
	iRet = NET_DVR_GetDVRConfig(IUserID, NET_DVR_GET_CCDPARAMCFG, struDeviceInfo.byStartChan, \
		&param, sizeof(NET_DVR_CAMERAPARAMCFG), &dwReturnLen);
	param.struDayNight.byDayNightFilterType = 3;
	param.struDayNight.byBeginTime = this->camera_info.begintime;
	param.struDayNight.byBeginTimeMin = this->camera_info.begintimemin;
	param.struDayNight.byBeginTimeSec = this->camera_info.begintimesec;
	param.struDayNight.byEndTime = this->camera_info.endtime;
	param.struDayNight.byEndTimeMin = this->camera_info.endtimemin;
	param.struDayNight.byEndTimeSec = this->camera_info.endtimesec;

	qDebug() << "riyegongneng ::::::::" << param.struDayNight.byAlarmTrigState;
	iRet = NET_DVR_SetDVRConfig(IUserID, NET_DVR_SET_CCDPARAMCFG, struDeviceInfo.byStartChan, \
		&param, sizeof(NET_DVR_CAMERAPARAMCFG));

	//设置压缩参数
	struParams.struNormHighRecordPara.dwVideoFrameRate = this->camera_info.dwVideoFrameRate; //帧率
	struParams.struNormHighRecordPara.byResolution = this->camera_info.byResolution; //分辨率
	struParams.struNormHighRecordPara.dwVideoBitrate = this->camera_info.dwVideoBitrate; //视频码率
	struParams.struNormHighRecordPara.byStreamType = this->camera_info.byStreamType; //流类型

	iRet = NET_DVR_SetDVRConfig(IUserID, NET_DVR_SET_COMPRESSCFG_V30, struDeviceInfo.byStartChan, \
		&struParams, sizeof(NET_DVR_COMPRESSIONCFG_V30));
	if (!iRet)
	{
		qDebug() << "set:" << NET_DVR_GetLastError();
		NET_DVR_Logout(IUserID);
		NET_DVR_Cleanup();
		return false;
	}

	//获取设置后的参数
	////获取压缩参数
	//iRet = NET_DVR_GetDVRConfig(IUserID, NET_DVR_GET_COMPRESSCFG_V30, struDeviceInfo.byStartChan, \
		//	&struParams, sizeof(NET_DVR_COMPRESSIONCFG_V30), &dwReturnLen);
	//if (!iRet)
	//{
	//	qDebug() << "get:" << NET_DVR_GetLastError();
	//	NET_DVR_Logout(IUserID);
	//	NET_DVR_Cleanup();
	//	return false;
	//}
	//qDebug() << "fps" << struParams.struNormHighRecordPara.dwVideoFrameRate;
	//压缩
	return true;
}

//保存视频
bool CHKCamera::SavaVideo(){
	qDebug() << "start save:"<<IRealPlayHandle;
	bool save;
	save = NET_DVR_SaveRealData(IRealPlayHandle,"d:\\test.mp4");
	if (!save){
		qDebug() << "save error:" << NET_DVR_GetLastError();
		return false;
	}
	return true;
}

//停止保存
bool CHKCamera::StopSava(){
	NET_DVR_StopSaveRealData(IRealPlayHandle);
	return true;
}
