#include "MainFrame.h"
#include <QtWidgets/QApplication>
#include "MainWidget.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	//MainFrame w;
	//w.show();
	//w.move((QApplication::desktop()->width() - w.width()) / 2, (QApplication::desktop()->height() - w.height()) / 2 - 20);
	//qDebug() << w.width() << "  " << w.height() << endl;

	//加载界面格式QSS文件
	QFile styleFile(":/QtControllerShow/Resources/MainWidget.qss");
	styleFile.open(QIODevice::ReadOnly);
	QString setStyleSheet(styleFile.readAll());;
	a.setStyleSheet(setStyleSheet);

	MainWidget pMainWidget;
	
	pMainWidget.show();
	pMainWidget.setMaximumHeight(QApplication::desktop()->height());
	pMainWidget.setMaximumWidth(QApplication::desktop()->height() * 4 / 3);
	pMainWidget.setMinimumHeight(int(QApplication::desktop()->height()*0.9));
	pMainWidget.setMinimumWidth(int(QApplication::desktop()->height()*0.9 * 4 / 3));
	pMainWidget.move((QApplication::desktop()->width() - pMainWidget.width()) / 2, (QApplication::desktop()->height() - pMainWidget.height()) / 2 - 20);

	return a.exec();
}
