#pragma once
#include "CParams.h"
#include"CBlockingQueue.h"
//==============================================================================
//	@name		:	Camera
//	@biref		:	通用相机接口 使用线程的方式采集图片
//					实际的相机类应当继承此类并实现其中的方法
//	@author		:	ZZH
//===============================================================================

class QWidget;

class CCamera : public QObject{
	Q_OBJECT

public:
	//相机监控楼层
	int LowFloor;
	int HighFloor;

	virtual ~CCamera() { };

	virtual bool open() = 0;
	virtual void startRealPlay() = 0;
	virtual void stopRealPlay() = 0;
	virtual void close() = 0;

	//protected:
	//读取图片后写入对应缓冲区
	CBlockingQueue<cv::Mat>* dstQueue;
signals:
	void Matput_Sig();

private:

};

