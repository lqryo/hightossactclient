#include "CStitchThread.h"

CStitchThread::CStitchThread(CParams InputParam)
{
	//pStitchedPic = new StitchedPicShowFrame_Sig
	pStitchManage = new StitchManage();
	connect(this, SIGNAL(NextStitch_Sig(CDocument*,int )), this, SLOT(StitchFunction(CDocument*,int)), Qt::QueuedConnection);
}

CStitchThread::~CStitchThread()
{
}

/*************************************************
//  Method:    convertTo3Channels
//  Description: 将单通道图像转为三通道图像
//  Returns:   cv::Mat
//  Parameter: binImg 单通道图像对象
*************************************************/
Mat CStitchThread::convertTo3Channels(const Mat& binImg)
{
	Mat three_channel = Mat::zeros(binImg.rows, binImg.cols, CV_8UC3);
	vector<Mat> channels;
	for (int i = 0; i<3; i++)
	{
		channels.push_back(binImg);
	}
	merge(channels, three_channel);
	return three_channel;
}

/*************************************************
//  Method:    transAndFlip
//  Description: 逆时针旋转90度
//  Returns:   cv::Mat
//  Parameter: binImg 单通道图像对象
*************************************************/
Mat CStitchThread::transAndFlip(Mat img)
{
	Mat img_trans, img_transFlip;
	//逆时针旋转90°
	flip(img, img_trans, 1);
	transpose(img_trans, img_transFlip);
	return img_transFlip;
}

/*************************************************
//  Method:    transAndFlip2
//  Description: 顺时针旋转90度
//  Returns:   cv::Mat
//  Parameter: binImg 单通道图像对象
*************************************************/
Mat CStitchThread::transAndFlip2(Mat img)
{
	Mat img_trans, img_transFlip;
	//逆时针旋转90°
	transpose(img, img_trans);
	flip(img_trans, img_transFlip, 1);
	return img_transFlip;
}


void CStitchThread::StitchFunction(CDocument* pDoc, int flag)
{
	qDebug() << "Stitch Start！！！  " << pDoc->CameraQueue11.m_pCount->getValue() << "  " << pDoc->CameraQueue21.m_pCount->getValue();
	
	//	
	if (pDoc->par_all.stitchtest == true)
	{
		Mat img01, img02;
		CNetTransmission tmp11, tmp21;
		tmp11 = pDoc->CameraQueue11.Take();
		
		tmp21 = pDoc->CameraQueue21.Take();
		img01 = tmp11.frame.clone();
		img02 = tmp21.frame.clone();
		//img01=transAndFlip2(img01);
		//img02=transAndFlip2(img02);
		if (!img01.empty() && !img02.empty())
		{
			if (img01.channels() != 1)
				cvtColor(img01, img01, CV_RGB2GRAY); //不是灰度图时转化为灰度图像   
			if (img02.channels() != 1)
				cvtColor(img02, img02, CV_RGB2GRAY); //不是灰度图时转化为灰度图像 
			vector<vector<Mat>> stitchimgs;
			vector<Mat> img1, img2;
			img1.push_back(img01);
			img2.push_back(img02);
			stitchimgs.push_back(img1);
			stitchimgs.push_back(img2);
			Mat stitchResult = pStitchManage->stitchMN(stitchimgs);
			pDoc->WholeImages.Put(stitchResult);
			emit testShow_Sig();
		}
	}
	else
	{
		//tmp11 = pDoc->getBlockQueueVector()[0][0]->Take();
		//tmp21 = pDoc->getBlockQueueVector()[1][0]->Take();
		transNet2Mat(pDoc);
		if (pDoc->isAllStitchNotEmpty())
		{
			Mat stitchResult = pStitchManage->stitchMN(pDoc->getCameraImage());//
			pDoc->WholeImages.Put(stitchResult);
			emit ShowFrame_Sig();
		}
	}
	
	//if (!img01.empty() && !img02.empty())
	//{
	//	if (img01.channels() != 1)
	//		cvtColor(img01, img01, CV_RGB2GRAY); //不是灰度图时转化为灰度图像   
	//	if (img02.channels() != 1)
	//		cvtColor(img02, img02, CV_RGB2GRAY); //不是灰度图时转化为灰度图像   
	//	/*if (flag == 1)
	//	{
	//		img01 = transAndFlip(img01);
	//		img02 = transAndFlip(img02);
	//	}*/
	//	Mat tmp1 = pStitchManage->stitchMN(pDoc->getCameraImage());//得到的图为三通道
	//	if (!tmp1.empty()){
	//		qDebug() << "图片通道：" << tmp1.channels() << endl;
	//		if (flag == 1)
	//		{
	//			tmp1 = transAndFlip2(tmp1);
	//		}
	//		
	//		CNetTransmission tmp;
	//		tmp._time = tmp11._time;
	//		tmp.frame = tmp1.clone();
	//		/*int i = 0;
	//		if (tmp11.mDetectObjects.size()>0){
	//		for (; i < tmp11.mDetectObjects.size(); i++)
	//		tmp.mDetectObjects.push_back(tmp11.mDetectObjects[i]);
	//		}
	//		if (tmp21.mDetectObjects.size()>0){
	//		for (; i < tmp11.mDetectObjects.size() + tmp11.mDetectObjects.size(); i++)
	//		tmp.mDetectObjects.push_back(tmp11.mDetectObjects[i]);
	//		}*/
	//		pDoc->WholeImages.Put(tmp);
	//		if (pDoc->par_all.stitchtest == false)
	//			emit ShowFrame_Sig();
	//
	//		if (pDoc->par_all.stitchtest == true)
	//			emit testShow_Sig();		
	//	}		
	//}
	qDebug() << "Stitch End！！！" << endl;
}

/*************************************************
date：          // 2018-01-12
Author：        // lee
Function:       // transNet2Mat
Description:    // 从缓存网络相机数据队列之中取出imgage
Input:          // CDocument *pDoc ---全局变量
Return:         // void
Others:         // null
*************************************************/
void CStitchThread::transNet2Mat(CDocument *pDoc){
	int m = pDoc->par_all.CameraR;
	int n = pDoc->par_all.CameraC;
	for (int i = 0; i < m; ++i){
		for (int j = 0; j < n; ++j){
			//CNetTransmission tmp = pDoc->CameraQueneMN[i][j].Take();
			//pDoc->CameraImageMN[i][j] = tmp.frame.clone();
			//pDoc->getCameraImage()[i][j] = pDoc->getBlockQueueVector()[i][j]->Take().frame.clone();
			Mat tmpgray = pDoc->getBlockQueueVector()[i][j]->Take();
			if (tmpgray.channels() != 1)
				cvtColor(tmpgray, tmpgray, CV_RGB2GRAY); //不是灰度图时转化为灰度图像 
			pDoc->setCameraImage(i, j, tmpgray.clone());
			tmpgray.release();
		}
	}

	//
	/*
	if (pDoc->isAllStitchNotEmpty())
	{
		//start Stitch
	}*/
}
