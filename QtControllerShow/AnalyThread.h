#pragma once
/*
分析网络传输的数据按camera id分配到相对应的队列
*/
#include <QObject>
#include "CDocument.h"
//#include "H264_Decoder.h"
class AnalyThread : public QObject
{
	Q_OBJECT

public:
	//H264_Decoder decoder;
	CDocument *mDoc;
	AnalyThread(CDocument *pDoc);
	~AnalyThread();

	/*将网络传输的数据按camera id分配到相对应的队列中*/
	void enqueneByCameraID(CNetTransmission net);

	public slots:
	void AnalysisFunction();
signals:
	void NextAna_Sig();
	void start_Sig();//开启拼接信号
	void start_Sig(CDocument*, int);

};
