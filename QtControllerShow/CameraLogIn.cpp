#include "CameraLogIn.h"
#include "CDocument.h"
#include "ControlMsg.h"
#include "CHKCamera.h"
CameraLogIn::CameraLogIn(CDocument * pDoc)
{
	ui.setupUi(this);
	this->pDoc = pDoc;
	//设置表格行标题  
	QStringList headerLabels;
	headerLabels << "主机" << "客户端" << "相机IP地址" << "相机账号" << "相机密码" << "相机ID" << "最低楼层" << "最高楼层" << "相机登录状态" ;
	ui.camera_table->setHorizontalHeaderLabels(headerLabels);
	//设置表格行标题的对齐方式  
	ui.camera_table->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
	ui.camera_table->verticalHeader()->setVisible(false);    //行表头隐藏
	ui.camera_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ClientShowInit();
	ClientAddInit();
	connections();
}

CameraLogIn::~CameraLogIn()
{
}

void CameraLogIn::connections()
{
	connect(ui.camera_table, SIGNAL(cellClicked(int, int)), SLOT(LogInCameraFun(int, int)));
	connect(ui.refresh_button, SIGNAL(clicked()), this, SLOT(ClientShowupdate()));
	connect(ui.refresh_button_2, SIGNAL(clicked()), this, SLOT(ClientAddupdate()));
	connect(ui.add_button, SIGNAL(clicked()), this, SLOT(ClientAddFun()));
}

/*
初始化客户端的列表
*/
void CameraLogIn::ClientShowInit()
{
	// TODO: get target to send
	CTCPServer *tcpServer = CTCPServer::instance();
	QList<CameraNode> clients = tcpServer->getAllClients();
	foreach(const CameraNode &cameraNode, clients) {
		//debug(cameraNode.toString().toStdString(), UTIL_DEFAULT);
		/*插入一行*/
		int row = ui.camera_table->rowCount();
		InsertRow(row,cameraNode);
	}
}

/*
刷新客户端显示的信息
*/
void CameraLogIn::ClientShowupdate()
{
	// TODO: get target to send
	//将之前保存的以及显示的清空
	ui.camera_table->setRowCount(0);
	ui.camera_table->clearContents();

	CTCPServer *tcpServer = CTCPServer::instance();
	QList<CameraNode> clients = tcpServer->getAllClients();
	
	if (clients.size() <= 0)
	{
		QMessageBox::about(NULL, "警告", "没有主机打开\n请打开主机");
		return;
	}	
	foreach(const CameraNode &client, clients) {
		/*插入一行*/
		qDebug() <<"cameraID: "<< client.cameraID;
		int row = ui.camera_table->rowCount();
		InsertRow(row, client);
	}
}

/*
登录对应主机以及客户端的相机
*/
void CameraLogIn::LogInCameraFun(int row, int col)
{
	if (col == 8)
	{
		// TODO: get target to send
		if (MSK == false)MSK = true;
		ui.camera_table->item(row, col)->setSelected(false);
		
		QString host = ui.camera_table->item(row, 0)->text();
		quint16 id = ui.camera_table->item(row, 1)->text().toUInt();
		
		CTCPServer *tcpServer = CTCPServer::instance();
		QList<CameraNode> clients = tcpServer->getAllClients();	
		CameraNode cameraNode;	// = SelectCameraNode(host, id);
		foreach(const CameraNode &client, clients){
			if (client.host == host&&client.id == id){
				cameraNode = client;	break;
			}
		}		
		if (cameraNode.status & CNSConnected){//已经登录的相机可以进行相机的退出
			QMessageBox::StandardButton rb = QMessageBox::question(NULL, "提醒", "确定退出？", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
			if (rb == QMessageBox::Yes){
				ControlMsg controlMsg;
				controlMsg.type = CtrlTypeLogout;
				tcpServer->sendMsg(cameraNode.tcpSocket, &controlMsg);// for Logout camera command
			}
			return;
		}
		else{//未登录的相机可以进行相机的登录
			cameraRight = true;
			QString cameraIP = getCameraIP(true, row);//相机登录IP
			//字符串分割
			int arrip[4];
			GetIpString(cameraIP.toStdString(), arrip);


			QString cameraUsername = getCameraUser(true, row);//相机登录账号名
			QString cameraPass = getCameraPSW(true, row);//相机登录密码
			int cameraID = getCameraID(true, row);//相机登录ID
			int cameraLowFloor = getCameraLowFloor(true, row);//获得相机监控的最低楼层
			int cameraHighFloor = getCameraHighFloor(true, row);//获得相机的最高楼层
			//控制端登录相机
			if (cameraRight == false)return;
			int row = cameraID / 10; int col = cameraID % 10;
			pDoc->CameraModuleMN[row][col] = new HKCamera(cameraIP.toStdString(), cameraUsername.toStdString(), cameraPass.toStdString());
			pDoc->CameraModuleMN[row][col]->hwnd_ = (HWND)pDoc->pShow[row][col]->winId();

//			pDoc->CameraModuleMN[row][col] = new CHKCamera(pDoc->par_all.camera_info);
//			bool right = pDoc->CameraModuleMN[row][col]->OpenCamera(cameraIP, cameraUsername, cameraPass, cameraLowFloor, cameraHighFloor);

			bool right = pDoc->CameraModuleMN[row][col]->open();						
			if (right == false){
				QMessageBox::about(NULL, "警告", "客户端相机登录失败");
				pDoc->CameraModuleMN[row][col] = NULL;
				ControlMsg controlMsg;
				controlMsg.type = CtrlTypeLogin;
				qstr = "";
				controlMsg.text = qstr;
				controlMsg.len = qstr.length();
				tcpServer->sendMsg(cameraNode.tcpSocket, &controlMsg);// for Login camera command	
			}
			else{
			    pDoc->CameraModuleMN[row][col]->startRealPlay();
				CameraFloorUpdate_Sig(row,col);//发送更新相机监控楼层显示信号
				//检测端发送登录信息
				ControlMsg controlMsg;
				controlMsg.type = CtrlTypeLogin;
				controlMsg.text = qstr;
				controlMsg.len = qstr.length();
				tcpServer->sendMsg(cameraNode.tcpSocket, &controlMsg);// for Login camera command	
			}
		}		
	}
}

/*
向主机中增加客户端同时会登录摄像机，显示端登录相同的相机
*/
void CameraLogIn::ClientAddFun()
{
	if (MSK == false)MSK = true;
	// TODO: get target to send
	QString host = ui.hostBox->currentText();
	CTCPServer *tcpServer = CTCPServer::instance();
	QList<CameraNode> hosts = tcpServer->getAllHosts();
	CameraNode cameraNode;
	if (hosts.size()<=0){
		QMessageBox::about(NULL, "警告", "没有主机打开\n请打开主机"); return;
	}
	if (host == "随机"){
		cameraNode = tcpServer->getMinClientCountHost();
	}
	else{		
		foreach(const CameraNode &Node, hosts) {
			if (Node.host == host){
				cameraNode = Node;	break;
			}
		}
	}
	cameraRight = true;
	QString cameraIP = getCameraIP(false);//相机登录IP
	//字符串分割
	int arrip[4];
	GetIpString(cameraIP.toStdString(), arrip);


	QString cameraUsername = getCameraUser(false);//相机登录账号名
	QString cameraPass = getCameraPSW(false);//相机登录密码
	int cameraID = getCameraID(false);//相机登录ID
	int cameraLowFloor = getCameraLowFloor(false);//获得相机监控的最低楼层
	int cameraHighFloor = getCameraHighFloor(false);//获得相机的最高楼层
	
	if (cameraRight == false)return;
	if (!cameraNode.isEmpty()) {
		ControlMsg controlMsg;
		controlMsg.type = CtrlTypeNew;
		//控制端登录相机
		int row = cameraID / 10;
		int col = cameraID % 10;
//		pDoc->CameraModuleMN[row][col] = new CHKCamera(pDoc->par_all.camera_info);
//		pDoc->CameraModuleMN[row][col] = &HKCamera<CAMERAID>::Instance(this->pDoc, arrip[0], arrip[1], arrip[2], arrip[3], cameraUsername.toStdString(), cameraPass.toStdString());
		pDoc->CameraModuleMN[row][col] = new HKCamera(cameraIP.toStdString(), cameraUsername.toStdString(), cameraPass.toStdString());
		//		bool right = pDoc->CameraModuleMN[row][col]->OpenCamera(cameraIP, cameraUsername, cameraPass, cameraLowFloor, cameraHighFloor);
		bool right = pDoc->CameraModuleMN[row][col]->open();


		if (right == false){
			QMessageBox::about(NULL, "警告", "客户端相机登录失败");
			pDoc->CameraModuleMN[row][col] = NULL;
			qstr = "";
			controlMsg.text = qstr;
			controlMsg.len = qstr.length();
			tcpServer->sendMsg(cameraNode.tcpSocket, &controlMsg);// for Login camera command	
		}
		else{
			pDoc->CameraModuleMN[row][col]->startRealPlay();
			CameraFloorUpdate_Sig(row, col);//发送更新相机监控楼层显示信号
			//开启新实例		
			controlMsg.text = qstr;
			controlMsg.len = qstr.length();
			tcpServer->sendMsg(cameraNode.tcpSocket, &controlMsg);
			tcpServer->addPendingNew(cameraNode);			// for new instance command
		}		
	}
	else {
		error("No client connected.\n");
	}
}

/*
客户端增加主机选择的界面的初始化
*/
void CameraLogIn::ClientAddInit()
{
	CTCPServer *tcpServer = CTCPServer::instance();
	QList<CameraNode> hosts = tcpServer->getAllHosts();
	if (hosts.size() <= 0)
	{
		ui.hostBox->addItem("没有主机");
		return;
	}
	foreach(const CameraNode &Node, hosts)
	{
		ui.hostBox->addItem(Node.host);
	}
	ui.hostBox->addItem("随机");
}

/*
客户端增加主机选择的刷新
*/
void CameraLogIn::ClientAddupdate()
{
	ui.hostBox->clear();
	CTCPServer *tcpServer = CTCPServer::instance();
	QList<CameraNode> hosts = tcpServer->getAllHosts();
	if (hosts.size() <= 0){
		ui.hostBox->addItem("没有主机");
		QMessageBox::about(NULL, "警告", "没有主机打开\n请打开主机");
		return;
	}
	foreach(const CameraNode &Node, hosts)
	{
		ui.hostBox->addItem(Node.host);
	}
	ui.hostBox->addItem("随机");
}

/*
相机登录结果显示
*/ 
void CameraLogIn::LogInRresult(quint8 type, bool success)
{
	if (MSK == true){
		switch (type) {
		case CtrlTypeNew:{
			//新的实例启动是否成功
			if (success == true)
				ui.information_label->setText("客户端启动成功！");
			else
				ui.information_label->setText("客户端启动失败！");
			break;
		}
		case CtrlTypeLogin:{
			//已有实例登录是否成功
			MSK = false;
			if (success == true)
				QMessageBox::about(NULL, "提示", "登录成功！");
			else
				QMessageBox::about(NULL, "提示", "登录失败\n请重新登录！");
			break;
		}
		case CtrlTypeLogout:{
			MSK = false;
			if (success == true)
				QMessageBox::about(NULL, "提示", "退出成功！");
			else
				QMessageBox::about(NULL, "提示", "退出失败！");
			break;
		}
		case JsonRespNew:{
			MSK = false;
			//新的实例启动登录是否成功
			if (success == true)
				QMessageBox::about(NULL, "提示", "登录成功！");
			else
				QMessageBox::about(NULL, "提示", "登录失败\n请重新登录！");
			break;
		}
		default:
			qDebug() << "Unknown Message！";

		}
		ClientShowupdate();
		ClientAddupdate();		
	}
	

}

//查找主机号与客户端id相等的实例
CameraNode CameraLogIn::SelectCameraNode(QString host,quint16 id)
{
	CTCPServer *tcpServer = CTCPServer::instance();
	QList<CameraNode> clients = tcpServer->getAllClients();
	CameraNode cameraNode;
	foreach(const CameraNode &client, clients)
	{
		if (client.host == host&&client.id == id)
		{
			cameraNode = client;
			return cameraNode;
		}
	}
	return NULL;
}

//查找主机号相等的实例
CameraNode CameraLogIn::SelectCameraNode(QString host)
{
	CTCPServer *tcpServer = CTCPServer::instance();
	QList<CameraNode> clients = tcpServer->getAllClients();
	CameraNode cameraNode;
	foreach(const CameraNode &client, clients)
	{
		if (client.host == host)
		{
			cameraNode = client;
			return cameraNode;
		}
	}
	return NULL;
}

/*向tablewidget中添加一行数据*/
void CameraLogIn::InsertRow(int row, CameraNode cameraNode)
{
	ui.camera_table->insertRow(row);
	ui.camera_table->setItem(row, 0, new QTableWidgetItem(cameraNode.host));
	ui.camera_table->item(row, 0)->setFlags(ui.camera_table->item(row, 0)->flags() & (~Qt::ItemIsEditable));
	ui.camera_table->item(row, 0)->setFlags(ui.camera_table->item(row, 0)->flags() & (~Qt::ItemIsSelectable));
	ui.camera_table->item(row, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

	ui.camera_table->setItem(row, 1, new QTableWidgetItem(QString::number(cameraNode.id)));
	ui.camera_table->item(row, 1)->setFlags(ui.camera_table->item(row, 1)->flags() & (~Qt::ItemIsEditable));
	ui.camera_table->item(row, 1)->setFlags(ui.camera_table->item(row, 1)->flags() & (~Qt::ItemIsSelectable));
	ui.camera_table->item(row, 1)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

	if (cameraNode.status & CNSConnected)
	{
		int cameraID = cameraNode.cameraID;
		int cameraIDX = cameraID / 10;
		int cameraIDY = cameraID % 10;
		ui.camera_table->setItem(row, 2, new QTableWidgetItem(cameraNode.cameraIP));
		ui.camera_table->item(row, 2)->setFlags(ui.camera_table->item(row, 2)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 2)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 3, new QTableWidgetItem(cameraNode.cameraUsername));
		ui.camera_table->item(row, 3)->setFlags(ui.camera_table->item(row, 3)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 3)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 4, new QTableWidgetItem(cameraNode.cameraPass));
		ui.camera_table->item(row, 4)->setFlags(ui.camera_table->item(row, 4)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 4)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 5, new QTableWidgetItem(QString::number(cameraNode.cameraID)));
		ui.camera_table->item(row, 5)->setFlags(ui.camera_table->item(row, 5)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 5)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
		
		ui.camera_table->setItem(row, 6, new QTableWidgetItem(QString::number(pDoc->CameraModuleMN[cameraIDX][cameraIDY]->LowFloor)));
		ui.camera_table->item(row, 6)->setFlags(ui.camera_table->item(row, 5)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 6)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 7, new QTableWidgetItem(QString::number(pDoc->CameraModuleMN[cameraIDX][cameraIDY]->HighFloor)));
		ui.camera_table->item(row, 7)->setFlags(ui.camera_table->item(row, 5)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 7)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 8, new QTableWidgetItem("退出相机"));	
		ui.camera_table->item(row, 8)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
		ui.camera_table->item(row, 8)->setBackground(QPixmap("Resources/image/camera/pane/19b.png"));
	}		
	else{
		ui.camera_table->setItem(row, 2, new QTableWidgetItem("192.168.1.64"));
		ui.camera_table->item(row, 2)->setFlags(ui.camera_table->item(row, 2)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 2)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 3, new QTableWidgetItem("admin"));
		ui.camera_table->item(row, 3)->setFlags(ui.camera_table->item(row, 3)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 3)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 4, new QTableWidgetItem("IDM8779235101"));
		ui.camera_table->item(row, 4)->setFlags(ui.camera_table->item(row, 4)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 4)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 5, new QTableWidgetItem("**"));
		ui.camera_table->item(row, 5)->setFlags(ui.camera_table->item(row, 5)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 5)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 6, new QTableWidgetItem());
		ui.camera_table->item(row, 6)->setFlags(ui.camera_table->item(row, 5)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 6)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 7, new QTableWidgetItem());
		ui.camera_table->item(row, 7)->setFlags(ui.camera_table->item(row, 5)->flags() & (~Qt::ItemIsSelectable));
		ui.camera_table->item(row, 7)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

		ui.camera_table->setItem(row, 8, new QTableWidgetItem("登录相机"));
		ui.camera_table->item(row, 8)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
		ui.camera_table->item(row, 8)->setBackground(QPixmap("Resources/image/camera/pane/19b.png"));

	}
		
}

/*
获得相机登录IP
table=ture：从已有实例表格中获取 row：有意义，实例信息所在的行
table=false：从新增实例信息列表中获取
*/
QString CameraLogIn::getCameraIP(bool table,int row)
{
	qstr = "";
	QString cameraIP = "";
	if (table == true){
		if (ui.camera_table->item(row, 2)->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机IP为空"); 
		}
		else{
			cameraIP = ui.camera_table->item(row, 2)->text();			
		}
	}
	else{
		if (ui.CameraIPEdit->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机IP为空"); 
		}
		else{
			cameraIP = ui.CameraIPEdit->text();
		}
	}
	qstr = qstr + cameraIP+" ";
	return cameraIP;
}
/*
获得相机登录用户名
table=ture：从已有实例表格中获取 row：有意义，实例信息所在的行
table=false：从新增实例信息列表中获取
*/
QString CameraLogIn::getCameraUser(bool table, int row)
{
	QString cameraUsername = "";
	if (table == true){
		if (ui.camera_table->item(row, 3)->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机账户名为空");
		}
		else{			
			cameraUsername = ui.camera_table->item(row, 3)->text();
		}
	}
	else{
		if (ui.AccountEdit->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机账户名为空");
		}
		else{
			cameraUsername = ui.AccountEdit->text();
		}
	}
	qstr = qstr + cameraUsername + " ";
	return cameraUsername;
}
/*
获得相机登录密码
table=ture：从已有实例表格中获取 row：有意义，实例信息所在的行
table=false：从新增实例信息列表中获取
*/
QString CameraLogIn::getCameraPSW(bool table, int row)
{
	QString cameraPass = "";
	if (table == true){
		if (ui.camera_table->item(row, 4)->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机密码为空");
		}
		else{
			cameraPass = ui.camera_table->item(row, 4)->text();		
		}		
	}
	else{
		if (ui.CameraPWEdit->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机密码为空");
		}
		else{
			cameraPass = ui.CameraPWEdit->text();
		}		
	}
	qstr = qstr + cameraPass + " ";
	return cameraPass;
}
/*
获得登录相机的ID
table=ture：从已有实例表格中获取 row：有意义，实例信息所在的行
table=false：从新增实例信息列表中获取
*/
int CameraLogIn::getCameraID(bool table, int row)
{
	int cameraID = -1;
	if (table == true){
		if (ui.camera_table->item(row, 5)->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机ID为空");
		}
		else if (!AssertCameraID(ui.camera_table->item(row, 5)->text())){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机ID设置错误");
		}
		else{
			cameraID = ui.camera_table->item(row, 5)->text().toInt();
			qstr = qstr + ui.camera_table->item(row, 5)->text();
		}
	}
	else{
		if (ui.CameraIDEdit->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机ID为空");
		}
		else if (!AssertCameraID(ui.CameraIDEdit->text())){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "相机ID设置错误"); 
		}
		else{
			cameraID = ui.CameraIDEdit->text().toInt();
			qstr = qstr + ui.CameraIDEdit->text();
		}
	}
	return cameraID;
}

/*
获取相机监控的低楼层
table=ture：从已有实例表格中获取 row：有意义，实例信息所在的行
table=false：从新增实例信息列表中获取
*/
int CameraLogIn::getCameraLowFloor(bool table, int row)
{
	int lowfloor = -1;
	if (table == true){
		if (ui.camera_table->item(row, 6)->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "最低楼层号为空");
		}
		else{
			lowfloor = ui.camera_table->item(row, 6)->text().toInt();
		}
	}
	else{
		if (ui.lowfloor->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "最低楼层号为空");
		}
		else{
			lowfloor = ui.lowfloor->text().toInt();
		}
	}
	return lowfloor;
}
/*
获取相机拍摄的高楼层
table=ture：从已有实例表格中获取 row：有意义，实例信息所在的行
table=false：从新增实例信息列表中获取
*/
int CameraLogIn::getCameraHighFloor(bool table, int row)
{
	int highfloor = -1;
	if (table == true){
		if (ui.camera_table->item(row, 7)->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "最高楼层号为空");
		}
		else{
			highfloor = ui.camera_table->item(row, 7)->text().toInt();
		}
	}
	else{
		if (ui.lowfloor->text().isEmpty()){
			cameraRight = false;
			QMessageBox::about(NULL, "警告", "最高楼层号为空");
		}
		else{
			highfloor = ui.lowfloor->text().toInt();
		}
	}
	return highfloor;
}
/*
相机登录ID判断是否合法
返回值：true：合法，false：不合法
*/
bool CameraLogIn::AssertCameraID(QString cameraIDStr){
	int cameraID = cameraIDStr.toInt();
	if (cameraID / 10 < pDoc->par_all.CameraR && cameraID % 10 < pDoc->par_all.CameraC)
		return true;
	else
		return false;
}


//将IP地址拆分为数组
void CameraLogIn::GetIpString(string strIpAdd, int iIp[])
{
	int OppPos = 0, nowPos = 0, i = 0;
	while (OppPos = strIpAdd.find_first_of('.', nowPos))
	{
		if (string::npos == OppPos)
		{
			iIp[i] = atoi(strIpAdd.substr(nowPos).c_str());
			break;
		}
		iIp[i] = atoi(strIpAdd.substr(nowPos, OppPos - nowPos).c_str());
		nowPos = OppPos + 1;
		i++;
	}
	return;
}

//刷新显示画面
void CameraLogIn::DisplayUpdate00()
{
	emit CameraDisplay_Sig(0, 0);
}

void CameraLogIn::DisplayUpdate01()
{
	emit CameraDisplay_Sig(0, 1);
}

void CameraLogIn::DisplayUpdate10()
{
	emit CameraDisplay_Sig(1, 0);
}

void CameraLogIn::DisplayUpdate11()
{
	emit CameraDisplay_Sig(1, 1);
}

void CameraLogIn::DisplayUpdate20()
{
	emit CameraDisplay_Sig(2, 0);
}

void CameraLogIn::DisplayUpdate21()
{
	emit CameraDisplay_Sig(2, 1);
}

void CameraLogIn::DisplayUpdate30()
{
	emit CameraDisplay_Sig(3, 0);
}

void CameraLogIn::DisplayUpdate31()
{
	emit CameraDisplay_Sig(3, 1);
}

void CameraLogIn::DisplayUpdate40()
{
	emit CameraDisplay_Sig(4, 0);
}

void CameraLogIn::DisplayUpdate41()
{
	emit CameraDisplay_Sig(4, 1);
}

void CameraLogIn::DisplayUpdate50()
{
	emit CameraDisplay_Sig(5, 0);
}

void CameraLogIn::DisplayUpdate51()
{
	emit CameraDisplay_Sig(5, 1);
}

void CameraLogIn::DisplayUpdate60()
{
	emit CameraDisplay_Sig(6, 0);
}

void CameraLogIn::DisplayUpdate61()
{
	emit CameraDisplay_Sig(6, 1);
}
