#ifndef TCPCLIENT_H_
#define TCPCLIENT_H_

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

#include<string>
#include"CNetTransmission.h"
#include "ControlMsg.h"

//const unsigned int cTCPSocketBufferSize = 1024 * 1024 * 1;		// 1M
//const unsigned int cTCPSocketBufferSize = 1024 * 8 * 1;		// 8K

class CTCPClient : public QObject
{
	Q_OBJECT

public:
	static CTCPClient * instance(std::string ip = "", short port = 0)
	{
		static CTCPClient inst(ip, port);
		return &inst;
	}
	~CTCPClient();

	/*
	The return value can not represent this connection is established, see handleConnected().
	*/
	bool connectServer(std::string ip, unsigned short port);
	void sendMsg(CNetTransmission * msg);//这里的参数应该是CNetTransmission类型的结构体，函数用来将该类型的结构体数据发送至服务器
	//std::string recieve_message();//这个函数主要是接收服务器传送过来的控制信息，应该是一个消息类型id+具体消息内容的格式，函数需要将具体的操作解析出来，并返回

	unsigned short getSvrPort() const 
	{
		return svrPort;
	}

	std::string getSvrIP() const 
	{
		return svrIP;
	}

	bool isConnected() const 
	{
		return _isConnected;
	}

private slots:
	void readPendingStreams();
	void handleConnected();

private:
	std::string svrIP;				// tcp server ip address string
	unsigned short svrPort;			// tcp server port

	bool _isConnected;				// true if current connected to tcp server

	QTcpSocket *tcpClient;
	QTcpServer *tcpServer;

	void init();
	void handleMsg(ControlMsg *msg);

	CTCPClient(std::string ip, unsigned short port) :
		svrIP(ip), svrPort(port),
		_isConnected(false),
		tcpClient(nullptr), tcpServer(nullptr)
	{
		init();
	}

	CTCPClient(const CTCPClient & other) {}
	CTCPClient & operator= (const CTCPClient & other) {}
};

#endif // TCPCLIENT_H_