#ifndef CDetectionNet_H_
#define CDetectionNet_H_

#include "DetectResults.h"
#include <vector>
#include <QDataStream>
#include <iostream>
#include "Utils.h"

class CDetectionNet
{
public:
	
	int camera_ID;//相机的编号
	long _time;//时间戳
	int frameWidth;
	int frameHeight;
	std::vector<DetectResults> mDetectObjects;//高空抛物检测结果保存
	CDetectionNet clone()
	{
		CDetectionNet a;
		a.camera_ID = this->camera_ID;
		a._time = this->_time;
		a.frameWidth = this->frameWidth;
		a.frameHeight = this->frameHeight;
		a.mDetectObjects = this->mDetectObjects;
		return a;
	}

	~CDetectionNet() {
		mDetectObjects.clear();
	}
};

inline QDataStream & operator<< (QDataStream & out, const CDetectionNet & msg)
{
	out << msg.camera_ID;		// cameraID
	quint64 time = msg._time;
	out << time;				// timestamp
	out << msg.frameWidth;		//frame Width
	out << msg.frameHeight;		//frame Height
	// detection results start
	int resLen = msg.mDetectObjects.size();
	out << resLen;
	for (int i = 0; i < resLen; ++i) {
		out << msg.mDetectObjects[i];
	}

	return out;

}

inline QDataStream & operator>> (QDataStream & in, CDetectionNet & msg)
{
	// !!! The variable in order must match out !!!
	in >> msg.camera_ID;
	quint64 time;
	in >> time;
	msg._time = time;

	in >> msg.frameWidth;
	in >> msg.frameHeight;

	// read detection results start
	int resLen;
	in >> resLen;
	debug("resLen = " + std::to_string(resLen));
	msg.mDetectObjects.resize(resLen);
	for (int i = 0; i < resLen; ++i) {
		in >> msg.mDetectObjects[i];
	}

	return in;

}

#endif	// NETTRANSMISSION_H_