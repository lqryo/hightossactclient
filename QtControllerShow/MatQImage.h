#pragma once
#include <opencv2\opencv.hpp>
#include <QtWidgets>
class MatQImage
{
public:
	MatQImage();
	~MatQImage();
	QImage cvMat2QImage(const cv::Mat& mat);
	cv::Mat QImage2cvMat(QImage image);
};