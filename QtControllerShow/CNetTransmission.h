#ifndef NETTRANSMISSION_H_
#define NETTRANSMISSION_H_

#include "DetectResults.h"
#include<vector>
#include <QDataStream>
#include <iostream>
#include "Utils.h"

class CNetTransmission
{
public:
	int camera_ID;//相机的编号
	long _time;//时间戳
	cv::Mat frame;//压缩的视频帧图像
	std::vector<DetectResults> mDetectObjects;//高空抛物检测结果保存
	//std::vector<cv::Point2d> trace;//运动对象运动轨迹坐标
	//cv::Point2d first_trace;//保存高空抛物的起始位置
	CNetTransmission clone()
	{
		CNetTransmission a;
		a.camera_ID = this->camera_ID;
		a._time = this->_time;
		a.frame = this->frame.clone();
		a.mDetectObjects = this->mDetectObjects;
		return a;
	}

	~CNetTransmission() {
		
	}
};

inline QDataStream & operator<< (QDataStream & out, const CNetTransmission & msg)
{
	out << msg.camera_ID;		// cameraID
	quint64 time = msg._time;
	out << time;				// timestamp

	// detection results start
	int resLen = msg.mDetectObjects.size();
	out << resLen;
	for (int i = 0; i < resLen; ++i) {
		out << msg.mDetectObjects[i];
	}
	// /detection results end 

	// image frame start 
	const cv::Mat & frame = msg.frame;
	out << frame.rows
		<< frame.cols
		<< frame.type();
	uint dataLen = frame.total() * frame.elemSize();
	out << dataLen;
	out.writeRawData((const char *)frame.data, dataLen);
	// /image frame end
		
	return out;

}

inline QDataStream & operator>> (QDataStream & in, CNetTransmission & msg)
{
	// !!! The variable in order must match out !!!
	in >> msg.camera_ID;
	quint64 time;
	in >> time;
	msg._time = time;

	// read detection results start
	int resLen;
	in >> resLen;
	debug("resLen = " + std::to_string(resLen));
	msg.mDetectObjects.resize(resLen);
	for (int i = 0; i < resLen; ++i) {
		in >> msg.mDetectObjects[i];
	}
	// /read detection results end

	// read image frame start
	int rows;
	int cols;
	int type;
	in >> rows
		>> cols
		>> type;
	uint dataLen;
	char *data;
	in >> dataLen;
	data = new char[dataLen]{0};			// to be delete [] 1
	int readImgLen = in.readRawData(data, dataLen);
	debug("readImgLen = " + std::to_string(readImgLen));
	msg.frame = cv::Mat(rows, cols, type, data).clone();

	delete[] data;							// delete[] @1
	// /read image frame end

	return in;

}

#endif	// NETTRANSMISSION_H_