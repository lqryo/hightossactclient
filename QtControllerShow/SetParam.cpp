#include "SetParam.h"

SetParam::SetParam(CDocument * pDoc)
{
	ui.setupUi(this);

	TITLE_INIT(system_title_widget); // 初始化标题宏参数
	connect(ui.system_close_button, SIGNAL(clicked()), this, SLOT(CloseClicked()));
	ui.system_tabwidget->setTabPosition(QTabWidget::West);
	this->setWindowFlags(Qt::FramelessWindowHint);
	ui.system_tabwidget->setStyleSheet("QTabWidget::tab-bar{"
		"left:40px;}"
		"QTabBar::tab:first{margin-top: 15px;margin-right: 40px;"
		"min-width:105px;min-height:35px;max-width:105px; max-height:35px;"
		"border-image:url(:/QtControllerShow/Resources/image/system/often.png);}"
		"QTabBar::tab:first:hover,QTabBar::tab:first:selected{"
		"background-image:url(:/QtControllerShow/Resources/image/system/left_choose.png);}"
		"QTabBar::tab:last{ margin-top: 15px; margin-right: 40px;"
		" min-width:105px;min-height:35px; max-width:105px;max-height:35px;"
		"background:transparent;}"
		"QTabBar::tab:last:hover,QTabBar::tab:first:selected{"
		"background-image:url(:/QtControllerShow/Resources/image/system/left_choose.png); }");

	this->pDoc = pDoc;
	ui.FloorNum->setText(QString::number(pDoc->par_all.FloorNum));
	ui.PerFloor->setText(QString::number(pDoc->par_all.PerFloor));
	ui.Distance->setText(QString::number(pDoc->par_all.distance));
	ui.CaHeight->setText(QString::number(pDoc->par_all.CaHeight));
	ui.CameraR->setText(QString::number(pDoc->par_all.CameraR));
	ui.CameraC->setText(QString::number(pDoc->par_all.CameraC));
	ui.pFloors->setText(QString::number(pDoc->par_all.pFloors));
	ui.startFloor->setText(QString::number(pDoc->par_all.startFloor));
	
	initStitchState();

	connect(ui.system_save, SIGNAL(clicked()), this, SLOT(setParamFun()));
	connect(ui.stitchCheck, SIGNAL(stateChanged(int)), this, SLOT(onStitchStateChanged(int)));

	//connect(ui.CancelBtn, SIGNAL(clicked()), this, SLOT(CancelFun()));
}

SetParam::~SetParam()
{
}
void SetParam::initStitchState()
{
	if (pDoc->par_all.stitchMsk == true) // "选中"
	{
		ui.stitchCheck->setChecked(true);
	}
	else // 未选中 - Qt::Unchecked
	{
		ui.stitchCheck->setChecked(false);
	}
}
//
void SetParam::onStitchStateChanged(int state)
{
	if (state == Qt::Checked) // "选中"
	{
		pDoc->par_all.stitchMsk=true;
	}
	else // 未选中 - Qt::Unchecked
	{
		pDoc->par_all.stitchMsk = false;
	}
}
//设置参数槽函数
void SetParam::setParamFun()
{
	if (!ui.FloorNum->text().isEmpty())
		pDoc->par_all.FloorNum = ui.FloorNum->text().toInt();
	else
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "警告", "楼层数为空");
		return;
	}
	if (!ui.PerFloor->text().isEmpty())
		pDoc->par_all.PerFloor = ui.PerFloor->text().toFloat();
	else
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "警告", "每层楼高度设置为空");
		return;
	}
	if (!ui.Distance->text().isEmpty())
		pDoc->par_all.distance = ui.Distance->text().toDouble();
	else
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "警告", "相机安装与大楼距离设置为空");
		return;
	}
	if (!ui.CaHeight->text().isEmpty())
		pDoc->par_all.CaHeight = ui.CaHeight->text().toDouble();
	else
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "警告", "相机安装高度设置为空");
		return;
	}
	if (!ui.startFloor->text().isEmpty()){
		pDoc->par_all.startFloor = ui.startFloor->text().toInt();
	}
	else
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "警告", "开始检测楼层设置为空");
		return;
	}
	if (!ui.pFloors->text().isEmpty()){
		pDoc->par_all.pFloors = ui.pFloors->text().toInt();
	}
	else
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "警告", "单个相机拍摄楼层数设置为空");
		return;
	}
	if (!ui.CameraR->text().isEmpty())
	{
		pDoc->par_all.CameraR = ui.CameraR->text().toInt();
		if (pDoc->par_all.CameraR > pDoc->par_all.CameraRMax)
		{
			pDoc->par_all.SetParamMsk = false;
			QMessageBox::about(NULL, "警告", "竖直方向相机个数过大\n超过系统支持最大值");
			return;
		}
	}
	else
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "警告", "竖直方向相机个数设置为空");
		return;
	}
	if (!ui.CameraC->text().isEmpty())
	{
		pDoc->par_all.CameraC = ui.CameraC->text().toInt();
		if (pDoc->par_all.CameraC > pDoc->par_all.CameraCMax)
		{
			pDoc->par_all.SetParamMsk = false;
			QMessageBox::about(NULL, "警告", "水平方向相机个数过大\n超过系统支持最大值");
			return;
		}
	}		
	else
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "警告", "水平方向相机个数设置为空");
		return;
	}
	if (pDoc->par_all.FloorNum==0)
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "提示", "楼层总数为 0 ");
		return;
	}
	else if (pDoc->par_all.CameraR==0)
	{
		pDoc->par_all.SetParamMsk = false;
		QMessageBox::about(NULL, "提示", "相机总数为 0 ");
		return;
	}
	else
	{
		if (ui.stitchCheck->isChecked()){
			pDoc->par_all.stitchMsk = true;
		}
		else
		{
			pDoc->par_all.stitchMsk = false;
		}
		pDoc->initCameraModule();
		pDoc->initDecodeSize();
		pDoc->initCameraQuene();
		//pDoc->par_all.pFloors = ceil(pDoc->par_all.FloorNum / pDoc->par_all.CameraR) + 1;
		pDoc->par_all.SetParamMsk = true;//设置成功
		InitWidget_Sig();
		this->close();
	}	
}

//取消设置槽函数,若取消设置参数的话，则参数设置失败。
void SetParam::CancelFun()
{
	pDoc->par_all.SetParamMsk = false;
	this->close();
}
