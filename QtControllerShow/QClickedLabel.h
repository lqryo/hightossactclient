﻿#pragma once
#include <QLabel>

class QClickedLabel : public QLabel {
	Q_OBJECT

public:
	QClickedLabel(QWidget  * parent = Q_NULLPTR);
	~QClickedLabel();

protected:
	void mouseReleaseEvent(QMouseEvent *ev);

signals:
	void clicked();

	
};
