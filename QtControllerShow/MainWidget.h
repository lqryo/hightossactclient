#pragma once

#include <QWidget>
#include <QSound>
#include "titleinter.h"
#include "ui_MainWidget.h"

#include "CDocument.h"

#define CAMERA_NUM 14
class CStitchThread;
class AnalyThread;
class CTCPServer;

class SetParam;
class PlaybackWidget;
class ButtonWidget;
class CameraLogIn;
class MainWidget : public QWidget
{
	Q_OBJECT

public:
	MainWidget(QWidget *parent = Q_NULLPTR);
	~MainWidget();

public:
	double t1, t2;
	QSound *sound;//定义声音插件
	/*
		定义主界面按钮
	*/
	ButtonWidget * BtnStart;
	ButtonWidget * BtnVedioShow;
	ButtonWidget * BtnVedioStitchShow;
	ButtonWidget * BtnPlayback;
	ButtonWidget * BtnLogIn;

	ButtonWidget * BtnUserManager;
	ButtonWidget * BtnParmSet;
	ButtonWidget * button3;
	ButtonWidget * button4;

	QGridLayout *pQGridLayout;
//	QVector<QVector<QLabel*>> pShow;//单摄像头显示窗口，由相机个数决定
	QVector<QVector<QLabel*>> pShowEdit;//单摄像头拍摄楼层信息

	CDocument * pDoc;
	SetParam * pSetParam;//设置界面
	CameraLogIn *pLogInCaWidget;//设备管理界面

	
	CTCPServer * tcpServer;//网络传输模块
	CStitchThread * pCStitchThread;//拼接模块
	AnalyThread * pAnalyThread;//传输图片相机分析模块

	//定义子线程对象
	QThread tcpServerSubThread;
	QThread StitchSubThread;
	QThread AnalySubThread;

private:
	void connections();//链接函数
	void Init();//初始化函数
	void InitOperateWidget();//初始化控制界面函数
	HKCamera* camera[CAMERA_NUM];


public slots:
	void StartFun();//控制面板开始按钮执行操作，广播发送开始信号
	void perCameraShowFun();//显示端直接读取摄像头进行单个摄像头显示
	void PerShowFun();//跳转单个摄像头显示tab
	void StitchShowFun();//跳转拼接后显示tab
	void PlaybackFun();//查看并能够播放保存的抛物时间片段
	void LogInCameraFun();//登录相机,点击登录相机按钮可以进行相机登录

	void SubWidgetSetP();//设置系统参数二级界面

	void InitShowWidget();//初始化单摄像头显示界面函数
	void CameraFloorUpdate(int row,int col);//更新单摄像头显示时监控的楼层表示
	
	void ExitFun();//发送退出信号槽函数 程序退出

	void StitchtestFun();//发送拼接信号，加载本地的视频，测试拼接效果
	void ReadFrame();//读取本地视频测试拼接
	void testShow();//本地视频拼接显示

	void updateFrame();//显示拼接好的图片以及检测结果
	void perCaShow();//测试网络直接传输过来的图片
	void ResultShowUpdate();//检测结果显示

	void actionShowClientList();//打印出所有的客户端


//	void DisplayUpdate(int row, int col);//更新显示画面
	void update_frame(HKCamera* camera);

signals:
	void updateFrame_Sig();
	void ReadFrame_Sig();
	
	void OpenCaemra_Sig();//打开相机信号
	
	void StartAcquiring_Sig(CBlockingQueue<cv::Mat> &);//打开采集图像信号
	void StitchedPic_Sig(CDocument*, int);//拼接信号
	void test_sig();

protected:
	TITLE_EVENT
protected slots:
	TITLE_MINMAXCLOSE
private:
	Ui::MainWidget ui;
	TITLE_MEMBER
};
