#ifndef UTILS_H_
#define UTILS_H_

#define UTIL_PREFIX 1
#define UTIL_NEWLINE (1 << 2)
#define UTIL_DEFAULT (UTIL_PREFIX | UTIL_NEWLINE)
#define UTIL_NONE 0

#include <iostream>
#include <string>

extern void debug(const char *msg, int flag = UTIL_DEFAULT);
extern void debug(const std::string & msg, int flag = UTIL_DEFAULT);
extern void debug(long val, int flag = UTIL_DEFAULT);

extern void error(const char *msg, int flag = UTIL_DEFAULT);
extern void error(const std::string & msg, int flag = UTIL_DEFAULT);
extern void error(long val, int flag = UTIL_DEFAULT);

#endif