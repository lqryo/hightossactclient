#include "RawUDPServer.h" 
#include "TCPServer.h"
#include "SysController.h"

#include "CStitchThread.h"
#include "AnalyThread.h"
#include "LogInCaWidget.h"
#include "PlaybackWidget.h"
#include "SetParam.h"
SysController::SysController()
{
	Init();
}

SysController& SysController::getInstance(){
	static SysController instance;
	return instance;
}

SysController::~SysController()
{
}

void SysController::Init()
{
	pDoc = new CDocument();
	CRawUDPServer* udpServer = CRawUDPServer::instance();
	udpServer->start();

	//接收传输数据TCPServer初始化
	tcpServer = CTCPServer::instance();
	tcpServer->moveToThread(&tcpServerThread);
	tcpServerThread.start();
	tcpServer->run(pDoc);

	//设置界面的初始化
	pSetParam = new SetParam(pDoc);

	//子线程初始化
	pCStitchThread = new CStitchThread(pDoc->par_all);
	pCStitchThread->moveToThread(&StitchSubThread);
	StitchSubThread.start();

	pAnalyThread = new AnalyThread(pDoc);
	pAnalyThread->moveToThread(&AnalySubThread);
	AnalySubThread.start();
}

//连接函数
void SysController::connections()
{
	//网络传输来的图片进行拼接
	connect(tcpServer, SIGNAL(AnalyStart_Sig()), pAnalyThread, SLOT(AnalysisFunction()));//接收传输信号进行分析链接
	//connect(pAnalyThread, SIGNAL(start_Sig()), this, SLOT(SendmsgStart()), Qt::QueuedConnection);
	connect(pAnalyThread, SIGNAL(start_Sig(CDocument*, int)), pCStitchThread, SLOT(StitchFunction(CDocument*, int)), Qt::QueuedConnection);
	//connect(pCStitchThread, SIGNAL(ShowFrame_Sig()), this, SLOT(updateFrame()), Qt::QueuedConnection);

}
