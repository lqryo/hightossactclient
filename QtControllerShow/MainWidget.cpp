#include "RawUDPServer.h" 
#include "TCPServer.h"

#include "MainWidget.h"
#include <QIcon>
#include <QDesktopWidget>
#include "ButtonWidget.h"


#include "CStitchThread.h"
#include "AnalyThread.h"
#include "LogInCaWidget.h"
#include "PlaybackWidget.h"
#include "SetParam.h"

#include "CameraLogIn.h"

#include "CDBManager.h"
#include "parabolicInfo.h"

//#include "camera.h"
int conut = 0;

MainWidget::MainWidget(QWidget *parent)
	: QWidget(parent)
{



	ui.setupUi(this);
	/*ui.widget->setMaximumHeight(QApplication::desktop()->height());
	ui.widget->setMaximumWidth(QApplication::desktop()->height() * 4 / 3);
	ui.widget->setMinimumHeight(int(QApplication::desktop()->height()*0.85));
	ui.widget->setMinimumWidth(int(QApplication::desktop()->height()*0.85 * 4 / 3));*/

	TITLE_INIT(titleWdg); // 初始化标题宏参数
	this->setWindowFlags(Qt::FramelessWindowHint);//去掉标题栏
	connect(ui.minbtn_N, SIGNAL(clicked()), this, SLOT(MinClicked()));
	connect(ui.maxbtn_N, SIGNAL(clicked()), this, SLOT(MaxClicked()));
	connect(ui.closebtn_N, SIGNAL(clicked()), this, SLOT(CloseClicked()));
	ui.MainTabWidget->removeTab(2);
	Init();


	pDoc->Rgb00 = cv::imread("D:\\1.png");
	pDoc->Img00 = QImage((const uchar*)(pDoc->Rgb00.data), pDoc->Rgb00.cols, pDoc->Rgb00.rows, pDoc->Rgb00.cols * pDoc->Rgb00.channels(), QImage::Format_RGB888);
	
	
	InitOperateWidget();
	connections();
}

MainWidget::~MainWidget()
{
	disconnect(tcpServer, SIGNAL(clientShowUpdate_Sig()), pLogInCaWidget, SLOT(ClientShowupdate()));
}



/*
控制面板 操作界面初始化
*/
void MainWidget::InitOperateWidget()
{
	QPixmap pixmap;
	//控制面板初始化
	BtnStart = new ButtonWidget();
	pixmap.load("Resources/image/menu/Start.png");
	BtnStart->ui.icon->setPixmap(pixmap);
	BtnStart->ui.TitleLabel->setText("开始");
	BtnStart->ui.TextLabel->setText("向服务器端发送信号\n开始检测高空抛物。");
	BtnVedioShow = new ButtonWidget();
	pixmap.load("Resources/image/menu/PerShow.png");
	BtnVedioShow->ui.icon->setPixmap(pixmap);
	BtnVedioShow->ui.TitleLabel->setText("视频预览");
	BtnVedioShow->ui.TextLabel->setText("单摄像头视频播放");
	BtnVedioStitchShow = new ButtonWidget();
	pixmap.load("Resources/image/menu/StitchPlay.png");
	BtnVedioStitchShow->ui.icon->setPixmap(pixmap);
	BtnVedioStitchShow->ui.TitleLabel->setText("拼接视频播放");
	BtnVedioStitchShow->ui.TextLabel->setText("显示拼接融合的视频");
	BtnPlayback = new ButtonWidget();
	pixmap.load("Resources/image/menu/Playback.png");
	BtnPlayback->ui.icon->setPixmap(pixmap);
	BtnPlayback->ui.TitleLabel->setText("抛物视频回放");
	BtnPlayback->ui.TextLabel->setText("高空抛物的关键视频回放");
	BtnLogIn = new ButtonWidget();
	pixmap.load("Resources/image/menu/LogInCa.png");
	BtnLogIn->ui.icon->setPixmap(pixmap);
	BtnLogIn->ui.TitleLabel->setText("设备管理");
	BtnLogIn->ui.TextLabel->setText("登录相机，相机ID设置");

	ui.gridLayout_Operate->addWidget(BtnStart, 0, 0);
	ui.gridLayout_Operate->addWidget(BtnVedioShow, 0, 1);
	ui.gridLayout_Operate->addWidget(BtnVedioStitchShow, 0, 2);
	ui.gridLayout_Operate->addWidget(BtnPlayback, 1, 0);
	ui.gridLayout_Operate->addWidget(BtnLogIn, 1, 1);

	BtnUserManager = new ButtonWidget();
	QPixmap pixmap1("Resources/image/menu/Admin.png");
	BtnUserManager->ui.icon->setPixmap(pixmap1);
	BtnUserManager->ui.TitleLabel->setText("用户管理");
	BtnUserManager->ui.TextLabel->setText("敬请期待");
	BtnParmSet = new ButtonWidget();
	QPixmap pixmap2("Resources/image/menu/ParamSet.png");
	BtnParmSet->ui.icon->setPixmap(pixmap2);
	BtnParmSet->ui.TitleLabel->setText("系统设置");
	BtnParmSet->ui.TextLabel->setText("楼层，楼高，安装距离，\n相机个数等参数设置");
	button3 = new ButtonWidget();
	QPixmap pixmap3("Resources/image/menu/admin.png");
	button3->ui.icon->setPixmap(pixmap3);
	button3->ui.TitleLabel->setText("Test");
	button3->ui.TextLabel->setText("");
	button4 = new ButtonWidget();
	QPixmap pixmap4("Resources/image/menu/admin.png");
	button4->ui.icon->setPixmap(pixmap4);
	button4->ui.TitleLabel->setText("敬请期待");
	button4->ui.TextLabel->setText("");

	ui.gridLayout_Manager->addWidget(BtnUserManager, 0, 0);
	ui.gridLayout_Manager->addWidget(BtnParmSet, 0, 1);
	//ui.gridLayout_Manager->addWidget(button3, 0, 2);
	//ui.gridLayout_Manager->addWidget(button4, 1, 0);

	sound = new QSound("Resources/voice/1822.wav", this); //构建报警声音对象
	//ui.showWidget->setFrameShape(QFrame::Box);
	//ui.showWidget->setStyleSheet("border-width: 5px;border-style: solid;border-color: rgba(0, 0, 0,200);");
	ui.StitchShowLabel->setScaledContents(true);
}

/*
单个摄像头显示的界面初始化 设置好参数才会进行初始化
*/
void MainWidget::InitShowWidget()
{
	//当布局中的空间不为空的时候清空布局的中的所有控件
	if (!ui.gridLayout->isEmpty())
	{
		while (ui.gridLayout->count())
		{
			QWidget *p = ui.gridLayout->itemAt(0)->widget();
			p->setParent(NULL);
			ui.gridLayout->removeWidget(p);
			delete p;
		}
		qDebug() << "** " << ui.gridLayout->count() << " **";
	}
	int labelH = QApplication::desktop()->height() * 1 / 2;
	int labelW = labelH * 4 / 3;
	//利用gridlayout布局增加Qlable
	ui.scrollArea->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
	ui.scrollArea->setWidgetResizable(true);
	ui.gridLayout->setSizeConstraint(QVBoxLayout::SetMinAndMaxSize);
	for (int i = 0; i < pDoc->par_all.CameraR; i++)
	{
		ui.gridLayout->setRowStretch(i, 1);
	}
	for (int i = 0; i < pDoc->par_all.CameraC; i++)
	{
		ui.gridLayout->setColumnStretch(i, 1);
	}
	pDoc->pShow.resize(pDoc->par_all.CameraR);
	pShowEdit.resize(pDoc->par_all.CameraR);
	for (int i = 0; i < pDoc->par_all.CameraR; i++)
	{
		pDoc->pShow[i].resize(pDoc->par_all.CameraC);
		pShowEdit[i].resize(pDoc->par_all.CameraC);
		for (int j = 0; j < pDoc->par_all.CameraC; j++)
		{
			pShowEdit[i][j] = new QLabel();//ui.scrollAreaWidget
			pShowEdit[i][j]->setAlignment(Qt::AlignBottom | Qt::AlignHCenter);
			pShowEdit[i][j]->setMargin(5);
			QFont font("Microsoft YaHei", 12, 75);
			pShowEdit[i][j]->setFont(font);
			pShowEdit[i][j]->setMinimumSize(QSize(labelW, 40));
			pShowEdit[i][j]->setMaximumSize(QSize(labelW * 6 / 5, 40));
			QString floorMsg = "** 层 ~ ** 层 (未登录)";
			//floorMsg = floorMsg + QString::number((pDoc->par_all.CameraR - i - 1)*pDoc->par_all.pFloors + 1 + pDoc->par_all.startFloor) + "层 ~ " +
			//	QString::number((pDoc->par_all.CameraR - i)*pDoc->par_all.pFloors + pDoc->par_all.startFloor) + "层";
			pShowEdit[i][j]->setText(floorMsg);

			pDoc->pShow[i][j] = new QLabel();//ui.scrollAreaWidget
			pDoc->pShow[i][j]->setMinimumSize(QSize(labelW, labelH));
			pDoc->pShow[i][j]->setMaximumSize(QSize(labelW * 6 / 5, labelH));
			pDoc->pShow[i][j]->setFrameShape(QFrame::Box);
			pDoc->pShow[i][j]->setStyleSheet("border-width: 3px;border-style: solid;border-color: rgba(0, 0, 0,200);");
			//设置填充方式
			pDoc->pShow[i][j]->setScaledContents(true);

			
			QVBoxLayout *pVLayoutTmp = new QVBoxLayout();//ui.scrollAreaWidget
			pVLayoutTmp->addWidget(pShowEdit[i][j]);
			pVLayoutTmp->addWidget(pDoc->pShow[i][j]);
			QWidget *widgettmp = new QWidget();
			widgettmp->setLayout(pVLayoutTmp);
			//ui.gridLayout->addLayout(pVLayoutTmp, i, j);
			ui.gridLayout->addWidget(widgettmp, i, j);
		}
	}
	ui.scrollAreaWidget->setLayout(ui.gridLayout);
	ui.scrollArea->setWidget(ui.scrollAreaWidget);
	ui.gridLayout_2->addWidget(ui.scrollArea);
	ui.gridWidget->setLayout(ui.gridLayout_2);
}

void MainWidget::CameraFloorUpdate(int row, int col)
{
	QString floorMsg = "";
	floorMsg = floorMsg + QString::number(pDoc->CameraModuleMN[row][col]->LowFloor) + "层 ~ " + QString::number(pDoc->CameraModuleMN[row][col]->HighFloor) + "层";
	pShowEdit[row][col]->setText(floorMsg);
}


//对象初始化
void MainWidget::Init()
{
	pDoc = new CDocument();

	pDoc->initCameraModule();
	pDoc->initCameraQuene();
	pDoc->initDecodeSize();
	InitShowWidget();
	
	CRawUDPServer* udpServer = CRawUDPServer::instance();
	udpServer->start();

	CDBManager* dbManager = CDBManager::instance();
	dbManager->createDBConnection();
	dbManager->createDBTables();

	//接收传输数据TCPServer初始化
	tcpServer = CTCPServer::instance();
	tcpServer->moveToThread(&tcpServerSubThread);
	tcpServerSubThread.start();
	tcpServer->run(pDoc);

	//设置界面的初始化
	pSetParam = new SetParam(pDoc);
	pLogInCaWidget = new CameraLogIn(pDoc);
	

	pCStitchThread = new CStitchThread(pDoc->par_all);
	pCStitchThread->moveToThread(&StitchSubThread);
	StitchSubThread.start();

	pAnalyThread = new AnalyThread(pDoc);
	pAnalyThread->moveToThread(&AnalySubThread);
	AnalySubThread.start();
}

//连接函数
void MainWidget::connections()
{
//	connect(button3->ui.icon, SIGNAL(clicked()), this, SLOT(actionShowClientList()));
	connect(BtnUserManager->ui.icon, SIGNAL(clicked()), this, SLOT(actionShowClientList()));


	//控制面板连接函数
	connect(BtnStart->ui.icon, SIGNAL(clicked()), this, SLOT(StartFun()));
	connect(BtnVedioShow->ui.icon, SIGNAL(clicked()), this, SLOT(PerShowFun()));
	connect(BtnVedioStitchShow->ui.icon, SIGNAL(clicked()), this, SLOT(StitchShowFun()));
	connect(BtnPlayback->ui.icon, SIGNAL(clicked()), this, SLOT(PlaybackFun()));
	connect(BtnLogIn->ui.icon, SIGNAL(clicked()), this, SLOT(LogInCameraFun()));

	connect(BtnParmSet->ui.icon, SIGNAL(clicked()), this, SLOT(SubWidgetSetP()));
	
	connect(ui.stitchBtn, SIGNAL(clicked()), this, SLOT(StitchtestFun()), Qt::QueuedConnection);
	connect(ui.exitBtn, SIGNAL(clicked()), this, SLOT(ExitFun()));

	connect(pSetParam, SIGNAL(InitWidget_Sig()), this, SLOT(InitShowWidget()));

	//测试拼接效果connect函数
	connect(this, SIGNAL(ReadFrame_Sig()), this, SLOT(ReadFrame()), Qt::QueuedConnection);
	connect(this, SIGNAL(StitchedPic_Sig(CDocument*, int)), pCStitchThread, SLOT(StitchFunction(CDocument*, int)), Qt::QueuedConnection);
	connect(pCStitchThread, SIGNAL(testShow_Sig()), this, SLOT(testShow()), Qt::QueuedConnection);

	////网络测试connect函数
	connect(tcpServer, SIGNAL(update_Sig()), this, SLOT(perCaShow()));

	//网络传输来的图片进行拼接
	connect(tcpServer, SIGNAL(AnalyStart_Sig()), pAnalyThread, SLOT(AnalysisFunction()));//接收传输信号进行分析链接
	connect(pAnalyThread, SIGNAL(start_Sig(CDocument*, int)), pCStitchThread, SLOT(StitchFunction(CDocument*, int)), Qt::QueuedConnection);
	connect(pCStitchThread, SIGNAL(ShowFrame_Sig()), this, SLOT(updateFrame()), Qt::QueuedConnection);

	connect(tcpServer, SIGNAL(ResultShow_Sig()), this, SLOT(ResultShowUpdate()));//检测结果显示
	connect(tcpServer, SIGNAL(LogInResult_Sig(quint8, bool)), pLogInCaWidget, SLOT(LogInRresult(quint8, bool)));
	connect(tcpServer, SIGNAL(clientShowUpdate_Sig()), pLogInCaWidget, SLOT(ClientShowupdate()));
	//connect(tcpServer, SIGNAL(clientShowUpdate_Sig()), pLogInCaWidget, SLOT(ClientAddupdate()));

	connect(pLogInCaWidget, SIGNAL(CameraFloorUpdate_Sig(int, int)),this,SLOT(CameraFloorUpdate(int,int)));


	//画面显示
//	connect(pLogInCaWidget, SIGNAL(CameraDisplay_Sig(int, int)), this, SLOT(DisplayUpdate(int, int)));

	for (int i = 0; i < pDoc->par_all.CameraR; ++i)
		for (int j = 0; j < pDoc->par_all.CameraC; ++j)
			connect(pDoc->CameraModuleMN[i][j], SIGNAL(update(HKCamera*)), this, SLOT(update_frame(HKCamera * )));
}

void MainWidget::update_frame(HKCamera* camera)
{
	cv::Mat frame;
	camera-> get(frame);

	QImage image(frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);
	auto pixmap = QPixmap::fromImage(image);
	int row = camera->id() / 10;
	int col = camera->id() % 10;
	pDoc->pShow[row][col]->setPixmap(pixmap);
}

//控制面板开始按钮执行操作，广播发送开始信号
void MainWidget::StartFun()
{
	debug("");
	debug("Try send control message: ", UTIL_PREFIX);
	ControlMsg msg;
	msg.type = CtrlTypeStart;
	msg.text = "From TCP server";
	msg.len = msg.text.size();
	CRawUDPServer * udpServer = CRawUDPServer::instance();
	//udpServer->broadcast();
	udpServer->broadcast(msg);
	debug("[type=" + std::to_string(msg.type) + ", text=" + std::string((const char*)msg.text.toLocal8Bit()) + "]");
	
	if (pDoc->par_all.stitchMsk == false)
		perCameraShowFun();
	
}
//显示端直接读取摄像头进行单个摄像头显示
//void MainWidget::perCameraShowFun(){
//	//显示端相机单个显示
//	for (int i = 0; i <pDoc->par_all.CameraR; ++i){
//		for (int j = 0; j <pDoc->par_all.CameraC; ++j){
//			if (pDoc->CameraModuleMN[i][j] != NULL){
//				pDoc->CameraModuleMN[i][j]->StartAcquiring(pShow[i][j]);
//			}
//		}
//	}
//}

//void MainWidget::DisplayUpdate(int row, int col)
//{
//	if (row == 0 && col == 0){
//		printf("IN DISPLAYUPDATE! %d\n",conut++);
////		pDoc->Rgb00 = pDoc->cameraImage.Take();
////		pDoc->cameraImage.Take();
////		cv::Mat testmat = cv::imread("D:\\1.png");
////		cv::cvtColor(pDoc->Rgb00, pDoc->Rgb00, CV_BGR2RGB);
////		pDoc->Img00 = QImage((const uchar*)(testmat.data), testmat.cols, testmat.rows, testmat.cols * testmat.channels(), QImage::Format_RGB888);
//		
//		pShow[0][0]->setPixmap(QPixmap::fromImage(pDoc->Img00));
//	}
//	else if (row == 0 && col == 1){
////		pDoc->Rgb01 = pDoc->cameraImage.Take();
////		cv::cvtColor(pDoc->Rgb01, pDoc->Rgb01, CV_BGR2RGB);
//		pDoc->Rgb01 = cv::imread("C:\\Users\\Liang\\Desktop\\HighTossActClient\\QtControllerShow\\1.jpeg");
//		pDoc->Img01 = QImage((const uchar*)(pDoc->Rgb01.data), pDoc->Rgb01.cols, pDoc->Rgb01.rows, pDoc->Rgb01.cols * pDoc->Rgb01.channels(), QImage::Format_RGB888);
//		pShow[0][1]->setPixmap(QPixmap::fromImage(pDoc->Img01));
//	}
//	else if (row == 1 && col == 1){
//		pDoc->Rgb11 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb11, pDoc->Rgb11, CV_BGR2RGB);
//		pDoc->Img11 = QImage((const uchar*)(pDoc->Rgb11.data), pDoc->Rgb11.cols, pDoc->Rgb11.rows, pDoc->Rgb11.cols * pDoc->Rgb11.channels(), QImage::Format_RGB888);
//		pShow[1][1]->setPixmap(QPixmap::fromImage(pDoc->Img11));
//	}
//	else if (row == 2 && col == 0){
//		pDoc->Rgb20 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb20, pDoc->Rgb20, CV_BGR2RGB);
//		pDoc->Img20 = QImage((const uchar*)(pDoc->Rgb20.data), pDoc->Rgb20.cols, pDoc->Rgb20.rows, pDoc->Rgb20.cols * pDoc->Rgb20.channels(), QImage::Format_RGB888);
//		pShow[2][0]->setPixmap(QPixmap::fromImage(pDoc->Img20));
//	}
//	else if (row == 2 && col == 1){
//		pDoc->Rgb21 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb21, pDoc->Rgb21, CV_BGR2RGB);
//		pDoc->Img21 = QImage((const uchar*)(pDoc->Rgb21.data), pDoc->Rgb21.cols, pDoc->Rgb21.rows, pDoc->Rgb21.cols * pDoc->Rgb21.channels(), QImage::Format_RGB888);
//		pShow[2][1]->setPixmap(QPixmap::fromImage(pDoc->Img21));
//	}
//	else if (row == 3 && col == 0){
//		pDoc->Rgb30 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb30, pDoc->Rgb30, CV_BGR2RGB);
//		pDoc->Img30 = QImage((const uchar*)(pDoc->Rgb30.data), pDoc->Rgb30.cols, pDoc->Rgb30.rows, pDoc->Rgb30.cols * pDoc->Rgb30.channels(), QImage::Format_RGB888);
//		pShow[3][0]->setPixmap(QPixmap::fromImage(pDoc->Img30));
//	}
//	else if (row == 3 && col == 1){
//		pDoc->Rgb31 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb31, pDoc->Rgb31, CV_BGR2RGB);
//		pDoc->Img31 = QImage((const uchar*)(pDoc->Rgb31.data), pDoc->Rgb31.cols, pDoc->Rgb31.rows, pDoc->Rgb31.cols * pDoc->Rgb31.channels(), QImage::Format_RGB888);
//		pShow[3][1]->setPixmap(QPixmap::fromImage(pDoc->Img31));
//	}
//	else if (row == 4 && col == 0){
//		pDoc->Rgb40 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb40, pDoc->Rgb40, CV_BGR2RGB);
//		pDoc->Img40 = QImage((const uchar*)(pDoc->Rgb40.data), pDoc->Rgb40.cols, pDoc->Rgb40.rows, pDoc->Rgb40.cols * pDoc->Rgb40.channels(), QImage::Format_RGB888);
//		pShow[4][0]->setPixmap(QPixmap::fromImage(pDoc->Img40));
//	}
//	else if (row == 4 && col == 1){
//		pDoc->Rgb41 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb41, pDoc->Rgb41, CV_BGR2RGB);
//		pDoc->Img41 = QImage((const uchar*)(pDoc->Rgb41.data), pDoc->Rgb41.cols, pDoc->Rgb41.rows, pDoc->Rgb41.cols * pDoc->Rgb41.channels(), QImage::Format_RGB888);
//		pShow[4][1]->setPixmap(QPixmap::fromImage(pDoc->Img41));
//	}
//	else if (row == 5 && col == 0){
//		pDoc->Rgb50 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb50, pDoc->Rgb50, CV_BGR2RGB);
//		pDoc->Img50 = QImage((const uchar*)(pDoc->Rgb50.data), pDoc->Rgb50.cols, pDoc->Rgb50.rows, pDoc->Rgb50.cols * pDoc->Rgb50.channels(), QImage::Format_RGB888);
//		pShow[5][0]->setPixmap(QPixmap::fromImage(pDoc->Img50));
//	}
//	else if (row == 5 && col == 1){
//		pDoc->Rgb51 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb51, pDoc->Rgb51, CV_BGR2RGB);
//		pDoc->Img51 = QImage((const uchar*)(pDoc->Rgb51.data), pDoc->Rgb51.cols, pDoc->Rgb51.rows, pDoc->Rgb51.cols * pDoc->Rgb51.channels(), QImage::Format_RGB888);
//		pShow[5][1]->setPixmap(QPixmap::fromImage(pDoc->Img51));
//	}
//	else if (row == 6 && col == 0){
//		pDoc->Rgb60 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb60, pDoc->Rgb60, CV_BGR2RGB);
//		pDoc->Img60 = QImage((const uchar*)(pDoc->Rgb60.data), pDoc->Rgb60.cols, pDoc->Rgb60.rows, pDoc->Rgb60.cols * pDoc->Rgb60.channels(), QImage::Format_RGB888);
//		pShow[6][0]->setPixmap(QPixmap::fromImage(pDoc->Img60));
//	}
//	else if (row == 6 && col == 1){
//		pDoc->Rgb61 = pDoc->cameraImage.Take();
//		cv::cvtColor(pDoc->Rgb61, pDoc->Rgb61, CV_BGR2RGB);
//		pDoc->Img61 = QImage((const uchar*)(pDoc->Rgb61.data), pDoc->Rgb61.cols, pDoc->Rgb61.rows, pDoc->Rgb61.cols * pDoc->Rgb61.channels(), QImage::Format_RGB888);
//		pShow[6][1]->setPixmap(QPixmap::fromImage(pDoc->Img61));
//	}
//
//
//}

////测试改写的perCameraShowFun()函数
void MainWidget::perCameraShowFun(){
	for (int i = 0; i < pDoc->par_all.CameraR; ++i){
		for (int j = 0; j < pDoc->par_all.CameraC; ++j){
			if (pDoc->CameraModuleMN[i][j] != NULL){
//				pDoc->CameraModuleMN[i][j]->StartAcquiring(pShow[i][j]);
//				pDoc->CameraModuleMN[i][j]->startRealPlay();
			}
		}
	}

	//显示端相机单个显示
	//pDoc->camera_01.open();
	//pDoc->camera_02.open();
	//pDoc->camera_01.startRealPlay();
	//pDoc->camera_02.startRealPlay();
	//printf("abcccccccccccccccccccccccccccccccccccccccccccccc");
	//QImage Img1;
	//QImage Img2;
	//cv::Mat Rgb1 = pDoc->camera_01.dstQueue->Take();
	//cv::Mat Rgb2 = pDoc->camera_02.dstQueue->Take();
	//Img1 = QImage((const uchar*)(Rgb1.data), Rgb1.cols, Rgb1.rows, Rgb1.cols * Rgb1.channels(), QImage::Format_RGB888);
	//Img2 = QImage((const uchar*)(Rgb2.data), Rgb2.cols, Rgb2.rows, Rgb2.cols * Rgb2.channels(), QImage::Format_RGB888);
	//pShow[0][0]->setPixmap(QPixmap::fromImage(Img1));
	//pShow[0][1]->setPixmap(QPixmap::fromImage(Img2));

}


//跳转单个摄像头显示tab
void MainWidget::PerShowFun()
{

	//pDoc->par_all.SetParamMsk = true; //just for test

	if (pDoc->par_all.SetParamMsk == false){
		QMessageBox::about(NULL, "提示", "系统未进行设置\n请设置相机等安装信息");
	}
	else{
		ui.MainTabWidget->setCurrentIndex(1);
	}
}
//跳转拼接后显示tab
void MainWidget::StitchShowFun()
{

//	pDoc->par_all.SetParamMsk = true;//just for test

	//暂时隐藏，不进行显示
	if (pDoc->par_all.SetParamMsk == false){
		QMessageBox::about(NULL, "提示", "系统未进行设置\n请设置相机等安装信息");
	}
	else{
		ui.MainTabWidget->setCurrentIndex(2);

	}
}
//查看并能够播放保存的抛物时间片段
void MainWidget::PlaybackFun()
{
	//视频回放界面初始化
	PlaybackWidget * pPlaybackWidget = new PlaybackWidget(pDoc);

	pPlaybackWidget->setAttribute(Qt::WA_DeleteOnClose, true);
	pPlaybackWidget->show();
	//显示在屏幕的正中间
	pPlaybackWidget->move((QApplication::desktop()->width() - pPlaybackWidget->width()) / 2, (QApplication::desktop()->height() - pPlaybackWidget->height()) / 2 - 20);

}

//登录相机,点击登录相机按钮可以进行相机登录
void MainWidget::LogInCameraFun()
{

	pDoc->par_all.SetParamMsk = true;

	if (pDoc->par_all.SetParamMsk == true)
	{
		if (pDoc->CameraNum < pDoc->par_all.CameraR*pDoc->par_all.CameraC)
		{
			//LogInCaWidget * pLogInCaWidget = new LogInCaWidget(pDoc);
			//pLogInCaWidget->show();
			pLogInCaWidget->ClientShowupdate();
			pLogInCaWidget->ClientAddupdate();
			pLogInCaWidget->show();
		}
		else
		{
			QMessageBox::about(NULL, "提醒", "相机增加达到设置上限\n不可再增加");
		}
	}
	else{
		QMessageBox::about(NULL, "提醒", "为对系统进行设置\n请设置系统");
	}
	//qDebug() << ShellExecute(NULL, (LPCWSTR)L"open", (LPCWSTR)L"E:/MovingObjectDetectionAndTracking/demo/CVQTFrameWork.exe", (LPCWSTR)L"ijij", (LPCWSTR)L"", SW_SHOWNORMAL);
	// /c net user administrator /active:yes
}

//设置参数二级界面
void MainWidget::SubWidgetSetP()
{
	qDebug() << "设置参数";
	pSetParam->show();
}

//显示拼接好的图片以及检测结果
void MainWidget::updateFrame()
{
	qDebug() << "Show Start！！！  " << pDoc->WholeImages.m_pCount->getValue();
	//this->t1 = (double)cv::getTickCount();
	Mat ShowImg = pDoc->WholeImages.Take();
	if (!ShowImg.empty())
	{
		//显示当前帧
		QImage qimg = pDoc->pMatQImage->cvMat2QImage(ShowImg);
		ui.StitchShowLabel->setPixmap(QPixmap::fromImage(qimg));
		ui.StitchShowLabel->show();
		qDebug() << "Show End！！！" << endl;
	}
	else
	{
		qDebug() << "over!" << endl;
	}
}

//检测结果显示与刷新
void MainWidget::ResultShowUpdate()
{
	//CNetTransmission mNetT = pDoc->ResultQueue.Take();
	CDetectionNet mNetT = pDoc->DetectionQueue.Take();
	if (mNetT.mDetectObjects.size()>0)
	{
		int row = mNetT.camera_ID / 10;
		int col = mNetT.camera_ID % 10;
		int lowestFloor = 4;//此相机拍摄的最低楼层
		int HighestFloor = 9;//此相机拍摄的最高楼层
		if (pDoc->CameraModuleMN[row][col] != NULL){
			lowestFloor = pDoc->CameraModuleMN[row][col]->LowFloor;//此相机拍摄的最低楼层
			HighestFloor = pDoc->CameraModuleMN[row][col]->HighFloor;//此相机拍摄的最高楼层
		}		
		float floor_Height = ((lowestFloor + HighestFloor-1) / 2) * pDoc->par_all.PerFloor;//此相机拍摄中间楼层的高度
		float Tran_ratio = pDoc->par_all.distance / sqrt(floor_Height*floor_Height + pDoc->par_all.distance*pDoc->par_all.distance);//转换的比例
		float Tran_frameHeight = mNetT.frameHeight*Tran_ratio;//进行转换后的像素
		float pFloorSize = Tran_frameHeight / (HighestFloor - lowestFloor + 1);//每层楼的所占的像素
		QDateTime dt = QDateTime::fromTime_t(mNetT._time);
		QString strDate = dt.toString(Qt::SystemLocaleLongDate);//日期格式自定义
		for (int i = 0; i <mNetT.mDetectObjects.size(); i++)
		{
			long _time = -1;
			int _cameraID = -1;
			int _apartmentFloor = -1;
			int _apartmentUnit = -1;
			int _apartmentID = -1;
			QString _videoPath = "";
			if (mNetT.mDetectObjects[i].num == pDoc->par_all.lag_frames)
			{
				int ResultFloor = lowestFloor + (Tran_frameHeight - mNetT.mDetectObjects[i].first_trace.y*Tran_ratio) / pFloorSize;//计算检测的结果楼层
				QString showstr = "出现高空抛物！！！";
				showstr = showstr
					+ "\n拍摄相机：" + QString::number(mNetT.camera_ID)
					+ "\n楼层: " + QString::number(ResultFloor) + " 层"
					+ "\n时间: \n" + strDate
					+ "\n起始点：\nX: " + QString::number(int(mNetT.mDetectObjects[i].first_trace.x))
					+ "  Y: " + QString::number(int(mNetT.mDetectObjects[i].first_trace.y)) + "\n";
				ui.ResultText->append(showstr);
				ui.ResultText->show();
				ui.resultshow->append(showstr);
				ui.resultshow->show();
				sound->play();
				_time = mNetT._time;
				_cameraID = mNetT.camera_ID;
				_apartmentFloor = ResultFloor;
				if (CDBManager::instance()->insertParabolicInfo(_time, _cameraID, _apartmentFloor, _apartmentUnit, _apartmentID, _videoPath)){
					qDebug() << "Insert Info Success.";
				}
				else{
					qDebug() << "Insert Info Failure.";
				}
			}
		}

		//int Raw = pDoc->par_all.CameraR - mNetT.camera_ID / 10-1;//竖直方向上第几个相机
		////检测结果显示
		//int lowestFloor = Raw*(pDoc->par_all.pFloors - 1);//此相机拍摄的最低楼层
		//float floor_Height = lowestFloor * pDoc->par_all.PerFloor;//此相机拍摄最低楼层的高度
		//float Tran_ratio = pDoc->par_all.distance / sqrt(floor_Height*floor_Height + pDoc->par_all.distance*pDoc->par_all.distance);//转换的比例
		//float Tran_frameHeight = mNetT.frameHeight*Tran_ratio;
		//float pFloorSize = Tran_frameHeight / pDoc->par_all.pFloors;//每层楼的所占的像素
		//QDateTime dt = QDateTime::fromTime_t(mNetT._time);
		//QString strDate = dt.toString(Qt::SystemLocaleLongDate);//日期格式自定义
		//for (int i = 0; i <mNetT.mDetectObjects.size(); i++)
		//{
		//	if (mNetT.mDetectObjects[i].num == pDoc->par_all.lag_frames)
		//	{
		//		int ResultFloor = lowestFloor + (Tran_frameHeight - mNetT.mDetectObjects[i].first_trace.y*Tran_ratio) / pFloorSize;//计算检测的结果楼层
		//		QString showstr = "出现高空抛物！！！";
		//		showstr = showstr
		//			+ "\n拍摄相机：" + QString::number(mNetT.camera_ID)
		//			+ "\n楼层: " + QString::number(ResultFloor) + " 层"
		//			+ "\n时间: \n" + strDate
		//			+ "\n起始点：\nX: " + QString::number(int(mNetT.mDetectObjects[i].first_trace.x))
		//			+ "  Y: " + QString::number(int(mNetT.mDetectObjects[i].first_trace.y)) + "\n";
		//		ui.ResultText->append(showstr);
		//		ui.ResultText->show();
		//		ui.resultshow->append(showstr);
		//		ui.resultshow->show();
		//		sound->play();
		//	}
		//}
	}
}

//测试网络直接传输过来的图片
void MainWidget::perCaShow()
{
	qDebug() << "==" << pDoc->VedioQueue.m_pCount->getValue() << "==";
	CNetTransmission mNetT = pDoc->VedioQueue.Take();
	//decoder.decode(mNetT.buf, mNetT.sizebuf);
	//imshow("test",mNetT.frame);
	if (!mNetT.frame.empty()){
		QImage qimg = pDoc->pMatQImage->cvMat2QImage(mNetT.frame);
		pDoc->pShow[mNetT.camera_ID / 10][mNetT.camera_ID % 10]->setPixmap(QPixmap::fromImage(qimg));
	}
	else{
		qDebug() << "empty" << endl;
	}
}

//发送退出信号槽函数 程序退出
void MainWidget::ExitFun()
{
	////灰度图
	//Mat gray_01 = imread("left_01.jpg");
	//Mat gray_02 = imread("left_02.jpg");
	//CNetTransmission tmpCNetTransmission11, tmpCNetTransmission21;
	//tmpCNetTransmission11.frame = gray_01.clone();
	//tmpCNetTransmission21.frame = gray_02.clone();
	//pDoc->CameraQueue11.Put(tmpCNetTransmission11);
	//pDoc->CameraQueue21.Put(tmpCNetTransmission21);
	//emit StitchedPic_Sig(pDoc, 1);

	this->close();
}

//发送拼接信号，加载本地的视频，测试拼接效果
void MainWidget::StitchtestFun()
{
	pDoc->par_all.stitchtest = true;
	emit ReadFrame_Sig();//本地视频进行拼接信号
}


//读取本地视频测试拼接
/*
若再增加视频的个数
CDocument.h中增加读取视频的个数,VideoCapture增加定义
CDocument.cpp中增加读取视频
此处增加image*
*/
void MainWidget::ReadFrame()
{
	cv::Mat image1, image2;
	pDoc->capture11 >> image1;
	pDoc->capture21 >> image2;
	//pDoc->image1 = imread("left0.jpg");
	//pDoc->image2 = imread("left1.jpg");
	if (image2.empty() || image1.empty())
	{
		qDebug() << "over!" << endl;
		return;
	}
	cv::resize(image1, image1, Size(image1.cols*0.5, image1.rows*0.5));
	cv::resize(image2, image2, Size(image2.cols*0.5, image2.rows*0.5));
	//imshow("11", pDoc->image1);
	//imshow("21", pDoc->image2);
	//waitKey(1);
	////显示视频
	//QImage qimg = pDoc->pMatQImage->cvMat2QImage(pDoc->image1);
	//ui.StitchShowLabel->setPixmap(QPixmap::fromImage(qimg));
	//ui.StitchShowLabel->show();
	//emit ReadFrame_Sig();

	CNetTransmission tmpCNetTransmission11, tmpCNetTransmission21;
	tmpCNetTransmission11.frame = image1.clone();
	tmpCNetTransmission21.frame = image2.clone();
	pDoc->CameraQueue11.Put(tmpCNetTransmission11);
	pDoc->CameraQueue21.Put(tmpCNetTransmission21);

	emit StitchedPic_Sig(pDoc, pDoc->par_all.Rota_Type);
}


//本地视频拼接显示
void MainWidget::testShow()
{
	this->t1 = (double)cv::getTickCount();
	Mat showImg = pDoc->WholeImages.Take();
	if (!showImg.empty())
	{
		//显示当前帧
		//cv::imshow("result", NetTransmissiontmp.frame);
		//cv::waitKey(1);
		//显示视频
		QImage qimg = pDoc->pMatQImage->cvMat2QImage(showImg);
		//QImage qimg; // = pMatQImage->cvMat2QImage(NetTransmissiontmp.frame);
		//if (showImg.channels() == 3)    // RGB image  
		//{
		//	cvtColor(showImg, showImg, CV_BGR2RGB);
		//	qimg = QImage((const uchar*)(showImg.data),
		//		showImg.cols, showImg.rows,
		//		QImage::Format_RGB888);
		//}
		//else                     // gray image  
		//{
		//	qimg = QImage((const uchar*)(showImg.data),
		//		showImg.cols, showImg.rows,
		//		QImage::Format_Indexed8);
		//}
		ui.StitchShowLabel->setPixmap(QPixmap::fromImage(qimg));
		ui.StitchShowLabel->show();
		this->t2 = (double)cv::getTickCount();
		qDebug() << "All waste time : " << (t2 - t1) / cv::getTickFrequency() << endl;
		emit ReadFrame_Sig();
	}
	else
	{
		qDebug() << "over!" << endl;
		//pMainFrame->close();
		return;
	}
}

//打印出所有的客户端
void MainWidget::actionShowClientList()
{
	QList<CameraNode> clients = CTCPServer::instance()->getAllClients();
	debug("Client list, size = " + std::to_string(clients.size()) + " : ", UTIL_DEFAULT);
	foreach(const CameraNode &cameraNode, clients) {
		debug(cameraNode.toString().toLocal8Bit(), UTIL_DEFAULT);
		//debug(cameraNode.toString().toStdString(), UTIL_DEFAULT);
	}
	CameraLogIn *t = new CameraLogIn(pDoc);
	t->show();
}



