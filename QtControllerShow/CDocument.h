#pragma once
#include "CBlockingQueue.h"
#include <opencv2\opencv.hpp>
#include <QTimer> 
#include <qmutex.h>
#include "DetectResults.h"
#include "CNetTransmission.h"
#include "MatQImage.h"
#include "CParams.h"
#include "CDetectionNet.h"
#include "CImageFromNet.h"
#include "H264_Decoder.h"

#include "CHKCamera.h"
#include "HKCamera.h"
////==============================================================================
//	@name		:	CDocument
//	@biref		:	定义系统的全局变量
//	@author		:	
//	@version	:	2017.11.28
//===============================================================================
typedef std::vector< std::vector<CBlockingQueue<cv::Mat>* > > CameraBlockQueue;

class CDocument {

public:
	CDocument();
	~CDocument();

	/*初始化相机队列的个数，m*n*/
	void initCameraModule();

	/*初始化解码对象队列的个数*/
	void initDecodeSize();
	void updateDecodeSize();

	/*初始化相机队列vector 和要拼接的图片的vector （m*n） */
	void initCameraQuene();
	void resetCameraQueneParams(int m, int n);

	/*判断所有相机队列是否都不为空*/
	bool isNetQueneNotEmpty();

	/*判断等待拼接的队列是否都不为空*/
	bool isAllStitchNotEmpty();

	/**获取相机队列的二维vector*/
	CameraBlockQueue getBlockQueueVector();
	void setBlockQueueVector(int i, int j, cv::Mat net);

	std::vector< std::vector<cv::Mat> > getCameraImage();
	void setCameraImage(int i, int j, cv::Mat mat);

public:
	// 相机读取的图片序列
	CBlockingQueue<cv::Mat> cameraImage;
	//相机对象，用二维数组保存
//	std::vector<std::vector<CCamera*>> CameraModuleMN;
	std::vector<std::vector<HKCamera*>> CameraModuleMN;
	QVector<QVector<QLabel*>>pShow;

//	HKCamera<192, 168, 1, 65> camera_01 = HKCamera<192, 168, 1, 65>::Instance("admin", "IDM8779235101");
//	HKCamera<192, 168, 1, 66> camera_02 = HKCamera<192, 168, 1, 66>::Instance("admin", "IDM8779235102");

//	std::vector<std::vector<ICamera*>> myCameraModuleMN;


	static int CameraNum;
	CBlockingQueue<CNetTransmission> CameraQueue;//保存网络传输来的数据
	CBlockingQueue<CNetTransmission> VedioQueue;
	CBlockingQueue<CNetTransmission> ResultQueue;//保存检测结果
	CBlockingQueue<CDetectionNet> DetectionQueue;//保存检测结果

	//测试使用
	//解析传输数据放到对应的相机队列中
	CBlockingQueue<CNetTransmission> CameraQueue11;
	CBlockingQueue<CNetTransmission> CameraQueue12;
	CBlockingQueue<CNetTransmission> CameraQueue21;
	CBlockingQueue<CNetTransmission> CameraQueue22;
	//CBlockingQueue<CNetTransmission>[2] CameraQueneVec;


	/***********************************************************
	相机队列  m*n m行n列相机， e.g. 4行2列相机对应相机摆放位置
	-----------
	| 00 | 01 |
	-----------
	| 10 | 11 |
	-----------
	| 20 | 21 |
	-----------
	| 30 | 31 |
	-----------
	************************************************************/
	CameraBlockQueue CameraQueneMN;
	std::vector< std::vector<cv::Mat> > CameraImageMN;

	std::vector< std::vector<H264_Decoder> > pH264_DecoderMN;

	//保存拼接完成的图片队列
	CBlockingQueue<cv::Mat> WholeImages;

	CNetTransmission WholeResult;

	//图像转换类
	MatQImage * pMatQImage;

	//视频当前帧
	cv::Mat nowframe;

	//配置参数
	CParams par_all;

	//测试使用	
	cv::VideoCapture capture11;
	cv::VideoCapture capture21;
	//cv::Mat image1;
	//cv::Mat image2;



	QImage Img00, Img01, Img10, Img11, Img20, Img21, Img30, Img31, Img40, Img41, Img50, Img51, Img60, Img61;
	cv::Mat Rgb00, Rgb01, Rgb10, Rgb11, Rgb20, Rgb21, Rgb30, Rgb31, Rgb40, Rgb41, Rgb50, Rgb51, Rgb60, Rgb61;
		

private:
	//相机队列和拼接队列的锁,避免重新设置相机队列参数时同时读取vector造成访问异常
	QMutex vec_mutex_bqueue;
	QMutex vec_mutex_mat;
};
