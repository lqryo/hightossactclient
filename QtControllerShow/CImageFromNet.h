#include <vector>
#include <QDataStream>
#include <iostream>
#include "Utils.h"
#include <stdint.h>
#include <vector>

class CImageFromNet
{
public:
	int camera_ID;//相机的编号
	long _time;//时间戳
	int sizebuf;
	quint8 *buf;
	CImageFromNet clone(){
		CImageFromNet a;
		a.camera_ID = this->camera_ID;
		a._time = this->_time;
		a.sizebuf = this->sizebuf;
		for (int i = 0; i < this->sizebuf; i++){
			a.buf[i] = this->buf[i];
		}
		return a;
	}
	~CImageFromNet(){

	}
};

inline QDataStream & operator <<(QDataStream &out, const CImageFromNet &image)
{
	out << image.camera_ID;
	quint64 time = image._time;
	out << time;				// timestamp
	out << image.sizebuf;
	int num;
	for (num = 0; num < image.sizebuf; num++){
		out << image.buf[num];
	}
	return out;
}

inline QDataStream & operator >>(QDataStream &in, CImageFromNet &image)
{
	in >> image.camera_ID;
	quint64 time;
	in >> time;
	image._time = time;
	in >> image.sizebuf;
	int size = image.sizebuf;
	image.buf = new uint8_t[size];
	int num;
	for (num = 0; num < size; num++){
		in >> image.buf[num];
	}
	return in;
}