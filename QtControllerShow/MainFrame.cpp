#include "RawUDPServer.h" 
#include "TCPServer.h"

#include "windows.h"
#include "MainFrame.h"
#include "CHKCamera.h"
#include "CCamera.h"
#include "CStitchThread.h"
#include "AnalyThread.h"
#include "LogInCaWidget.h"
#include "PlaybackWidget.h"
#include "SetParam.h"

MainFrame::MainFrame(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	getScreenInfo();
	Init();	
	InitOperateWidget();
	InitShowWidget();
	
	connections();
}

MainFrame::~MainFrame()
{

}

/*
控制面板 操作界面初始化
*/
void MainFrame::InitOperateWidget()
{
	ui.centralWidget->setMaximumHeight(QApplication::desktop()->height());
	ui.centralWidget->setMaximumWidth(QApplication::desktop()->width());
	ui.centralWidget->setMinimumHeight(int(QApplication::desktop()->height()*0.85));
	ui.centralWidget->setMinimumWidth(int(QApplication::desktop()->width()*0.88));

	//控制面板初始化
	BtnStart = new ButtonWidget();
	BtnStart->ui.TitleLabel->setText("开始");
	BtnStart->ui.TextLabel->setText("向服务器端发送信号\n开始检测高空抛物。");
	BtnVedioShow = new ButtonWidget();
	BtnVedioShow->ui.TitleLabel->setText("视频预览");
	BtnVedioShow->ui.TextLabel->setText("在界面中播放单摄像头的视频");
	BtnVedioStitchShow = new ButtonWidget();
	BtnVedioStitchShow->ui.TitleLabel->setText("拼接视频播放");
	BtnVedioStitchShow->ui.TextLabel->setText("敬请期待");
	BtnPlayback = new ButtonWidget();
	BtnPlayback->ui.TitleLabel->setText("抛物视频回放");
	BtnPlayback->ui.TextLabel->setText("敬请期待\n可以进行高空抛物的视频的回放");
	BtnLogIn = new ButtonWidget();
	BtnLogIn->ui.TitleLabel->setText("登录相机");
	BtnLogIn->ui.TextLabel->setText("登录相机，相机ID设置");

	ui.gridLayout_Operate->addWidget(BtnStart, 0, 0);
	ui.gridLayout_Operate->addWidget(BtnVedioShow, 0, 1);
	ui.gridLayout_Operate->addWidget(BtnVedioStitchShow, 0, 2);
	ui.gridLayout_Operate->addWidget(BtnPlayback, 1, 0);
	ui.gridLayout_Operate->addWidget(BtnLogIn, 1, 1);

	BtnUserManager = new ButtonWidget();
	QPixmap pixmap1("Resources/image/admin.png");
	BtnUserManager->ui.icon->setPixmap(pixmap1);
	BtnUserManager->ui.TitleLabel->setText("用户管理");
	BtnUserManager->ui.TextLabel->setText("待定");
	BtnParmSet = new ButtonWidget();
	BtnParmSet->ui.TitleLabel->setText("系统设置");
	BtnParmSet->ui.TextLabel->setText("设置楼层，楼高，安装距离，相机个数等参数");
	button3 = new ButtonWidget();
	button3->ui.TitleLabel->setText("待定");
	button3->ui.TextLabel->setText("待定");
	button4 = new ButtonWidget();
	button4->ui.TitleLabel->setText("待定");
	button4->ui.TextLabel->setText("");

	ui.gridLayout_Manager->addWidget(BtnUserManager, 0, 0);
	ui.gridLayout_Manager->addWidget(BtnParmSet, 0, 1);
	ui.gridLayout_Manager->addWidget(button3, 0, 2);
	ui.gridLayout_Manager->addWidget(button4, 1, 0);

	StartBtn = ui.startBtn;
	StopBtn = ui.stopBtn;
	ExitBtn = ui.exitBtn;
	RestartBtn = ui.restartBtn;
	table = ui.ResultText_2;
	sound = new QSound("./Resource/voice/1822.wav", this); //构建报警声音对象
	showlabel = ui.showWidget;
	showlabel->setFrameShape(QFrame::Box);
	showlabel->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 170, 0);");
	showlabel->setScaledContents(true);
}
/*
单个摄像头显示的界面初始化 设置好参数才会进行初始化
*/
void MainFrame::InitShowWidget()
{
	//只有设置参数成功时，单个摄像头显示界面才会进行界面初始化
	if (pDoc->par_all.SetParamMsk == true){
		//当布局中的空间不为空的时候清空布局的中的所有控件
		if (!ui.gridLayout->isEmpty())
		{
			qDebug() << "** " << ui.gridLayout->count() << " **";
			while (ui.gridLayout->count())
			{
				QWidget *p = ui.gridLayout->itemAt(0)->widget();
				p->setParent(NULL);
				ui.gridLayout->removeWidget(p);
				delete p;
			}
			qDebug() << "** " << ui.gridLayout->count() << " **";
		}
		int labelW = g_nActScreenW * 1 / 3;// this->width() * 3 / 5;
		int labelH = g_nActScreenW * 3 / 8;
		//利用gridlayout布局增加Qlable
		ui.scrollArea->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
		ui.scrollArea->setWidgetResizable(true);
		ui.gridLayout->setSizeConstraint(QVBoxLayout::SetMinAndMaxSize);		
		for (int i = 0;i < pDoc->par_all.CameraR; i++)
		{
			ui.gridLayout->setRowStretch(i, 1);
		}
		for (int i = 0; i < pDoc->par_all.CameraC; i++)
		{
			ui.gridLayout->setColumnStretch(i, 1);
		}
		pShow.resize(pDoc->par_all.CameraR);
		for (int i = 0; i < pDoc->par_all.CameraR; i++)
		{
			pShow[i].resize(pDoc->par_all.CameraC);
			for (int j = 0; j < pDoc->par_all.CameraC; j++)
			{
				pShow[i][j] = new QLabel(ui.scrollAreaWidget);
				pShow[i][j]->setMinimumSize(QSize(labelW, labelH));
				pShow[i][j]->setFrameShape(QFrame::Box);
				pShow[i][j]->setStyleSheet("border-width: 2px;border-style: solid;border-color: rgb(255, 170, 0);");
				//设置填充方式
				pShow[i][j]->setScaledContents(true);
				ui.gridLayout->addWidget(pShow[i][j], i, j);
			}
		}
		ui.scrollAreaWidget->setLayout(ui.gridLayout);
		ui.scrollArea->setWidget(ui.scrollAreaWidget);
		ui.gridLayout_2->addWidget(ui.scrollArea);
		ui.gridWidget->setLayout(ui.gridLayout_2);
	}
	
}

//初始化类
void MainFrame::Init()
{
	theTimer = new QTimer(this);
	pDoc = new CDocument();

	CRawUDPServer* udpServer = CRawUDPServer::instance();
	udpServer->start();

	//接收传输数据TCPServer初始化
	tcpServer = CTCPServer::instance();
	tcpServer->moveToThread(&tcpServerThread);
	tcpServerThread.start();
	tcpServer->run(pDoc);

	//设置界面的初始化
	pSetParam = new SetParam(pDoc);

	pCStitchThread = new CStitchThread(pDoc->par_all);
	pCStitchThread->moveToThread(&StitchSubThread);
	StitchSubThread.start();

	pAnalyThread = new AnalyThread(pDoc);
	pAnalyThread->moveToThread(&AnalySubThread);
	AnalySubThread.start();
}

//连接函数
void MainFrame::connections()
{
	connect(button3->ui.icon, SIGNAL(clicked()), this, SLOT(actionShowClientList()));

	//控制面板连接函数
	connect(BtnStart->ui.icon, SIGNAL(clicked()), this, SLOT(StartFun()));
	connect(BtnVedioShow->ui.icon, SIGNAL(clicked()), this, SLOT(PerShowFun()));
	connect(BtnVedioStitchShow->ui.icon, SIGNAL(clicked()), this, SLOT(StitchShowFun()));
	connect(BtnPlayback->ui.icon, SIGNAL(clicked()), this, SLOT(VedioShowFun()));
	connect(BtnLogIn->ui.icon, SIGNAL(clicked()), this, SLOT(LogInCameraFun()));
	
	connect(BtnParmSet->ui.icon, SIGNAL(clicked()), this, SLOT(SubWidgetSetP()));

	connect(StartBtn, SIGNAL(clicked()), this, SLOT(SendmsgStart()),Qt::QueuedConnection);
	//StartBtn->setEnabled(false);
	connect(StopBtn, SIGNAL(clicked()), this, SLOT(SendmsgStop()));
	connect(ExitBtn, SIGNAL(clicked()), this, SLOT(SendmsgExit()));
	connect(RestartBtn, SIGNAL(clicked()), this, SLOT(SendmsgRestart()));

	connect(pSetParam, SIGNAL(InitWidget_Sig()), this, SLOT(InitShowWidget()));

	connect(this, SIGNAL(updateFrame_Sig()), this, SLOT(updateFrame()), Qt::QueuedConnection);

	//测试拼接效果connect函数
	connect(this, SIGNAL(showFrame_Sig()), this, SLOT(ReadFrame()), Qt::QueuedConnection);
	connect(this, SIGNAL(StitchedPic_Sig(CDocument*, int)), pCStitchThread, SLOT(StitchFunction(CDocument*, int)), Qt::QueuedConnection);
	connect(pCStitchThread, SIGNAL(testShow_Sig()), this, SLOT(testShow()), Qt::QueuedConnection);
	
	//connect(this, SIGNAL(test_sig()), this, SLOT(NetupdateFrame()));
	connect(theTimer, SIGNAL(timeout()), this, SLOT(NetupdateFrame()));
	////网络测试connect函数
	connect(tcpServer, SIGNAL(start_Sig()), this, SLOT(SendmsgStart()), Qt::QueuedConnection);
	connect(tcpServer, SIGNAL(update_Sig()), this, SLOT(NetupdateFrame()), Qt::QueuedConnection);
	
	//网络传输来的图片进行拼接
	connect(tcpServer, SIGNAL(AnalyStart_Sig()), pAnalyThread, SLOT(AnalysisFunction()));//接收传输信号进行分析链接
	connect(pAnalyThread, SIGNAL(start_Sig()), this, SLOT(SendmsgStart()), Qt::QueuedConnection);
	connect(pAnalyThread, SIGNAL(start_Sig(CDocument*, int)), pCStitchThread, SLOT(StitchFunction(CDocument*, int)), Qt::QueuedConnection);
	connect(pCStitchThread, SIGNAL(ShowFrame_Sig()), this, SLOT(updateFrame()), Qt::QueuedConnection);
}

//控制面板开始按钮执行操作，广播发送开始信号
void MainFrame::StartFun()
{
	debug("");
	debug("Try send control message: ", UTIL_PREFIX);
	ControlMsg msg;
	msg.type = CtrlTypeStart;
	msg.text = "From TCP server";
	msg.len = msg.text.size();

	CRawUDPServer * udpServer = CRawUDPServer::instance();
	//udpServer->broadcast();
	udpServer->broadcast(msg);
	debug("[type=" + std::to_string(msg.type) + ", text=" + msg.text.toStdString() + "]");
}

//跳转单个摄像头显示tab
void MainFrame::PerShowFun()
{
	if (pDoc->par_all.SetParamMsk == false){
		QMessageBox::about(NULL, "提示", "系统未进行设置\n请设置相机等安装信息");
	}
	else{
		ui.tabWidget->setCurrentIndex(1);
	}	
}
//跳转拼接后显示tab
void MainFrame::StitchShowFun()
{
	if (pDoc->par_all.SetParamMsk == false){
		QMessageBox::about(NULL, "提示", "系统未进行设置\n请设置相机等安装信息");
	}
	else{
		ui.tabWidget->setCurrentIndex(2);
		
	}
}
//查看并能够播放保存的抛物时间片段
void MainFrame::VedioShowFun()
{	
	//视频回放界面初始化
	PlaybackWidget * pPlaybackWidget = new PlaybackWidget(pDoc);
	
	pPlaybackWidget->setAttribute(Qt::WA_DeleteOnClose, true);
	pPlaybackWidget->show();
	pPlaybackWidget->move((QApplication::desktop()->width() - pPlaybackWidget->width()) / 2, (QApplication::desktop()->height() - pPlaybackWidget->height()) / 2 - 20);

	//qDebug()<< ShellExecute(NULL, (LPCWSTR)L"open", (LPCWSTR)L"E:/MovingObjectDetectionAndTracking/demo/CVQTFrameWork.exe", (LPCWSTR)L"ijij", (LPCWSTR)L"", SW_SHOWNORMAL);
	//ShellExecute(NULL, "open", "NOTEPAD.EXE", NULL, NULL, SW_SHOWNORMAL); /c net user administrator /active:yes
	//ShellExecute(0, "open", LPCSTR("cmd.exe"), LPCSTR("/c net user administrator /active:yes"), "", SW_HIDE);
	//WinExec("E:/MovingObjectDetectionAndTracking/demo/CVQTFrameWork.exe", SW_MINIMIZE);
}
//登录相机
void MainFrame::LogInCameraFun()
{
	if (pDoc->par_all.SetParamMsk==true)
	{
		if (pDoc->CameraNum < pDoc->par_all.CameraR*pDoc->par_all.CameraC)
		{
			LogInCaWidget * pLogInCaWidget = new LogInCaWidget(pDoc);
			pLogInCaWidget->show();
		}
		else
		{
			QMessageBox::about(NULL, "提醒", "相机增加达到设置上限\n不可再增加");
		}
	}
	else{
		QMessageBox::about(NULL, "提醒", "为对系统进行设置\n请设置系统");
	}
	
		
	//qDebug() << ShellExecute(NULL, (LPCWSTR)L"open", (LPCWSTR)L"E:/MovingObjectDetectionAndTracking/demo/CVQTFrameWork.exe", (LPCWSTR)L"ijij", (LPCWSTR)L"", SW_SHOWNORMAL);
	// /c net user administrator /active:yes
}

//设置参数二级界面
void MainFrame::SubWidgetSetP()
{
	qDebug() << "设置参数";
	pSetParam->show();
	
}

//发送开始信号槽函数
void MainFrame::SendmsgStart()
{
	ui.label_2->setText("success!");
	pDoc->par_all.stitchtest = false;
	emit StitchedPic_Sig(pDoc, 1);//发送拼接信号
	//emit updateFrame_Sig();//发送拼接好的图片显示信号
	//emit showFrame_Sig();//本地视频进行拼接信号
}

//发送暂停信号槽函数
void MainFrame::SendmsgStop()
{	
	theTimer->start(30);
	//test_sig();
}

//发送退出信号槽函数 程序退出
void MainFrame::SendmsgExit()
{
	////灰度图
	//Mat gray_01 = imread("left_01.jpg");
	//Mat gray_02 = imread("left_02.jpg");
	//CNetTransmission tmpCNetTransmission11, tmpCNetTransmission21;
	//tmpCNetTransmission11.frame = gray_01.clone();
	//tmpCNetTransmission21.frame = gray_02.clone();
	//pDoc->CameraQueue11.Put(tmpCNetTransmission11);
	//pDoc->CameraQueue21.Put(tmpCNetTransmission21);
	//emit StitchedPic_Sig(pDoc, 1);

	this->close();
}

//发送重启信号槽函数  加载本地的视频，测试拼接效果
void MainFrame::SendmsgRestart()
{
	pDoc->par_all.stitchtest = true;
	emit showFrame_Sig();//本地视频进行拼接信号
}

//发送拼接好的图片显示信号  拼接好的图片以及检测结果进行显示函数
void MainFrame::updateFrame()
{

	qDebug() << "Show Start！！！  " << pDoc->WholeImages.m_pCount->getValue();
	//this->t1 = (double)cv::getTickCount();
	Mat ShowImg = pDoc->WholeImages.Take();
	if (!ShowImg.empty())
	{
		//显示当前帧
		QImage qimg = pDoc->pMatQImage->cvMat2QImage(ShowImg);
		showlabel->setPixmap(QPixmap::fromImage(qimg));
		showlabel->show();
		qDebug() << "Show End！！！" << endl;
	}
	else
	{
		qDebug() << "over!" << endl;
	}

	//qDebug() << "Show Start！！！  " << pDoc->WholeImages.m_pCount->getValue();
	//this->t1 = (double)cv::getTickCount();
	//CNetTransmission NetTransmissiontmp = pDoc->WholeImages.Take();
	//if (!NetTransmissiontmp.frame.empty())
	//{
	//	//显示当前帧
	//	cv::Mat showmat = NetTransmissiontmp.frame.clone();
	//	/////////////////////////////////////////////////////
	//	////显示视频
	//	//QImage qimg;
	//	//if (showmat.channels() == 3)    // RGB image  
	//	//{
	//	//	cvtColor(showmat, showmat, CV_BGR2RGB);
	//	//	qimg = QImage((const uchar*)(showmat.data),
	//	//		showmat.cols, showmat.rows,
	//	//		QImage::Format_RGB888);
	//	//}
	//	//else                     // gray image  
	//	//{
	//	//	qimg = QImage((const uchar*)(showmat.data),
	//	//		showmat.cols, showmat.rows,
	//	//		QImage::Format_Indexed8);
	//	//}
	//	QImage qimg = pDoc->pMatQImage->cvMat2QImage(showmat);
	//	showlabel->setPixmap(QPixmap::fromImage(qimg));
	//	showlabel->show();
	//	/////////////////////////////////////////////////////		
	//	if (NetTransmissiontmp.mDetectObjects.size() > 0)
	//	{
	//		for (int i = 0; i < NetTransmissiontmp.mDetectObjects.size(); i++)
	//		{
	//			//检测结果显示
	//			QString showstr = "起始点：\n";
	//			showstr = showstr
	//				+ "X: " + QString::number(NetTransmissiontmp.mDetectObjects[i].first_trace.x)
	//				+ "  Y: " + QString::number(NetTransmissiontmp.mDetectObjects[i].first_trace.y);
	//			table->append(showstr);
	//			table->show();
	//			sound->play();
	//		}
	//	}
	//	showmat.release();
	//	this->t2 = (double)cv::getTickCount();
	//	//qDebug() << "All waste time : " << (t2 - t1) / cv::getTickFrequency() << endl;
	//	qDebug() << "Show End！！！" << endl;
	//		//emit updateFrame_Sig();
	//	
	//}
	//else
	//{
	//	qDebug() << "over!" << endl;
	//	//return;
	//}
}

//读取本地视频测试拼接
void MainFrame::ReadFrame()
{
	cv::Mat image1, image2;
	pDoc->capture11 >> image1;
	pDoc->capture21 >> image2;
	if (image2.empty() || image1.empty())
	{
		qDebug() << "over!" << endl;
		return;
	}
	cv::resize(image1, image1, Size(image1.cols*0.5, image1.rows*0.5));
	cv::resize(image2, image2, Size(image2.cols*0.5, image2.rows*0.5));
	//imshow("11", pDoc->image1);
	//imshow("21", pDoc->image2);
	//waitKey(1);
	CNetTransmission tmpCNetTransmission11, tmpCNetTransmission21;
	tmpCNetTransmission11.frame = image1.clone();
	tmpCNetTransmission21.frame = image2.clone();
	pDoc->CameraQueue11.Put(tmpCNetTransmission11);
	pDoc->CameraQueue21.Put(tmpCNetTransmission21);

	emit StitchedPic_Sig(pDoc, pDoc->par_all.Rota_Type);
}

//本地视频拼接显示
void MainFrame::testShow()
{
	this->t1 = (double)cv::getTickCount();
	Mat showImg = pDoc->WholeImages.Take();
	if (!showImg.empty())
	{
		//显示当前帧
		//cv::imshow("result", NetTransmissiontmp.frame);
		//cv::waitKey(1);
		//显示视频
		QImage qimg = pDoc->pMatQImage->cvMat2QImage(showImg);
		showlabel->setPixmap(QPixmap::fromImage(qimg));
		showlabel->show();
		this->t2 = (double)cv::getTickCount();
		qDebug() << "All waste time : " << (t2 - t1) / cv::getTickFrequency() << endl;
		emit showFrame_Sig();
	}
	else
	{
		qDebug() << "over!" << endl;
		//pMainFrame->close();
		return;
	}

	//this->t1 = (double)cv::getTickCount();
	//CNetTransmission NetTransmissiontmp = pDoc->WholeImages.Take();
	//if (!NetTransmissiontmp.frame.empty())
	//{
	//	//显示当前帧
	//	cv::Mat showmat = NetTransmissiontmp.frame.clone();
	//	cv::imshow("result",showmat);
	//	cv::waitKey(1);
	//	//显示视频
	//	QImage qimg = pDoc->pMatQImage->cvMat2QImage(showmat);
	//	showlabel->setPixmap(QPixmap::fromImage(qimg));
	//	showlabel->show();
	//	if (NetTransmissiontmp.mDetectObjects.size()>0)
	//	{
	//		for (int i = 0; i <NetTransmissiontmp.mDetectObjects.size(); i++)
	//		{
	//			//检测结果显示
	//			QString showstr = "起始点：\n";
	//			showstr = showstr
	//				+ "X: " + QString::number(NetTransmissiontmp.mDetectObjects[i].first_trace.x)
	//				+ "  Y: " + QString::number(NetTransmissiontmp.mDetectObjects[i].first_trace.y);
	//			table->append(showstr);
	//			table->show();
	//			sound->play();
	//		}
	//	}
	//	showmat.release();
	//	this->t2 = (double)cv::getTickCount();
	//	qDebug() << "All waste time : " << (t2 - t1) / cv::getTickFrequency() << endl;
	//	emit showFrame_Sig();
	//}
	//else
	//{
	//	qDebug() << "over!" << endl;
	//	//pMainFrame->close();
	//	return;
	//}
}

void MainFrame::actionShowClientList()
{
	QList<CameraNode> clients = CTCPServer::instance()->getAllClients();
	debug("Client list, size = " + std::to_string(clients.size()) + " : ", UTIL_DEFAULT);
	foreach(const CameraNode &cameraNode, clients) {
		debug(cameraNode.toString().toStdString(), UTIL_DEFAULT);
	}

}


//测试网络直接传输过来的图片
void MainFrame::NetupdateFrame()
{
	/*pDoc->capture11 >> pDoc->image1;
	pDoc->capture21 >> pDoc->image2;
	QImage qimg1 = pDoc->pMatQImage->cvMat2QImage(pDoc->image1);
	QImage qimg2 = pDoc->pMatQImage->cvMat2QImage(pDoc->image2);
	pShow[0][0]->setPixmap(QPixmap::fromImage(qimg1));
	pShow[0][1]->setPixmap(QPixmap::fromImage(qimg2));
	pShow[1][0]->setPixmap(QPixmap::fromImage(qimg2));
	pShow[1][1]->setPixmap(QPixmap::fromImage(qimg1));*/

	//CNetTransmission NetTransmissiontmp = pDoc->CameraQueue.Take();
	//if (!NetTransmissiontmp.frame.empty())
	//{
	//	//显示当前帧
	//	cv::Mat showmat = NetTransmissiontmp.frame.clone();
	//	imshow("result", showmat);
	//	cv::waitKey(1);
	//	/////////////////////////////////////////////////////
	//	//显示视频
	//	QImage qimg;
	//	if (showmat.channels() == 3)    // RGB image  
	//	{
	//		cvtColor(showmat, showmat, CV_BGR2RGB);
	//		qimg = QImage((const uchar*)(showmat.data),
	//			showmat.cols, showmat.rows,
	//			QImage::Format_RGB888);
	//	}
	//	else                     // gray image  
	//	{
	//		qimg = QImage((const uchar*)(showmat.data),
	//			showmat.cols, showmat.rows,
	//			QImage::Format_Indexed8);
	//	}
	//	//QImage qimg = pDoc->pMatQImage->cvMat2QImage(showmat);
	//	showlabel->setPixmap(QPixmap::fromImage(qimg));
	//	//showlabel->show();
	//	/////////////////////////////////////////////////////		
	//	if (NetTransmissiontmp.mDetectObjects.size()>0)
	//	{
	//		for (int i = 0; i <NetTransmissiontmp.mDetectObjects.size(); i++)
	//		{
	//			//检测结果显示
	//			QString showstr = "起始点：\n";
	//			showstr = showstr
	//				+ "X: " + QString::number(NetTransmissiontmp.mDetectObjects[i].first_trace.x)
	//				+ "  Y: " + QString::number(NetTransmissiontmp.mDetectObjects[i].first_trace.y);
	//			table->append(showstr);
	//			//table->show();
	//			sound->play();
	//		}
	//		//qimg = qimg.scaled(200, 150);
	//		//pMainFrame->Resultlabel->setPixmap(QPixmap::fromImage(qimg));
	//		//pMainFrame->Resultlabel->show();
	//	}
	//	showmat.release();
	//	this->t2 = (double)cv::getTickCount();
	//	qDebug() << "All waste time : " << (t2 - t1) / cv::getTickFrequency() << endl;
	//	//updateFrame_Sig();
	//}
	//else
	//{
	//	qDebug() << "over!" << endl;
	//	//pMainFrame->close();
	//	//return;
	//}

	CNetTransmission mNetT = pDoc->CameraQueue.Take();
	if (!mNetT.frame.empty()){
		QImage qimg = pDoc->pMatQImage->cvMat2QImage(mNetT.frame);
		pShow[mNetT.camera_ID / 10][mNetT.camera_ID % 10]->setPixmap(QPixmap::fromImage(qimg));
		if (mNetT.mDetectObjects.size()>0)
		{
			for (int i = 0; i <mNetT.mDetectObjects.size(); i++)
			{
				//检测结果显示
				int lowestFloor = (int(mNetT.camera_ID / 10))*pDoc->par_all.pFloors;//此相机拍摄的最低楼层
				float floor_Height = lowestFloor* pDoc->par_all.PerFloor;//此相机拍摄最低楼层的高度
				float Tran_ratio = floor_Height / sqrt(floor_Height*floor_Height + pDoc->par_all.distance*pDoc->par_all.distance);
				QDateTime dt = QDateTime::fromTime_t(mNetT._time);
				QString strDate = dt.toString(Qt::SystemLocaleLongDate);//日期格式自定义
				QString showstr = "出现高空抛物！！！";
				showstr = showstr
					+ "\n拍摄相机：" + QString::number(mNetT.camera_ID)
					+ "\n楼层: " + QString::number(int(lowestFloor + (pDoc->par_all.frame_height - mNetT.mDetectObjects[i].first_trace.y*Tran_ratio) / (pDoc->par_all.frame_height / pDoc->par_all.pFloors))) + " 层"
					+ "\n时间: \n" + strDate
					+ "\n起始点：\nX: " + QString::number(int(mNetT.mDetectObjects[i].first_trace.x))
					+ "  Y: " + QString::number(int(mNetT.mDetectObjects[i].first_trace.y)) + "\n";
				ui.ResultText->append(showstr);
				ui.ResultText->show();
				sound->play();
			}
		}
	}
	else{
		qDebug() << "empty" << endl;
	}
	
}


//获得主机屏幕分辨率
void MainFrame::getScreenInfo()
{
	QDesktopWidget* desktopWidget = QApplication::desktop();
	//获取可用桌面大小    
	QRect deskRect = desktopWidget->availableGeometry();
	//获取设备屏幕大小    
	QRect screenRect = desktopWidget->screenGeometry();

	g_nActScreenW = screenRect.width();
	g_nActScreenH = screenRect.height();

	//获取系统设置的屏幕个数（屏幕拷贝方式该值为1）    
	g_nScreenCount = desktopWidget->screenCount();
}
