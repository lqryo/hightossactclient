#pragma once

#include "ip.h"
#include <winsock2.h>
#include <string>
#include <vector>

#include <QThread>

#include "ControlMsg.h"

const unsigned short cUDPClientPort = 41314;
//const int  cUDPClientPort = 41315;

// The broadcast rate (in seconds)
const int cBroadcastRate = 5;
const int cTimeoutBroadcast = 1000 * cBroadcastRate;

class CRawUDPServer : public QThread
{
	Q_OBJECT

public:
	static CRawUDPServer * instance()
	{
		static CRawUDPServer inst;
		return &inst;
	}

	~CRawUDPServer();

	void run() Q_DECL_OVERRIDE;
	/*
		Broadcast default content: local TCP server's port.
	*/
	bool broadcast();
	/*
		Broadcast given controlMsg.
	*/
	bool broadcast(const ControlMsg &controlMsg);
	//
private:
	unsigned short _port;				// server port
	std::string _ip;						// server IP address
	std::vector<std::string> _clients;	// last avaliable client IP address list

	int _sockfd;
	struct sockaddr_in dest;

	void init();
	void getLocalIP();
	bool broadcast(const char *data, uint16_t len);

	CRawUDPServer() : 
		_ip(""),
		_port(cUDPClientPort)
	{
		init();
	}

	CRawUDPServer(const CRawUDPServer & other) {}
	CRawUDPServer & operator= (const CRawUDPServer & other) {}
	//
};