#pragma once
#include "CDocument.h"
#include <QWidget>
#include "ui_LogInCaWidget.h"

class LogInCaWidget : public QWidget
{
	Q_OBJECT

public:
	CDocument * pDoc;
	LogInCaWidget(CDocument * pDoc);
	~LogInCaWidget();

public slots:
	void ConfirmFun();
	void CancelFun();

private:
	Ui::LogInCaWidget ui;
};
