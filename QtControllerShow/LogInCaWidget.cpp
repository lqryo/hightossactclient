#include "LogInCaWidget.h"
#include "windows.h"
#include "qdebug.h"
#include "ControlMsg.h"
#include "TCPServer.h"

#include <QList>

LogInCaWidget::LogInCaWidget(CDocument * pDoc)
{
	ui.setupUi(this);
	this->pDoc = pDoc;
	connect(ui.ConfirmBtn, SIGNAL(clicked()), this, SLOT(ConfirmFun()));
	connect(ui.CancelBtn, SIGNAL(clicked()), this, SLOT(CancelFun()));
}

LogInCaWidget::~LogInCaWidget()
{
}

//void LogInCaWidget::ConfirmFun()
//{
//	QString qstr = "";
//	qstr = qstr + ui.IPText->text()+" ";
//	qstr = qstr + ui.AccountText->text() + " ";
//	qstr = qstr + ui.PWText->text() + " ";
//	qstr = qstr + ui.CamIDText->text();
//	std::string str = qstr.toStdString();
//	int l = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, NULL, 0);
//	LPWSTR sendstr = new TCHAR[l];
//	MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, sendstr, l);
//	if ((ShellExecute(NULL, (LPCWSTR)L"open", (LPCWSTR)L"E:/MovingObjectDetectionAndTracking/demo/CVQTFrameWork.exe", sendstr, (LPCWSTR)L"", SW_SHOWNORMAL)) < (HANDLE)32)
//	{
//		QMessageBox::about(NULL, "提示", "打开失败");
//	}
//	
//	//结束进程
//	//std::string temp = std::string("taskkill /f /t /im ") + "Ezviz.exe";
//	//WinExec(temp.c_str(), SW_HIDE);
//	this->close();
//}

/*
This function need more arguments, the target IP and port
*/
void LogInCaWidget::ConfirmFun()
{
	QString qstr = "";
	qstr = qstr + ui.IPText->text() + " ";
	qstr = qstr + ui.AccountText->text() + " ";
	qstr = qstr + ui.PWText->text() + " ";
	qstr = qstr + ui.CamIDText->text();

	//std::string str = qstr.toStdString();
	//int l = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, NULL, 0);
	//LPWSTR sendstr = new TCHAR[l];
	//MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, sendstr, l);
	//HANDLE result = ShellExecute(NULL, (LPCWSTR)L"open", (LPCWSTR)L"CVQTFrameWork.exe", sendstr, (LPCWSTR)L"", SW_SHOWNORMAL);
	//if (result < (HANDLE)32)
	//{
	//	qDebug() << "** " << result << " **";
	//	QMessageBox::about(NULL, "提示", "启动失败");
	//}
	//else
	//{
	//	pDoc->CameraNum++;
	//}
	
	ControlMsg controlMsg;
	//controlMsg.type = CtrlTypeLogin;
	controlMsg.type = CtrlTypeNew;
	controlMsg.text = qstr;
	controlMsg.len = qstr.length();

	// TODO: get target to send
	CTCPServer *tcpServer = CTCPServer::instance();
	CameraNode cameraNode = tcpServer->getMinClientCountHost();
	QList<CameraNode> cameras = tcpServer->getAllClients();
	if (cameras.size() <= 0) {
		debug("No available camera instance found", UTIL_DEFAULT);
	}
	else {
		cameraNode = cameras[0];
	}

	if (!cameraNode.isEmpty()) {
		tcpServer->sendMsg(cameraNode.tcpSocket, &controlMsg);
		tcpServer->addPendingNew(cameraNode);			// for new instance command
	}
	else {
		error("No client connected.\n");
	}

	this->close();
}

void LogInCaWidget::CancelFun()
{
	this->close();
}