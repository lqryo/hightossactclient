#ifndef UDPSERVER_H_
#define UDPSERVER_H_

#include <QObject>
#include <QUdpSocket>
#include <QTimerEvent>
#include<string>
#include<vector>
#include <iostream>
using std::cout;
using std::endl;

const unsigned short cUDPDefaultPort = 51314;
const unsigned short cUDPClientPort = 41314;

// The broadcast rate (in seconds)
const int cBroadcastRate = 10;
const int cTimeoutBroadcast = 1000 * cBroadcastRate;

class CUDPServer : public QObject
{
	Q_OBJECT

public:
	static CUDPServer * instance() 
	{
		static CUDPServer inst;
		return &inst;
	}
	~CUDPServer();
	/*
	broadcast local TCP server's port.
	*/
	bool broadcast();
	bool broadcast(std::string s);
	void run();
	
	//void send_message(std::string s);//这里的参数应该包含服务器自身IP，主动查询局域网内有哪些在线的机器
	//std::string recieve_message();//这个函数主要是接收客户端返回的消息（含客户端IP）
	//根据得到的服务器IP和客户端IP，建立TCP连接，传输数据

private slots:
	/*
	the core method to read datagram
	*/
	void readPendingDatagrams();

private:
	unsigned short port;				// server port
	std::string ip;						// server IP address
	std::vector<std::string> clients;	// last avaliable client IP address list

	QUdpSocket *udpSocket;				// Qt udp socket
	int timerIDBroadcast;				// broadcast timer id

	static unsigned short DEFAULT_PORT;

	void handleDatagram(QByteArray*, QHostAddress *, quint16);
	void init();
	virtual void timerEvent(QTimerEvent *);

	CUDPServer(QObject *parent = nullptr) : QObject(parent), port(cUDPDefaultPort), ip(""), udpSocket(nullptr)
	{
		init();
	}

	CUDPServer (const CUDPServer & other) {}
	CUDPServer & operator= (const CUDPServer & other) {}
};

#endif
