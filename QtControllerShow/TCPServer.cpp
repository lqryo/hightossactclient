#include "TCPServer.h"

#include <QDataStream>

#include <iostream>
using std::cout;
using std::endl;

#include "Utils.h"
#include "Settings.h"
#include "json/json.h"

#define TAG "<TCPServer> "

int CTCPServer::connect_num = 0;

void CTCPServer::run(CDocument *doc)
{
	this->cdoc = doc;
	if (!isInited)
		init();

	connect(tcpServer, SIGNAL(newConnection()), this, SLOT(handleConnection()));
	//connect(tcpServer1, SIGNAL(newConnection()), this, SLOT(handleConnection()));
	//connect(tcpServer2, SIGNAL(newConnection()), this, SLOT(handleConnection()));
	//connect(tcpServer3, SIGNAL(newConnection()), this, SLOT(handleConnection()));
	//connect(tcpServer4, SIGNAL(newConnection()), this, SLOT(handleConnection()));
	//connect(tcpServer5, SIGNAL(newConnection()), this, SLOT(handleConnection()));
	//connect(tcpServer6, SIGNAL(newConnection()), this, SLOT(handleConnection()));
	debug("TCP server is running...");

}

void CTCPServer::handleConnection() {

	//CTCPServer::connect_num++;

	debug("<TCPServer> new connection arrived: ", UTIL_PREFIX);

#ifdef TCP_SK_BUFFER_SIZE
	int bufferSize = TCP_SK_BUFFER_SIZE;
#endif 

	//debug("connect number is %d", CTCPServer::connect_num);

			QTcpSocket *tcpClient = tcpServer->nextPendingConnection();
			connect(tcpClient, &QTcpSocket::readyRead, [=]() {
				debug(TAG "new data arrived...");
				CTCPServer::handleClient(tcpClient);
			});
			connect(tcpClient, &QTcpSocket::disconnected, [=]() {
				CTCPServer::removeClient(tcpClient);
			});
			tcpClient->setSocketOption(QAbstractSocket::SendBufferSizeSocketOption, bufferSize);
			tcpClient->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, bufferSize);

			// save this client connection
			QString key = keyOf(tcpClient);
			//clientMap.insert(key, tcpClient);

			CameraNode cameraNode(tcpClient);
			clientMap.insert(key, cameraNode);

			debug(std::string((const char*)key.toLocal8Bit()), UTIL_NEWLINE);
			//debug(key.toStdString(), UTIL_NEWLINE);


	
}

//void CTCPServer::handleConnection()
//{
//}

void CTCPServer::removeClient(QTcpSocket * client)
{
	QString key = keyOf(client);

	// disconnect signals
	disconnect(client);

	// remove from cache
	clientMap.remove(key);
	clientBufMap.remove(key);

	debug("<TCPServer> client: " + std::string((const char*)key.toLocal8Bit()) + " disconnected.");
	clientShowUpdate_Sig();
	//debug("<TCPServer> client: " + key.toStdString() + " disconnected.");

}

void CTCPServer::handleClient(QTcpSocket *tcpClient)
{
	double t1 = (double)cv::getTickCount();

	debug("<TCPServer> Message from: " + std::string((const char*)keyOf(tcpClient).toLocal8Bit()), UTIL_DEFAULT);
	//debug("<TCPServer> Message from: " + keyOf(tcpClient).toStdString(), UTIL_DEFAULT);

	QDataStream in(tcpClient);
	in.setVersion(QDataStream::Qt_4_0);
	uint msgLen;						// match TCPClient::sendMsg()
	quint8 respType;

	CClientBuffer & buf = clientBufMap[keyOf(tcpClient)];
	static unsigned long timeStart = (double)cv::getTickCount();
	static unsigned long timeEnd = 0;
	if (buf.msgLen == 0) {			// no previous data found, should read length
		if (tcpClient->bytesAvailable() < sizeof(uint)) {
			error("Too small data, check you code!");
			return;
		}
		in >> msgLen;
		//cout << msgLen << endl;
		buf.msgLen = msgLen;
		debug("msgLen = " + std::to_string(msgLen));

		if (tcpClient->bytesAvailable() < sizeof(quint8)) {
			error("No data type assigned.");
			return;
		}
		in >> respType;
		buf.msgType = respType;
		debug("msgType = " + std::to_string(respType));
	}
	else {
		// this is a piece of previous message
	}

	uint pieceLen = tcpClient->bytesAvailable();
	QByteArray block = tcpClient->readAll();
	buf.data += block;
	buf.curLen += pieceLen;

	debug("piece of message: pieceLen=" + std::to_string(pieceLen) + ", curLen=" + std::to_string(buf.curLen));
	//double t2 = (double)cv::getTickCount();
	//qDebug() << "this piece cost : " << (t2 - t1) / cv::getTickFrequency();
	while (buf.isCompleted()) {			// end of this client's message
		QDataStream in(buf.nextMsg());
		if (buf.msgType == RespTypeJson) {
			JsonResp json;
			in >> json;
			handleRespJson(tcpClient, &json);
		}
		else if (buf.msgType == RespTypeDetection) {
			CNetTransmission msg;
			double t11 = (double)cv::getTickCount();
			in >> msg;
			double t22 = (double)cv::getTickCount();
			qDebug() << "Analysis cost : " << (t22 - t11) / cv::getTickFrequency();
			handleRespDetection(tcpClient, &msg);
		}
		else if (buf.msgType == RespTypeDetectionOnly) {
			CDetectionNet msg;
			double t11 = (double)cv::getTickCount();
			in >> msg;
			double t22 = (double)cv::getTickCount();
			qDebug() << "Analysis cost : " << (t22 - t11) / cv::getTickFrequency();
			handleRespDetection(tcpClient, &msg);
		}
		else if (buf.msgType == RespTypeMat){ //处理接收的mat
			CImageFromNet mat;
			in >> mat;
			handelRespMat(tcpClient, &mat);
		}
		else {
			error("Unkonw response type: " + std::to_string(buf.msgType), UTIL_DEFAULT);
		}
		timeEnd = (double)cv::getTickCount();
		debug("<TCPServer> time elapse=" + std::to_string((timeEnd - timeStart)/ cv::getTickFrequency() ) + "s\n");
		timeStart = timeEnd;
		
		buf.cutdown();			// cutdown buffer, this is important for next message
	}
	
}

void CTCPServer::handleRespCMD(QTcpSocket *tcpClient, quint8 cmd, bool success)
{
	CameraNode &cameraNode = clientMap[keyOf(tcpClient)];
	
	switch (cmd) {
	case CtrlTypeNew: {
		QString key2 = cameraNode.key2();
		if (success) {
			quint8 status = pendingNewMap[key2] | cNewCMDSuccess;
			pendingNewMap[key2] = status;
			debug("New instance start successful. now status = " + std::to_string(status), UTIL_DEFAULT);
		}
		else {
			pendingNewMap.remove(key2);
			debug("New instance start failed.", UTIL_DEFAULT);
		}
		
		break;
	}
	case CtrlTypeLogin: {
		debug("Before login: " + std::string((const char*)cameraNode.toString().toLocal8Bit()), UTIL_DEFAULT);
		//debug("Before login: " + cameraNode.toString().toStdString(), UTIL_DEFAULT);
		if (success) {
			cameraNode.status |= CNSConnected;
			
		}
		else { }
		debug("After login: " + std::string((const char*)cameraNode.toString().toLocal8Bit()), UTIL_DEFAULT);
		//LogInResult_Sig(1, success);
		//debug("After login: " + cameraNode.toString().toStdString(), UTIL_DEFAULT);
		break;
	}
	case CtrlTypeLogout:{

	}
	default:
		break;
	}
}

void CTCPServer::handleRespCMD(QTcpSocket *tcpClient, quint8 cmd, bool success, Json::Value root)
{
	CameraNode &cameraNode = clientMap[keyOf(tcpClient)];

	switch (cmd) {
	case CtrlTypeNew: {
		QString key2 = cameraNode.key2();
		if (success) {
			quint8 status = pendingNewMap[key2] | cNewCMDSuccess;
			pendingNewMap[key2] = status;
			debug("New instance start successful. now status = " + std::to_string(status), UTIL_DEFAULT);
		}
		else {
			pendingNewMap.remove(key2);
			debug("New instance start failed.", UTIL_DEFAULT);
		}

		break;
	}
	case CtrlTypeLogin: {
		debug("Before login: " + std::string((const char*)cameraNode.toString().toLocal8Bit()), UTIL_DEFAULT);
		//debug("Before login: " + cameraNode.toString().toStdString(), UTIL_DEFAULT);
		cameraNode.cameraIP = QString::fromStdString(root["cameraIP"].asString());
		cameraNode.cameraUsername = QString::fromStdString(root["cameraUsername"].asString());
		cameraNode.cameraPass = QString::fromStdString(root["cameraPass"].asString());
		cameraNode.cameraID = root["cameraID"].asInt();
		if (success) {
			cameraNode.status |= CNSConnected;
		}
		else {}
		debug("After login: " + std::string((const char*)cameraNode.toString().toLocal8Bit()), UTIL_DEFAULT);
		//LogInResult_Sig(1, success);
		//debug("After login: " + cameraNode.toString().toStdString(), UTIL_DEFAULT);
		break;
	}
	case CtrlTypeLogout:{
		debug("Before Logout: " + std::string((const char*)cameraNode.toString().toLocal8Bit()), UTIL_DEFAULT);
		cameraNode.cameraIP = QString::fromStdString(root["cameraIP"].asString());
		cameraNode.cameraUsername = QString::fromStdString(root["cameraUsername"].asString());
		cameraNode.cameraPass = QString::fromStdString(root["cameraPass"].asString());
		cameraNode.cameraID = root["cameraID"].asInt();
		if (success){
			cameraNode.status &= CNSAvailable;
		}
		else{ }
		debug("After login: " + std::string((const char*)cameraNode.toString().toLocal8Bit()), UTIL_DEFAULT);

	}
	default:
		break;
	}
}


void CTCPServer::handleRespJson(QTcpSocket *tcpClient, const JsonResp *json)
{
	debug("<TcpServer> response from '"
		+ std::string((const char*)keyOf(tcpClient).toLocal8Bit()) + "': " + std::string((const char*)json->toString().toLocal8Bit()));
	Json::Value root;
	Json::Reader reader;
	if (!reader.parse(std::string((const char*)json->text.toLocal8Bit()), root)) {
		error("Parse json error.");
		return;
	}
	switch (json->type) {
	case JsonRespCMD:{			// control command
		quint8 cmd = root["cmd"].asUInt();
		bool success = root["result"].asBool();
		debug(std::string((const char*)QString("{cmd: %1, result: %2}").arg(cmd).arg(success).toLocal8Bit()), UTIL_DEFAULT);
		handleRespCMD(tcpClient, cmd, success,root);	
		LogInResult_Sig(cmd, success);
		break;
	}
	case JsonRespNew:{
		uint id = root["id"].asUInt();
		bool isMaster = root["isMaster"].asBool();
		bool isLogin = root["isLogin"].asBool();
		uint masterID = root["masterID"].asUInt();
		debug(std::string((const char*)
			QString("{id: %1, isMaster: %2, isLogin: %3, masterID: %4}")
			.arg(id).arg(isMaster).arg(isLogin).arg(masterID)
			.toLocal8Bit()), UTIL_DEFAULT);

		QString key2 = tcpClient->peerAddress().toString() + "." + QString::number(masterID);
		if (pendingNewMap.find(key2) != pendingNewMap.end()) {
			quint8 status = pendingNewMap[key2];
			status |= cNewSuccess;
			debug("now status = " + std::to_string(status), UTIL_DEFAULT);

			if ((status & cNewPending != 0) 
				&& (status & cNewCMDSuccess != 0) & (status & cNewSuccess != 0)) {
				debug("New instance connected successful.", UTIL_DEFAULT);
			}
			else {
				debug("New instance connection error.", UTIL_DEFAULT);
			}
		}

		// update status
		QString key = keyOf(tcpClient);
		CameraNode & cameraNode = clientMap[key];
		cameraNode.id = id;
		cameraNode.isMaster = isMaster;
		cameraNode.status |= CNSAvailable;
		cameraNode.cameraIP = QString::fromStdString(root["cameraIP"].asString());
		cameraNode.cameraUsername = QString::fromStdString(root["cameraUsername"].asString());
		cameraNode.cameraPass = QString::fromStdString(root["cameraPass"].asString());
		cameraNode.cameraID = root["cameraID"].asInt();
		if (isLogin) {
			cameraNode.status |= CNSConnected;
		}
		LogInResult_Sig(json->type, isLogin);
		break;
		
	}
	default:
		debug("Unknown json response type: " + std::to_string(json->type), UTIL_DEFAULT);
		break;
	}

}

void CTCPServer::handleRespDetection(QTcpSocket *client, CDetectionNet *msg)
{
	// TODO when receive client's message
	debug("Message [time=" + std::to_string(msg->_time) + ", cameraID=" + std::to_string(msg->camera_ID) + ",...]\n");

	if (msg->mDetectObjects.size() > 0)
	{
		cdoc->DetectionQueue.Put(*msg);
		ResultShow_Sig();
	}
	qDebug() << "---------";
	//qDebug() << "** "<<cdoc->CameraQueue.m_pCount->getValue()<<" **";
	//cdoc->CameraQueue.Put(*msg);
	//if (cdoc->par_all.stitchMsk == true){
	//	AnalyStart_Sig();
	//}
	//else
	//{
	//	update_Sig();//执行MainFrame的perCaShow()，直接刷新界面
	//}
	//cv::imshow("src", msg->frame);
	//cv::waitKey(1);
	//#ifdef DEBUG
	//	cv::imwrite("tmp.jpg", msg->frame);
	//	imshow("aa",msg->frame);
	//#endif
}

void CTCPServer::handleRespDetection(QTcpSocket *client, CNetTransmission *msg)
{
	// TODO when receive client's message
	debug("Message [time=" + std::to_string(msg->_time) + ", cameraID=" + std::to_string(msg->camera_ID) + ",...]\n");
	
	if (msg->mDetectObjects.size() > 0)
	{
		cdoc->ResultQueue.Put(*msg);
		ResultShow_Sig();
	}
	qDebug() << "---------";
	//qDebug() << "** "<<cdoc->CameraQueue.m_pCount->getValue()<<" **";
	//cdoc->CameraQueue.Put(*msg);
	//if (cdoc->par_all.stitchMsk == true){
	//	AnalyStart_Sig();
	//}
	//else
	//{
	//	update_Sig();//执行MainFrame的perCaShow()，直接刷新界面
	//}
	//cv::imshow("src", msg->frame);
	//cv::waitKey(1);
//#ifdef DEBUG
//	cv::imwrite("tmp.jpg", msg->frame);
//	imshow("aa",msg->frame);
//#endif
}

void CTCPServer::handelRespMat(QTcpSocket *client, CImageFromNet *mat){
	//debug("Message [time=" + std::to_string(mat->_time) + ", cameraID=" + std::to_string(mat->camera_ID) + ",...]\n");
	qDebug() << "Message [time=" << mat->_time <<", cameraID=" << mat->camera_ID << ",...]\n";
	//qDebug() << "size:::::::::::" << mat->sizebuf;
	cdoc->pH264_DecoderMN[mat->camera_ID / 10][mat->camera_ID % 10].decode(mat->buf, mat->sizebuf);
	//decoder.decode(mat->buf, mat->sizebuf);
	//decoder.play();
	if (!cdoc->pH264_DecoderMN[mat->camera_ID / 10][mat->camera_ID % 10].getMat().empty()){
		/*if (mat->camera_ID == 0){
			cv::imshow("00", decoder.getMat());
		}
		else{
			cv::imshow("10", decoder.getMat());
		}*/
		cv::waitKey(1);
		CNetTransmission ReceiveMat;
		ReceiveMat.camera_ID = mat->camera_ID;
		ReceiveMat._time = mat->_time;
		ReceiveMat.frame = cdoc->pH264_DecoderMN[mat->camera_ID / 10][mat->camera_ID % 10].getMat().clone();
		cdoc->VedioQueue.Put(ReceiveMat);
		if (cdoc->par_all.stitchMsk == true){
			AnalyStart_Sig();
		}
		else
		{
			update_Sig();//执行MainFrame的perCaShow()，直接刷新界面
		}
	}
	
	
}

//
//void CTCPServer::receiveMsg(QTcpSocket *client, CNetTransmission *msg)
//{
//	// TODO when receive client's message
//	debug("Message [time=" + std::to_string(msg->_time) + ", cameraID=" + std::to_string(msg->camera_ID) + ",...]");
//	if (!msg->frame.empty())
//	{
//		cv::imshow("dst", msg->frame);
//		cv::waitKey(1);
//	}
//	//cdoc->CameraQueue.Put(*msg);
//	//if (cdoc->par_all.Analymask == true){
//	//	cdoc->par_all.Analymask = false;
//	//	AnalyStart_Sig();
//	//}
//	//qDebug() << "Camera Queue size = " << cdoc->CameraQueue.m_pCount->getValue() << endl;
//
//#ifdef DEBUG
//	cv::imwrite("tmp.jpg", msg->frame);
//#endif
//}

void CTCPServer::sendMsg(QTcpSocket *client, ControlMsg *msg)
{
	if (tcpServer == nullptr || client == nullptr) {
		error("Connection is not exist.");
		return;
	}

	if (msg == nullptr) {
		error("Empty message.");
		return;
	}

	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_4_0);

	out << *msg;
	client->write(block);

}

void CTCPServer::sendMsg(ControlMsg *msg)
{
	if (tcpServer == nullptr || !isInited) {
		error("TCP server is not running.");
		return;
	}

	//foreach(QTcpSocket *client, clientMap) {
	//	sendMsg(client, msg);
	//}
	foreach(const CameraNode &cameraNode, clientMap) {
		sendMsg(cameraNode.tcpSocket, msg);
	}

}

void CTCPServer::addPendingNew(CameraNode cameraNode)
{
	QString key2 = cameraNode.key2();
	pendingNewMap.insert(key2, cNewPending);
}

QList<CameraNode> CTCPServer::getAllHosts() const 
{
	QList<CameraNode> list;
	foreach(const CameraNode &cameraNode, clientMap) {
		if (cameraNode.isMaster) {
			list.push_back(cameraNode);
		}
	}
	return list;

}

CameraNode CTCPServer::getMinClientCountHost() const
{
	QMap<QString, quint8> hostMap = getHostMap();

	QString minHost = "";
	quint8 min = 255;
	foreach(const QString &host, hostMap.keys()) {
		if (hostMap[host] < min) {
			min = hostMap[host];
			minHost = host;
		}
	}
	debug("Min host = " + std::string((const char*)minHost.toLocal8Bit()), UTIL_DEFAULT);
	//debug("Min host = " + minHost.toStdString(), UTIL_DEFAULT);

	foreach(const CameraNode &cameraNode, clientMap) {
		if (cameraNode.isMaster && cameraNode.host == minHost) {
			return cameraNode;
		}
	}

	return CameraNode();

}

QMap<QString, quint8> CTCPServer::getHostMap() const
{
	QMap<QString, quint8> hostMap;

	foreach(const CameraNode & cameraNode, clientMap) {
		hostMap[cameraNode.host] += 1;
	}

	return hostMap;
}

QString CTCPServer::keyOf(QTcpSocket *client)
{

	QString key = "";
	if (client != nullptr) {
		key = client->peerAddress().toString() + "." + QString::number(client->peerPort());
	}
	//debug("keyOfClient() " + key.toStdString());
	//debug("keyOfClient() " + std::string((const char*)key.toLocal8Bit()));
	return key;

}

CTCPServer::~CTCPServer()
{

	if (tcpServer != nullptr) {
		delete tcpServer;
		tcpServer = nullptr;
	}

}

void CTCPServer::stop()
{
	// disconnect signals
	disconnect(tcpServer);

	// disconnected all avaliable client
	//foreach(QTcpSocket *client, clientMap) {
	//	disconnect(client);
	//	client->disconnectFromHost();
	//}
	foreach(const CameraNode &cameraNode, clientMap) {
		disconnect(cameraNode.tcpSocket);
		cameraNode.tcpSocket->disconnectFromHost();
	}

	// clear cache
	clientMap.clear();

	// close server socket
	tcpServer->close();
	delete tcpServer;
	tcpServer = nullptr;



	isInited = false;
	debug("<TCPServer> stopped.");

}

void CTCPServer::init() {
	debug( "<TCPServer> init with port: " + std::to_string(port));

	tcpServer = new QTcpServer();
	tcpServer->listen(QHostAddress::AnyIPv4, port);





	isInited = true;

}
