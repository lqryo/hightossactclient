#include "Utils.h"
#include "Settings.h"

void debug(const char *msg, int flag)
{
#ifdef DEBUG
	if ((flag & UTIL_PREFIX) != 0) {
		std::cout << "DEBUG: ";
	}
	std::cout << msg;
	if ((flag & UTIL_NEWLINE) != 0) {
		std::cout << std::endl;
	}
#endif

}

void debug(const std::string & msg, int flag) 
{
	debug(msg.c_str(), flag);

}

void debug(long val, int flag) 
{
	debug(std::to_string(val), flag);

}

void error(const char *msg, int flag)
{
	if ((flag & UTIL_PREFIX) != 0) {
		std::cerr << "ERROR: ";
	}
	std::cerr << msg;
	if ((flag & UTIL_NEWLINE) != 0) {
		std::cerr << std::endl;
	}

}
void error(const std::string & msg, int flag)
{
	error(msg.c_str(), flag);

}
void error(long val, int flag)
{
	error(std::to_string(val), flag);

}
