#pragma once

#include <QObject>
#include "CDocument.h"
#include "StitchedPic.h"
#include "StitchManage.h"
class CStitchThread : public QObject
{
	Q_OBJECT

public:
	//StitchedPic * pStitchedPic;
	StitchManage * pStitchManage;
	CStitchThread(CParams InputParam);
	~CStitchThread();

	void transNet2Mat(CDocument* pDoc);


public:
	Mat convertTo3Channels(const Mat& binImg);
	Mat transAndFlip(Mat img);
	Mat transAndFlip2(Mat img);
	public slots:
	void StitchFunction(CDocument* pDoc, int flag);
signals:
	void testShow_Sig();
	void NextStitch_Sig(CDocument* pDoc,int );
	void ShowFrame_Sig();
};
