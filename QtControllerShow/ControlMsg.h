#ifndef CONTROLMSG_H_
#define CONTROLMSG_H_

#include <QString>
#include <QDataStream>

/*
enums for ControlMsg.type

@init	Establish tcp connection between client and server
@start	Start detection
@stop	Stop detection
@pause	Pause detection
@login	Login camera
@logout	Stop detection & Log out camera
@new	New instance
*/
enum CtrlMsgType {
	CtrlTypeStart			= 1,
	CtrlTypeStop			= 2,
	CtrlTypePause			= 4,
	CtrlTypeInit			= 8,
	CtrlTypeLogin			= 16,
	CtrlTypeLogout			= 32,
	CtrlTypeNew = CtrlTypeLogin + 1
	// more defines ...
};

/*
controlmsg - The control message struct
*/
typedef struct controlmsg {
	quint8 type;		// message id(type)
	quint16 len;		// message content length(optional)
	QString text;		// message content text

	controlmsg()
	{
		type = 0;
		len = 0;
		text = "";
	}

	quint16 size() const
	{
		return sizeof(type) + sizeof(len) + text.size();
	}
} ControlMsg;

inline QDataStream & operator<< (QDataStream & out, const ControlMsg & msg)
{
	out << msg.type
		<< msg.len
		<< msg.text;
	return out;

}

inline QDataStream & operator>> (QDataStream & in, ControlMsg & msg)
{
	in >> msg.type
		>> msg.len
		>> msg.text;
	return in;

}


#endif	// CONTROLMSG_H_
